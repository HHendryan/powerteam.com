<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['upload_path']		= './assets/img/';
$config['allowed_types']	= 'jpeg|jpg|png';
$config['file_ext_tolower'] = true;
$config['max_filename']		= 200;
$config['max_size']			= 2048;