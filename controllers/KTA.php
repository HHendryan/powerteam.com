<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KTA extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('KTA_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$this->load->view('kta');
	}

	function load_data_kta($ket){
		$data['data'] = $this->KTA_model->get_data_kta($ket);
		echo json_encode($data);
	}
}
