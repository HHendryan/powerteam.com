<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printer extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Tim_model');
		$this->load->model('Data_model');
	}

	//INDEX FUNCTION
	function index(){
		
	}

	function dpt(){
		$get = $this->input->get();
		$data = $this->get_data_dpt_print($get['kel'],$get['tps']);

		$data['tps'] = $get['tps'];
		$data['tanggal'] = date('Y-m-d h:i:s');
 		$this->load->view('print_dpt',$data);
		// var_dump($data);
	}

	function dpt_checked(){
		$get = $this->input->get();
		$data = $this->get_data_dpt_print_checked($get['kel'],$get['tps'], $get['guraklih']);

		$query = "SELECT cp from m_team where id=".$get['guraklih'];
		$data['guraklih'] = $this->db->query($query)->row('cp');
		$data['tps'] = $get['tps'];
		$data['tanggal'] = date('Y-m-d h:i:s');
 		$this->load->view('print_dpt_checked',$data);
		// var_dump($data);
	}

	function get_data_dpt_print($kel,$tps){

		$hasil = $this->db->query("SELECT a.name as kel,b.name as kec, c.name as kab, d.name as prov, c.id as id_kab from m_villages a
			inner join m_districts b on a.district_id = b.id
			inner join m_regencies c on b.regency_id = c.id
			inner join m_provinces d on c.province_id = d.id
			where a.id = $kel")->row();

		$this->db2 = $this->load->database("db_kab",true);
		$queryTabel = "SELECT id,nik,nama,alamat,tanggal_lahir, tempat_lahir, jns_kelamin, tps FROM kab_".$hasil->id_kab." WHERE idKel = $kel AND tps= $tps";

		$data["data"] = array();
		$i=1;
		$query = $this->db2->query($queryTabel);
		foreach ($query->result() as $row)
		{
			$tmp = array();

			$tmp["no"] = $i;
			$tmp["id"] = $row->id;
            $tmp["nama"] = $row->nama;
            $tmp["nik"] = $row->nik;
            $tmp["alamat"] = $row->alamat;
            $tmp["tanggal_lahir"] = $row->tanggal_lahir;
            $tmp["tps"] = $row->tps;
            $tmp["jns_kelamin"] = $row->jns_kelamin;

            array_push($data["data"], $tmp);
            $i++;
		}
		$data['prov'] = $hasil->prov;
		$data['kab'] = $hasil->kab;
		$data['kec'] = $hasil->kec;
		$data['kel'] = $hasil->kel;
		return $data;
	}

	function get_data_dpt_print_checked($kel,$tps,$guraklih){

		$hasil = $this->db->query("SELECT a.name as kel,b.name as kec, c.name as kab, d.name as prov, c.id as id_kab from m_villages a
			inner join m_districts b on a.district_id = b.id
			inner join m_regencies c on b.regency_id = c.id
			inner join m_provinces d on c.province_id = d.id
			where a.id = $kel")->row();

		$this->db2 = $this->load->database("db_kab_final",true);
		$queryTabel = "SELECT id,nik,nama,alamat,tanggal_lahir, tempat_lahir, jns_kelamin, tps FROM kab_".$hasil->id_kab." WHERE idKel = $kel AND tps = $tps AND checked = 1 AND guraklihId = $guraklih";

		$data["data"] = array();
		$i=1;
		$query = $this->db2->query($queryTabel);
		foreach ($query->result() as $row)
		{
			$tmp = array();

			$tmp["no"] = $i;
			$tmp["id"] = $row->id;
            $tmp["nama"] = $row->nama;
            $tmp["nik"] = $row->nik;
            $tmp["alamat"] = $row->alamat;
            $tmp["tanggal_lahir"] = $row->tanggal_lahir;
            $tmp["tps"] = $row->tps;
            $tmp["jns_kelamin"] = $row->jns_kelamin;

            array_push($data["data"], $tmp);
            $i++;
		}
		$data['prov'] = $hasil->prov;
		$data['kab'] = $hasil->kab;
		$data['kec'] = $hasil->kec;
		$data['kel'] = $hasil->kel;
		return $data;
	}
}
