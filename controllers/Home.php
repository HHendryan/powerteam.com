<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		// $this->load->model('Data_model');
		$this->load->model('Auth_model');
		$this->load->model('Home_model');
		$this->load->model('Data_model');
		// $this->load->model('Auth_model');
		$this->load->model('Tangible_model');

		$this->Auth_model->can_access_by_login();
		$data = $this->session->userdata('guraklih');
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$user = $this->session->userdata('guraklih');
		if($user['role_detail'] == 'All'){
			$data['nama_wilayah'] = 'NASIONAL';
			$data['id_wilayah'] = 0;
			$data['tipe_wilayah'] = 'NASIONAL';
		}else{
			if($user['role'] == 'korprov'){
				$detail = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
				$data['nama_wilayah'] = $detail->name;
				$data['id_wilayah'] = $detail->id;
				$data['tipe_wilayah'] = 'provinsi';
			};
			if($user['role'] == 'korkab'){
				$detail = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
				$data['nama_wilayah'] = $detail->name;
				$data['id_wilayah'] = $detail->id;
				$data['tipe_wilayah'] = 'kabupaten';
			};

		}
 		$this->load->view('home',$data);
	}

	function load_table(){
		$id = $this->input->post('id');
		$tipe = $this->input->post('tipe');
		$nama = $this->input->post('nama');
		$data = $this->Home_model->get_data2($id,$tipe);
		$data['id'] = $id;
		$data['tipe'] = $tipe;
		$data['nama'] = $nama;
		$this->load->view('table_dashboard_guraklih',$data);
	}

	function load_table2(){
		$id = $this->input->post('id');
		$tipe = $this->input->post('tipe');
		$nama = $this->input->post('nama');
		$data = $this->Home_model->get_data2($id,$tipe);
		$data['id'] = $id;
		$data['tipe'] = $tipe;
		$data['nama'] = $nama;
		$this->load->view('table_dashboard_guraklih2',$data);
	}

	function load_table3(){
		$id = $this->input->post('id');
		$tipe = $this->input->post('tipe');
		$nama = $this->input->post('nama');
		$data = $this->Home_model->get_data2($id,$tipe);
		echo json_encode($data);
	}

	function get_detail_dpt($kelurahan,$tps){
		if($kelurahan == 0 ){
			$this->datatables->set_database('db_dpt');
			$this->datatables->select("id,nik,nama,tanggal_lahir,alamat");
			$this->datatables->from('dpt_null');
			echo $this->datatables->generate();
		}else{
			$id_prov = $this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$kelurahan")->row('province_id');
			$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel');
			$this->datatables->set_database('db_dpt');
			$this->datatables->select("id,nik,nama, jns_kelamin, usia, tanggal_lahir,alamat");
			$this->datatables->from($tabel);
			$this->datatables->where("idProv", $id_prov);
			$this->datatables->where("idKel", $kelurahan);
			$this->datatables->where("tps", $tps);
			echo $this->datatables->generate();
		}
	}

	function get_detail_dpt2($kelurahan,$tps){
		if($kelurahan == 0 ){
			$this->datatables->set_database('db_dpt');
			$this->datatables->select("id,nik,nama,tanggal_lahir,alamat");
			$this->datatables->from('dpt_null');
			echo $this->datatables->generate();
		}else{
			$id_kab = $this->db->query("SELECT c.id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$kelurahan")->row('id');
			$tabel = 'kab_'.$id_kab;
			$this->datatables->set_database('db_kab');
			$this->datatables->select("id,CONCAT (SUBSTRING(nik, 1, 12),'****') as nik,nama,REPLACE(tanggal_lahir, '|', '-') as tanggal_lahir,alamat, jns_kelamin, usia");
			$this->datatables->from($tabel);
			$this->datatables->where("idKab", $id_kab);
			$this->datatables->where("idKel", $kelurahan);
			$this->datatables->where("tps", $tps);
			echo $this->datatables->generate();
			// var_dump($id_kab);
		}
	}
}
