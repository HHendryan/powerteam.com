<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	//constructor
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Auth_model');
    }
    //Login
	public function index(){
		$this->Auth_model->is_login();
		$this->load->view('login');
	}

	public function login(){
		$this->Auth_model->login($this->input->post('username'),$this->input->post('password'));
		$this->Auth_model->is_login();
	}

	public function logout(){
		$this->Auth_model->logout();
	}

	public function sign_up(){
		$data['provinsi'] = $this->db->get('provinsi')->result();
		$this->load->view('signup',$data);
	}

	function daftar(){
		$this->Auth_model->daftar($this->input->post());
	}
	function get_select_kab(){
		$id=$this->input->post('id_prov');
		echo $this->Auth_model->select_kabupaten($id);
	}
	public function error_404(){
		$this->load->view('errors/404');
	}
}
