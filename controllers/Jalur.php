<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jalur extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Jalur_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$this->Auth_model->can_access_by_level(3,'jalur');
		$data = $this->column_jalur();
		$this->load->view('jalur',$data);
	}

	function column_jalur(){
		$data['jalur']=$this->db->get('jalur')->result();
		$tes[0]['data'] = 'wilayah';
		$i=1;
		foreach ($data['jalur'] as $value) {
			$tes[$i]['data'] = $value->jalur;
			$i++;
		}
		$data['string'] = json_encode($tes);
		return $data;
	}

	function kabupaten($id_prov){
		$this->Auth_model->can_access_by_level(2,'jalur');
		$this->Auth_model->can_access_by_detail('kab',$id_prov);
		$data = $this->column_jalur();
		$data['id'] = $id_prov;
		$this->load->view('jalur_kab',$data);
	}

	function kecamatan($id_kab){
		$this->Auth_model->can_access_by_detail('kec',$id_kab);
		$data = $this->column_jalur();
		$data['id'] = $id_kab;
		$this->load->view('jalur_kec',$data);
	}

	function kelurahan($id_kec){
		$this->Auth_model->can_access_by_detail('kel',$id_kec);
		$data = $this->column_jalur();
		$data['id'] = $id_kec;
		$this->load->view('jalur_kel',$data);
	}
	function tps($id_kel){

	}
	
	function get_jalur_prov(){
		$data['data'] = $this->Jalur_model->summary_jalur_prov();
		echo json_encode($data);
	}

	function get_jalur_kab($id_prov){
		$data['data'] = $this->Jalur_model->summary_jalur_kab($id_prov);
		echo json_encode($data);
	}

	function get_jalur_kec($id_kab){
		$data['data'] = $this->Jalur_model->summary_jalur_kec($id_kab);
		echo json_encode($data);
	}

	function get_jalur_kel($id_kec){
		$data['data'] = $this->Jalur_model->summary_jalur_kel($id_kec);
		echo json_encode($data);
	}

	function test(){
		$data['data'] = $this->Jalur_model->summary_jalur_kel('1');
		var_dump($data);
	}

}
