<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cari extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Cari_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$data['provinsi'] = $this->db->get('provinsi')->result();
		$this->load->view('usia',$data);
	}

	function dpt(){
		$data['provinsi'] = $this->db->get('provinsi')->result();
		$this->load->view('cari_dpt',$data);
	}
	function tim(){
		$this->load->view('cari_tim');
	}
	function usia(){
		$data['provinsi'] = $this->db->get('provinsi')->result();
		$this->load->view('cari_usia',$data);
	}

	function get_kabupaten(){
		$id = $this->input->post('id');
		echo $this->Cari_model->get_kabupaten($id);
	}

	function get_kecamatan(){
		$id = $this->input->post('id');
		echo $this->Cari_model->get_kecamatan($id);
	}
	function load_data_dpt($prov,$kab,$kec,$nik){
		if($this->Cari_model->get_data_dpt($prov,$kab,$kec,$nik)){
			echo $this->Cari_model->get_data_dpt($prov,$kab,$kec,$nik);
		}
	}

	function load_data_tim($keyword){
		$data['data'] = $this->Cari_model->get_data_tim($keyword);
		echo json_encode($data);
	}
	function test(){
		$data = $this->db->query("SELECT * from dpt_sumatera_selatan where nik='andi' OR nama LIKE '%andi%' ")->result();
		var_dump($data);
	}
}
