<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		// $this->load->model('Form_model');
		$this->load->model('Auth_model');
		$this->load->model('Wilayah_model');

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
		$this->db1 = $this->load->database("db_dpt",true);
	}

	function index(){
		
	}
	
	function kelurahan(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		//$data = $this->Tangible_model->option_tangible();
		$this->load->view('wilayah',$data);
	}

	function kecamatan(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		//$data = $this->Tangible_model->option_tangible();
		$this->load->view('wilayah_kecamatan',$data);
	}

	function get_data_kelurahan(){
		$this->datatables->select('a.id,
			a.district_id,
			a.`name` AS kelurahan,
			b.`name` AS kecamatan,
			c.`name` AS kabupaten,
			b.regency_id,
			c.province_id,
			d.`name` as provinsi
			');
		$this->datatables->from('m_villages a');
		$this->datatables->join('m_districts b','a.district_id=b.id','inner');
		$this->datatables->join('m_regencies c','b.regency_id=c.id','inner');
		$this->datatables->join('m_provinces d','c.province_id=d.id','inner');
		$this->datatables->add_column('button', '<button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit" style="
    height: 26px;">View & Edit</button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete" style="
    height: 26px;">Delete</button>', 'id');
		echo $this->datatables->generate();
	}

	function get_data_kecamatan(){
		$this->datatables->select('a.id,
			a.regency_id,
			a.`name` AS kecamatan,
			c.`name` AS kabupaten,
			a.regency_id,
			c.province_id,
			d.`name` as provinsi
			');
		$this->datatables->from('m_districts a');
		$this->datatables->join('m_regencies c','a.regency_id=c.id','inner');
		$this->datatables->join('m_provinces d','c.province_id=d.id','inner');
		$this->datatables->add_column('button', '<button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit" style="
    height: 26px;">View & Edit</button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete" style="
    height: 26px;">Delete</button>', 'id');
		echo $this->datatables->generate();
	}

	function get_kabupaten(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kabupaten($id);
	}

	function get_kecamatan(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kecamatan($id);
	}	
	
	function get_kelurahan(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kelurahan($id);
	}

	//CRUD
	function add_kelurahan(){
		$data =$this->input->post();
		$idKel = $this->create_id_kelurahan($data['district_id']);
		$data['id'] = $idKel;
		$this->db->insert('m_villages',$data);
		$detail = $this->db->query("SELECT a.id as id_kel, a.name as nama_kel, b.id as id_kec, b.name as nama_kec, c.id as id_kab, c.name as nama_kab, d.id as id_prov, d.name as nama_prov from m_villages a
			inner join m_districts b on a.district_id = b.id
			inner join m_regencies c on b.regency_id = c.id
			inner join m_provinces d on c.province_id = d.id
			where a.id = $idKel");

		$row = $detail->row();

		$input['idProv'] = $row->id_prov;
		$input['idKab'] = $row->id_kab;
		$input['idKec'] = $row->id_kec;
		$input['idKel'] = $row->id_kel;
		$input['provinsi'] = $row->nama_prov;
		$input['kabupaten'] = $row->nama_kab;
		$input['kecamatan'] = $row->nama_kec;
		$input['kelurahan'] = $row->nama_kel;
		$input['dpt'] = 0;
		$input['STATUS'] = 1;
		$input['table_name'] = $this->db1->get_where('m_dpt', array('idProv' => $row->id_prov))->row('tabel');
		$input['upd'] = 'admin';
		$input['lup'] = date('Y-m-d h:i:s');
		$otherdb = $this->load->database("db_dpt",true);
		$otherdb->insert('m_area',$input);

		// $area = $this->db->query("SELECT")
		// $query = "INSERT INTO db_dpt.m_area(idProv, idKab, idKec, idKel, provinsi, kabupaten, kecamatan, kelurahan, dpt, STATUS, table_name, upd, lup)
		// 			SELECT a.id AS idProv, 
		// 			b.id AS idKab, 
		// 			c.id AS idKec, 
		// 			d.id AS idKel, 
		// 			a.name AS provinsi, 
		// 			b.name AS kabupaten, 
		// 			c.name AS kecamatan, 
		// 			0 AS dpt, 
		// 			d.name AS kelurahan, 
		// 			1 AS STATUS, 
		// 			(SELECT e.tabel FROM db_dpt.m_dpt e WHERE e.idProv=a.id) AS table_name, 
		// 			'' AS upd, 
		// 			'0000-00-00 00:00:00' AS lup
		// 			FROM db_sipp.m_provinces a, db_sipp.m_regencies b, db_sipp.m_districts c, db_sipp.m_villages d
		// 			WHERE a.id=b.province_id AND
		// 			      b.id=c.regency_id AND
		// 			      c.id=d.district_id AND 
		// 			      d.id='$idKel'";

		// $this->$db1->query($sql);

		echo json_encode($data);
	}

	function add_kecamatan(){
		$data =$this->input->post();
		$data['id'] = $this->create_id_kecamatan($data['regency_id']);
		$this->db->insert('m_districts',$data);
		$this->db1->insert('m_districts',$data);
		echo json_encode($data);
	}

	function create_id_kelurahan($id_kecamatan){
		$id_kelurahan = $this->db->query("SELECT id FROM m_villages WHERE district_id='$id_kecamatan' order by id DESC LIMIT 1")->row('id');
		if($id_kelurahan == NULL){
			$id_kelurahan = $id_kecamatan.'000';
		}
		return $id_kelurahan+1;
	}

	function create_id_kecamatan($id_kabupaten){
		$id_kecamatan = $this->db->query("SELECT id FROM m_districts WHERE regency_id='$id_kabupaten' order by id DESC LIMIT 1")->row('id');
		if($id_kecamatan == NULL){
			$id_kecamatan = $id_kabupaten.'000';
		}
		return $id_kecamatan+1;
	}
	
	function edit(){
		$data = $this->input->post();
		$this->db->where('id',$data['id']);
		$this->db->update('m_villages',$data);

		$this->db1->where('id',$data['id']);
		$this->db1->update('m_villages',$data);
		echo json_encode($data);
	}

	function edit_kecamatan(){
		$data = $this->input->post();
		$this->db->where('id',$data['id']);
		$this->db->update('m_districts',$data);

		$this->db1->where('id',$data['id']);
		$this->db1->update('m_districts',$data);
		echo json_encode($data);
	}

	function get_kecamatan_detail($id){
		$data = $this->db->get_where('m_districts', array('id' => $id))->row();
		echo json_encode($data);
	}

	
	public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('m_villages');

        $this->db1->where('idKel', $id);
        $this->db1->delete('m_area');

        echo json_encode('SUKSES');
        
    }

    public function delete_kecamatan($id)
    {       
        $this->db->where('id', $id);
        $this->db->delete('m_districts');

        $this->db1->where('id', $id);
        $this->db1->delete('m_districts');
        echo json_encode('SUKSES');
        
    }

	
}
