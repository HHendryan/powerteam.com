<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {
	private $_username;

	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		// $this->load->model('Form_model');
		$this->load->model('Auth_model');
		$this->load->model('Tangible_model');
		$this->load->model('Guraklih_model');
		$this->load->model('Data_model');

		$this->Auth_model->can_access_by_login();
		$data = $this->session->userdata('guraklih');
		$this->_username = $data['username'];
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
	}

	function get_kabupaten(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kabupaten($id);
	}

	function get_kecamatan(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kecamatan($id);
	}	
	
	function get_kecamatan_multi(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kecamatan_multi($id);
	}	
	
	function get_kelurahan(){
		$this->output->enable_profiler(false);
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kelurahan($id);
	}

	function get_kelurahan_by_kec(){
		$this->output->enable_profiler(false);
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kelurahan_by_kec($id);
	}	
		
	function tangible(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$data['pertokohan'] = $this->Tangible_model->get_pertokohan_tangible();
		$data['parpol'] = $this->Tangible_model->get_parpol();
		//$data = $this->Tangible_model->option_tangible();
		$this->load->view('tangible',$data);
	}

	function get_data_tangible(){
		$this->datatables->select('id_intangible,name,address,phone,gender');
		$this->datatables->from('intangible');
		$this->datatables->where('type', 'tangible');
		$this->datatables->where('is_deleted', '0');
		$this->datatables->add_column('button', '<button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit" style="
    height: 26px;">View & Edit</button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete" style="
    height: 26px;">Delete</button>', 'id_intangible');
		echo $this->datatables->generate();
	}

	
	function intangible(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$data['pertokohan'] = $this->Tangible_model->get_pertokohan_intangible();
		$data['parpol'] = $this->Tangible_model->get_parpol();
		$this->load->view('intangible',$data);
	}

	function get_data_intangible(){
		$this->datatables->select('id_intangible,name,address,phone,gender');
		$this->datatables->from('intangible');
		$this->datatables->where('type', 'intangible');
		$this->datatables->where('is_deleted', '0');
		$this->datatables->add_column('button', '<button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit" style="
    height: 26px;">View & Edit</button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete" style="
    height: 26px;">Delete</button>', 'id_intangible');
		echo $this->datatables->generate();
	}

	 //CRUD
	 
	 function add_tangible(){
		$this->Tangible_model->add_tangible($this->input->post());
	}
	
	 function add_intangible(){
		$this->Tangible_model->add_intangible($this->input->post());
	}
	
	
	function get_tangible($id){
		$data = $this->db->get_where('intangible',array('id_intangible' => $id ))->row();
		$data->kabupaten = $this->db->get_where('m_regencies', array('id' => $data->id_city))->row('name');
		$data->id_kabupaten = $this->db->get_where('m_regencies', array('id' => $data->id_city))->row('id');
		$data->kecamatan = $this->db->get_where('m_districts', array('id' => $data->id_districts))->row('name');
		$data->id_kecamatan = $this->db->get_where('m_districts', array('id' => $data->id_districts))->row('id');
		$data->kelurahan = $this->db->get_where('m_villages', array('id' => $data->id_village))->row('name');
		$data->id_kelurahan = $this->db->get_where('m_villages', array('id' => $data->id_village))->row('id');
		echo json_encode($data);
	}
	
	function get_intangible($id){
		$data = $this->db->get_where('intangible',array('id_intangible' => $id, ))->row();
		$data->kabupaten = $this->db->get_where('m_regencies', array('id' => $data->id_city))->row('name');
		$data->id_kabupaten = $this->db->get_where('m_regencies', array('id' => $data->id_city))->row('id');
		$data->kecamatan = $this->db->get_where('m_districts', array('id' => $data->id_districts))->row('name');
		$data->id_kecamatan = $this->db->get_where('m_districts', array('id' => $data->id_districts))->row('id');
		$data->kelurahan = $this->db->get_where('m_villages', array('id' => $data->id_village))->row('name');
		$data->id_kelurahan = $this->db->get_where('m_villages', array('id' => $data->id_village))->row('id');
		echo json_encode($data);
	}
	
	function edit(){
		$data = $this->input->post();
		$this->db->where('id_intangible',$data['id_intangible']);
		$this->db->update('intangible',$data);
		echo json_encode($data);
	}

	function delete($id){
		$this->db->where('id_intangible', $id);
		$data['is_deleted'] = '1';
		$this->db->update('intangible',$data);
		echo json_encode($data);
	}

	function create_id(){
		$id_lama = $this->db->query('SELECT id_intangible from intangible order by id_intangible desc limit 1')->row('id_intangible');
		echo $id_lama;
	}

	/*
	* Function for show list of coordinator (Hari Hendryan)
	*/
	function list_guraklih(){
		$user = $this->session->userdata('guraklih');
		if($user['role'] == 'korprov'){
			$data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
			$data['kabupaten'] = $this->db->get_where('m_regencies', array('province_id' => $user['role_detail']))->result();
		}else if($user['role'] == 'korkab'){
			$data['kabupaten'] = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
			$data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $data['kabupaten']->province_id))->row();
			$data['kecamatan'] = $this->db->get_where('m_districts', array('regency_id' => $user['role_detail']))->result();
		}else{
			$data['provinsi'] = $this->db->get('m_provinces')->result();
		}
		
		$this->load->view('guraklih',$data);
	}

	/*
	* Function for get coordinator data (Hari Hendryan)
	*/
	function get_data_coordinator(){
		$user = $this->session->userdata('guraklih');

		$this->datatables->select('id, cp, address, phone, photo, nik, type, is_block');
		$this->datatables->from('m_team');
		$this->datatables->where('is_deleted', '0');
		if($user['role'] == 'korprov'){
			$this->datatables->where('type !=', 'korprov');
			$this->datatables->where('id_provinces', $user['role_detail']);
		}else if($user['role'] == 'korkab'){
			$this->datatables->where('type !=', 'korprov');
			$this->datatables->where('type !=', 'korkab');
			$this->datatables->where('id_regency', $user['role_detail']);
		}
		$this->datatables->add_column('button', '<button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit"><i class="fa fa-pencil"></i></button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i></button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-blk"><i class="fa fa-lock"></i></button>', 'id');
		echo $this->datatables->generate();
	}

	/*
	* Function for get coordinator by id (Hari Hendryan)
	*/
	function get_coordinator_by_id($id){
		$query = $this->db->query("SELECT id, id_provinces, id_regency, id_district, id_village, type, cp, address, nik, phone, photo FROM m_team WHERE id=$id");

		$data = $query->row();
		$data->kabupaten = $this->db->get_where('m_provinces', array('id' => $data->id_provinces))->row('name');
		$data->id_kabupaten = $this->db->get_where('m_provinces', array('id' => $data->id_provinces))->row('id');
		$data->kabupaten = $this->db->get_where('m_regencies', array('id' => $data->id_regency))->row('name');
		$data->id_kabupaten = $this->db->get_where('m_regencies', array('id' => $data->id_regency))->row('id');
		$data->kecamatan = $this->db->get_where('m_districts', array('id' => $data->id_district))->row('name');
		$data->id_kecamatan = $this->db->get_where('m_districts', array('id' => $data->id_district))->row('id');
		$data->kelurahan = $this->db->get_where('m_villages', array('id' => $data->id_village))->row('name');
		$data->id_kelurahan = $this->db->get_where('m_villages', array('id' => $data->id_village))->row('id');
		echo json_encode($data);
	}

	/*
	* Function for edit coordinator data (Hari Hendryan)
	*/
	function edit_coordinator(){
		$data = $this->input->post();
		$this->db->where('id',$data['id']);
		$this->db->update('m_team',$data);
		echo json_encode($data);
	}

	/*
	* Function for delete coordinator data (Hari Hendryan)
	*/
	function delete_coordinator($id){
		$this->db->where('id', $id);
		$data['is_deleted'] = '1';
		$this->db->update('m_team',$data);
		echo json_encode($data);
	}

	/*
	* Function for block/ unblock coordinator data (Hari Hendryan)
	*/
	function block_coordinator($id){
		$this->db->where('id', $id);

		$query = $this->db->query("SELECT id, is_block FROM m_team WHERE id=$id");
		$data1 = $query->row();
		if ($data1->is_block == '0') {
			$data['is_block'] = '1';
		} else {
			$data['is_block'] = '0';
		}
		$this->db->update('m_team',$data);
		echo json_encode($data);
	}

	/*
	* Function for add coordinator data (Hari Hendryan)
	*/
	function add_guraklih(){
		$this->Guraklih_model->add_guraklih($this->input->post());
	}

	/*
	* Function for show list of dapil (Hari Hendryan)
	*/
	function list_dapil(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$this->load->view('dapil',$data);
	}

	/*
	* Function for get dapil data (Hari Hendryan)
	*/
	function get_data_dapil(){
		$this->output->enable_profiler(false);
		$arrsearch = $this->input->post('search');
		$search = $arrsearch['value'];
		if($search == '') {
			$this->datatables->select('id_dapil, nama_dapil, (CASE
									    WHEN type ="1" THEN "DPR RI"
									    WHEN type ="2" THEN "DPRD PROV"
									    WHEN type ="3" THEN "DPRD KAB"
									    ELSE "" END) as type, periode');
		} else {
			$this->datatables->select('id_dapil, nama_dapil, type, periode');
		}

		$this->datatables->from('m_dapil');
		$this->datatables->where('is_deleted', '0');
		
		$this->datatables->add_column('button', '<button type="button" data-id=$1 data-nama="$2" data-tipe="$3" data-periode=$4 class="btn btn-success btn-sm btn-edit" style="height: 26px;">Edit</button>&nbsp;<button type="button" data-id=$1  class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modalDelete" style="height: 26px;">Delete</button>', 'id_dapil, nama_dapil, type, periode');
		echo $this->datatables->generate();
	}

	/*
	* Function for get dapil data (Hari Hendryan)
	*/
	function get_data_dapil_v2(){
		$this->output->enable_profiler(false);
		$arrsearch = $this->input->post('search');
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $arrsearch['value'];
		
		$qsearch = '';
		if($search != '') {
			$qsearch = " (id_dapil LIKE '%".$search."%' OR
				nama_dapil LIKE '%".$search."%' OR
				periode LIKE '%".$search."%' ) AND ";
		} 

		$json = array();
		
		$sql = " 
			SELECT 
				id_dapil, 
				nama_dapil, 
				(CASE WHEN type ='1' THEN 'DPR RI' WHEN type ='2' THEN 'DPRD PROV' WHEN type ='3' THEN 'DPRD KAB' ELSE '' END) as type, 
				periode 
			FROM m_dapil
			WHERE
				".$qsearch."
				is_deleted != '1'
			ORDER BY 
				nama_dapil ASC
		";

		$query = $this->db->query($sql);

		$total = $query->num_rows();
		$json['recordsFiltered'] = $total;
		$json['recordsTotal'] = $total;
		$json['draw'] = $this->input->post('draw');
		
		$json['data'] = array();

		$sql .= "
			LIMIT ".$start.",".$length."
		";

		$query = $this->db->query($sql);

		$i=0;
		foreach ($query->result() as $k => $v) {
			$id = $v->id_dapil;
			$json['data'][$i]['button'] = '<button type="button" data-id='.$id.' data-nama="ac2" data-tipe="type" data-periode=233 class="btn btn-success btn-sm btn-edit" style="height: 26px;">Edit</button>&nbsp;<button type="button" data-id='.$id.'  class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modalDelete" style="height: 26px;">Delete</button>';
			$json['data'][$i]['id_dapil'] = $v->id_dapil;
			$json['data'][$i]['nama_dapil'] = $v->nama_dapil;
			$json['data'][$i]['periode'] = $v->periode;
			$json['data'][$i]['type'] = $v->type;
			$i++;
		}

		
		
		echo json_encode($json);
	}


	/*
	* Function for get dapil by id (Hari Hendryan)
	*/
	function get_dapil_by_id($id){
		$this->output->enable_profiler(false);
		$query = $this->db->query("select id_dapil, nama_dapil, type, periode from m_dapil where is_deleted='0' AND id_dapil = $id");
		$data['dapil'] = $query->row();

		if ($data['dapil']->type=='3') {
			// $sql = "
			// 	SELECT DISTINCT
			// 		a.id_provinces, 
			// 		b.name as provinsi,  
			// 		a.id_regency, 
			// 		c.name as kabupaten,  
			// 		a.id_district, 
			// 		d.name as kecamatan 
			// 	FROM 
			// 		dapil_has_area a, 
			// 		m_provinces b, 
			// 		m_regencies c, 
			// 		m_districts d 
			// 	WHERE  
			// 		b.id = a.id_provinces AND 
			// 		c.id = a.id_regency AND  
			// 		d.id = a.id_district AND 
			// 		a.id_dapil=$id
			// ";
			$sql = "
				SELECT 
					idx,
					a.id_provinces, 
					b.name as provinsi,  
					a.id_regency, 
					c.name as kabupaten,  
					a.id_district, 
					d.name as kecamatan,
					a.id_village, 
					e.name as kelurahan 
				FROM 
					dapil_has_area a, 
					m_provinces b, 
					m_regencies c, 
					m_districts d,
					m_villages e
				WHERE  
					b.id = a.id_provinces AND 
					c.id = a.id_regency AND  
					d.id = a.id_district AND 
					e.id = a.id_village AND
					a.id_dapil=$id AND
					a.is_deleted != '1'
			";
			$query = $this->db->query($sql);
		} else {
			$sql = "
				SELECT DISTINCT
					a.id_provinces, 
					b.name as provinsi, 
					a.id_regency, 
					c.name as kabupaten,  
					a.id_district, 
					d.name as kecamatan
				FROM 
					dapil_has_area a, 
					m_provinces b, 
					m_regencies c,
					m_districts d
				WHERE 
					a.id_provinces =b.id AND 
					a.id_regency=c.id  AND 
					a.id_dapil=$id AND
					d.id = a.id_district AND 
					a.is_deleted != '1'
			";
			$query = $this->db->query($sql);
			// $query = $this->db->query("SELECT idx, a.id_provinces, b.name as provinsi,  a.id_regency, c.name as kabupaten,  a.id_district FROM dapil_has_area a, m_provinces b, m_regencies c WHERE a.id_provinces =b.id AND a.id_regency=c.id  AND a.id_dapil=1 ");
		}
		$data['area'] = $query->result();
		if ($data['dapil']->type=='3') {
			$i=0;
			foreach ($query->result() as $k => $v) {
				$data['area'][$i]->id_village = $v->id_village;
				$data['area'][$i]->kelurahan = $v->kelurahan;
				$i++;
			}
		}
		echo json_encode($data);
	}

	/*
	* Function for add dapil data (Hari Hendryan)
	*/
	function add_dapil(){
		$this->output->enable_profiler(false);
		$this->Data_model->add_m_dapil($this->input->post());
	}

	/*
	* Function for delete coordinator data (Hari Hendryan)
	*/
	function delete_dapil($id){
		$this->db->where('id_dapil', $id);
		$data['is_deleted'] = '1';
		$this->db->update('m_dapil',$data);
		echo json_encode($data);
	}

	/*
	* Function for edit dapil data (Hari Hendryan)
	*/
	function edit_dapil(){
		$this->Data_model->edit_m_dapil($this->input->post());
	}

	/*
	* Function for show list of pilpres (Hari Hendryan)
	*/
	function list_suara_pilpres(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$user = $this->session->userdata('guraklih');
		if($user['role_detail'] == 'All'){
			$data['nama_wilayah'] = 'NASIONAL';
			$data['id_wilayah'] = 0;
			$data['tipe_wilayah'] = 'NASIONAL';
		}else{
			if($user['role'] == 'korprov'){
				$detail = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
				$data['nama_wilayah'] = $detail->name;
				$data['id_wilayah'] = $detail->id;
				$data['tipe_wilayah'] = 'provinsi';
			};
			if($user['role'] == 'korkab'){
				$detail = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
				$data['nama_wilayah'] = $detail->name;
				$data['id_wilayah'] = $detail->id;
				$data['tipe_wilayah'] = 'kabupaten';
			};

		}
		$this->load->view('suara_pilpres',$data);
	}

	/*
	* Function for get suara pilpres data (Hari Hendryan)
	*/
	function get_suara_pilpres(){
		$id = $this->input->post('id');
		$tipe = $this->input->post('tipe');
		$nama = $this->input->post('nama');
		$tabel_suara ='suara_pilpres a';

		if ($id==0) {
			$this->datatables->select('a.*, a.id as id, a.idProv as idProv, (SELECT name from m_provinces WHERE id=a.idProv) as provinsi,
				 (SELECT name from m_regencies WHERE id=a.idKab) as kabkot, (SELECT name from m_districts WHERE id=a.idKec) as kecamatan,
				 (SELECT name from m_villages WHERE id=a.idKel) as kelurahan');
			$this->datatables->from($tabel_suara);
			$this->datatables->where('is_deleted', '0');
			$this->datatables->add_column('button', '<div><button type="button" data-id=$1 data-prov=$2 class="btn btn-success btn-sm btn-edit" style="
		    height: 26px; width:80px;margin-bottom:5px">Edit</button></div><div><button type="button" data-id=$1  data-prov=$2  class="btn btn-danger btn-sm btn-delete" style="
		    height: 26px; width:80px;">Delete</button></div>', 'id, idProv');
		} else {
			if ($tipe=='provinsi') {
				$this->datatables->select('a.*, a.id as id, a.idProv as idProv, (SELECT name from m_provinces WHERE id=a.idProv) as provinsi,
						 (SELECT name from m_regencies WHERE id=a.idKab) as kabkot, (SELECT name from m_districts WHERE id=a.idKec) as kecamatan,
						 (SELECT name from m_villages WHERE id=a.idKel) as kelurahan');
				$this->datatables->from($tabel_suara);
				$this->datatables->where('is_deleted', '0');
				$this->datatables->where('idProv', $id);
				$this->datatables->add_column('button', '<div><button type="button" data-id=$1 data-prov=$2 class="btn btn-success btn-sm btn-edit" style="
				    height: 26px; width:80px;margin-bottom:5px">Edit</button></div><div><button type="button" data-id=$1  data-prov=$2  class="btn btn-danger btn-sm btn-delete" style="
				    height: 26px; width:80px;">Delete</button></div>', 'id, idProv');
			} else if ($tipe=='kabupaten') {
				//$idProv = substr($id, 0, 2);
				//$tabel = $this->db->get_where('m_dpt', array('idProv' => $idProv))->row('tabel_suara');

				$this->datatables->select('a.*, a.id as id, a.idProv as idProv, (SELECT name from m_provinces WHERE id=a.idProv) as provinsi,
						 (SELECT name from m_regencies WHERE id=a.idKab) as kabkot, (SELECT name from m_districts WHERE id=a.idKec) as kecamatan,
						 (SELECT name from m_villages WHERE id=a.idKel) as kelurahan');
				$this->datatables->from($tabel_suara);
				$this->datatables->where('is_deleted', '0');
				$this->datatables->where('idKab', $id);
				$this->datatables->add_column('button', '<div><button type="button" data-id=$1 data-prov=$2 class="btn btn-success btn-sm btn-edit" style="
				    height: 26px; width:80px;margin-bottom:5px">Edit</button></div><div><button type="button" data-id=$1  data-prov=$2  class="btn btn-danger btn-sm btn-delete" style="
				    height: 26px; width:80px;">Delete</button></div>', 'id, idProv');
			}
		}

		echo $this->datatables->generate();
	}

	/*
	* Function for suara pilpres data (Hari Hendryan)
	*/
	function add_suara_pilpres(){
		$data = $this->input->post();
		$this->db->insert('suara_pilpres',$data);
		echo json_encode($data);
	}

	/*
	* Function for delete suara pilpres data (Hari Hendryan)
	*/
	function delete_suara_pilpres($id, $idProv){
		$this->db->where('id', $id);
		$data['is_deleted'] = '1';
		$this->db->update('suara_pilpres',$data);
		echo json_encode($data);
	}

	/*
	* Function for get suara pilpres by id (Hari Hendryan)
	*/
	function get_suara_pilpres_by_id($id, $idProv){
		$query = $this->db->query("SELECT a.*, a.id as id, (SELECT name from m_provinces WHERE id=a.idProv) as provinsi,
					 (SELECT name from m_regencies WHERE id=a.idKab) as kabkot, (SELECT name from m_districts WHERE id=a.idKec) as kecamatan,
					 (SELECT name from m_villages WHERE id=a.idKel) as kelurahan FROM suara_pilpres a WHERE is_deleted='0' AND id=".$id);
		$data = $query->row();
		echo json_encode($data);
	}

	/*
	* Function for edit suara pilpres by id (Hari Hendryan)
	*/
	function edit_legislator($idProv){
		$data = $this->input->post();

		$this->db->where('id',$data['id']);
		$this->db->update('suara_pilpres',$data);
		echo json_encode($data);
	}

	/*
    * Check unique code (Hari Hendryan)
    */
    function cek_unique_id_pilpres($unique_id){
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->get_where('suara_pilpres', array('unique_id' => $unique_id))->row();
    	if($data == NULL){
    		echo json_encode(1);
    	}else{
    		echo json_encode(0);
    	}
    }

    /*
    * Function for upload guraklih data via excel (Hari Hendryan)
    */
    function upload_suara_pilpres(){
    	$count=0;
        $fileName = $this->input->post('file', TRUE);
        $path = realpath(APPPATH . '../excel_dpt/');
        $config['upload_path'] = $path; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 150000;
        $cek = '';
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
          
        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
        }else{
            $upload_data = $this->upload->data();
            $this->load->library('excel');
            $file = 'excel_dpt/'.$upload_data['file_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $header = $sheetData[0];
            $cek = 0;
            for ($i=1; $i < sizeof($sheetData); $i++) { 
                for ($j=0; $j < sizeof($sheetData[$i]) ; $j++) { 
                    if($header[$j] == 'TAHUN'){
                        if($sheetData[$i][$j] == ''){
                            $cek = 1;
                            break;
                        }
                    }
                    $data['periode'] = $sheetData[$i][0];
                    $data['idProv'] = $this->input->post('idProv');
                    $data['idKab'] = $this->input->post('idKab');
                    $data['idKec'] = $this->input->post('idKec');
                    $data['idKel'] = $this->input->post('idKel');
                    $data['tps'] = $sheetData[$i][6];
                    $data['prabowo'] = $sheetData[$i][7];
                    $data['jokowi'] = $sheetData[$i][8];
                    $data['sah'] = $sheetData[$i][9];
                    $data['tidak_sah'] = $sheetData[$i][10];
                    $data['golput'] = $sheetData[$i][11];
                    
                    $data['unique_id'] = $this->input->post('unique');
                    
                }
                if($cek == 1){
                    break;
                }
                $count++;
                $this->db->insert('suara_pilpres', $data);
            }

        }
        echo json_encode($count);
    }

    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function get_data_dpt_belum_upload(){
    	$this->output->enable_profiler(false);
    	$start  = $this->input->post('start');
        $length = $this->input->post('length');
        $draw = $this->input->post('draw');
        $search = $this->input->post('search');

        $keyword = $search['value'];

        $this->db2 = $this->load->database("db_kab_final",true);

        $where = '';
        if($keyword != '') {
        	$where .= " 	
        		AND (
        			provinsi LIKE '%".$keyword."%' OR 
        			kabupaten LIKE '%".$keyword."%' OR 
        			kecamatan LIKE '%".$keyword."%' OR 
        			kelurahan LIKE '%".$keyword."%'
        		)
        	";
        }

        $sql = "
            SELECT * 
            FROM m_area
            WHERE dpt = 0 ".$where."
            

        ";
        $query = $this->db2->query($sql);

        $json = array();
        
        $json['draw'] = $draw;
		$json['recordsFiltered'] = $query->num_rows();
		$json['recordsTotal'] = $query->num_rows();

        $sql .= " LIMIT ".$start.",".$length.";";
		$query = $this->db2->query($sql);

		$i=0;
        foreach ($query->result() as $k => $v) {
			$json['data'][$i]['id']= $v->id;
			$json['data'][$i]['provinsi']= $v->provinsi;
			$json['data'][$i]['kabupaten']= $v->kabupaten;
			$json['data'][$i]['kecamatan']= $v->kecamatan;
			$json['data'][$i]['kelurahan']= $v->kelurahan;
			$json['data'][$i]['dpt'] = $v->dpt;
			$json['data'][$i]['button'] = '<button type="button" data-id="'.$v->id.'" class="btn btn-success btn-sm btn-delete" data-toggle="modal" data-target="#modalUpload" onclick="detailData('.$v->id.')" style="height: 26px;">Upload</button>';
			$i++;
		}

		echo json_encode($json);
    }

    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function get_data_dpt_upload_detail(){
    	$id  = $this->input->get('id');
        
        $this->db2 = $this->load->database("db_kab_final",true);

        $sql = "
            SELECT * 
            FROM m_area
        	WHERE id = ".$id."

        ";
        $query = $this->db2->query($sql);

        $json = array();
        
        foreach ($query->result() as $k => $v) {
			$json['id']= $v->id;
			$json['provinsi']= $v->provinsi;
			$json['kabupaten']= $v->kabupaten;
			$json['kecamatan']= $v->kecamatan;
			$json['kelurahan']= $v->kelurahan;
			
		}

		echo json_encode($json);
    }

    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function get_data_dpt_sudah_upload() {
    	$this->output->enable_profiler(false);
    	$start  = $this->input->post('start');
        $length = $this->input->post('length');
        $draw = $this->input->post('draw');
        $search = $this->input->post('search');

        $keyword = $search['value'];

        $this->db2 = $this->load->database("db_kab_final",true);

        $where = '';
        if($keyword != '') {
        	$where .= " 	
        		AND (
        			provinsi LIKE '%".$keyword."%' OR 
        			kabupaten LIKE '%".$keyword."%' OR 
        			kecamatan LIKE '%".$keyword."%' OR 
        			kelurahan LIKE '%".$keyword."%'
        		)
        	";
        }

        $sql = "
            SELECT * 
            FROM m_area
            WHERE dpt > 0 ".$where."
            

        ";
        $query = $this->db2->query($sql);

        $json = array();
        
        $json['draw'] = $draw;
		$json['recordsFiltered'] = $query->num_rows();
		$json['recordsTotal'] = $query->num_rows();

        $sql .= " LIMIT ".$start.",".$length.";";
		$query = $this->db2->query($sql);

		$i=0;
        foreach ($query->result() as $k => $v) {
			$json['data'][$i]['id']= $v->id;
			$json['data'][$i]['provinsi']= $v->provinsi;
			$json['data'][$i]['kabupaten']= $v->kabupaten;
			$json['data'][$i]['kecamatan']= $v->kecamatan;
			$json['data'][$i]['kelurahan']= $v->kelurahan;
			$json['data'][$i]['dpt'] = $v->dpt;
			$json['data'][$i]['button'] = '<button type="button" data-id="'.$v->id.'" class="btn btn-success btn-sm btn-delete" data-toggle="modal" onclick="openDetailDPT('.$v->id.');" style="height: 26px;">View</button>&nbsp;&nbsp;<button type="button" data-id="'.$v->id.'" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalUpload" style="height: 26px;" onclick="detailData('.$v->id.')">Upload</button>';
			$i++;
		}

		echo json_encode($json);
    }

    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function get_data_dpt_detail(){
  		$this->output->enable_profiler(false);
    	$start  = $this->input->post('start');
        $length = $this->input->post('length');
        $draw = $this->input->post('draw');
        $id = $this->input->post('id');

        $this->db2 = $this->load->database("db_kab_final",true);

        $sql = "
            SELECT * 
            FROM m_area
            WHERE id = ".$id."
        ";
        $query = $this->db2->query($sql);

        $kab = 0;
        foreach ($query->result() as $k => $v) {
        	$kab = $v->idKab;
        }

        $sql = "
            SELECT * 
            FROM kab_".$kab."
        ";

        $query = $this->db2->query($sql);

        $json = array();
        
        $json['draw'] = $draw;
		$json['recordsFiltered'] = $query->num_rows();
		$json['recordsTotal'] = $query->num_rows();

        $sql .= " LIMIT ".$start.",".$length.";";
		$query = $this->db2->query($sql);

		$json = array();
		$i=0;
		foreach ($query->result() as $k => $v) {
			$json['data'][$i]['id']= $v->id;
			$json['data'][$i]['nokk']= $v->no_kk;
			$json['data'][$i]['nik']= $v->nik;
			$json['data'][$i]['nama']= $v->nama;
			$json['data'][$i]['alamat']= $v->alamat;
			$json['data'][$i]['gender'] = $v->jns_kelamin;
			$json['data'][$i]['updater'] = $v->guraklihId;
			$date = explode(' ',$v->lup);
			$arrdt = explode('-',$date[0]);
			$json['data'][$i]['tanggal'] = $arrdt[2].'/'.$arrdt[1].'/'.$arrdt[0];
			$i++;
		}

		echo json_encode($json);
    }

    /*
	* Function for show list of dapil (Hari Hendryan)
	*/
	function form_data_dpt_upload(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$this->load->view('form_data_dpt_upload',$data);
	}

	/*
    * Function for show list of dapil (Hari Hendryan)
    */
    function upload_belum(){
        $data['provinsi'] = $this->db->get('m_provinces')->result();
        $this->load->view('upload_belum',$data);
    }

    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function upload_sudah(){
    	$data['provinsi'] = $this->db->get('m_provinces')->result();
        $this->load->view('upload_sudah',$data);
    }

    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function data_list_dpt(){
        $data['id'] = $this->input->get('id');
        $this->load->view('list_dpt',$data);
    }

    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function upload_dpt(){
    	$this->output->enable_profiler(false);
        $pho = $this->input->post('i');
        $idx = $this->input->post('id');

        $result = array();

		$id = rand(111111111,999999999);

		$target_dir = "/var/www/html/guraklih/assets/upload/";

		$filename = 'preview_'.$id.'_'.date('YmdHis');
		$target_file = $target_dir . $filename;

		//$path = create_images_from_string($pho,'preview_images/'.$filename);    // ENABLE WHEN PROD

	    $crt = file_put_contents($target_file,base64_decode($pho));

	    $this->load->model("data_model");

        $query = $this->data_model->get_detail_area($idx);
        $kab = 0;
        $kec = 0;
        $kel = 0;
        foreach ($query as $k => $v) {
        	$kab = $v->idKab;
        	$kec = $v->idKec;
        	$kel = $v->idKel;
        }

        $json = array();
	    $row = 1;
	    $is_table_exist = false;
		if (($handle = fopen($target_file, "r")) !== FALSE) {
			
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				//echo "<p> $num fields in line $row: <br /></p>\n";
				//print_r($data);
				$row++;
				// for ($c=0; $c < $num; $c++) {
				//     echo $data[$c] . "<br />\n";

				// }

				if(!$is_table_exist) {
					$this->data_model->create_custom_table($kab);
					$is_table_exist = true;
				}

				//if(is_numeric($data[1])) {
				if($row > 6) {
					//$id,$idkec,$idkel,$tps,$no_kk,$nik,$nama,$tempat,$tanggal,$usia,$gender,$alamat,$rt,$rw,$disabilitas,$status_kawin
					$this->data_model->insert_dpt($kab,
						$kec,$kel,$data[14],$data[1],$data[2],$data[3],
						$data[4],$data[5],$data[6],$data[8],$data[9],
						$data[10],$data[11],$data[12],$data[7]
					);

				}

			}
			$this->data_model->update_dpt_v2($idx,$kab);
			fclose($handle);

			$result['status_code'] = '000';
			$result['status'] = 'success';
		
		} else {
			$result['status_code'] = '001';
			$result['status'] = 'Upload Gagal';
		
		}

		echo json_encode($result);

    }


    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function upload_dpt_v2(){
    	$this->output->enable_profiler(false);
    	$count=0;
        $fileName = $this->input->post('file', TRUE);
        $path = realpath(APPPATH . '../excel_dpt/');
        $config['upload_path'] = $path; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 150000;
        $cek = '';
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
        
        $status = '';
        
        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            //var_dump($error);
            $status = 'error';
        }else{
        	$idx = $this->input->post('id');
        	$page = $this->input->post('page');
        	$this->load->model("data_model");
	        $query = $this->data_model->get_detail_area($idx);
	        $kab = 0;
	        $kec = 0;
	        $kel = 0;
	        foreach ($query as $k => $v) {
	        	$kab = $v->idKab;
	        	$kec = $v->idKec;
	        	$kel = $v->idKel;
	        }

	        $this->data_model->create_custom_table($kab);

            $upload_data = $this->upload->data();
            $this->load->library('excel');
            $file = 'excel_dpt/'.$upload_data['file_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $header = $sheetData[0];
            $cek = 0;

            for ($i=5; $i < sizeof($sheetData); $i++) { 
            	// print_r($sheetData[$i]);
            	$data = $sheetData[$i];
                $this->data_model->insert_dpt($kab,
					$kec,$kel,$data[14],$data[1],$data[2],addslashes($data[3]),
					addslashes($data[4]),$data[5],$data[6],$data[8],addslashes($data[9]),
					$data[10],$data[11],$data[12],$data[7]
				);
                
                //$count++;
                //$this->db->insert('suara_pilpres', $data);
            }

            $this->data_model->update_dpt_v2($idx,$kab);
            $status = 'success';
        }
        header('header: '.$page.'?status='.$status);
    }

    

}
