<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyelenggara extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Penyelenggara_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function get_penyelenggara($id){
		$data = $this->db->get_where('m_penyelenggara',array('id' => $id ))->row();
		echo json_encode($data);
	}

	function update_penyelenggara(){
		$id=$this->input->post('id');
		$this->Penyelenggara_model->update_penyelenggara($id,$this->input->post());
	}

	function delete_penyelenggara($id){
		$this->Penyelenggara_model->delete_penyelenggara($id);
	}

	function kpu(){
		$this->load->view('p_kpu');
	}

	function load_data_kpu(){
		$data['data'] = $this->Penyelenggara_model->kpu();
		echo json_encode($data);
	}

	function add_kpu(){
		$this->Penyelenggara_model->add_kpu($this->input->post());
	}

	function panwas(){
		$this->load->view('p_panwas');
	}
	function load_data_panwas(){
		$data['data'] = $this->Penyelenggara_model->panwas();
		echo json_encode($data);
	}
	function add_panwas(){
		$this->Penyelenggara_model->add_panwas($this->input->post());
	}
	function bawaslu(){
		$this->load->view('p_bawaslu');
	}
	function load_data_bawaslu(){
		$data['data'] = $this->Penyelenggara_model->bawaslu();
		echo json_encode($data);
	}
	function add_bawaslu(){
		$this->Penyelenggara_model->add_bawaslu($this->input->post());
	}
}
