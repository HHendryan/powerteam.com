<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
		$this->load->model('Target_model');
		$this->Auth_model->can_access_by_login();
		$data = $this->session->userdata('guraklih');
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		redirect('target/entry');
	}

	function entry(){
		$user = $this->session->userdata('guraklih');

		if($user['role_detail'] == 'All'){
			$data['nama_wilayah'] = 'NASIONAL';
			$data['id_wilayah'] = 0;
			$data['tipe_wilayah'] = 'NASIONAL';
		}else{
			if($user['role'] == 'korprov'){
				$detail = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
				$data['nama_wilayah'] = $detail->name;
				$data['id_wilayah'] = $detail->id;
				$data['tipe_wilayah'] = 'provinsi';
				$data['target'] = $this->db->get_where('am_prov_target', array('idProv' => $detail->id))->row('target');
			};
			if($user['role'] == 'korkab'){
				$detail = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
				$data['nama_wilayah'] = $detail->name;
				$data['id_wilayah'] = $detail->id;
				$data['tipe_wilayah'] = 'kabupaten';
				$data['target'] = $this->db->get_where('am_kab_target', array('idKab' => $detail->id))->row('target');
			};

		}
		$this->load->view('target_entry',$data);
	}

	// function monitoring(){
	// 	$this->load->view('target_monitoring',$data);
	// }

	function get_target(){
		$tipe = $this->input->post('tipe');
		$id = $this->input->post('id');
		if($tipe == 'provinsi'){
			$tabel = 'am_prov_target';
			$id_select = 'idProv';
		};
		if($tipe == 'kabupaten'){
			$tabel = 'am_kab_target';
			$id_select = 'idKab';	
		};
		if($tipe == 'kecamatan'){
			$tabel = 'am_kec_target';
			$id_select = 'idKec';	
		};
		if($tipe == 'kelurahan'){
			$tabel = 'am_kel_target';
			$id_select = 'idKel';	
		};

		$data = $this->db->query("SELECT * FROM $tabel where $id_select=$id")->row();
		echo json_encode($data);
	}

	function edit_target(){
		$data = $this->input->post();
		$input['target'] = $data['target'];
		if($data['tipe'] == 'provinsi'){
			$tabel = 'am_prov_target';
			$id_select = 'idProv';
		}
		if($data['tipe'] == 'kabupaten'){
			$tabel = 'am_kab_target';
			$id_select = 'idKab';
		};
		if($data['tipe'] == 'kecamatan'){
			$tabel = 'am_kec_target';
			$id_select = 'idKec';	
		};
		if($data['tipe'] == 'kelurahan'){
			$tabel = 'am_kel_target';
			$id_select = 'idKel';	
		};
		$this->db->where($id_select,$data['id']);
		if($this->db->update($tabel,$input)){
			echo json_encode($input['target']);
		}else{
			set_status_header(422);
			echo json_encode('Database Error');
			return;
		}
	}
	function load_table(){
		$id = $this->input->post('id');
		$tipe = $this->input->post('tipe');
		$nama = $this->input->post('nama');
		$data = $this->Target_model->get_data($id,$tipe);
		$data['id'] = $id;
		$data['tipe'] = $tipe;
		$data['nama'] = $nama;
		$this->load->view('table_target_entry',$data);
	}
}
