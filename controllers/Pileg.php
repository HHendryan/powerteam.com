<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pileg extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Master_model');
		$this->load->model('Auth_model');
		$this->load->model('Tangible_model');

		$this->Auth_model->can_access_by_login();
		//$this->Auth_model->can_access_by_level(99,'Master');
		$data = $this->session->userdata('guraklih');
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){

	}
	function legislator(){
		$data['parpol'] = $this->Tangible_model->get_parpol();
		$this->load->view('legislator', $data);
	}

	function get_table_legislator(){
		$this->datatables->select('id_legislator as id,id_dapil,nama,alamat,type,phone,periode,status');
		$this->datatables->from('m_legislator');
		// $this->datatables->where('type', 'tangible');
		$this->datatables->where('is_deleted', '0');
		$this->datatables->add_column('button', '<button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit" style="
    height: 26px;">View & Edit</button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete" style="
    height: 26px;">Delete</button>', 'id');
		echo $this->datatables->generate();
	}

	function get_dapil(){
		$type = $this->input->post("type");
		$periode = $this->input->post("periode");
		$option = '<option disabled selected value>PILIH</option>';
		$data = $this->db->get_where('m_dapil', array('type' => $type, 'periode' => $periode, 'is_deleted'=>'0'))->result();
		foreach ($data as $key => $value) {
			$option .= '<option value="'.$value->id_dapil.'">'.$value->nama_dapil.'</option>';
		}
		echo ($option);
	}

	function get_parpol() {

	}

	function input_legislator(){
		$config['upload_path']          = '../img_legislator/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 2048;
		$config['file_ext_tolower'] 	= true;
		$config['max_filename']			= 200;
		$config['encrypt_name'] 		= TRUE;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('berkas')){
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		} else {
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name'];
			$data = $this->input->post();
			$data['photo'] = $file_name;
			$this->db->insert("m_legislator",$data);
			echo json_encode($data);
		}
	}

	function delete_legislator($id){
		$data['is_deleted'] = '1';
		$this->db->where('id_legislator',$id);
		$this->db->update('m_legislator',$data);
		echo json_encode($data);
	}

	function detail_legislator($id){
		$this->db->select('a.*,b.nama_dapil');
		$this->db->from('m_legislator a');
		$this->db->join('m_dapil b', 'a.id_dapil = b.id_dapil', 'left');
		$this->db->where('a.id_legislator',$id);
		$data = $this->db->get()->row();
		echo json_encode($data);
	}

	function edit_legislator(){
		$data = $this->input->post();
		$this->db->where('id_legislator',$data['id_legislator']);
		$this->db->update('m_legislator',$data);
		echo json_encode($data);
	}

	function suara(){
		$data['legislator'] = $this->db->get_where('m_legislator', array('is_deleted' => '0'))->result();
		$this->load->view('legislator_suara',$data);
	}

	function get_table_dapil_suara($id_legislator){
		$data = $this->db->get_where('m_legislator', array('is_deleted' => '0', 'id_legislator' => $id_legislator))->row();
		if(!$data){
			$id_dapil = 0;
		}else{
			$id_dapil = $data->id_dapil;
		}


		$this->datatables->select('b.id_dapil as id, b.id_dapil, b.id_provinces,b.id_regency,b.id_district,a.id as id_village,d.name as prov, c.name as kab, e.name as kec, a.name as kel');
		$this->datatables->from('m_villages a');
		$this->datatables->join('dapil_has_area b', 'a.district_id=b.id_district', 'inner');
		$this->datatables->join('m_regencies c', 'b.id_regency = c.id', 'inner');
		$this->datatables->join('m_provinces d', 'b.id_provinces = d.id', 'inner');
		$this->datatables->join('m_districts e', 'b.id_district = e.id', 'inner');
		$this->datatables->where('b.is_deleted', '0');
		$this->datatables->where('b.id_dapil', $id_dapil);
		$this->datatables->add_column('button', '<button type="button" data-dapil=$1 data-prov=$2 data-kab=$3 data-kec=$4 data-kel=$5 class="btn btn-success btn-sm btn-edit" style="
    height: 26px;">Edit Suara</button>', 'id_dapil,id_provinces,id_regency,id_district,id_village');
		echo $this->datatables->generate();
	}

	function detail_suara(){
		$data = $this->input->get();
		$result = $this->check_suara($data);
		if(!$result){
			$data['total'] = 0 ;
			$this->db->insert('suara_legislator',$data);
			$result = $this->check_suara($data);
		}
		echo json_encode($result);
	}

	function check_suara($data){
		$this->db->where('id_dapil',$data['id_dapil']);
		$this->db->where('id_legislator',$data['id_legislator']);
		$this->db->where('id_provinces',$data['id_provinces']);
		$this->db->where('id_regency',$data['id_regency']);
		$this->db->where('id_district',$data['id_district']);
		$this->db->where('id_village',$data['id_village']);
		$result = $this->db->get('suara_legislator')->row();
		return $result;
	}

	function edit_suara(){
		$data = $this->input->post();
		$this->db->where('id_dapil',$data['id_dapil']);
		$this->db->where('id_legislator',$data['id_legislator']);
		$this->db->where('id_provinces',$data['id_provinces']);
		$this->db->where('id_regency',$data['id_regency']);
		$this->db->where('id_district',$data['id_district']);
		$this->db->where('id_village',$data['id_village']);
		$this->db->update('suara_legislator',$data);
		echo json_encode($data);
	}
	
	/*
	* Function for show list of suara pileg (Hari Hendryan)
	*/
	function suara_legislator(){
		$data['parpol'] = $this->Tangible_model->get_parpol();
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$this->load->view('suara_legislator', $data);
	}

	/*
	* Function for show data from suara pileg (Hari Hendryan)
	*/
	function get_table_suara_legislator(){
		$tabel_suara ='suara_legislator_new a';
		$this->datatables->select('a.*, a.id as id, a.idProv as idProv, (SELECT name from m_provinces WHERE id=a.idProv) as provinsi,
				 (SELECT name from m_regencies WHERE id=a.idKab) as kabkot, (SELECT name from m_districts WHERE id=a.idKec) as kecamatan,
				 (SELECT name from m_villages WHERE id=a.idKel) as kelurahan, (SELECT nama_dapil from m_dapil WHERE id_dapil=a.id_dapil) as dapil, 
				 (SELECT nama from m_legislator WHERE id_legislator=a.id_legislator) as caleg,(SELECT alias from m_parpol WHERE id_parpol=a.id_parpol) as parpol,
				 (CASE WHEN state ="1" THEN "DD1" WHEN state ="2" THEN "DB1" WHEN state ="3" THEN "DA1" WHEN state ="4" THEN "C1" ELSE "" END) as state,
				 (CASE WHEN state ="1" THEN a.dd1 WHEN state ="2" THEN a.db1 WHEN state ="3" THEN a.da1 WHEN state ="4" THEN a.c1 ELSE "" END) as suara');
		$this->datatables->from($tabel_suara);
		$this->datatables->where('is_deleted', '0');
		$this->datatables->add_column('button', '<div><button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit" style="
				    height: 26px; width:80px;margin-bottom:5px">Edit</button></div><div><button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete" style="
				    height: 26px; width:80px;">Delete</button></div>', 'id');
		echo $this->datatables->generate();
	}

	/*
	* Function for get legislator list by id dapil (Hari Hendryan)
	*/
	function get_legislator(){
		$id_dapil = $this->input->post("id_dapil");
		$option = '<option disabled selected value>PILIH</option>';
		$data = $this->db->get_where('m_legislator', array('id_dapil' => $id_dapil, 'is_deleted'=>'0'))->result();
		foreach ($data as $key => $value) {
			$option .= '<option value="'.$value->id_legislator.'">'.$value->nama.'</option>';
		}
		echo ($option);
	}

	/*
	* Function for get legislator list by id dapil (Hari Hendryan)
	*/
	function get_legislator_parpol(){
		$option = '';
		$id_legislator = $this->input->post("id_legislator");
		$this->db->select('a.id_parpol as id_parpol, b.alias as alias');
		$this->db->from('m_legislator a');
		$this->db->join('m_parpol b', 'a.id_parpol = b.id_parpol', 'left');
		$this->db->where('a.id_legislator',$id_legislator);
		$data = $this->db->get()->result();

		foreach ($data as $key => $value) {
			$option .= '<option value="'.$value->id_parpol.'">'.$value->alias.'</option>';
		}
		echo ($option);
	}

	/*
	* Function for get provinsi by id dapil (Hari Hendryan)
	*/
	function get_prov_dapil() {
		$option = '<option disabled selected value>PILIH</option>';
		$id_dapil = $this->input->post("id_dapil");
		$this->db->select('a.id_provinces as id_provinces, b.name as name');
		$this->db->from('dapil_has_area a');
		$this->db->join('m_provinces b', 'a.id_provinces = b.id', 'left');
		$this->db->where('a.id_dapil',$id_dapil);
		$this->db->limit(1);
		$data = $this->db->get()->result();

		foreach ($data as $key => $value) {
			$option .= '<option value="'.$value->id_provinces.'">'.$value->name.'</option>';
		}
		echo ($option);
	}

	/*
	* Function for input suara legislator(Hari Hendryan)
	*/
	function input_suara_legislator(){
		$table='';
		$data = $this->input->post();
		unset($data['type']);
		if ($data['state']==1) {
			$table = 'suara_legislator_dd1';
			$data['idKec'] = NULL;
			$data['idKel'] = NULL;
			$data['tps'] = NULL;
		} else if ($data['state']==2) {
			$table = 'suara_legislator_db1';
			$data['idKel'] = NULL;
			$data['tps'] = NULL;
		} else if ($data['state']==3) {
			$table = 'suara_legislator_da1';
			$data['tps'] = NULL;
		} else if ($data['state']==4) {
			$table = 'suara_legislator_c1';
		}

		$this->db->insert($table,$data);
		echo json_encode($data);
	}

	/*
	* Function for delete suara legislator(Hari Hendryan)
	*/
	function delete_suara_legislator($id){
		$data['is_deleted'] = '1';
		$this->db->where('id',$id);
		$this->db->update('suara_legislator_new',$data);
		echo json_encode($data);
	}

	/*
	* Function for get detail suara legislator(Hari Hendryan)
	*/
	function detail_suara_legislator($id){
		$this->db->select('a.*, a.id as id, a.idProv as idProv, (SELECT name from m_provinces WHERE id=a.idProv) as provinsi,
				 (SELECT name from m_regencies WHERE id=a.idKab) as kabkot, (SELECT name from m_districts WHERE id=a.idKec) as kecamatan,
				 (SELECT name from m_villages WHERE id=a.idKel) as kelurahan, (SELECT nama_dapil from m_dapil WHERE id_dapil=a.id_dapil) as dapil, 
				 (SELECT nama from m_legislator WHERE id_legislator=a.id_legislator) as caleg,(SELECT alias from m_parpol WHERE id_parpol=a.id_parpol) as parpol,
				 (SELECT type from m_dapil WHERE id_dapil=a.id_dapil) as type, (SELECT `periode` from m_dapil WHERE id_dapil=a.id_dapil) as periode,
				 
				 (CASE WHEN state ="1" THEN a.dd1 WHEN state ="2" THEN a.db1 WHEN state ="3" THEN a.da1 WHEN state ="4" THEN a.c1 ELSE "" END) as suara');
		$this->db->from('suara_legislator_new a');
		$this->db->where('a.id',$id);
		$data = $this->db->get()->row();
		echo json_encode($data);
	}

	/*
	* Function for edit  suara legislator(Hari Hendryan)
	*/
	function edit_suara_legislator(){
		$data = $this->input->post();
		if ($data['state']==1) {
			$data['dd1'] = $data['suara'];
			$data['idKec'] = NULL;
			$data['idKel'] = NULL;
			$data['tps'] = NULL;
		} else if ($data['state']==2) {
			$data['db1'] = $data['suara'];
			$data['idKel'] = NULL;
			$data['tps'] = NULL;
		} else if ($data['state']==3) {
			$data['da1'] = $data['suara'];
			$data['tps'] = NULL;
		} else if ($data['state']==4) {
			$data['c1'] = $data['suara'];
		}
		unset($data['type']);
		unset($data['suara']);
		
		$this->db->where('id',$data['id']);
		$this->db->update('suara_legislator_new',$data);
		echo json_encode($data);
	}

	/*
    * Function for upload guraklih data via excel (Hari Hendryan)
    */
    function upload_m_legislator(){
    	$count=0;
        $fileName = $this->input->post('file', TRUE);
        $path = realpath(APPPATH . '../excel_dpt/');
        $config['upload_path'] = $path; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 150000;
        $cek = '';
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
          
        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
        }else{
            $upload_data = $this->upload->data();
            $this->load->library('excel');
            $file = 'excel_dpt/'.$upload_data['file_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $header = $sheetData[0];
            $cek = 0;
            for ($i=1; $i < sizeof($sheetData); $i++) { 
                for ($j=0; $j < sizeof($sheetData[$i]) ; $j++) { 
                    if($header[$j] == 'NAMA'){
                        if($sheetData[$i][$j] == ''){
                            $cek = 1;
                            break;
                        }
                    }
                    $data['id_dapil'] = $this->input->post('id_dapil1');
                    $data['nama'] = $sheetData[$i][0];
                    $data['jk'] = $sheetData[$i][1];
                    $data['alamat'] = $sheetData[$i][2];
                    $data['type'] = $this->input->post('type1');
                    $data['periode'] = $this->input->post('periode1');
                    $data['status'] = $sheetData[$i][3];
                    $data['no_urut'] = $sheetData[$i][4];
                   	$id_parpol = $this->db->query("SELECT id_parpol FROM m_parpol where alias ='".$sheetData[$i][5]."'")->row('id_parpol');  
                    $data['id_parpol'] = $id_parpol;
                    $data['photo'] = $sheetData[$i][6];
                    
                    $data['unique_id'] = $this->input->post('unique');
                    
                }
                if($cek == 1){
                    break;
                }
                $count++;
                $this->db->insert('m_legislator', $data);
            }

        }
        echo json_encode($count);
    }

    /*
    * Check unique code (Hari Hendryan)
    */
    function cek_unique_id_legislator($unique_id){
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->get_where('m_legislator', array('unique_id' => $unique_id))->row();
    	if($data == NULL){
    		echo json_encode(1);
    	}else{
    		echo json_encode(0);
    	}
    }

    /*
    * Check count of data by unique code (Hari Hendryan)
    */
    function check_jumlah_legislator($unique_id){
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->query("SELECT count(*) as jml FROM m_legislator where unique_id='$unique_id'")->row('jml');
    	if($data == '0'){
    		echo json_encode(0);
    	}else{
    		echo json_encode('JUMLAH DATA : '.$data);
    	}
    }

     /*
    * Check delete data by unique code (Hari Hendryan)
    */
    function hapus_data_legislator($unique_id){
    	$id = urldecode($unique_id);
    	$this->db->where('unique_id', $id);
    	if($this->db->delete('m_legislator')){
    		echo json_encode(1);
    	} else {
    		echo json_encode(0);
    	}
    }

    /*
    * Check unique code (Hari Hendryan)
    */
    function cek_unique_suara_legislator($unique_id, $state){
    	$table='';
    	$unique_id = urldecode($unique_id);

    	if ($state==1) {
			$table = 'suara_legislator_dd1';
		} else if ($state==2) {
			$table = 'suara_legislator_db1';
		} else if ($state==3) {
			$table = 'suara_legislator_da1';
		} else if ($state==4) {
			$table = 'suara_legislator_c1';
		}

    	$data = $this->db->get_where($table, array('unique_id' => $unique_id))->row();
    	if($data == NULL){
    		echo json_encode(1);
    	}else{
    		echo json_encode(0);
    	}
    }

    /*
    * Function for upload guraklih data via excel (Hari Hendryan)
    */
    function upload_suara_legislator(){
    	$table='';
    	$count=0;
        $fileName = $this->input->post('file', TRUE);
        $path = realpath(APPPATH . '../excel_dpt/');
        $config['upload_path'] = $path; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 150000;
        $cek = '';
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
          
        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
        }else{
            $upload_data = $this->upload->data();
            $this->load->library('excel');
            $file = 'excel_dpt/'.$upload_data['file_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $header = $sheetData[0];
            $cek = 0;
            $state = $this->input->post('state1');
            for ($i=1; $i < sizeof($sheetData); $i++) { 
                for ($j=0; $j < sizeof($sheetData[$i]) ; $j++) { 
                    if($header[$j] == 'NAMA'){
                        if($sheetData[$i][$j] == ''){
                            $cek = 1;
                            break;
                        }
                    }
                    $data['idProv'] = $this->input->post('idProv1');
                    $data['idKab'] = $this->input->post('idKab1');
                    $data['idKec'] = $this->input->post('idKec1');
                    $data['idKel'] = $this->input->post('idKel1');
                    $data['id_dapil'] = $this->input->post('id_dapil1');
                    $data['periode'] = $this->input->post('periode1');
                    $data['state'] = $this->input->post('state1');

                    $id_legislator = $this->db->query("SELECT id_legislator FROM m_legislator where nama ='".$sheetData[$i][0]."' AND id_dapil=".$data['id_dapil']." AND periode=".$data['periode'])->row('id_legislator');
                    $data['id_legislator'] = $id_legislator;

                    $id_parpol = $this->db->query("SELECT id_parpol FROM m_parpol where alias ='".$sheetData[$i][1]."'")->row('id_parpol');
                    $data['id_parpol'] = $id_parpol;

                    $data['tps'] = $sheetData[$i][6];
                    $data['suara'] = $sheetData[$i][7];
                    
                    $data['unique_id'] = $this->input->post('unique');
                    
                }
                if($cek == 1){
                    break;
                }
                $count++;

                if ($state==1) {
					$table = 'suara_legislator_dd1';
				} else if ($state==2) {
					$table = 'suara_legislator_db1';
				} else if ($state==3) {
					$table = 'suara_legislator_da1';
				} else if ($state==4) {
					$table = 'suara_legislator_c1';
				}

                $this->db->insert($table, $data);
            }

        }
        echo json_encode($count);
    }

    /*
    * Check count of data by unique code (Hari Hendryan)
    */
    function check_jumlah_suaracaleg($unique_id){
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->query("SELECT count(*) as jml FROM m_legislator where unique_id='$unique_id'")->row('jml');
    	if($data == '0'){
    		echo json_encode(0);
    	}else{
    		echo json_encode('JUMLAH DATA : '.$data);
    	}
    }

     /*
    * Check delete data by unique code (Hari Hendryan)
    */
    function hapus_data_suaracaleg($unique_id){
    	$id = urldecode($unique_id);
    	$this->db->where('unique_id', $id);
    	if($this->db->delete('m_legislator')){
    		echo json_encode(1);
    	} else {
    		echo json_encode(0);
    	}
    }
}
