<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usia extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Usia_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$data['provinsi'] = $this->db->get('provinsi')->result();
		$this->load->view('usia',$data);
	}

	function get_kabupaten(){
		$id = $this->input->post('id');
		echo $this->Usia_model->get_kabupaten($id);
	}

	function get_kecamatan(){
		$id = $this->input->post('id');
		echo $this->Usia_model->get_kecamatan($id);
	}
	function load_data_usia($prov,$kab,$kec){
		$data['data'] = $this->Usia_model->get_data_usia($prov,$kab,$kec);
		echo json_encode($data);
	}
}
