<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guraklih extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Jalur_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
        $data = $this->session->userdata('guraklih');
        $this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$user = $this->session->userdata('guraklih');
        if($user['role'] == 'korprov'){
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('province_id' => $user['role_detail']))->result();
        }else if($user['role'] == 'korkab'){
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $data['kabupaten']->province_id))->row();
            $data['kecamatan'] = $this->db->get_where('m_districts', array('regency_id' => $user['role_detail']))->result();
        }else{
            $data['provinsi'] = $this->db->get('m_provinces')->result();
        }
		$this->load->view('monitoring_guraklih',$data);
	}

	function monitoring_guraklih(){
		$user = $this->session->userdata('guraklih');
        if($user['role'] == 'korprov'){
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('province_id' => $user['role_detail']))->result();
        }else if($user['role'] == 'korkab'){
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $data['kabupaten']->province_id))->row();
            $data['kecamatan'] = $this->db->get_where('m_districts', array('regency_id' => $user['role_detail']))->result();
        }else{
            $data['provinsi'] = $this->db->get('m_provinces')->result();
        }
		$this->load->view('monitoring_guraklih',$data);
	}

	function get_wilayah($tipe){
		$id = $this->input->post('id');
		if($tipe == 'provinsi'){
			$tabel = 'm_regencies';
			$id_select = 'province_id';
		}
		if($tipe == 'kabupaten'){
			$tabel = 'm_districts';
			$id_select = 'regency_id';
		}
		if($tipe == 'kecamatan'){
			$tabel = 'm_villages';
			$id_select = 'district_id';
		}
		if($tipe == 'kelurahan'){
			$id_prov = $this->db->query("SELECT c.province_id from m_villages a
					LEFT JOIN m_districts b on a.district_id=b.id
					LEFT JOIN m_regencies c on b.regency_id=c.id
					where a.id='$id'")->row('province_id');
			$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
			$id_select = 'idKel';
		}

		$result = $this->db->get_where($tabel, array($id_select => $id))->result();
		
		$list="<option value='all' selected>ALL</option>";
		foreach ($result as $value ){
			if($tipe != 'kelurahan'){
				$list.= "<option value='$value->id'>$value->name</option>";
			}else{
				$list.= "<option value='$value->tps'>$value->tps</option>";
			}
			
		}

		echo $list;
		
	}

	function get_guraklih($prov,$kab,$kec,$kel,$tps){
		$this->datatables->select('a.id as id, b.name as prov, c.name as kab, d.name as kec, e.name as kel, f.tps as tps, a.cp as nama, IFNULL(jml_prospek, 0) as jml');
		$this->datatables->from('m_team a');
		$this->datatables->join('m_provinces b', 'a.id_provinces = b.id', 'left');
		$this->datatables->join('m_regencies c', 'a.id_regency = c.id', 'left');
		$this->datatables->join('m_districts d', 'a.id_district = d.id', 'left');
		$this->datatables->join('m_villages e', 'a.id_village = e.id', 'left');
		$this->datatables->join('m_team_has_tps f', 'a.id = f.id_team', 'left');

		if($prov != 'all'){
			$this->datatables->where('id_provinces',$prov);
		}
		if($kab != 'all'){
			$this->datatables->where('id_regency',$kab);
		}
		if($kec != 'all'){
			$this->datatables->where('id_district',$kec);
		}
		if($kel != 'all'){
			$this->datatables->where('id_village',$kel);
		}
		if($tps != 'all'){
			$this->datatables->where('f.tps',$tps);
		}

		echo $this->datatables->generate();

	}
	

}
