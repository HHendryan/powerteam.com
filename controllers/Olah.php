<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Olah extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Olah_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){

	}

	function ganda_by_nik(){
		$this->load->view('ganda_by_nik');
	}
	function get_dpt_ganda_by_nik($nik){
		$data['data']=$this->Olah_model->dpt_by_nik($nik);
		echo json_encode($data);
	}

	function ganda_by_nama(){
		$this->load->view('ganda_by_nama');
	}
	function get_dpt_ganda_by_nama($nama,$tanggal_lahir){
		$data['data']=$this->Olah_model->dpt_by_nama($nama,$tanggal_lahir);
		echo json_encode($data);
	}


}
