<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tim extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Tim_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		
		$data['user'] = $this->Auth_model->get_data();
		$data['jalur'] = $this->db->get('jalur')->result();
		$this->load->view('template/header',$data,TRUE);
	}

	//INDEX FUNCTION
	function index(){
		$user = $this->Auth_model->get_data();
		$data['jalur'] = $this->db->get('jalur')->result();
		//KORCAM
		if (isset($_GET['korwil'])){
			$data['id'] = $_GET['korwil'];
			$data['nama'] = $this->db->get_where('tim_view', array('id_user' => $user->id,'tim_id' => $_GET['korwil']))->row('nama');
			$this->load->view('tim_korcam',$data);
			return;
		}
		//KORLAP
		if (isset($_GET['korcam'])){
			$data['id'] = $_GET['korcam'];
			$data['nama'] = $this->db->get_where('tim_view', array('id_user' => $user->id,'tim_id' => $_GET['korcam']))->row('nama');
			$this->load->view('tim_korlap',$data);
			return;
		}
		//PEMILIH
		if (isset($_GET['korlap'])){
			$data['id'] = $_GET['korlap'];
			$data['nama'] = $this->db->get_where('tim_view', array('id_user' => $user->id,'tim_id' => $_GET['korlap']))->row('nama');
			$this->load->view('tim_pemilih',$data);
			return;
		}
		$this->load->view('tim',$data);
	}

	//GENERAL FUNCTION
	function cari_dpt_for_tim($nik){
		$data = $this->Tim_model->get_data_by_nik($nik);
		echo json_encode($data);
	}

	function get_tim($id){
		$data = $this->db->get_where('tim', array('id' => $id))->result();
		echo json_encode($data);
	}

	function update_tim(){
		$id=$this->input->post('id');
		$this->Tim_model->update_tim($id,$this->input->post());
	}

	// KORWIL FUNCTION
	function get_data_korwil(){
		$data['data']=$this->Tim_model->get_data_korwil();
		echo json_encode($data);
	}

	function input_korwil(){
		$this->Tim_model->add_korwil($this->input->post());
	}
	
	function delete_korwil($id){
		$this->Tim_model->delete_korwil($id);
	}

	// KORCAM FUNCTION
	function korcam($id_korwil){
		$user = $this->Auth_model->get_data();
		$data['id'] = $id_korwil;
		$data['nama'] = $this->db->get_where('tim_view', array('id_user' => $user->id,'tim_id' => $id_korwil))->row('nama');
		$this->load->view('tim_korcam',$data);	
	}
	function get_data_korcam($id){
		$data['data']=$this->Tim_model->get_data_korcam($id);
		echo json_encode($data);
	}

	function input_korcam(){
		$this->Tim_model->add_korcam($this->input->post());
	}

	function delete_korcam($id){
		$this->Tim_model->delete_korcam($id);
	}

	// KORLAP FUNCTION
	function korlap($id_korcam){
		$user = $this->Auth_model->get_data();
		$data['id'] = $id_korcam;
		$data['nama'] = $this->db->get_where('tim_view', array('id_user' => $user->id,'tim_id' => $id_korcam))->row('nama');
		$this->load->view('tim_korlap',$data);
	}
	function get_data_korlap($id){
		$data['data']=$this->Tim_model->get_data_korlap($id);
		echo json_encode($data);
	}

	function input_korlap(){
		$this->Tim_model->add_korlap($this->input->post());
	}

	function delete_korlap($id){
		$this->Tim_model->delete_korlap($id);
	}
	// PEMILIH FUNCTION
	function pemilih($id_korlap){
		$user = $this->Auth_model->get_data();
		$data['id'] =$id_korlap;
		$data['nama'] = $this->db->get_where('tim_view', array('id_user' => $user->id,'tim_id' => $id_korlap))->row('nama');
		$this->load->view('tim_pemilih',$data);
	}
	function get_data_pemilih($id){
		$data['data']=$this->Tim_model->get_data_pemilih($id);
		echo json_encode($data);
	}

	function input_pemilih(){
		$this->Tim_model->add_pemilih($this->input->post());
	}
	function delete_pemilih($id){
		$this->Tim_model->delete_pemilih($id);
	}
	function test(){
		$time_pre = microtime(true);
		$data=$this->Tim_model->get_data_tim_korcam(1);
		$time_post = microtime(true);
		$exec_time = $time_post - $time_pre;
		var_dump($data);
	}
}
