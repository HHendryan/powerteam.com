<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DPS extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Data_model');
		$this->load->model('Auth_model');
		$this->load->model('Tangible_model');
		//$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		
		$data['provinsi'] = $this->db->get('m_provinces')->result();		
		$this->load->view('dps_view',$data);
		
	}
	function daftar(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();		
		$this->load->view('dps_list',$data);
	}

	
	function upload_biasa(){
		$provinsi = $this->input->post('provinsi');
		$table = $this->db->get_where('m_provinces', array('id' => $provinsi))->row('table_dpt');

		$fileName = $this->input->post('file', TRUE);
        // $campaign = $this->db->get_where('m_campaign',array('id' => $this->input->post('id_campaign')))->row();
        $path = realpath(APPPATH . '../excel_dpt/');
        $config['upload_path'] = $path; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 150000;
        $cek = '';
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
          
        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
        }else{
            $upload_data = $this->upload->data();
            $this->load->library('excel');
            $file = 'excel_dpt/'.$upload_data['file_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $jum_data = sizeof($sheetData);
            $objPHPExcel->getActiveSheet()->getStyle('B2:B'.$jum_data)->getNumberFormat()->setFormatCode('0');
            $objPHPExcel->getActiveSheet()->getStyle('C2:C'.$jum_data)->getNumberFormat()->setFormatCode('0');
            $header = $sheetData[0];
            $cek = 0;
			// var_dump(sizeof($sheetData));
            for ($i=1; $i < sizeof($sheetData); $i++) { 
                for ($j=0; $j < sizeof($sheetData[$i]) ; $j++) { 
                    if($header[$j] == 'no_kk'){
                        if($sheetData[$i][$j] == ''){
                            $cek = 1;
                            break;
                        }
                    }
                    $data['provinsi'] = $this->input->post('provinsi');
                    $data['kabupaten'] = $this->input->post('kabupaten');
                    $data['kecamatan'] = $this->input->post('kecamatan');
                    $data['kelurahan'] = $this->input->post('kelurahan');
					$data['tps'] = $this->input->post('tps');
                    $data['no_kk'] = $sheetData[$i][1];
                    $data['nik'] = $sheetData[$i][2];
                    $data['nama'] = $sheetData[$i][3];
                    $data['tempat_lahir'] = $sheetData[$i][4];
                    $date = $sheetData[$i][5];
                    $tanggal_array = explode("|",$date);
                    $tanggal=(string)$tanggal_array[0];
                    $bulan=(string)$tanggal_array[1];
                    $tahun=(string)$tanggal_array[2];
                    $new_date = $tahun.'-'.$bulan.'-'.$tanggal;
                    $data['tanggal_lahir'] = $new_date;
                    $data['usia'] = $sheetData[$i][6];
                    $data['status_kawin'] = $sheetData[$i][7];
                    $data['jns_kelamin'] = $sheetData[$i][8];
                    $data['alamat'] = $sheetData[$i][9];
                    $data['no_hp'] = $sheetData[$i][10];
                    $data['keterangan'] = $sheetData[$i][11];
                    $data['agama'] = $sheetData[$i][12];
                    $data['suku'] = $sheetData[$i][13];
					$data['pekerjaan'] = $sheetData[$i][14];
					$data['unique_id'] = $this->input->post('unique');
					$data['date_registered'] = date('Y-m-d h:ï:s');
				}	
					$this->db->insert($table, $data);
					
					 if($cek == 1){
						break;
					}
            }
            $error = 'SUKSES';
        }
        echo json_encode($error);
		// redirect('DPS');
	}
	function upload_gabung(){
		$provinsi = $this->input->post('provinsi');
		$table = $this->db->get_where('m_provinces', array('id' => $provinsi))->row('table_dpt');

		$fileName = $this->input->post('file', TRUE);
        // $campaign = $this->db->get_where('m_campaign',array('id' => $this->input->post('id_campaign')))->row();
        $path = realpath(APPPATH . '../excel_dpt');
        $config['upload_path'] = $path; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 150000;
        $cek = '';
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
          
        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
        }else{
            $upload_data = $this->upload->data();
            $this->load->library('excel');
            $file = 'excel_dpt/'.$upload_data['file_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $jum_data = sizeof($sheetData);
            $objPHPExcel->getActiveSheet()->getStyle('B2:B'.$jum_data)->getNumberFormat()->setFormatCode('0');
            $objPHPExcel->getActiveSheet()->getStyle('C2:C'.$jum_data)->getNumberFormat()->setFormatCode('0');
            $header = $sheetData[0];
            $cek = 0;
			// var_dump(sizeof($sheetData));
            for ($i=1; $i < sizeof($sheetData); $i++) { 
                for ($j=0; $j < sizeof($sheetData[$i]) ; $j++) { 
                    if($header[$j] == 'no_kk'){
                        if($sheetData[$i][$j] == ''){
                            $cek = 1;
                            break;
                        }
                    }
                    $data['provinsi'] = $this->input->post('provinsi');
                    $data['kabupaten'] = $this->input->post('kabupaten');
                    $data['kecamatan'] = $this->input->post('kecamatan');
                    $data['kelurahan'] = $this->input->post('kelurahan');
					$data['tps'] = $this->input->post('tps');
                    $data['no_kk'] = $sheetData[$i][1];
                    $data['nik'] = $sheetData[$i][2];
                    $data['nama'] = $sheetData[$i][3];
                    $data['tempat_lahir'] = $sheetData[$i][4];
                    $date = $sheetData[$i][5];
                    $tanggal_array = explode("|",$date);
                    $tanggal=(string)$tanggal_array[0];
                    $bulan=(string)$tanggal_array[1];
                    $tahun=(string)$tanggal_array[2];
                    $new_date = $tahun.'-'.$bulan.'-'.$tanggal;
                    $data['tanggal_lahir'] = $new_date;
                    $data['usia'] = $sheetData[$i][6];
                    $data['status_kawin'] = $sheetData[$i][7];
                    $data['jns_kelamin'] = $sheetData[$i][8];
                    $alamat = $sheetData[$i][9];
                    $rt = $sheetData[$i][10];
                    $rw = $sheetData[$i][11];
                    $alamat_lengkap = $alamat.' RT '.$rt.' RW '.$rw;
                    $data['no_hp'] = $sheetData[$i][12];
                    $data['keterangan'] = $sheetData[$i][13];
                    $data['agama'] = $sheetData[$i][14];
                    $data['suku'] = $sheetData[$i][15];
					$data['pekerjaan'] = $sheetData[$i][16];
					$data['unique_id'] = $this->input->post('unique');
					$data['date_registered'] = date('Y-m-d h:ï:s');
				}	
					$this->db->insert($table, $data);
					
					 if($cek == 1){
						break;
					}
            }

        }
        echo json_encode($i-1);
		// redirect('DPS');
	}

	function cek_unique($unique_id,$id_prov){
		$table = $this->db->get_where('m_provinces', array('id' => $id_prov))->row('table_dpt');
		$this->check_and_create($table);
		$input['exist_table'] = 1;
		$this->db->where('id',$id_prov);
		$this->db->update('m_provinces',$input);
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->get_where($table, array('unique_id' => $unique_id))->row();
    	if($data == NULL){
    		echo json_encode(1);
    	}else{
    		echo json_encode(0);
    	}
    }

    function check_tps($id_prov,$id_kab,$id_kec,$id_kel,$tps){
		$data = $this->db->query("SELECT id from m_tps 
			where provinsi='$id_prov' and kabupaten='$id_kab' and kecamatan='$id_kec' and kelurahan='$id_kel' and tps='$tps'")->row();
		if($data){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function get_option($tipe,$id){
		if($tipe == 'kabupaten'){
			$table = 'm_regencies';
			$parent = 'province_id';
		}else if($tipe == 'kecamatan'){
			$table = 'm_districts';
			$parent = 'regency_id';
		}else if($tipe == 'kelurahan'){
			$table = 'm_villages';
			$parent = 'district_id';
		}
		
		$string = '<option disabled selected value>PILIH</option>';
		if($tipe == 'tps'){
			$data = $this->db->get_where('m_tps', array('kelurahan' => $id))->result();
			foreach ($data as $key => $value) {
				$string .= "<option value='$value->tps'>$value->tps</option>";
			}
		}else{
			$data = $this->db->get_where($table, array($parent => $id))->result();	
			foreach ($data as $key => $value) {
				$string .= "<option value='$value->id'>$value->name</option>";
			}
		}
		echo ($string);
	}

	function load_tps($id_prov,$id_kab,$id_kec,$id_kel){
		$this->datatables->select('id,provinsi,kabupaten,kecamatan,kelurahan,tps');
		$this->datatables->from('m_tps');
		$this->datatables->where('provinsi', $id_prov);
		$this->datatables->where('kabupaten', $id_kab);
		$this->datatables->where('kecamatan', $id_kec);
		$this->datatables->where('kelurahan', $id_kel);
		$this->datatables->add_column('button', '<button data-prov=$1 data-kab=$2 data-kec=$3 data-kel=$4 data-tps=$5 class="btn btn-success-outline ks-rounded btn-upload-biasa">Upload Versi 1</button>&nbsp;<button data-prov=$1 data-kab=$2 data-kec=$3 data-kel=$4 data-tps=$5 class="btn btn-success-outline ks-rounded btn-upload-gabung">Upload Versi 2</button>&nbsp;<button data-id=$6 class="btn btn-danger-outline ks-rounded btn-hapus-tps">HAPUS</button>&nbsp;<button data-id=$6 class="btn btn-danger-outline ks-rounded btn-hapus-dps">HAPUS DPS</button>', 'provinsi,kabupaten,kecamatan,kelurahan,tps,id');
		echo $this->datatables->generate();
	}

	function load_dps($id_prov,$id_kab,$id_kec,$id_kel,$tps){
		$table = $this->db->get_where("m_provinces", array('id' => $id_prov))->row('table_dpt');

		$this->datatables->select('id,no_kk,nik,nama,tempat_lahir,tanggal_lahir');
		$this->datatables->from($table);
		$this->datatables->where('provinsi', $id_prov);
		$this->datatables->where('kabupaten', $id_kab);
		$this->datatables->where('kecamatan', $id_kec);
		$this->datatables->where('kelurahan', $id_kel);
		$this->datatables->where('tps', $tps);
		echo $this->datatables->generate();
	}

	function input_tps(){
		$data = $this->input->post();
		$check = $this->check_tps($data['provinsi'],$data['kabupaten'],$data['kecamatan'],$data['kelurahan'],$data['tps']);
		if($check == FALSE){
			$this->db->insert('m_tps',$data);
			echo json_encode($data);
		}else{
			echo json_encode('N');
		}
		
	}

	function delete($id){
		$this->db->delete('m_tps', array('id' => $id));
		echo json_encode('SUKSES');
	}

	function delete_dps($id){
		$data = $this->db->get_where('m_tps', array('id' => $id))->row();
		$table = $this->db->get_where('m_provinces', array('id' => $data->provinsi))->row('table_dpt');
		$this->db->where('provinsi',$data->provinsi);
		$this->db->where('kabupaten',$data->kabupaten);
		$this->db->where('kecamatan',$data->kecamatan);
		$this->db->where('kelurahan',$data->kelurahan);
		$this->db->where('tps',$data->tps);
		if($this->db->delete($table)){
			$jumlah = $this->db->affected_rows(); 
			echo json_encode($jumlah);
		}else{
			echo json_encode('N');
		}
	}

	function check_and_create($table){
		$val = mysql_query('select 1 from '.$table.' LIMIT 1');
		if($val !== FALSE){
		   //DO SOMETHING! IT EXISTS!
		}else{
			$sql = "CREATE TABLE IF NOT EXISTS `".$table."` (
			      `id` int(11) NOT NULL AUTO_INCREMENT,
				  `provinsi` varchar(3) NOT NULL DEFAULT '0',
				  `kabupaten` varchar(5) NOT NULL DEFAULT '0',
				  `kecamatan` varchar(8) NOT NULL DEFAULT '0',
				  `kelurahan` varchar(11) NOT NULL DEFAULT '0',
				  `tps` varchar(3) NOT NULL DEFAULT '0',
				  `no_kk` varchar(30) DEFAULT NULL,
				  `nik` varchar(25) DEFAULT NULL,
				  `nama` varchar(255) DEFAULT NULL,
				  `tempat_lahir` varchar(255) DEFAULT NULL,
				  `tanggal_lahir` date DEFAULT NULL,
				  `usia` varchar(2) DEFAULT NULL,
				  `status_kawin` varchar(2) DEFAULT NULL,
				  `jns_kelamin` varchar(2) DEFAULT NULL,
				  `alamat` text,
				  `no_hp` varchar(14) DEFAULT NULL,
				  `keterangan` text,
				  `agama` varchar(30) DEFAULT NULL,
				  `suku` varchar(30) DEFAULT NULL,
				  `pekerjaan` varchar(50) DEFAULT NULL,
				  `unique_id` varchar(50) DEFAULT NULL,
				  `date_registered` datetime DEFAULT NULL,
				  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`)
				) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;";
		  	$query = $this->db->query($sql);
		}
	  	
	}  

}
