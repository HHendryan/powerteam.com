<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form2 extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		// $this->load->model('Form_model');
		$this->load->model('Auth_model');
		$this->load->model('Tangible_model');

		$this->Auth_model->can_access_by_login();
        $data = $this->session->userdata('guraklih');
        $this->load->view('template/header',$data,TRUE);
	}

	function index(){
	}

	function get_kabupaten(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kabupaten($id);
	}

	function get_kecamatan(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kecamatan($id);
	}	
	
	function get_kelurahan(){
		$id = $this->input->post('id');
		echo $this->Tangible_model->get_kelurahan($id);
	}	
		
	function tangible(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$data['pertokohan'] = $this->Tangible_model->get_pertokohan();
		$this->load->view('tangible',$data);
	}

	function get_data_tangible(){
		$this->datatables->select('id_intangible,name,address,phone,gender');
		$this->datatables->from('intangible');
		$this->datatables->where('type', 'tangible');
		$this->datatables->where('is_deleted', '0');
		$this->datatables->add_column('button', '<button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit" style="
    height: 26px;">View & Edit</button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete" style="
    height: 26px;">Delete</button>', 'id_intangible');
		echo $this->datatables->generate();
	}

	function intangible(){
		$data['provinsi'] = $this->db->get('m_provinces')->result();
		$data['pertokohan'] = $this->Tangible_model->get_pertokohan();
		$this->load->view('intangible',$data);
	}

	function get_data_intangible(){
		$this->datatables->select('id_intangible,name,address,phone,gender');
		$this->datatables->from('intangible');
		$this->datatables->where('type', 'intangible');
		$this->datatables->where('is_deleted', '0');
		$this->datatables->add_column('button', '<button type="button" data-id=$1 class="btn btn-success btn-sm btn-edit" style="
    height: 26px;">View & Edit</button>&nbsp;<button type="button" data-id=$1 class="btn btn-danger btn-sm btn-delete" style="
    height: 26px;">Delete</button>', 'id_intangible');
		echo $this->datatables->generate();
	}

	 //CRUD
	 
	 function add_tangible(){
		$this->Tangible_model->add_tangible($this->input->post());
	}
	
	function get_tangible($id){
		$data = $this->db->get_where('intangible',array('id_intangible' => $id ))->row();
		echo json_encode($data);
	}
	
	 function add_intangible(){
		$this->Tangible_model->add_intangible($this->input->post());
	}
	
	

    function delete($table,$id){
        $this->db->delete($table, array('id' => $id));
        echo json_encode('delete sukses');
    }
}
