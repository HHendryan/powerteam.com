<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Data_model');
		$this->load->model('Auth_model');
		$this->load->model('Tangible_model');
		$this->load->model('Guraklih_model');

		$this->Auth_model->can_access_by_login();
        $data = $this->session->userdata('guraklih');
        $this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$this->Auth_model->can_access_by_level(3,'data');
		
		$this->load->view('data');
	}

	function kabupaten($id_prov){
		$this->Auth_model->can_access_by_level(2,'data');
		$this->Auth_model->can_access_by_detail('kab',$id_prov);

		$data['id'] = $id_prov;
		$data['sub_header'] = $this->db->get_where('provinsi',array('id' => $id_prov ))->row('provinsi');
		$this->load->view('data_kab',$data);
	}

	function kecamatan($id_kab){
		$this->Auth_model->can_access_by_detail('kec',$id_kab);

		$data['id'] = $id_kab;
		$data['sub_header'] = $this->db->get_where('kabupaten',array('id' => $id_kab ))->row('kabupaten');
		$this->load->view('data_kec',$data);
	}

	function kelurahan($id_kec){
		$this->Auth_model->can_access_by_detail('kel',$id_kec);
		$data['id'] = $id_kec;
		$data['sub_header'] = $this->db->get_where('kecamatan',array('id' => $id_kec ))->row('kecamatan');
		$this->load->view('data_kel',$data);
	}

	function tps($id_kel){
		$data['id'] = $id_kel;
		$data['sub_header'] = $this->db->get_where('kelurahan',array('id' => $id_kel ))->row('kelurahan');
		$this->load->view('data_tps',$data);
	}

	function pemilih($id_kel,$tps){
		$data['id_kel'] = $id_kel;
		$data['tps'] = $tps;
		$data['sub_header'] = $this->db->get_where('kelurahan',array('id' => $id_kel ))->row('kelurahan');
		$this->load->view('data_pemilih',$data);
	}

	function get_data_prov($pp){
		$data['pp']=$pp;
		$data['data']=$this->Data_model->get_data_prov($pp);
		echo json_encode($data);
	}
	function get_data_kab($id,$pp){
		$data['data']=$this->Data_model->get_data_kab($id,$pp);
		echo json_encode($data);
	}
	function get_data_kec($id,$pp){
		$data['data']=$this->Data_model->get_data_kec($id,$pp);
		echo json_encode($data);
	}
	function get_data_kel($id,$pp){
		$data['data']=$this->Data_model->get_data_kel($id,$pp);
		echo json_encode($data);
	}
	function get_data_tps($id,$pp){
		$data['data']=$this->Data_model->get_data_tps($id,$pp);
		echo json_encode($data);
	}
	function get_data_pemilih($id_kel,$tps){
		$data['data']=$this->Data_model->get_data_pemilih($id_kel,$tps);
		echo json_encode($data);
	}

	function get_data_pemilih_details($id){
		$data = $this->db->get_where('dpt_sumatera_selatan', array('id' => $id ))->result();
		echo json_encode($data);
	}

	function update_dpt(){
		$this->Data_model->update_data($this->input->post(),'dpt_sumatera_selatan');
	}
	function test(){
		$time_pre = microtime(true);
		$data = $this->Auth_model->get_data();
		$dapil = explode(";",$data->dapil);
			$last = end($dapil);
			$query="SELECT * FROM kecamatan WHERE ";
			foreach ($dapil as $value) {
				if($value==$last){
					$query.="id=$value";
				}else{
					$query.="id=$value OR ";
				}
				
			}
		$time_post = microtime(true);
		$exec_time = $time_post - $time_pre;
		var_dump($exec_time);
		var_dump($query);
	}

	//Influence
	function get_data_influence($tipe,$condong,$kategori){
		$data['data']=$this->Data_model->get_data_influence($tipe,$condong,$kategori);
		echo json_encode($data);
	}

	function get_data_influence_2($tipe,$condong,$kategori){
		$data['data']=$this->Data_model->get_data_influence_2($tipe,$condong,$kategori);
		echo json_encode($data);
	}

	function influence_result(){
		//$this->Auth_model->can_access_by_level(3,'data');
		$data['parpol'] = $this->db->get('m_parpol')->result();
		$data['pertokohan'] = $this->db->get('m_pertokohan')->result();
		$data['tangible'] = $this->db->query("SELECT COUNT(*) AS jml FROM intangible where is_deleted = '0' and type='tangible'")->row('jml');
		$data['intangible'] = $this->db->query("SELECT COUNT(*) AS jml FROM intangible where is_deleted = '0' and type='intangible'")->row('jml');
		$data['kecondongan'] = $this->db->query("SELECT a.inclination, b.alias, count(*) as jml from intangible a left join m_parpol b on a.inclination=b.id_parpol group by inclination;")->result();
		$data['jum_tangible'] = $this->db->query("SELECT a.id_pertokohan, b.nama, count(*) as jml from intangible a left join m_pertokohan b on a.id_pertokohan=b.id_pertokohan where b.type='1' group by a.id_pertokohan")->result();
		$data['jum_intangible'] = $this->db->query("SELECT a.id_pertokohan, b.nama, count(*) as jml from intangible a left join m_pertokohan b on a.id_pertokohan=b.id_pertokohan where b.type='0' group by a.id_pertokohan")->result();
		$this->load->view('influence', $data);
	}


	function get_detail_influencer($id_prov,$tipe,$condong,$kategori){
		$data['data'] = $this->Data_model->get_detail_influencer($id_prov,$tipe,$condong,$kategori);
		echo json_encode($data);

	}
	function influence_kab($id_prov){
		$data['id'] = $id_prov;
		$data['sub_header'] = $this->db->get_where('provinsi',array('id' => $id_prov ))->row('provinsi');
		$data['parpol'] = $this->Tangible_model->get_parpol();
		$data['pertokohan'] = $this->Tangible_model->get_pertokohan();
		$data['tangible'] = $this->db->query("SELECT COUNT(*) AS jml FROM intangible where is_deleted = '0' and id_provinces='$id_prov' and type='tangible'")->row('jml');
		$data['intangible'] = $this->db->query("SELECT COUNT(*) AS jml FROM intangible where is_deleted = '0' and id_provinces='$id_prov' and type='intangible'")->row('jml');
		$this->load->view('data_kab',$data);
	}

	function change_date($date){
		$date = explode('/', $date);
		$date_baru = $date[2].'-'.$date[0].'-'.$date[1];
		return $date_baru;
	}
	function upload_tangible($type){
        $fileName = $this->input->post('file', TRUE);
        // $campaign = $this->db->get_where('m_campaign',array('id' => $this->input->post('id_campaign')))->row();
        $path = realpath(APPPATH . '../excel_dpt/');
        $config['upload_path'] = $path; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 150000;
        $cek = '';
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
          
        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
        }else{
            $upload_data = $this->upload->data();
            $this->load->library('excel');
            $file = 'excel_dpt/'.$upload_data['file_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $header = $sheetData[0];
            $cek = 0;
            for ($i=1; $i < sizeof($sheetData); $i++) { 
                for ($j=0; $j < sizeof($sheetData[$i]) ; $j++) { 
                    if($header[$j] == 'nama'){
                        if($sheetData[$i][$j] == ''){
                            $cek = 1;
                            break;
                        }
                    }
                    $data['id_intangible'] = $this->Tangible_model->create_id();
                    $data['name'] = $sheetData[$i][0];
                    $data['address'] = $sheetData[$i][1];
                    if($sheetData[$i][2] != ''){
                    	$phone = '0'.$sheetData[$i][2];
                    }else{
                    	$phone = $sheetData[$i][2];
                    }
                    $data['phone'] = $sheetData[$i][2];
                    if($sheetData[$i][3] == 'L'){
                    	$gender = 'laki-laki';
                    }elseif($sheetData[$i][3] == 'P'){
                    	$gender = 'perempuan';
                    }else{
                        $gender = NULL;
                    }
                    $data['gender'] = $gender;
                    $data['nik'] = $sheetData[$i][4];
                    $data['occupation'] = $sheetData[$i][5];
                    $data['tempat_lahir'] = $sheetData[$i][6];
                    // if($sheetData[$i][7] != ''){
                    // 	$date = $this->change_date($sheetData[$i][7]);
                    // }else{
                    // 	$date = $sheetData[$i][7];
                    // }
                    $data['tanggal_lahir'] = $sheetData[$i][7];
                    $data['last_education'] = $sheetData[$i][8];
                    $data['institution'] = $sheetData[$i][9];
                    $data['jabatan'] = $sheetData[$i][10];
                    $data['organisation'] = $sheetData[$i][11];
                    $data['specific_history'] = $sheetData[$i][12];
                    $data['id_provinces'] = $this->db->get_where('m_provinces', array('name' => $sheetData[$i][13]))->row('id');
                    $data['id_city'] = $this->db->get_where('m_regencies', array('name' => $sheetData[$i][14]))->row('id');
                    $data['id_districts'] = $this->db->get_where('m_districts', array('name' => $sheetData[$i][15]))->row('id');
                    $data['id_village'] = $this->db->get_where('m_villages', array('name' => $sheetData[$i][16]))->row('id');
                    $data['id_pertokohan'] = $this->db->get_where('m_pertokohan', array('nama' => $sheetData[$i][17]))->row('id_pertokohan');
                    $data['influence'] = $sheetData[$i][18];
                    $data['unique_id'] = $this->input->post('unique');
                    if($sheetData[$i][19] == 'NETRAL'){
                    	$condong = 0;
                    }else{
                    	$condong = $this->db->get_where('m_parpol', array('alias' => $sheetData[$i][19]))->row('id_parpol');
                    }
                    $data['inclination'] = $condong;
                    $data['status'] = $sheetData[$i][20];
                    $data['informasi_tambahan'] = $sheetData[$i][21];
                    $data['email'] = $sheetData[$i][22];
                    $data['facebook'] = $sheetData[$i][23];
                    $data['twitter'] = $sheetData[$i][24];
                    $data['type'] = $type;
                }
                if($cek == 1){
                    break;
                }
                $this->db->insert('intangible', $data);
                $dats['id_surveyor'] = 771;
                $dats['id_intangible'] = $data['id_intangible'];
                $this->db->insert('surveyor_has_intangible', $dats);
            }

        }
        echo json_encode($i-1);
    }

    function cek_unique($unique_id){
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->get_where('intangible', array('unique_id' => $unique_id))->row();
    	if($data == NULL){
    		echo json_encode(1);
    	}else{
    		echo json_encode(0);
    	}
    }

    function check_jumlah($unique_id){
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->query("SELECT count(*) as jml FROM intangible where unique_id='$unique_id'")->row('jml');
    	if($data == '0'){
    		echo json_encode(0);
    	}else{
    		echo json_encode('JUMLAH DATA : '.$data);
    	}
    }

    function hapus_data($unique_id){
    	$id = urldecode($unique_id);
    	$this->db->where('unique_id', $id);
    	if($this->db->delete('intangible')){
    		echo json_encode(1);
    	}else{
    		echo json_encode(0);
    	}
    }

    /*
    * Check unique code (Hari Hendryan)
    */
    function cek_unique_id($unique_id){
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->get_where('m_team', array('unique_id' => $unique_id))->row();
    	if($data == NULL){
    		echo json_encode(1);
    	}else{
    		echo json_encode(0);
    	}
    }

    /*
    * Check count of data by unique code (Hari Hendryan)
    */
    function check_jumlah_guraklih($unique_id){
    	$unique_id = urldecode($unique_id);
    	$data = $this->db->query("SELECT count(*) as jml FROM m_team where unique_id='$unique_id'")->row('jml');
    	if($data == '0'){
    		echo json_encode(0);
    	}else{
    		echo json_encode('JUMLAH DATA : '.$data);
    	}
    }

    /*
    * Check delete data by unique code (Hari Hendryan)
    */
    function hapus_data_guraklih($unique_id){
    	$id = urldecode($unique_id);
    	$this->db->where('unique_id', $id);
    	if($this->db->delete('m_team')){
    		echo json_encode(1);
    	} else {
    		echo json_encode(0);
    	}
    }

	/*
    * Function for upload guraklih data via excel (Hari Hendryan)
    */
    function upload_guraklih(){
        $fileName = $this->input->post('file', TRUE);
        $path = realpath(APPPATH . '../excel_dpt/');
        $config['upload_path'] = $path; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 150000;
        $cek = '';
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
          
        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
        }else{
            $upload_data = $this->upload->data();
            $this->load->library('excel');
            $file = 'excel_dpt/'.$upload_data['file_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $header = $sheetData[0];
            $cek = 0;
            for ($i=1; $i < sizeof($sheetData); $i++) { 
                for ($j=0; $j < sizeof($sheetData[$i]) ; $j++) { 
                    if($header[$j] == 'nama'){
                        if($sheetData[$i][$j] == ''){
                            $cek = 1;
                            break;
                        }
                    }
                    $data['cp'] = $sheetData[$i][0];
                    $data['address'] = $sheetData[$i][1];
                    $data['nik'] = $sheetData[$i][2];
                    if($sheetData[$i][3] != ''){
                    	$phone = '0'.$sheetData[$i][3];
                    }else{
                    	$phone = $sheetData[$i][3];
                    }
                    $data['phone'] = $sheetData[$i][3];
                    $data['type'] = $sheetData[$i][4];
                    $data['id_provinces'] = $this->db->get_where('m_provinces', array('name' => $sheetData[$i][5]))->row('id');
                    $data['id_regency'] = $this->db->get_where('m_regencies', array('name' => $sheetData[$i][6]))->row('id');
                    $data['id_district'] = $this->db->get_where('m_districts', array('name' => $sheetData[$i][7]))->row('id');
                    $data['id_village'] = $this->db->get_where('m_villages', array('name' => $sheetData[$i][8]))->row('id');
                    $data['tps'] = $sheetData[$i][9];
                    $data['nama_team'] = 'guraklih';
                    $data['id_tenant'] = 1;
                    $data['unique_id'] = $this->input->post('unique');
                }
                if($cek == 1){
                    break;
                }
                $tps = $data['tps'];
				unset($data['tps']);

                $this->db->insert('m_team', $data);
                $dats['id_team'] = $this->Guraklih_model->create_id();
                $dats['tps'] = $tps;
				$this->db->insert('m_team_has_tps', $dats);
            }

        }
        echo json_encode($i-1);
    }

    /*
    * Function for show result of guraklih (Hari Hendryan)
    */
    function guraklih_result(){
        $data['guraklih'] = $this->Guraklih_model->get_target_guraklih('ALL');
        $this->load->view('guraklih_result', $data);
    }

    /*
    * Function for get guraklih data summary
    */
    function get_data_guraklih($tipe){
        $data['data']=$this->Guraklih_model->get_data_guraklih($tipe);
        echo json_encode($data);
    }

    /*
    * Function for get detail guraklih data (Hari Hendryan)
    */
    function get_detail_guraklih($id, $tipe){
        $data['data'] = $this->Guraklih_model->get_detail_guraklih($id, $tipe);
        echo json_encode($data);

    }

    /*
    * Function for get data print dpt(Hari Hendryan)
    */
    function guraklih_print_dpt(){
        $user = $this->session->userdata('guraklih');
        if($user['role'] == 'korprov'){
            $data['provinsi'] = $this->db->get_where('m_dpt', array('idProv' => $user['role_detail']))->row();
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('province_id' => $user['role_detail']))->result();
        }else if($user['role'] == 'korkab'){
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
            $data['provinsi'] = $this->db->get_where('m_dpt', array('idProv' => $data['kabupaten']->province_id))->row();
            $data['kecamatan'] = $this->db->get_where('m_districts', array('regency_id' => $user['role_detail']))->result();
        }else{
            $data['kecamatan'] = $this->db->get('m_districts')->result();
            $this->db1 = $this->load->database("db_dpt",true);
            $data['provinsi'] = $this->db->get('m_dpt')->result();
        }
        
        $this->load->view('graklih_print_dpt',$data);
    }

    /*
    * Function for get Kabupaten (Hari Hendryan)
    */
    function get_kabupaten(){
        $id = $this->input->post('id');
        echo $this->Data_model->get_kabupaten($id);
    }

    /*
    * Function for get Kecamatan (Hari Hendryan)
    */
    function get_kecamatan(){
        $id = $this->input->post('id');
        echo $this->Data_model->get_kecamatan($id);
    }

    /*
    * Function for get Kelurahan (Hari Hendryan)
    */
    function get_kelurahan(){
        $id = $this->input->post('id');
        echo $this->Data_model->get_kelurahan($id);
    }

    /*
    * Function for get tps (Hari Hendryan)
    */
    function get_tps(){
        $id = $this->input->post('id');
        echo $this->Guraklih_model->get_tps($id);
    }

    /*
    * Function for get dpt (Hari Hendryan)
    */
    function load_data_dpt($kel,$tps){
        $data['data'] = $this->Data_model->get_data_dpt($kel,$tps);

        echo json_encode($data);
    }

    function load_data_dpt_check($kel,$tps,$guraklih){
        $data['data'] = $this->Data_model->get_data_dpt_check($kel,$tps,$guraklih);

        echo json_encode($data);
    }

    /*
    * Function for check data prospek(Hari Hendryan)
    */
    function guraklih_check_prospek($idKel){
        $detail = $this->db->query("SELECT * FROM m_area WHERE idKel = $idKel");

        $data['area'] = $detail->row();
        $data['kecamatan'] = $this->db->get('m_districts')->result();
        $data['provinsi'] = $this->db->get('m_provinces')->result();
        $this->load->view('guraklih_check_prospek',$data);
    }

    function guraklih_uncheck_prospek($idKel){
        $detail = $this->db->query("SELECT * FROM m_area WHERE idKel = $idKel");

        $data['area'] = $detail->row();
        $data['kecamatan'] = $this->db->get('m_districts')->result();
        $data['provinsi'] = $this->db->get('m_provinces')->result();
        $this->load->view('guraklih_uncheck_prospek',$data);
    }

    /*
    * Function for check data area prospek(Hari Hendryan)
    */
    function guraklih_check_area_prospek(){
        $user = $this->session->userdata('guraklih');
        if($user['role'] == 'korprov'){
            $data['provinsi'] = $this->db->get_where('m_dpt', array('idProv' => $user['role_detail']))->row();
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('province_id' => $user['role_detail']))->result();
        }else if($user['role'] == 'korkab'){
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
            $data['provinsi'] = $this->db->get_where('m_dpt', array('idProv' => $data['kabupaten']->province_id))->row();
            $data['kecamatan'] = $this->db->get_where('m_districts', array('regency_id' => $user['role_detail']))->result();
        }else{
            $data['kecamatan'] = $this->db->get('m_districts')->result();
            $this->db1 = $this->load->database("db_dpt",true);
            $data['provinsi'] = $this->db->get('m_dpt')->result();
        }

        $this->load->view('guraklih_area_prospek',$data);
    }

    /*
    * Function for get dpt area (Hari Hendryan)
    */
    function load_data_dpt_area($kel){
        $result = $this->Data_model->get_data_dpt_area($kel);
        echo $result;
    }

    /*
    * Function for set flag dpt (Hari Hendryan)
    */
    function set_flag_dpt(){
        $data = $this->input->post();
        $guraklih_id = $data['guraklih_id'];
        $tps = $data['tps'];
        $input['checked'] = 1;
        $input['guraklihId'] = $data['guraklih_id'];
        $tabel_kab = 'kab_'.$data['id_kab'];
        $tabel_sum = $this->db->get_where('m_dpt', array('idProv' => $data['id_prov']))->row('tabel_sum');
        
        $this->db2 = $this->load->database("db_kab_final",true);
        foreach ($data['array'] as $key => $value) {
            $this->db2->where('id',$value);
            if(!$this->db2->update($tabel_kab,$input)){
                set_status_header(422);
                echo json_encode('Error Update Tahap 1, Data hanya ter-checklis sebagian');
                return;
            }
        }

        $input2['prospek'] = $this->db2->query("SELECT COUNT(1) as jml FROM $tabel_kab where checked=1 and tps='$tps'")->row('jml');
        $this->db->where('idKab',$data['id_kab']);
        $this->db->where('idKec',$data['id_kec']);
        $this->db->where('idKel',$data['id_kel']);
        $this->db->where('tps',$data['tps']);
        if(!$this->db->update($tabel_sum,$input2)){
            set_status_header(422);
            echo json_encode('Error Update Tahap 2 [Summary Prospek]');
            return;
        }

        $input3['jml_prospek'] = $this->db2->query("SELECT COUNT(1) as jml FROM $tabel_kab where guraklihId=$guraklih_id")->row('jml');
        $this->db->where('id',$guraklih_id);
        if(!$this->db->update('m_team',$input3)){
            set_status_header(422);
            echo json_encode('Error Update Tahap 3 [Summary by Guraklih]');
            return;
        }
        
        set_status_header(204);
    }

    function set_unflag_dpt(){
        $data = $this->input->post();
        $guraklih_id = $data['guraklih_id'];
        $tps = $data['tps'];
        $input['checked'] = 0;
        $input['guraklihId'] = NULL;
        $tabel_kab = 'kab_'.$data['id_kab'];
        $tabel_sum = $this->db->get_where('m_dpt', array('idProv' => $data['id_prov']))->row('tabel_sum');
        
        $this->db2 = $this->load->database("db_kab_final",true);
        foreach ($data['array'] as $key => $value) {
            $this->db2->where('id',$value);
            if(!$this->db2->update($tabel_kab,$input)){
                set_status_header(422);
                echo json_encode('Error Update Tahap 1, Data hanya ter-checklis sebagian');
                return;
            }
        }

        $input2['prospek'] = $this->db2->query("SELECT COUNT(1) as jml FROM $tabel_kab where checked=1 and tps='$tps'")->row('jml');
        $this->db->where('idKab',$data['id_kab']);
        $this->db->where('idKec',$data['id_kec']);
        $this->db->where('idKel',$data['id_kel']);
        $this->db->where('tps',$data['tps']);
        if(!$this->db->update($tabel_sum,$input2)){
            set_status_header(422);
            echo json_encode('Error Update Tahap 2 [Summary Prospek]');
            return;
        }

        $input3['jml_prospek'] = $this->db2->query("SELECT COUNT(1) as jml FROM $tabel_kab where guraklihId=$guraklih_id")->row('jml');
        $this->db->where('id',$guraklih_id);
        if(!$this->db->update('m_team',$input3)){
            set_status_header(422);
            echo json_encode('Error Update Tahap 3 [Summary by Guraklih]');
            return;
        }
        
        set_status_header(204);
    }

    /*
    * Function for set status guraklih in m_area (Hari Hendryan)
    */
    function set_status_guraklih(){
        $id = $this->input->post('id');
        $status =  $this->input->post('status');
        $result = $this->Data_model->set_status_guraklih($id, $status);
        echo $result;
    }

    /*
    * Function for clear flag dpt (Hari Hendryan)
    */
    function clear_flag_dpt(){
        $idKab = $this->input->post('id_kab');
        $idKec = $this->input->post('id_kec');
        $idKel = $this->input->post('id_kel');
        $tps = $this->input->post('tps');
        $id = $this->input->post('id');
        $result = $this->Data_model->clear_flag_dpt($idKab, $idKec, $idKel, $tps, $id);
        echo $result;
    }

    /*
    * Function for set finish flag dpt (Hari Hendryan)
    */
    function set_flag_dpt_sum(){
        $idProv = $this->input->post('id_prov');
        $idKab = $this->input->post('id_kab');
        $idKec = $this->input->post('id_kec');
        $idKel = $this->input->post('id_kel');
        $tps = $this->input->post('tps');
        $result = $this->Data_model->set_flag_dpt_sum($idProv, $idKab, $idKec, $idKel, $tps);
        echo $result;
    }

    function get_guraklih(){
        $data = $this->input->post();
        $this->db->select("a.id,a.cp");
        $this->db->from('m_team a');
        $this->db->join('m_team_has_tps b', 'b.id_team = a.id');
        $this->db->where('a.id_provinces',$data['prov']);
        $this->db->where('a.id_regency',$data['kab']);
        $this->db->where('a.id_district',$data['kec']);
        $this->db->where('a.id_village',$data['kel']);
        $this->db->where('b.tps',$data['tps']);
        $this->db->group_by("a.id"); 
        $result = $this->db->get()->result();
        $string = '<option disabled selected value>Pilih Guraklih</option>';
        foreach ($result as $key => $value) {
            $string .= '<option value="'.$value->id.'">'.$value->cp.'</option>';
        };
        echo $string;

    }

    /*
    * Function for get double dpt (Hari Hendryan)
    */
    function double_dpt(){
        $user = $this->session->userdata('guraklih');
        if($user['role'] == 'korprov'){
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('province_id' => $user['role_detail']))->result();
        }else if($user['role'] == 'korkab'){
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $data['kabupaten']->province_id))->row();
            $data['kecamatan'] = $this->db->get_where('m_districts', array('regency_id' => $user['role_detail']))->result();
        }else{
            $data['provinsi'] = $this->db->get('m_provinces')->result();
        }
        $this->load->view('doble_dpt',$data);
    }

    /*
    * Function for get double dpt (Hari Hendryan)
    */
    function double_dpt2(){
        $user = $this->session->userdata('guraklih');
        if($user['role'] == 'korprov'){
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('province_id' => $user['role_detail']))->result();
        }else if($user['role'] == 'korkab'){
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $data['kabupaten']->province_id))->row();
            $data['kecamatan'] = $this->db->get_where('m_districts', array('regency_id' => $user['role_detail']))->result();
        }else{
            $data['provinsi'] = $this->db->get('m_provinces')->result();
        }
        $this->load->view('double_dpt2',$data);
    }

    /*
    * Function for get double dpt (Hari Hendryan)
    */
    function double_dpt3(){
        $user = $this->session->userdata('guraklih');
        if($user['role'] == 'korprov'){
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $user['role_detail']))->row();
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('province_id' => $user['role_detail']))->result();
        }else if($user['role'] == 'korkab'){
            $data['kabupaten'] = $this->db->get_where('m_regencies', array('id' => $user['role_detail']))->row();
            $data['provinsi'] = $this->db->get_where('m_provinces', array('id' => $data['kabupaten']->province_id))->row();
            $data['kecamatan'] = $this->db->get_where('m_districts', array('regency_id' => $user['role_detail']))->result();
        }else{
            $data['provinsi'] = $this->db->get('m_provinces')->result();
        }
        $this->load->view('double_dpt3',$data);
    }

    /*
    * Function for get dpt double(Hari Hendryan)
    */
    function load_data_dpt_double($kab, $kec, $kel, $tps, $nik, $nama, $tgl){
        $data['data'] = $this->Data_model->get_data_dpt_double($kab, $kec, $kel,$tps, $nik, $nama, $tgl);
        echo json_encode($data);
    }

    function get_detail_dpt_by_nik($kab, $nik){
        $tabel = 'kab_'.$kab;
        $this->datatables->set_database('db_kab');
        $this->datatables->select("id, nik,nama,REPLACE(tanggal_lahir, '|', '-') as tanggal_lahir,alamat, 
            jns_kelamin, usia, (SELECT kecamatan from m_area where ".$tabel.".idKec = m_area.idKec LIMIT 1) as kecamatan, (SELECT kelurahan from m_area where ".$tabel.".idKel = m_area.idKel LIMIT 1) as kelurahan, tps");
        $this->datatables->from($tabel);
        $this->datatables->where("nik", $nik);
        echo $this->datatables->generate();
    }

    function get_detail_dpt_by_nama_tgl($kab, $nama, $tgl){
        $nama = utf8_decode(urldecode($nama));
        $tgl = utf8_decode(urldecode($tgl));
        $tabel = 'kab_'.$kab;
        $this->datatables->set_database('db_kab');
        $this->datatables->select("id, nik,nama,REPLACE(tanggal_lahir, '|', '-') as tanggal_lahir,alamat, 
            jns_kelamin, usia, (SELECT kecamatan from m_area where ".$tabel.".idKec = m_area.idKec LIMIT 1) as kecamatan, (SELECT kelurahan from m_area where ".$tabel.".idKel = m_area.idKel LIMIT 1) as kelurahan, tps");
        $this->datatables->from($tabel);
        $this->datatables->where("nama", $nama);
        $this->datatables->where("tanggal_lahir", $tgl);
        echo $this->datatables->generate();
    }

    function get_detail_dpt_by_nama_tgl_nik($kab, $nik, $nama, $tgl){
        $nama = utf8_decode(urldecode($nama));
        $tgl = utf8_decode(urldecode($tgl));
        $tabel = 'kab_'.$kab;
        $this->datatables->set_database('db_kab');
        $this->datatables->select("id, nik,nama,REPLACE(tanggal_lahir, '|', '-') as tanggal_lahir,alamat, 
            jns_kelamin, usia, (SELECT kecamatan from m_area where ".$tabel.".idKec = m_area.idKec LIMIT 1) as kecamatan, (SELECT kelurahan from m_area where ".$tabel.".idKel = m_area.idKel LIMIT 1) as kelurahan, tps");
        $this->datatables->from($tabel);
        $this->datatables->where("nama", $nama);
        $this->datatables->where("tanggal_lahir", $tgl);
        $this->datatables->where("nik", $nik);
        echo $this->datatables->generate();
    }
    
    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function dashboard_upload(){
        $data['provinsi'] = $this->db->get('m_provinces')->result();
        $this->load->view('dashboard_dpt_upload',$data);
    }

    /*
    * Function for show list of dapil (Hari Hendryan)
    */
    function get_data_dpt_rekap(){
        $start  = $this->input->post('start');
        $length = $this->input->post('length');
        $draw = $this->input->post('draw');

        $this->db2 = $this->load->database("db_kab_final",true);

        $sql = "
            SELECT * 
            FROM m_dpt
            LIMIT
                ".$start.",".$length."

        ";
        $query = $this->db2->query($sql);

        $json = array();
        $i=0;
        foreach ($query->result() as $k => $v) {
            $json['data'][$i]['id']= $v->id;
            $json['data'][$i]['provinsi']= $v->provinsi;
            $json['data'][$i]['belum_upload']= 0;
            $json['data'][$i]['sudah_upload']= 0;
            $json['data'][$i]['jumlah_desa']= $v->desa;
            $json['data'][$i]['jumlah_dpt'] = $v->dpt;
            $i++;
        }

        $nrow = $query->num_rows();

        $json['draw'] = $draw;
        $json['recordsFiltered'] = $nrow;
        $json['recordsTotal'] = $nrow;

        echo json_encode($json);
    }

    
}
