<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Score extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$this->load->view('dashboard_score');
	}

	function real_count(){
		$this->load->view('count_real');
	}
	function quick_count(){
		$this->load->view('count_quick');
	}
	function exit_poll(){
		$this->load->view('count_exit_poll');
	}


}
