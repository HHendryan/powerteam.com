<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Master_model');
		$this->load->model('Auth_model');

		$this->Auth_model->can_access_by_login();
		$this->Auth_model->can_access_by_level(99,'Master');
		$data['user'] = $this->Auth_model->get_data();
		$this->load->view('template/header',$data,TRUE);
	}

	function index(){
		$this->user();
	}
	
	function get_select_kab(){
		$id=$this->input->post('id_prov');
		echo $this->Master_model->select_kabupaten($id);
	}

	function get_select_kab_edit(){
		$id=$this->input->post('id_prov');
		$kab=$this->input->post('kab');
		echo $this->Master_model->select_kabupaten_edit($id,$kab);
	}

	function user(){
		$data['provinsi'] = $this->db->get('provinsi')->result();
		$this->load->view('m_user',$data);
	}

	function load_user(){
		$data['data'] = $this->Master_model->data_user();
		echo json_encode($data);
	}

	function add_user(){
		$this->Master_model->add_user($this->input->post());
	}

	function get_user($id){
		$data = $this->db->get_where('user',array('id' => $id ))->row();
		echo json_encode($data);
	}
	function delete_user($id,$value){
		$this->Master_model->delete_user($id,$value);
	}

	function update_user(){
		$this->Master_model->update_user($this->input->post());
	}

	function update_password(){
		$this->Master_model->update_password($this->input->post());
	}
}
