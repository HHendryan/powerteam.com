
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/plyr/plyr.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/profile/settings.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/profile/social.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
</head>
<!-- END HEAD -->
<?php include 'template/header.php'; ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Dashboard</h3>
                <div class="ks-controls">
                    <button type="button" class="btn btn-primary-outline ks-light ks-profile-tabs-block-toggle" data-block-toggle=".ks-profile > .ks-tabs-container">Tabs</button>
                    <button type="button" class="btn btn-primary-outline ks-light ks-settings-menu-block-toggle" data-block-toggle=".ks-settings-tab > .ks-menu">Menu</button>
                </div>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card ks-card-widget ks-widget-payment-card-rate-details">
                        <br>    
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dashboard</a>
                          </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <br>
                            <div class="card-block">
                                <div class="table-responsive" id="progress"></div>
                            </div>
                          </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
                

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/echarts.min.js"></script>
<script src="<?=base_url()?>libs/plyr/plyr.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<?php include 'template/settings.php' ?>
<style>
    #chartdiv {
        width       : 100%;
        height      : 500px;
        font-size   : 11px;
    }  

    .amcharts-export-menu-top-right {
      top: 10px;
      right: 0;
    }               
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
        load_table();
        load_table2();
        load_table3();
    });
</script>
<script type="text/javascript">

    var vtipe = '<?=$tipe_wilayah?>';
    var vid = <?=$id_wilayah?>;
    var vnama = '<?=$nama_wilayah?>';
    $("#header").text(vnama);
    function load_table(){
        $("#table").empty();
        $.ajax({
            type:'POST',
            url:"<?php echo base_url(); ?>home/load_table2/",
            data: {tipe:vtipe,id:vid,nama:vnama},
            success:function(msg){
                if(msg == 'N'){
                    window.location = "<?=base_url()?>";
                }else{
                    $("#table").html(msg);    
                    $("#header").text(vnama);
                }
            
                // load_informasi(vid,vtipe,vnama,vlogo);
            },
            error: function(result){
                $("#table").html("Error"); 
            },
            fail:(function(status) {
                $("#table").html("Fail");
            }),
            beforeSend:function(d){
                $("#table").html("<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets/spinner.png'></strong></center>");
            }
        }); 
    }
</script>
<script type="text/javascript">

    var vtipe = '<?=$tipe_wilayah?>';
    var vid = <?=$id_wilayah?>;
    var vnama = '<?=$nama_wilayah?>';
    $("#header").text(vnama);
    function load_table2(){       
        $("#progress").empty();
        $.ajax({
            type:'POST',
            url:"<?php echo base_url(); ?>home/load_table2/",
            data: {tipe:vtipe,id:vid,nama:vnama},
            success:function(msg){
                if(msg == 'N'){
                    window.location = "<?=base_url()?>";
                }else{ 
                    $("#progress").html(msg);    
                    $("#header").text(vnama);
                }
            },
            error: function(result){
                $("#progress").html("Error"); 
            },
            fail:(function(status) {
                $("#progress").html("Fail");
            }),
            beforeSend:function(d){
                $("#progress").html("<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets/spinner.png'></strong></center>");
            }
        }); 
    }
</script>
<script>
var vtipe = '<?=$tipe_wilayah?>';
    var vid = <?=$id_wilayah?>;
    var vnama = '<?=$nama_wilayah?>';
    $("#header").text(vnama);
    function load_table3(){     
        $("#progress").empty();
        $.ajax({
            type:'POST',
            url:"<?php echo base_url(); ?>home/load_table3/",
            data: {tipe:vtipe,id:vid,nama:vnama},
            success:function(msg){
                if(msg == 'N'){
                    window.location = "<?=base_url()?>";
                }else{
                    var obj = JSON.parse(msg);
                    jsonObj = [];
                    for (var i = 0; i < obj.tabel.length; i++) {
                        item = {};
                        item ["wilayah"] = obj.tabel[i].wilayah;
                        item ["target"] = Math.round(((obj.tabel[i].jum_dpt*obj.tabel[i].target)/100));
                        item ["realisasi"] = obj.tabel[i].realisasi;
                        jsonObj.push(item);
                    };

                    jsonObj1 = [];
                    for (var i = 0; i < obj.tabel.length; i++) {
                        item = {};
                        item ["wilayah"] = obj.tabel[i].wilayah;
                        if (obj.tabel[i].target!=0) {
                            item ["target"] = obj.tabel[i].target;
                        } else {
                            item ["target"] = 0;
                        }
                        
                        item ["color"] = "#FF0F00";
                        jsonObj1.push(item);
                    };

                    /*var chart = AmCharts.makeChart("chartdiv", {
                    "hideCredits":true,
                    "type": "serial",
                     "theme": "light",
                    "categoryField": "wilayah",
                    "rotate": true,
                    "startDuration": 1,
                    "categoryAxis": {
                        "axisAlpha": 0,
                        "gridAlpha": 0,
                        // "inside": true,
                        "tickLength": 0
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonText": "Target:[[value]]",
                            "fillAlphas": 0.8,
                            "id": "AmGraph-1",
                            "lineAlpha": 0.2,
                            "title": "target",
                            "type": "column",
                            "valueField": "target",
                            "fixedColumnWidth": 10
                        },
                        {
                            "balloonText": "Realisasi:[[value]]",
                            "fillAlphas": 0.8,
                            "id": "AmGraph-2",
                            "lineAlpha": 0.2,
                            "title": "realisasi",
                            "type": "column",
                            "valueField": "realisasi",
                            "fixedColumnWidth": 10
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "axisAlpha": 0,
                            "labelsEnabled" : false,
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0.2,
                            "dashLength": 0
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "titles": [],
                    "dataProvider": jsonObj,
                    "export": {
                        "enabled": true
                     }

                });*/
                }
            },
            error: function(result){
                $("#progress").html("Error"); 
            },
            fail:(function(status) {
                $("#progress").html("Fail");
            }),
            beforeSend:function(d){
                $("#progress").html("<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets/spinner.png'></strong></center>");
            }
        }); 
    }
</script>

<div class="ks-mobile-overlay"></div>
</body>
</html>