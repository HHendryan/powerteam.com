
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/notie/notie.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Data Suara</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <select class="form-control ks-select select2" id="legislator" style="width: 30%" required>
                                <option disabled selected value>PILIH</option>
                                <?php foreach ($legislator as $key => $value) { ?>
                                    <option value="<?= $value->id_legislator ?>"><?= $value->nama ?></option>   
                                <?php } ?>
                            </select>
                            <br><br>
                            <div class="table-responsive">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="edit_suara">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">ID LEGISLATOR</label>
                            <input type="text" class="form-control" name="id_legislator" readonly required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">DAPIL</label>
                            <input type="text" class="form-control" name="id_dapil" readonly required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">PROVINSI</label>
                            <input type="text" class="form-control" name="id_provinces" readonly required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">KABUPATEN</label>
                            <input type="text" class="form-control" name="id_regency" readonly required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">KECAMATAN</label>
                            <input type="text" class="form-control" name="id_district" readonly required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">KELURAHAN</label>
                            <input type="text" class="form-control" name="id_village" readonly required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">SUARA</label>
                            <input type="text" class="form-control" name="total" onkeypress="return isNumberKey(event)" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>assets/notie/notie.min.js"></script>
<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('pileg/get_table_dapil_suara/0')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    {"data": "id_dapil","title": "DAPIL"},
                    {"data": "prov","title": "PROVINSI"},
                    {"data": "kab","title": "KABUPATEN"},
                    {"data": "kec","title": "KECAMATAN"},
                    {"data": "kel","title": "KELURAHAN"},
                    {"data": "button","title": "ACTION"},
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
    });
</script>
<script type="application/javascript">
    $(document).ready(function() {
        $('.flatpickr').flatpickr();
        $('#menu_swift').trigger('change');
    });
    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
      return true;
    }
    $("#legislator").change(function(){
        id = $(this).val();
        $("[name=id_legislator]").val(id);
        Pace.track(function(){
            table = $("#table").dataTable();
            table.api().ajax.url('<?=site_url("pileg/get_table_dapil_suara/")?>'+id).load();
        });
    })
</script>
<script type="text/javascript">
    

    $('.select2').select2();

    $("#table").on("click", ".btn-edit",function(event){
        dapil = $(this).data('dapil');
        prov = $(this).data('prov');
        kab = $(this).data('kab');
        kec = $(this).data('kec');
        kel = $(this).data('kel');
        legislator = $("[name=id_legislator]").val();
        // console.log(value);
        // var id; 
        $.ajax({
            url : "<?php echo site_url().'pileg/detail_suara/'?>",
            type: "GET",
            data: {id_dapil:dapil,id_provinces:prov,id_regency:kab,id_district:kec,id_village:kel,id_legislator:legislator},
            dataType: "JSON",
            success: function(data){
                console.log(data);
                $("[name=id_dapil]").val(data.id_dapil);
                $("[name=id_provinces]").val(data.id_provinces);
                $("[name=id_regency]").val(data.id_regency);
                $("[name=id_district]").val(data.id_district);
                $("[name=id_village]").val(data.id_village);
                $("[name=total]").val(data.total);
                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $('#edit_suara').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'pileg/edit_suara'?>",
                type : 'post',
                data : $( "#edit_suara" ).serialize(),
                dataType: "json",
                success : function(data){
                    var table = $('#table').DataTable();
                    document.getElementById("edit_suara").reset();
                    table.ajax.reload();
                    $("#modal_edit").modal('hide');                
                },
                error: function(data){
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });

</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>