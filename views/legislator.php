
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/notie/notie.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Data Legislator</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah Data</button>
                            <button class="btn btn-success" data-toggle="modal" data-target="#modal_excel">Upload Excel</button>
                            <button class="btn btn-danger btn-hapus">DELETE</button>
                            <br><br>
                            <div class="table-responsive">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="input_legislator" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select select2" id="type" name="type" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DPR RI</option>
                                <option value="2">DPRD PROVINSI</option>
                                <option value="3">DPRD KABUPATEN</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Periode</label>
                            <select class="form-control ks-select select2" name="periode" id="periode" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="2014">2014</option>
                                <option value="2019">2019</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">DAPIL</label>
                            <select class="form-control ks-select select2" name="id_dapil" id="id_dapil" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Status</label>
                            <select class="form-control ks-select select2" name="status" id="status" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">Legislator</option>
                                <option value="2">Caleg</option>
                                <option value="3">Caleg Sementara</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">No. Urut</label>
                            <input type="text" class="form-control" name="no_urut" placeholder="No. Urut" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Jenis Kelamin</label>
                            <select class="form-control ks-select select2" name="jk" id="jk" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">Laki-laki</option>
                                <option value="2">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">No. Telp</label>
                            <input type="text" class="form-control" name="phone" placeholder="No. Telp" onkeypress="return isNumberKey(event)">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tanggal Lahir</label>
                            <input type="text" class="form-control flatpickr" name="tanggal_lahir" placeholder="Tanggal Lahir">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Agama</label>
                            <select class="form-control ks-select select2" name="agama"  id="agama" style="width: 100%">
                                <option disabled selected value>PILIH</option>
                                <option value="islam">Islam</option>
                                <option value="katolik">Kristen Katolik</option>
                                <option value="protestan">Kristen Protestan</option>
                                <option value="hindu">Hindu</option>
                                <option value="buddha">Buddha</option>
                                <option value="konghucu">Konghucu</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Alamat</label>
                            <textarea type="text" class="form-control" name="alamat" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Riwayat Pendidikan</label>
                            <textarea type="text" class="form-control" name="riwayat_pendidikan" placeholder="Riwayat Pendidikan"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Riwayat Organisasi</label>
                            <textarea type="text" class="form-control" name="riwayat_organisasi" placeholder="Riwayat Organisasi"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Riwayat Karir</label>
                            <textarea type="text" class="form-control" name="riwayat_karir" placeholder="Riwayat Karir"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Riwayat Politik</label>
                            <textarea type="text" class="form-control" name="riwayat_politik" placeholder="Riwayat Politik"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Partai Politik</label>
                                <?php                  
                                echo form_dropdown(
                                   'id_parpol',
                                   $parpol,  
                                   set_value('id_parpol'),
                                   'class="form-control input required"  id="id_parpol" name="id_parpol"'
                                   );             
                            ?>  
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Aktif Dalam Kegiatan</label>
                            <textarea type="text" class="form-control" name="aktif_dalam_kegiatan" placeholder="Aktif Dalam Kegiatan"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Keluarga</label>
                            <textarea type="text" class="form-control" name="keluarga" placeholder="Keluarga"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Upload Foto</label><br>
                            <input type="file" name="berkas" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="edit_legislator">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <input type="hidden" name="id_legislator">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select" name="type" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DPR RI</option>
                                <option value="2">DPRD PROVINSI</option>
                                <option value="3">DPRD KABUPATEN</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Periode</label>
                            <select class="form-control ks-select" name="periode" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="2014">2014</option>
                                <option value="2019">2019</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">DAPIL</label>
                            <select class="form-control ks-select" name="id_dapil" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Status</label>
                            <select class="form-control ks-select" name="status" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">Legislator</option>
                                <option value="2">Caleg</option>
                                <option value="3">Caleg Sementara</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Jenis Kelamin</label>
                            <select class="form-control ks-select" name="jk" id="jk" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">Laki-laki</option>
                                <option value="2">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">No. Telp</label>
                            <input type="text" class="form-control" name="phone" placeholder="No. Telp" onkeypress="return isNumberKey(event)">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tanggal Lahir</label>
                            <input type="text" class="form-control flatpickr" name="tanggal_lahir" placeholder="Tanggal Lahir">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Agama</label>
                            <select class="form-control ks-select" name="agama" id="agama" style="width: 100%">
                                <option disabled selected value>PILIH</option>
                                <option value="islam">Islam</option>
                                <option value="katolik">Kristen Katolik</option>
                                <option value="protestan">Kristen Protestan</option>
                                <option value="hindu">Hindu</option>
                                <option value="buddha">Buddha</option>
                                <option value="konghucu">Konghucu</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Alamat</label>
                            <textarea type="text" class="form-control" name="alamat" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Riwayat Pendidikan</label>
                            <textarea type="text" class="form-control" name="riwayat_pendidikan" placeholder="Riwayat Pendidikan"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Riwayat Organisasi</label>
                            <textarea type="text" class="form-control" name="riwayat_organisasi" placeholder="Riwayat Organisasi"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Riwayat Karir</label>
                            <textarea type="text" class="form-control" name="riwayat_karir" placeholder="Riwayat Karir"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Riwayat Politik</label>
                            <textarea type="text" class="form-control" name="riwayat_politik" placeholder="Riwayat Politik"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Aktif Dalam Kegiatan</label>
                            <textarea type="text" class="form-control" name="aktif_dalam_kegiatan" placeholder="Aktif Dalam Kegiatan"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Keluarga</label>
                            <textarea type="text" class="form-control" name="keluarga" placeholder="Keluarga"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_excel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="upload_form">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select select2" id="type1" name="type1" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DPR RI</option>
                                <option value="2">DPRD PROVINSI</option>
                                <option value="3">DPRD KABUPATEN</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Periode</label>
                            <select class="form-control ks-select select2" name="periode1" id="periode1" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="2014">2014</option>
                                <option value="2019">2019</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">DAPIL</label>
                            <select class="form-control ks-select select2" name="id_dapil1" id="id_dapil1" style="width: 100%" required>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Unique ID</label>
                            <input type="text" class="form-control" name="unique" placeholder="Unique ID" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">File Excel</label>
                            <input type="file" class="form-control" name="file" placeholder="File Excel" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta" id="save">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>assets/notie/notie.min.js"></script>
<?php include 'template/settings.php' ?>
<script type="application/javascript">
    $(document).ready(function() {
        $('.flatpickr').flatpickr();
        $('#menu_swift').trigger('change');
    });
    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
      return true;
    }
</script>
<script type="text/javascript">
    

    $('.select2').select2();

    $("[name=type]").change(function(){
    	periode = $("[name=periode]").val();
    	type = $(this).val();
    	if(periode){
    		get_dapil(type,periode);
    	}
    })

    $("[name=periode]").change(function(){
    	type = $("[name=type]").val();
    	periode = $(this).val();
    	if(type){
    		get_dapil(type,periode);
    	}
    })

    $("[name=type1]").change(function(){
        periode = $("[name=periode1]").val();
        type = $(this).val();
        if(periode){
            get_dapil(type,periode);
        }
    })

    $("[name=periode1]").change(function(){
        type = $("[name=type1]").val();
        periode = $(this).val();
        if(type){
            get_dapil(type,periode);
        }
    })

    function get_dapil(type,periode){
    	$.ajax({
            url: "<?php echo site_url('pileg/get_dapil') ?>",
            cache: false,
            type:"POST",
            data:{type:type,periode:periode},
            success: function(respond){
            	// console.log(respond);
                $("[name=id_dapil]").html(respond);
                $("[name=id_dapil1]").html(respond);
            }
        })
    }

    $('#input_legislator').submit(function(event){
        event.preventDefault();
                    //alert($( "#input_legislator" ).serialize())
        var formData = new FormData(this);
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'pileg/input_legislator'?>",
                type : 'post',
                data : formData,//$( "#input_legislator" ).serialize(),
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success : function(data){
                    console.log(data);
                    document.getElementById('agama').selectedIndex = 0;
                    document.getElementById('id_dapil').selectedIndex = 0;
                    document.getElementById('status').selectedIndex = 0;
                    document.getElementById('type').selectedIndex = 0;
                    document.getElementById('id_parpol').selectedIndex = 0;
                    document.getElementById("input_legislator").reset();
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                    
                    $("#modal_tambah").modal('hide');                
                },
                error: function(data){
                	console.log(data);
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('pileg/get_table_legislator')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    {"data": "nama","title": "Nama"},
                    {"data": "alamat","title": "Alamat"},
                    {"data": "phone","title": "Phone"},
                    {"data": "periode","title": "periode"},
                    {"data": "button","title": "Action"},
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
    });
</script>

<!-- CRUD JAVASCRIPT -->
<script type="text/javascript">
    

    $('#edit_legislator').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'pileg/edit_legislator'?>",
                type : 'post',
                data : $( "#edit_legislator" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#table').DataTable();
                    document.getElementById("edit_legislator").reset();
                    table.ajax.reload();
                    $("#modal_edit").modal('hide');                
                },
                error: function(data){
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });
	
	$("#table").on("click", ".btn-edit",function(event){
        value = $(this).data('id');
		console.log(value);
        var id; 
        $.ajax({
            url : "<?php echo site_url().'pileg/detail_legislator/'?>"+value,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                console.log(data);
                $('[name="id_legislator"]').val(data.id_legislator);
                $('[name="type"]').val(data.type);
                $('[name="periode"]').val(data.periode);
                $('[name="status"]').val(data.status);
                $('[name="id_dapil"]').val(data.id_dapil);
                $('[name="nama"]').val(data.nama);
                $('[name="phone"]').val(data.phone);
                $('[name="agama"]').val(data.agama);
                $('[name="jk"]').val(data.jk);
                $('[name="riwayat_politik"]').val(data.riwayat_politik);
                $('[name="riwayat_karir"]').val(data.riwayat_karir);
                $('[name="riwayat_organisasi"]').val(data.riwayat_organisasi);
                $('[name="riwayat_pendidikan"]').val(data.riwayat_pendidikan);
                $('[name="aktif_dalam_kegiatan"]').val(data.aktif_dalam_kegiatan);
                $('[name="keluarga"]').val(data.keluarga);
                $('[name="alamat"]').val(data.alamat);
                $('[name="tempat_lahir"]').val(data.tempat_lahir);
                $('[name="tanggal_lahir"]').val(data.tanggal_lahir);
                // $('[name="id_dapil"]').append('<option value="'+data.id_kabupaten+'">'+data.kabupaten+'</option>');
                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $("#table").on("click", ".btn-delete",function(event){
        id = $(this).data('id');
        event.preventDefault();
            
        Pace.track(function(){
            $.ajax({
                url : "<?php echo site_url('pileg/delete_legislator')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                },
                error: function (data){
                    console.log(data);
                    alert('Error get data from ajax');
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $('#upload_form').submit(function(e){
        e.preventDefault();
        var id = $('[name="unique"]').val();
        var id_uri = encodeURI(id);
        $.ajax({
            url: "<?php echo site_url('pileg/cek_unique_id_legislator/') ?>"+id_uri,
            cache: false,
            type:"POST",
            success: function(respond){
                if(respond == 0){
                    alert('UNIQUE ID sudah ada');
                }else{
                    $("#save").prop('disabled', true);
                    var form = $('#upload_form')[0];
                    var data = new FormData(form);
                    Pace.track(function(){
                        $.ajax({
                            url  : "<?= site_url().'pileg/upload_m_legislator'?>",
                            type : 'POST',
                            enctype: 'multipart/form-data',
                            data : data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            timeout: 600000,
                            dataType: "json",
                            success: function(data){
                                $("#modal_excel").modal('hide'); 
                                $("#save").prop('disabled', false);
                                notie.alert({ type: 1, text: 'Data Berhasil Diupload sejumlah '+data, time: 2,position: 'bottom' })
                                var table = $('#table').DataTable();
                                table.ajax.reload();
                            },
                            error: function(xhr, status, error) {
                                var err = eval("(" + xhr.responseText + ")");
                                alert(err.Message);
                            }
                        });
                    })
                }
            }
        })
      // e.preventDefault();
      // 
    });
</script>
<script type="text/javascript">
    $( ".btn-hapus" ).click(function() {
        notie.input({
            text: 'Masukkan UNIQUE ID',
            submitText: 'Submit',
            cancelText: 'Cancel',
            position: 'bottom',
            cancelCallback: function (value) {

            },
            submitCallback: function (value) {
                id_uri = encodeURI(value);
                if(!id_uri){
                    alert('Unique ID HARUS DIISI');
                }else{
                    Pace.track(function(){
                        $.ajax({
                            url: "<?php echo site_url('pileg/check_jumlah_legislator/') ?>"+id_uri,
                            type:"POST",
                            success: function(respond){
                                if(respond == 0){
                                    notie.alert({ type: 3, text: 'Data dengan unique id tersebut tidak ada', time: 2,position: 'bottom'})
                                }else{
                                    notie.confirm({
                                        text: respond,
                                        position: 'bottom',
                                        submitText: 'Hapus',
                                        cancelText: 'Cancel',
                                        cancelCallback: function () {
                                            
                                        },
                                        submitCallback: function () {
                                            Pace.track(function(){
                                                $.ajax({
                                                    url: "<?php echo site_url('pileg/hapus_data_legislator/') ?>"+id_uri,
                                                    cache: false,
                                                    type:"POST",
                                                    success: function(respond){
                                                        if(respond == 1){
                                                            notie.alert({ type: 1, text: 'Data berhasil dihapus', time: 2,position: 'bottom'});
                                                            var table = $('#table').DataTable();
                                                            table.ajax.reload();
                                                        }else{
                                                            notie.alert({ type: 3, text: 'Data tidak berhasil dihapus', time: 2,position: 'bottom'});
                                                        }
                                                        
                                                    }
                                                })
                                            })
                                        }
                                    })
                                }
                            }
                        })
                    })    
                }
              },
        })
    });
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>