<table id="dataTable2" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>No.</th>
            <?php 
            if($id == 0){
                $wil = 'Provinsi';
            }else{
                if($tipe == 'kecamatan'){
                    $wil = 'Kelurahan';
                }else if($tipe == 'kelurahan'){
                    $wil = 'TPS';
                }else if($tipe == 'kabupaten'){
                    $wil = 'Kecamatan';
                }else if($tipe == 'provinsi'){
                    $wil = 'Kabupaten';
                }
            }
            ?>
            <th><?=$wil?></th>
            <th>DPT</th>
            <th>Target(%)</th>
            <th>Realisasi(%)</th>
            <th>Gap</th>
            <th>Guraklih</th>
            <!--th>Jumlah TPS</th>
            <th>Asumsi Suara Sah (75%)</th>
            <th>Target Menang (%)</th-->            
        </tr>
    </thead>
    <tbody>
        <?php foreach ($tabel as $key => $value) {
            if($value['jum_dpt'] != 0){
                $asumsi_suara_sah = round($value['jum_dpt']*0.75);
                $target_perolehan_suara = round($asumsi_suara_sah/$jumlah_paslon+($asumsi_suara_sah*0.05));
                $target = round(($value['jum_dpt']*$value['target'])/100);
                $target_menang = round($target_perolehan_suara/$asumsi_suara_sah*100,2);
                $sisa = $target - $value['realisasi'] ;
                $progress = round(($value['realisasi']/$target_perolehan_suara)*100, 2);
                $realisasi = round(($value['realisasi']/$value['jum_dpt'])*100);
                $sisper = round(($sisa/$value['jum_dpt'])*100);
            }else{
                $asumsi_suara_sah = 0;
                $target_perolehan_suara = 0;
                $target = 0;
                $target_menang = 0;
                $sisa = 0;
                $progress = 0;
                $realisasi = 0;
                $sisper = 0;
            }
            
        ?>
        <tr>
            <td style="text-align: left"><?= $value['id'] ?></td>

            <?php if($tipe == 'kelurahan'){ ?>
            <td style="text-align: center;"><b><a class="wilayah" data-id="<?= $value['wilayah'] ?>" href="javascript:void(0)"><?= $value['wilayah'] ?></b></a></td>
            <?php }else{ ?>
            <td style="text-align: left"><b><a class="wilayah" data-nama="<?= $value['wilayah']?>" data-id="<?= $value['id_wilayah'] ?>" data-logo="<?= $value['logo_wilayah'] ?>" href="javascript:void(0)"><?= $value['wilayah'] ?></a></b></td>
            <?php } ?>
            <td style="text-align: right"><?= number_format($value['jum_dpt']) ?></td>
            <td style="text-align: right"><?= number_format($target)?>&nbsp;&nbsp;&nbsp;<b style="color: #25628f">(<?=$value['target']?>%)</b></td>
            <td style="text-align: right"><?= number_format($value['realisasi'])?>&nbsp;&nbsp;&nbsp;<b style="color: #25628f">(<?=$realisasi?>%)</b></td>
            <td style="text-align: right"><?= number_format($sisa)?>&nbsp;&nbsp;&nbsp;<b style="color: #25628f">(<?=$sisper?>%)</b></td>
            <td style="text-align: right"><?= $value['guraklih']?></td>
            <!--td style="text-align: right"><?= number_format($value['jum_tps']) ?></td>
            <td style="text-align: right"><?= number_format($asumsi_suara_sah) ?></td>
            <td style="text-align: center"><?= $target_menang ?> %</td-->
            
            
        </tr>
        <?php } ?>
    </tbody>
   
</table>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_detail_dpt1" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="table-responsive">
                        <table id="table_detail_dpt1" class="table table-striped table-bordered" cellspacing="0" width="100%">          
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#dataTable2").dataTable({
    dom: 'rt',
    "lengthMenu": [[-1], ["All"]],
    buttons: [
        'excel'
    ],
    
});
</script>
<script type="text/javascript">
    $("#dataTable2").on("click", ".wilayah",function(event){
        tps = $(this).data('id');
        id_kel = '<?= $id?>';
        var table = $("#table_detail_dpt1").dataTable();
        var tipe = '<?= $tipe?>';
        if(tipe != 'kelurahan'){
            if(id_kel == 0){
                vtipe = 'provinsi';
                vid = $(this).data('id');
                vnama =$(this).data('nama');
                load_table2();
                load_table3();
            }
            if(tipe == 'provinsi'){
                vtipe = 'kabupaten';
                vid = $(this).data('id');
                vnama = $(this).data('nama');
                load_table2();
                load_table3();
            }
            if(tipe == 'kabupaten'){
                vtipe = 'kecamatan';
                vid = $(this).data('id');
                vnama = $(this).data('nama');
                load_table2();
                load_table3();
            }
            if(tipe == 'kecamatan'){
                vtipe = 'kelurahan';
                vid = $(this).data('id');
                vnama = $(this).data('nama');
                vlogo = $(this).data('logo');
                load_table2();
                load_table3();
            }
        }else{
            table.api().ajax.url("<?= site_url('home/get_detail_dpt/0/0')?>").load();
            table.api().ajax.url("<?= site_url('home/get_detail_dpt2/')?>"+id_kel+'/'+tps).load();
            $("#modal_detail_dpt1").modal("show");
        }
        // table.fnClearTable();
        // $("#table_detail").empty();
        
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table_detail_dpt1").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                "autoWidth": false,
                oLanguage: {
                    sProcessing: "<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets/spinner.png'></strong></center>"
                },
                processing: true,
                serverSide: true,
                "columnDefs": [
                    {"className": "dt-center", "targets": [4]}
                  ],
                ajax: {"url": "<?php echo site_url('home/get_detail_dpt/0/0')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    { "data": 'nik', "title": "NIK"},
                    { "data": 'nama', "title": "Nama"},
                    { "data": 'jns_kelamin', "title": "JK"},
                    { "data": 'alamat', "title": "Alamat"},
                    { "data": 'tanggal_lahir', "title": "Tanggal Lahir" },
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });    
    $("#table_detail_dpt_length").css("display", "none");
    });
</script>