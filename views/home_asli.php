
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/plyr/plyr.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/profile/settings.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/profile/social.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
</head>
<!-- END HEAD -->
<body class="ks-navbar-fixed ks-sidebar-sections ks-sidebar-position-fixed ks-theme-primary-light ks-page-loading"> <!-- remove ks-page-header-fixed to unfix header -->
<!-- BEGIN HEADER -->
    <nav class="navbar ks-navbar">
        <!-- BEGIN HEADER INNER -->
        <!-- BEGIN LOGO -->
        <div href="<?=base_url()?>" class="navbar-brand">
            <a href="#" class="ks-sidebar-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
            <a href="#" class="ks-sidebar-mobile-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
            
            <div class="ks-navbar-logo">
               <a href="<?=base_url()?>" class="ks-logo"><img src="<?=base_url()?>/assets/img/teknopol.png"></a>
            </div>
        </div>
        <!-- END LOGO -->

        <!-- BEGIN MENUS -->
        <div class="ks-wrapper">
            <nav class="nav navbar-nav">
                <!-- BEGIN NAVBAR MENU -->
                <div class="ks-navbar-menu">
                </div>
                <!-- END NAVBAR MENU -->

                <!-- BEGIN NAVBAR ACTIONS -->
                <div class="ks-navbar-actions">
                    <!-- BEGIN NAVBAR USER -->
                    <div class="nav-item nav-link btn-action-block">
                    <a class="btn btn-crusta-outline">
                        <span class="ks-action"><?php echo $user->username ?></span>
                        <?php if ($user->level==99){
                            $level = 'ADMIN';
                        }elseif ($user->level==3) {
                            $level = 'PILPRES';
                        }elseif ($user->level==2) {
                            $level = 'PILGUB';
                        } elseif ($user->level==1) {
                            $level = 'PILWALI/PILBUP';
                        }elseif ($user->level==0) {
                            $level = 'PILEG';
                        }    

                        ?>
                        <span class="ks-description"><?php echo $level ?></span>
                    </a>
                </div>
                    <div class="nav-item dropdown ks-user">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="ks-avatar">
                                <img src="<?=base_url()?><?php echo $user->url_image?>" width="36" height="36">
                                <!-- <?php echo $user->username ?> -->
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
                            <!-- <a class="dropdown-item" href="#">
                                <span class="la la-user ks-icon"></span>
                                <span>Profile</span>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="la la-wrench ks-icon" aria-hidden="true"></span>
                                <span>Settings</span>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="la la-question-circle ks-icon" aria-hidden="true"></span>
                                <span>Help</span>
                            </a> -->
                            <a class="dropdown-item" href="<?=site_url('auth/logout')?>">
                                <span class="la la-sign-out ks-icon" aria-hidden="true"></span>
                                <span>Logout</span>
                            </a>
                        </div>
                    </div>
                    <!-- END NAVBAR USER -->
                </div>
                <!-- END NAVBAR ACTIONS -->
            </nav>
        </div>
        <!-- END MENUS -->
        <!-- END HEADER INNER -->
    </nav>
    <!-- END HEADER -->

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Home</h3>
                <div class="ks-controls">
                    <button type="button" class="btn btn-primary-outline ks-light ks-profile-tabs-block-toggle" data-block-toggle=".ks-profile > .ks-tabs-container">Tabs</button>
                    <button type="button" class="btn btn-primary-outline ks-light ks-settings-menu-block-toggle" data-block-toggle=".ks-settings-tab > .ks-menu">Menu</button>
                </div>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-social-profile">
                <div class="ks-social-profile-header">
                    <div class="ks-cover" style="background-image: url('<?=base_url()?>assets/img/logo2.png')"></div>
                    <div class="ks-user">
                        <div class="ks-info">
                            <img src="<?=base_url()?><?php echo $user->url_image?>" class="ks-avatar" width="167" height="300">
                            <div class="ks-body">
                                <div class="ks-name">IT - RESEARCH & POLITIC CONSULTANT</div>
                                <div class="ks-description"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

            </div>
            <div class="ks-page-content-body ks-content-nav" style="margin-top: 0px;">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Influencer&ensp;</label>
                                        <select id="type" class="form-control ks-select" name="type" style="width: 100%">
                                            <!-- <option disabled selected value>PILIH</option> -->
                                            <option value="all">all</option>
                                            <option value="tangible">TANGIBLE</option>
                                            <option value="intangible">INTANGIBLE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                   <div class="form-group">
                                        <label class="form-control-label">Kecondongan&ensp;</label>
                                        <select id="kecondongan" class="form-control ks-select" name="type" style="width: 100%">
                                            <option value="all">ALL</option>
                                            <?php foreach ($parpol as $key => $value) { ?>
                                                <option value="<?php echo $value->id_parpol ?>"><?php echo $value->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div> 
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label">Kategori&ensp;</label>
                                        <select id="kategori" class="form-control ks-select" name="type" style="width: 100%">
                                            <option value="all">ALL</option>
                                            <?php foreach ($pertokohan as $key => $value) { ?>
                                                <option value="<?php echo $value->id_pertokohan ?>"><?php echo $value->nama ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="form-control-label" style="visibility: hidden">Influencer</label>
                                        <button class="btn btn-warning" onclick="cari()" style="width: 100%">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="table-responsive">
                                        <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Provinsi</th>
                                                    <th>Jumlah</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Provinsi</th>
                                                    <th>Jumlah</th>
                                                    <th>Action</th>
                                                </tr>

                                            </tfoot>
                                        </table>
                                    </div>        
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div id="chartdiv"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="chart_tangible" style="height: 300px"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="chart_intangible" style="height: 300px"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="chart_parpol" style="height: 300px"></div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade large-modal"  role="dialog" aria-labelledby="myLargeModalLabel" id="modal_detail" aria-hidden="true" width="100%">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Influencer</h5> 

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <br>
            <!--h5 class="modal-title" id="prov_name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sulawesi Tenggara</h5-->
            <form id="edit" width="100%">
            <div class="modal-body" width="100%">
                <div class="container-fluid">
                    <div class="table-responsive">
                                <table id="table_detail" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>No. Telp</th>
                                        <th>Ketokohan</th>
                                        <th>Condong</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Kabupaten</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>Informasi Tambahan</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Nama</th>
                                        <th>No. Telp</th>
                                        <th>Ketokohan</th>
                                        <th>Condong</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Kabupaten</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>Informasi Tambahan</th>
                                    </tr>

                                </tfoot>
                            </table>
                        </div>
                </div>
            </div>
            <div class="modal-footer" width="100%">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/echarts.min.js"></script>
<script src="<?=base_url()?>libs/plyr/plyr.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="application/javascript">
(function ($) {
    $(document).ready(function () {
        plyr.setup();

        $('[data-toggle="tooltip"]').tooltip({
            delay: {
                show: 500
            }
        });
    });
})(jQuery);
</script>
<script type="application/javascript">

    (function ($) {
        $(document).ready(function() {
            var table = $('#ks-datatable').DataTable({
                ajax: '<?=site_url('data/get_data_influence/all/all/all')?>',
                columns: [
                            { data: 'provinsi' },
                            { data: 'jum_tangible' },
                            { data: 'button' },
                        ],
                "autoWidth": false,
                buttons: [
                    { extend: 'copyHtml5', footer: true },
                    { extend: 'excelHtml5', footer: true },
                    { extend: 'csvHtml5', footer: true },
                    { extend: 'pdfHtml5', footer: true }
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
         
                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,.]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var totalkab = api
                        .column( 1 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );

                    $( api.column( 0 ).footer() ).html('Total');
                    $( api.column( 1 ).footer() ).html(totalkab);
                    
                },
                initComplete: function (settings, json) {
                    $('.dataTables_wrapper select').select2({
                        minimumResultsForSearch: Infinity
                    });
                    table.buttons().container().appendTo('#head_table');
                }
            });

            table.buttons().container().appendTo( '#ks-datatable_wrapper .col-md-6:eq(0)' );
            
            
            $('#tipe').select2({
                placeholder: "Pilih"
            });

            $('#inclination').select2({
                placeholder: "Pilih"
            });

            $('#id_pertokohan').select2({
                placeholder: "Pilih"
            });
        });
    })(jQuery);
</script>
<script type="text/javascript">
    $('#table_detail').DataTable({
                columns: [
                            { data: 'name' },
                            { data: 'phone' },
                            { data: 'pertokohan' },
                            { data: 'kecondongan' },
                            { data: 'gender' },
                            { data: 'kab' },
                            { data: 'kec' },
                            { data: 'kel' },
                            { data: 'informasi_tambahan' },
                        ],
                "autoWidth": false
            });
</script>
<script type="text/javascript">
    function cari(){
        tipe = $('#type').val();
        condong = $('#kecondongan').val();
        kategori = $('#kategori').val();
        var table = $('#ks-datatable').DataTable();
        if(tipe && condong && kategori){
            table.ajax.url( "<?= site_url('data/get_data_influence/')?>"+tipe+'/'+condong+'/'+kategori ).load(); 
        }
    }
</script>

<script type="text/javascript">
    $('#modal_detail').on('shown.bs.modal', function () {
    $(this).find('.modal-dialog').css({width:'auto',
                               height:'auto', 
                              'max-height':'100%'});
    });

    $("#ks-datatable").on("click", ".btn-detail",function(event){
        id = $(this).data('id');
        tipe = $('#type').val();
        condong = $('#kecondongan').val();
        kategori = $('#kategori').val();

        var table = $('#table_detail').DataTable();
        if(tipe && condong && kategori){
            table.ajax.url( "<?= site_url('data/get_detail_influencer/')?>"+id+'/'+tipe+'/'+condong+'/'+kategori ).load();
            $("#modal_detail").modal('show');
        }
    });
</script>

<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(all);
    google.charts.setOnLoadCallback(tangible);
    google.charts.setOnLoadCallback(intangible);
    google.charts.setOnLoadCallback(parpol);

    function all() {
        var data = google.visualization.arrayToDataTable([
            ['Type', 'Jumlah', { role: 'style' }],
            ['Tangible', <?php echo $tangible ?>, '#b87333'],
            ['Intangible', <?php echo $intangible ?>, '#00000']
        ]);

        var options = {
            title: 'Tangible & Intangible',
            chartArea: {width: '80%'},
            hAxis: {
                title: 'Total',
                minValue: 0
            },
            legend: {position: 'none'}
        };

        var chart = new google.visualization.BarChart(document.getElementById('chartdiv'));
        chart.draw(data, options);
    }

    function tangible() {

        var data = google.visualization.arrayToDataTable([
          ['Type', 'Jumlah'],
          <?php foreach ($jum_tangible as $key => $value) { ?>
            ['<?php echo $value->nama ?>', <?php echo $value->jml ?>],
          <?php } ?>
          
        ]);

        var options = {
          title: 'Tangible',
          chartArea: {width: '80%'},
          legend: {position: 'none'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_tangible'));

        chart.draw(data, options);
    }

    function intangible() {

        var data = google.visualization.arrayToDataTable([
          ['Type', 'Jumlah'],
          <?php foreach ($jum_intangible as $key => $value) { ?>
            ['<?php echo $value->nama ?>', <?php echo $value->jml ?>],
          <?php } ?>
          
        ]);

        var options = {
          title: 'Intangible',
          chartArea: {width: '80%'},
          legend: {position: 'none'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_intangible'));

        chart.draw(data, options);
    }

    function parpol() {

        var data = google.visualization.arrayToDataTable([
          ['Type', 'Jumlah'],
          <?php foreach ($kecondongan as $key => $value) { ?>
            ['<?php echo $value->alias ?>', <?php echo $value->jml ?>],
          <?php } ?>
          
        ]);

        var options = {
          title: 'Kecondongan Politik',
          chartArea: {width: '80%'},
          legend: {position: 'none'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_parpol'));

        chart.draw(data, options);
    }
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>