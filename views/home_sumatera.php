
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>Teknopol</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/plyr/plyr.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/profile/settings.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/profile/social.min.css">
</head>
<!-- END HEAD -->
<body class="ks-navbar-fixed ks-sidebar-sections ks-sidebar-position-fixed ks-theme-primary-light ks-page-loading"> <!-- remove ks-page-header-fixed to unfix header -->
<!-- BEGIN HEADER -->
    <nav class="navbar ks-navbar">
        <!-- BEGIN HEADER INNER -->
        <!-- BEGIN LOGO -->
        <div href="<?=base_url()?>" class="navbar-brand">
            <a href="#" class="ks-sidebar-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
            <a href="#" class="ks-sidebar-mobile-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
            
            <div class="ks-navbar-logo">
                <a href="<?=base_url()?>" class="ks-logo"><img src="<?=base_url()?>/assets/img/teknopol.png"></a>
            </div>
        </div>
        <!-- END LOGO -->

        <!-- BEGIN MENUS -->
        <div class="ks-wrapper">
            <nav class="nav navbar-nav">
                <!-- BEGIN NAVBAR MENU -->
                <div class="ks-navbar-menu">
                </div>
                <!-- END NAVBAR MENU -->

                <!-- BEGIN NAVBAR ACTIONS -->
                <div class="ks-navbar-actions">
                    <!-- BEGIN NAVBAR USER -->
                    <div class="nav-item nav-link btn-action-block">
                    <a class="btn btn-crusta-outline">
                        <span class="ks-action"><?php echo $user->username ?></span>
                        <?php if ($user->level==99){
                            $level = 'ADMIN';
                        }elseif ($user->level==3) {
                            $level = 'PILPRES';
                        }elseif ($user->level==2) {
                            $level = 'PILGUB';
                        } elseif ($user->level==1) {
                            $level = 'PILWALI/PILBUP';
                        }elseif ($user->level==0) {
                            $level = 'PILEG';
                        }    

                        ?>
                        <span class="ks-description"><?php echo $level ?></span>
                    </a>
                </div>
                    <div class="nav-item dropdown ks-user">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="ks-avatar">
                                <img src="<?=base_url()?>assets/img/hanuracalon.jpeg" width="36" height="36">
                                <!-- <?php echo $user->username ?> -->
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
                            <!-- <a class="dropdown-item" href="#">
                                <span class="la la-user ks-icon"></span>
                                <span>Profile</span>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="la la-wrench ks-icon" aria-hidden="true"></span>
                                <span>Settings</span>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="la la-question-circle ks-icon" aria-hidden="true"></span>
                                <span>Help</span>
                            </a> -->
                            <a class="dropdown-item" href="<?=site_url('auth/logout')?>">
                                <span class="la la-sign-out ks-icon" aria-hidden="true"></span>
                                <span>Logout</span>
                            </a>
                        </div>
                    </div>
                    <!-- END NAVBAR USER -->
                </div>
                <!-- END NAVBAR ACTIONS -->
            </nav>
        </div>
        <!-- END MENUS -->
        <!-- END HEADER INNER -->
    </nav>
    <!-- END HEADER -->

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Home</h3>
                <div class="ks-controls">
                    <button type="button" class="btn btn-primary-outline ks-light ks-profile-tabs-block-toggle" data-block-toggle=".ks-profile > .ks-tabs-container">Tabs</button>
                    <button type="button" class="btn btn-primary-outline ks-light ks-settings-menu-block-toggle" data-block-toggle=".ks-settings-tab > .ks-menu">Menu</button>
                </div>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-social-profile">
                <div class="ks-social-profile-header">
                    <div class="ks-cover" style="background-image: url('<?=base_url()?>assets/img/hanura.jpeg')"></div>
                    <div class="ks-user">
                        <div class="ks-info">
                            <img src="<?=base_url()?>assets/img/hanuracalon.jpeg" class="ks-avatar" width="167" height="167">
                            <div class="ks-body">
                                <div class="ks-name">DPD Partai Hanura Sumatera Selatan</div>
                                <div class="ks-description"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="ks-tabs-container ks-tabs-default ks-tabs-no-separator ks-full ks-light">
                    <ul class="nav ks-nav-tabs ks-tabs-page-default ks-tabs-full-page">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" data-toggle="tab" data-target="#pemilih">Data Pemilih</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="tab" data-target="#survey">Survey Sumsel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="tab" data-target="#pilwako">Survey Palembang</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active ks-column-section" id="pemilih" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ks-tabs-container ks-tabs-default ks-tabs-no-separator">
                                    <ul class="nav ks-nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#" data-toggle="tab" data-target="#pie">Pie</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" data-toggle="tab" data-target="#bar">Bar</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="pie" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            5 Besar Jumlah Pemilih
                                                        </h5>
                                                        <div id="jum_dpt" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Jenis Kelamin
                                                        </h5>
                                                            <div id="kelamin" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Usia Pemilih
                                                        </h5>
                                                            <div id="usia_dpt" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Agama Pemilih
                                                        </h5>
                                                            <div id="agama_dpt" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Suku Pemilih
                                                        </h5>
                                                            <div id="suku_dpt" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Status Pemilih
                                                        </h5>
                                                            <div id="status_dpt" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Pendidikan Pemilih
                                                        </h5>
                                                            <div id="pendidikan_dpt" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Pekerjaan Pemilih
                                                        </h5>
                                                            <div id="pekerjaan_dpt" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Golongan Darah Pemilih
                                                        </h5>
                                                            <div id="golongan_darah_dpt" style="width: 100%;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="bar" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            5 Besar Jumlah Pemilih
                                                        </h5>
                                                        <div id="jum_dpt_bar" style="width: 350px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Jenis Kelamin
                                                        </h5>
                                                            <div id="kelamin_bar" style="width: 300px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Usia Pemilih
                                                        </h5>
                                                            <div id="usia_dpt_bar" style="width: 300px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Agama Pemilih
                                                        </h5>
                                                            <div id="agama_dpt_bar" style="width: 300px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Suku Pemilih
                                                        </h5>
                                                            <div id="suku_dpt_bar" style="width: 300px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Status Pemilih
                                                        </h5>
                                                            <div id="status_dpt_bar" style="width: 300px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Pendidikan Pemilih
                                                        </h5>
                                                            <div id="pendidikan_dpt_bar" style="width: 300px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Pekerjaan Pemilih
                                                        </h5>
                                                            <div id="pekerjaan_dpt_bar" style="width: 300px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Golongan Darah Pemilih
                                                        </h5>
                                                            <div id="golongan_darah_dpt_bar" style="width: 300px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="survey" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ks-tabs-container ks-tabs-default ks-tabs-no-separator">
                                    <ul class="nav ks-nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#" data-toggle="tab" data-target="#pie_1">Pie</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" data-toggle="tab" data-target="#bar_1">Bar</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="pie_1" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Tahu PILGUB 2018
                                                        </h5>
                                                        <div id="tahu_pilwali" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Elektabilitas
                                                        </h5>
                                                            <div id="popularitas" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Tingkat Kesukaan
                                                        </h5>
                                                            <div id="kesukaan" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Harapan Terhadap Perubahan
                                                        </h5>
                                                            <div id="harapan" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Latar Belakang Calon Yang Diharapkan
                                                        </h5>
                                                            <div id="latar_belakang" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Partai Politik Yang Akan Dipilih
                                                        </h5>
                                                            <div id="partai" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            TV Nasional Favorit
                                                        </h5>
                                                            <div id="tv" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Media Yang Sering Diakses
                                                        </h5>
                                                            <div id="media" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Organisasi
                                                        </h5>
                                                            <div id="organisasi" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="bar_1" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            % Tahu PILWALI 2018
                                                        </h5>
                                                        <div id="tahu_pilwali_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Elektabilitas
                                                        </h5>
                                                            <div id="popularitas_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Tingkat Kesukaan
                                                        </h5>
                                                            <div id="kesukaan_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Harapan Terhadap Perubahan
                                                        </h5>
                                                            <div id="harapan_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Latar Belakang Calon Yang Diharapkan
                                                        </h5>
                                                            <div id="latar_belakang_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Partai Politik Yang Akan Dipilih
                                                        </h5>
                                                            <div id="partai_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            TV Nasional Favorit
                                                        </h5>
                                                            <div id="tv_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Media Yang Sering Diakses
                                                        </h5>
                                                            <div id="media_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card panel panel-default">
                                                        <h5 class="card-header">
                                                            Organisasi
                                                        </h5>
                                                            <div id="organisasi_bar" style="width: 358px;height:300px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane ks-column-section" id="pilwako" role="tabpanel">
                        CONTENT 3
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/echarts.min.js"></script>
<script src="<?=base_url()?>libs/plyr/plyr.js"></script>

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="application/javascript">
(function ($) {
    $(document).ready(function () {
        plyr.setup();

        $('[data-toggle="tooltip"]').tooltip({
            delay: {
                show: 500
            }
        });
    });
})(jQuery);
</script>
<script type="text/javascript">
    Number.prototype.k = function () {
        // We don't want any thousands processing if the number is less than 1000.
        if (this < 1000) {
            // edit 2 May 2013: make sure it's a string for consistency
            return this.toString();
        }
        // Round to 100s first so that we can get the decimal point in there
        // then divide by 10 for thousands
        var thousands = Math.round(this / 100) / 10;
        // Now convert it to a string and add the k
        return thousands.toString() + 'K';
    };
</script>
<script type="text/javascript">

        var jum_dpt = echarts.init(document.getElementById('jum_dpt'));

        // specify chart configuration item and data
        var option = {
            legend: {
                orient: 'horizontal',
                x: 'left',
                data:[<?php foreach ($jml_pemiih as $value): ?>'<?php echo $value->nama_kab ?>',<?php endforeach ?>]
            },
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'left'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '10',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:[
                        <?php foreach ($jml_pemiih as $value): ?>
                            {value:<?php echo $value->jml ?>, name:'<?php echo $value->nama_kab ?>'},
                        <?php endforeach ?>
                    ]
                }
            ]
        };

        // use configuration item and data specified to show chart
        jum_dpt.setOption(option);

        var kelamin = echarts.init(document.getElementById('kelamin'));

        // specify chart configuration item and data
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?php echo $jenis_kelamin[0]->laki ?>, name:'Laki-Laki'},
                        {value:<?php echo $jenis_kelamin[0]->wanita?>, name:'Perempuan'}
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        kelamin.setOption(option);

        var usia_dpt = echarts.init(document.getElementById('usia_dpt'));

        // specify chart configuration item and data
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?php echo $usia[0]->jml_1 ?>, name:'17-25 Thn'},
                        {value:<?php echo $usia[0]->jml_2 ?>, name:'26-35 Thn'},
                        {value:<?php echo $usia[0]->jml_3 ?>, name:'36-45 Thn'},
                        {value:<?php echo $usia[0]->jml_4 ?>, name:'46-55 Thn'},
                        {value:<?php echo $usia[0]->jml_5 ?>, name:'> 56 Thn'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        usia_dpt.setOption(option);

        var agama_dpt = echarts.init(document.getElementById('agama_dpt'));

        // specify chart configuration item and data
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?php echo $usia[0]->jml_1 ?>, name:'Islam'},
                        {value:<?php echo $usia[0]->jml_2 ?>, name:'Kristen'},
                        {value:<?php echo $usia[0]->jml_3 ?>, name:'Protestan'},
                        {value:<?php echo $usia[0]->jml_4 ?>, name:'Hindu'},
                        {value:<?php echo $usia[0]->jml_5 ?>, name:'Budha'},
                        {value:<?php echo $usia[0]->jml_5 ?>, name:'Konghucu'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        agama_dpt.setOption(option);

        var suku_dpt = echarts.init(document.getElementById('suku_dpt'));

        // specify chart configuration item and data
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?php echo $usia[0]->jml_1 ?>, name:'Bugis'},
                        {value:<?php echo $usia[0]->jml_2 ?>, name:'Palembang'},
                        {value:<?php echo $usia[0]->jml_3 ?>, name:'Dayak'},
                        {value:<?php echo $usia[0]->jml_4 ?>, name:'Madura'},
                        {value:<?php echo $usia[0]->jml_5 ?>, name:'Sunda'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        suku_dpt.setOption(option);

        var status_dpt = echarts.init(document.getElementById('status_dpt'));

        // specify chart configuration item and data
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?php echo $status[0]->jml_kawin ?>, name:'Kawin'},
                        {value:<?php echo $status[0]->jml_belumkawin ?>, name:'Belum Kawin'}
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        status_dpt.setOption(option);

        var pendidikan_dpt = echarts.init(document.getElementById('pendidikan_dpt'));
        
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?php echo $usia[0]->jml_1 ?>, name:'S1'},
                        {value:<?php echo $usia[0]->jml_2 ?>, name:'S2'},
                        {value:<?php echo $usia[0]->jml_3 ?>, name:'S3'},
                        {value:<?php echo $usia[0]->jml_4 ?>, name:'SMA'},
                        {value:<?php echo $usia[0]->jml_5 ?>, name:'SMP'},
                        {value:<?php echo $usia[0]->jml_5 ?>, name:'SD'},
                        {value:<?php echo $usia[0]->jml_5 ?>, name:'Tidak Sekolah'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        pendidikan_dpt.setOption(option);

        var pekerjaan_dpt = echarts.init(document.getElementById('pekerjaan_dpt'));
        
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?php echo $usia[0]->jml_1 ?>, name:'WIRASWASTA'},
                        {value:<?php echo $usia[0]->jml_2 ?>, name:'PNS'},
                        {value:<?php echo $usia[0]->jml_3 ?>, name:'Pengangguran'},
                        {value:<?php echo $usia[0]->jml_4 ?>, name:'Pelajar'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        pekerjaan_dpt.setOption(option);

        var golongan_darah_dpt = echarts.init(document.getElementById('golongan_darah_dpt'));
        
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?php echo $usia[0]->jml_1 ?>, name:'A'},
                        {value:<?php echo $usia[0]->jml_2 ?>, name:'B'},
                        {value:<?php echo $usia[0]->jml_3 ?>, name:'AB'},
                        {value:<?php echo $usia[0]->jml_4 ?>, name:'O'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        golongan_darah_dpt.setOption(option);

        var jum_dpt_bar = echarts.init(document.getElementById('jum_dpt_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },

            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
                
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: [<?php foreach ($jml_pemiih as $value): ?>'<?php echo $value->nama_kab ?>', <?php endforeach ?>]
            },

            series: [<?php foreach ($jml_pemiih as $value): ?>
                        {
                            name: '<?php echo $value->nama_kab ?>',
                            type: 'bar',
                            data: [<?php echo $value->jml ?>]
                        },
                <?php endforeach ?>
            ]
        };

        jum_dpt_bar.setOption(option);

        var kelamin_bar = echarts.init(document.getElementById('kelamin_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },

            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['Laki-Laki','Perempuan']
            },
            series: [
                {
                    name: 'Laki-Laki',
                    type: 'bar',
                    data: [<?php echo $jenis_kelamin[0]->laki ?>]
                },
                {
                    name: 'Perempuan',
                    type: 'bar',
                    data: [<?php echo $jenis_kelamin[0]->wanita ?>]
                },
            ]
        };

        kelamin_bar.setOption(option);

        var usia_dpt_bar = echarts.init(document.getElementById('usia_dpt_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['17-25','26-35','36-45','46-55','>56']
            },
            series: [
                {
                    name: '17-25',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: '26-35',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_2 ?>]
                },
                {
                    name: '36-45',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_3 ?>]
                },
                {
                    name: '46-55',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_4 ?>]
                },
                {
                    name: '>56',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_5 ?>]
                },
            ]
        };

        usia_dpt_bar.setOption(option);

        var agama_dpt_bar = echarts.init(document.getElementById('agama_dpt_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['Islam','Kristen','Protestan','Hindu','Budha','Konghucu']
            },
            series: [
                {
                    name: 'Islam',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'Kristen',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_2 ?>]
                },
                {
                    name: 'Protestan',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_3 ?>]
                },
                {
                    name: 'Hindu',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_4 ?>]
                },
                {
                    name: 'Budha',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_5 ?>]
                },
                {
                    name: 'Konghucu',
                    type: 'bar',
                    data: [5000]
                },
            ]
        };

        agama_dpt_bar.setOption(option);

        var suku_dpt_bar = echarts.init(document.getElementById('suku_dpt_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },

            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['Bugis','Palembang','Dayak','Madura','Sunda']
            },
            series: [
                {
                    name: 'Bugis',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'Palembang',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'Dayak',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'Madura',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'Sunda',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
            ]
        };

        suku_dpt_bar.setOption(option);

        var status_dpt_bar = echarts.init(document.getElementById('status_dpt_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['Kawin','Belum Kawin']
            },
            series: [
                {
                    name: 'Kawin',
                    type: 'bar',
                    data: [<?php echo $status[0]->jml_kawin ?>]
                },
                {
                    name: 'Belum Kawin',
                    type: 'bar',
                    data: [<?php echo $status[0]->jml_belumkawin ?>]
                },
            ]
        };

        status_dpt_bar.setOption(option);

        var pendidikan_dpt_bar = echarts.init(document.getElementById('pendidikan_dpt_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }

            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show: false
            },
            legend: {
                data: ['S1','S2','S3','SMA','SMP','SD','TIDAK SEKOLAH',]
            },
            series: [
                {
                    name: 'S1',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'S2',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_2 ?>]
                },
                {
                    name: 'S3',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_3 ?>]
                },
                {
                    name: 'SMA',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_4 ?>]
                },
                {
                    name: 'SMP',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_5 ?>]
                },
                {
                    name: 'SD',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'TIDAK SEKOLAH',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
            ]
        };

        pendidikan_dpt_bar.setOption(option);

        var pekerjaan_dpt_bar = echarts.init(document.getElementById('pekerjaan_dpt_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },

            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show: false
            },
            legend: {
                data: ['WIRASWASTA','PNS','Pengangguran','Pelajar']
            },
            series: [
                {
                    name: 'WIRASWASTA',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'PNS',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_2 ?>]
                },
                {
                    name: 'Pengangguran',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_3 ?>]
                },
                {
                    name: 'Pelajar',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_4 ?>]
                },
            ]
        };

        pekerjaan_dpt_bar.setOption(option);

        var golongan_darah_dpt_bar = echarts.init(document.getElementById('golongan_darah_dpt_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['A','B','AB','O']
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show: false
            },
            legend: {
                data: ['A','B','AB','O']
            },
            series: [
                {
                    name: 'A',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_1 ?>]
                },
                {
                    name: 'B',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_2 ?>]
                },
                {
                    name: 'AB',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_3 ?>]
                },
                {
                    name: 'O',
                    type: 'bar',
                    data: [<?php echo $usia[0]->jml_4 ?>]
                },
            ]
        };

        golongan_darah_dpt_bar.setOption(option);
</script>

<script type="text/javascript">
        var tahu_pilwali = echarts.init(document.getElementById('tahu_pilwali'));

        // specify chart configuration item and data
        var option = {
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:55.5, name:'Tahu'},
                        {value:44.5, name:'Tidak Tahu'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };

        // use configuration item and data specified to show chart
        tahu_pilwali.setOption(option);

        // based on prepared DOM, initialize echarts instance
        var popularitas = echarts.init(document.getElementById('popularitas'));

        // specify chart configuration item and data

        var option = {
            legend: {
                orient: 'horizontal',
                x: 'left',
                data:['IDA FITRIATI BASYUNI', 'NOVIRZAH DJAZULI', 'SOFYAN DJAMAL', 'ALPIAN', 'RUSLAN ABDUL GANI']
            },
            tooltip : {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'left'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '10',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:[
                        {value:40, name:'IDA FITRIATI BASYUNI'},
                        {value:30, name:'NOVIRZAH DJAZULI'},
                        {value:10, name:'SOFYAN DJAMAL'},
                        {value:5, name:'ALPIAN'},
                        {value:15, name:'RUSLAN ABDUL GANI'},
                    ],
                }
            ]
        };

        // use configuration item and data specified to show chart
        popularitas.setOption(option);

        // based on prepared DOM, initialize echarts instance
        var kesukaan = echarts.init(document.getElementById('kesukaan'));

        // specify chart configuration item and data
        var option = {
            legend: {
                orient: 'horizontal',
                x: 'left',
                data:['IDA FITRIATI BASYUNI', 'NOVIRZAH DJAZULI', 'SOFYAN DJAMAL', 'ALPIAN', 'RUSLAN ABDUL GANI']
            },
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'left'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '10',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:[
                        {value:40, name:'IDA FITRIATI BASYUNI'},
                        {value:30, name:'NOVIRZAH DJAZULI'},
                        {value:10, name:'SOFYAN DJAMAL'},
                        {value:5, name:'ALPIAN'},
                        {value:15, name:'RUSLAN ABDUL GANI'},
                    ],
                }
            ]
        };

        // use configuration item and data specified to show chart
        kesukaan.setOption(option);

        // based on prepared DOM, initialize echarts instance
        var harapan = echarts.init(document.getElementById('harapan'));

        // specify chart configuration item and data
        var option = {
            legend: {
                orient: 'horizontal',
                x: 'left',
                data:['Programnya baik', 'Cerdas', 'Kinerja baik', 'Jujur', 'Dekat dengan rakyat']
            },
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'left'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '10',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:[
                        {value:40, name:'Programnya baik'},
                        {value:30, name:'Cerdas'},
                        {value:10, name:'Kinerja baik'},
                        {value:5, name:'Jujur'},
                        {value:15, name:'Dekat dengan rakyat'},
                    ],
                }
            ]
        };

        // use configuration item and data specified to show chart
        harapan.setOption(option);

        // based on prepared DOM, initialize echarts instance
        var latar_belakang = echarts.init(document.getElementById('latar_belakang'));

        // specify chart configuration item and data
        var option = {
            legend: {
                orient: 'horizontal',
                x: 'left',
                data:['TNI/POLRI aktif', 'Pensiunan TNI/POLRI', 'Tokoh politik', 'Pimpinan PARPOL', 'Birokrat']
            },
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'left'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '10',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:[
                        {value:40, name:'TNI/POLRI aktif'},
                        {value:30, name:'Pensiunan TNI/POLRI'},
                        {value:10, name:'Tokoh politik'},
                        {value:5, name:'Pimpinan PARPOL'},
                        {value:15, name:'Birokrat'},
                    ],
                }
            ]
        };

        // use configuration item and data specified to show chart
        latar_belakang.setOption(option);

        var partai = echarts.init(document.getElementById('partai'));

        // specify chart configuration item and data
        var option = {
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:240, name:'NASDEM '},
                        {value:160, name:'PKB '},
                        {value:160, name:'PKS '},
                        {value:160, name:'PDIP '},
                        {value:160, name:'GOLKAR '}
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        partai.setOption(option);

        var tv = echarts.init(document.getElementById('tv'));

        // specify chart configuration item and data
        var option = {
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:240, name:'RCTI'},
                        {value:160, name:'SCTV'},
                        {value:160, name:'METRO TV'},
                        {value:160, name:'TRANS TV'},
                        {value:160, name:'TRANS 7'}
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        tv.setOption(option);

        var media = echarts.init(document.getElementById('media'));

        // specify chart configuration item and data
        var option = {
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:240, name:'Facebook'},
                        {value:160, name:'Twitter'},
                        {value:160, name:'Google'},
                        {value:160, name:'Instagram'},
                        {value:160, name:'Youtube'},
                        {value:160, name:'Portal Berita Lokal'},
                        {value:160, name:'Lainnya'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        media.setOption(option);

        var organisasi = echarts.init(document.getElementById('organisasi'));

        // specify chart configuration item and data
        var option = {
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'shadow'
                }
            },
            series : [
                {
                    name: 'PILWALI 2018',
                    type: 'pie',
                    radius : '50%',
                    center: ['50%', '50%'],
                    data:[
                        {value:240, name:'NU'},
                        {value:160, name:'Muhammadiyah'},
                        {value:160, name:'GKII'},
                        {value:160, name:'GKPI'},
                        {value:160, name:'Katholik'},
                        {value:160, name:'Lainnya'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        organisasi.setOption(option);

        var tahu_pilwali_bar = echarts.init(document.getElementById('tahu_pilwali_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                 data: ['Tahu','Tidak Tahu']
            },
            series: [
                {
                    name: 'Tahu',
                    type: 'bar',
                    data: [55.5]
                },
                {
                    name: 'Tidak Tahu',
                    type: 'bar',
                    data: [44.5]
                },
            ]
        };

        tahu_pilwali_bar.setOption(option);

        var popularitas_bar = echarts.init(document.getElementById('popularitas_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                 data: ['IDA FITRIATI BASYUNI', 'NOVIRZAH DJAZULI', 'SOFYAN DJAMAL', 'ALPIAN', 'SARIMUDA']
            },
            series: [
                {
                    name: 'IDA FITRIATI BASYUNI',
                    type: 'bar',
                    data: [40]
                },
                {
                    name: 'NOVIRZAH DJAZULI',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'SOFYAN DJAMAL',
                    type: 'bar',
                    data: [15]
                },
                {
                    name: 'ALPIAN',
                    type: 'bar',
                    data: [5]
                },
                {
                    name: 'SARIMUDA',
                    type: 'bar',
                    data: [50]
                },
            ]
        };

        popularitas_bar.setOption(option);

        var kesukaan_bar = echarts.init(document.getElementById('kesukaan_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                 data: ['IDA FITRIATI BASYUNI', 'NOVIRZAH DJAZULI', 'SOFYAN DJAMAL', 'ALPIAN', 'SARIMUDA']
            },
            series: [
                {
                    name: 'IDA FITRIATI BASYUNI',
                    type: 'bar',
                    data: [40]
                },
                {
                    name: 'NOVIRZAH DJAZULI',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'SOFYAN DJAMAL',
                    type: 'bar',
                    data: [15]
                },
                {
                    name: 'ALPIAN',
                    type: 'bar',
                    data: [5]
                },
                {
                    name: 'SARIMUDA',
                    type: 'bar',
                    data: [50]
                },
            ]
        };

        kesukaan_bar.setOption(option);

        var harapan_bar = echarts.init(document.getElementById('harapan_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                 data: ['Programnya baik', 'Cerdas', 'Kinerja baik', 'Jujur', 'Dekat dengan rakyat']
            },
            series: [
                {
                    name: 'Programnya baik',
                    type: 'bar',
                    data: [40]
                },
                {
                    name: 'Cerdas',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'Kinerja baik',
                    type: 'bar',
                    data: [10]
                },
                {
                    name: 'Jujur',
                    type: 'bar',
                    data: [5]
                },
                {
                    name: 'Dekat dengan rakyat',
                    type: 'bar',
                    data: [15]
                },
            ]
        };

        harapan_bar.setOption(option);

        var latar_belakang_bar = echarts.init(document.getElementById('latar_belakang_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                 data: ['TNI/POLRI aktif', 'Pensiunan TNI/POLRI', 'Tokoh politik', 'Pimpinan PARPOL', 'Birokrat']
            },
            series: [
                {
                    name: 'TNI/POLRI aktif',
                    type: 'bar',
                    data: [40]
                },
                {
                    name: 'Pensiunan TNI/POLRI',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'Birokrat',
                    type: 'bar',
                    data: [10]
                },
                {
                    name: 'Tokoh politik',
                    type: 'bar',
                    data: [5]
                },
                {
                    name: 'Pimpinan PARPOL',
                    type: 'bar',
                    data: [15]
                },
            ]
        };

        latar_belakang_bar.setOption(option);

        var partai_bar = echarts.init(document.getElementById('partai_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['NASDEM', 'PKB', 'PKS', 'PDIP', 'GOLKAR']
            },
            series: [
                {
                    name: 'NASDEM',
                    type: 'bar',
                    data: [240]
                },
                {
                    name: 'PKB',
                    type: 'bar',
                    data: [120]
                },
                {
                    name: 'PKS',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'PDIP',
                    type: 'bar',
                    data: [100]
                },
                {
                    name: 'GOLKAR',
                    type: 'bar',
                    data: [50]
                },
            ]
        };

        partai_bar.setOption(option);

        var tv_bar = echarts.init(document.getElementById('tv_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['RCTI', 'SCTV', 'METRO TV', 'TRANS TV', 'TRANS 7']
            },
            series: [
                {
                    name: 'RCTI',
                    type: 'bar',
                    data: [240]
                },
                {
                    name: 'SCTV',
                    type: 'bar',
                    data: [120]
                },
                {
                    name: 'METRO TV',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'TRANS TV',
                    type: 'bar',
                    data: [100]
                },
                {
                    name: 'TRANS 7',
                    type: 'bar',
                    data: [50]
                },
            ]
        };

        tv_bar.setOption(option);

        var media_bar = echarts.init(document.getElementById('media_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },
            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['Facebook', 'Twitter', 'Google', 'Instagram', 'Youtube', 'Lokal', 'Lainnya']
            },
            series: [
                {
                    name: 'Facebook',
                    type: 'bar',
                    data: [240]
                },
                {
                    name: 'Twitter',
                    type: 'bar',
                    data: [120]
                },
                {
                    name: 'Google',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'Instagram',
                    type: 'bar',
                    data: [100]
                },
                {
                    name: 'Youtube',
                    type: 'bar',
                    data: [50]
                },
                {
                    name: 'Lokal',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'Lainnya',
                    type: 'bar',
                    data: [30]
                },
            ]
        };

        media_bar.setOption(option);

        var organisasi_bar = echarts.init(document.getElementById('organisasi_bar'));
        
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLabel:{
                    fontSize:8,
                    formatter: function (value) {
                        return value.k(); 
                    } 
                }
            },

            yAxis: {
                type: 'category',
                data: ['Pemilih'],
                show : false
            },
            legend: {
                data: ['NU', 'Muhammadiyah', 'GKII', 'GKPI', 'Katholik', 'Lainnya']
            },
            series: [
                {
                    name: 'NU',
                    type: 'bar',
                    data: [240]
                },
                {
                    name: 'Muhammadiyah',
                    type: 'bar',
                    data: [120]
                },
                {
                    name: 'GKII',
                    type: 'bar',
                    data: [30]
                },
                {
                    name: 'GKPI',
                    type: 'bar',
                    data: [100]
                },
                {
                    name: 'Katholik',
                    type: 'bar',
                    data: [50]
                },
                {
                    name: 'Lainnya',
                    type: 'bar',
                    data: [30]
                },
            ]
        };

        organisasi_bar.setOption(option);

</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>