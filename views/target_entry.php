<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
</head>
<!-- END HEAD -->
<?php include 'template/header.php'; ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>
    <div class="ks-column ks-page">
        <?php if ($role_detail == 'All'){?>
        <div class="ks-page-header" style="width: 100%">
            <section class="ks-title-and-subtitle">
                <div class="ks-title-block">
                    <h3 class="ks-main-title"><?=$nama_wilayah?></h3>
                    <!-- <div id="sub-title" class="ks-sub-title">Target : <?=$target?>%</div> -->
                </div>                
            </section>
        </div>
        <?php }else{ ?>
        <div class="ks-page-header" style="width: 100%">
            <section class="ks-title-and-subtitle">
                <div class="ks-title-block">
                    <h3 class="ks-main-title"><?=$nama_wilayah?></h3>
                    <div id="sub-title" class="ks-sub-title">Target : <?=$target?>%</div>
                </div>
                <?php if($tipe_wilayah == 'provinsi'){ ?>
                    <button class="btn btn-primary ks-rounded btn-target">Add/Edit Target</button>
                <?php } ?>
                
            </section>
        </div>
        <?php } ?>
        
        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card ks-card-widget ks-widget-payment-card-rate-details">
                                    <br>    
                                    <div class="card-block">
                                        <div class="table-responsive" id="table"></div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add/Edit Target</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="form_input">
            <div class="modal-body">
                <fieldset class="form-group">
                    <label class="form-control-label">ID</label>
                    <input type="text" class="form-control" name="id" readonly>
                </fieldset>
                <fieldset class="form-group">
                    <label class="form-control-label">Tipe</label>
                    <input type="text" class="form-control" name="tipe" readonly>
                </fieldset>
                <fieldset class="form-group">
                    <label class="form-control-label">Target</label>
                    <input type="text" class="form-control form-control-success" name="target" maxlength="5" onkeypress="">
                </fieldset>   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

                

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/echarts.min.js"></script>
<script src="<?=base_url()?>libs/plyr/plyr.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    var id = '<?=$id_wilayah?>';
    var tipe = '<?=$tipe_wilayah?>';
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
        load_table();
    });

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $( ".btn-target" ).click(function() {
        load_form_add(id,tipe);
    });

    function load_form_add(id,tipe){
        $.ajax({
            type:'POST',
            url:"<?php echo base_url(); ?>target/get_target",
            dataType: 'json',
            data: {id:id,tipe:tipe},
            success:function(data){
                $("[name=target]").val(data.target);
                $("[name=id]").val(id);
                $("[name=tipe]").val(tipe);
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    alert(xhr.responseJSON);
                }else{
                    message = xhr.status+' : '+error;
                    alert(message);
                }
            }
        }); 
        $("#modal_tambah").modal('show');
    }
    $( "#form_input" ).submit(function( event ) {
        event.preventDefault();

        $.ajax({
            url: "<?= site_url().'target/edit_target'?>",
            type : 'post',
            data : $( "#form_input" ).serialize(),
            dataType: "json",
            success : function(data){
                $("#modal_tambah").modal('hide');
                if(data){
                    $("#sub-title").html('Target :'+data+'%');    
                }
                load_table();
                // table.ajax.reload();
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    alert(xhr.responseJSON);
                }else{
                    message = xhr.status+' : '+error;
                    alert(message);
                }
            }                
        });
    });

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>

<script type="text/javascript">

    var vtipe = '<?=$tipe_wilayah?>';
    var vid = <?=$id_wilayah?>;
    var vnama = '<?=$nama_wilayah?>';
    $("#header").text(vnama);
    function load_table(){     
        $("#table").empty();
        $.ajax({
            type:'POST',
            url:"<?php echo base_url(); ?>target/load_table/",
            data: {tipe:vtipe,id:vid,nama:vnama},
            success:function(msg){
                if(msg == 'N'){
                    window.location = "<?=base_url()?>";
                }else{
                    $("#table").html(msg);    
                    $("#header").text(vnama);
                }
            
                // load_informasi(vid,vtipe,vnama,vlogo);
            },
            error: function(result){
                $("#table").html("Error"); 
            },
            fail:(function(status) {
                $("#table").html("Fail");
            }),
            beforeSend:function(d){
                $("#table").html("<center><strong style='color:red'>Please Wait...<br><img class='daft-spinner' src='<?=base_url();?>assets/spinner.png'></strong></center>");
            }
        }); 
    }
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>