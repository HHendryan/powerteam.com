
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/profile/settings.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/widgets/payment.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/widgets/panels.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Dashboard Score</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid ks-rows-section">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card panel panel-default">
                                    <h5 class="card-header">
                                        Masuk
                                    </h5>
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-xl-3">
                                                <div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
                                                    <div class="ks-statistics">
                                                        <span class="ks-amount" data-count-up="3119">6,555</span>
                                                        <span class="ks-text">Hari Ini</span>
                                                    </div>
                                                    <div class="ks-chart">
                                                        <div id="ks-memory-radial-progress-chart" class="ks-radial-progress-chart ks-white"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3">
                                                <div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
                                                    <div class="ks-statistics">
                                                        <span class="ks-amount" data-count-up="3119">6,555</span>
                                                        <span class="ks-text">Kemarin</span>
                                                    </div>
                                                    <div class="ks-chart">
                                                        <div id="ks-memory-radial-progress-chart" class="ks-radial-progress-chart ks-white"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3">
                                                <div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
                                                    <div class="ks-statistics">
                                                        <span class="ks-amount" data-count-up="3119">6,555</span>
                                                        <span class="ks-text">Minggu Ini</span>
                                                    </div>
                                                    <div class="ks-chart">
                                                        <div class="ks-radial-progress-chart ks-white"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3">
                                                <div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
                                                    <div class="ks-statistics">
                                                        <span class="ks-amount" data-count-up="3119">6,555</span>
                                                        <span class="ks-text">Total</span>
                                                    </div>
                                                    <div class="ks-chart">
                                                        <div id="ks-memory-radial-progress-chart" class="ks-radial-progress-chart ks-white"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card panel panel-default">
                                    <h5 class="card-header">
                                        Pulsa Terpakai
                                    </h5>
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-xl-3">
                                                <div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
                                                    <div class="ks-statistics">
                                                        <span class="ks-amount" data-count-up="3119">6,555</span>
                                                        <span class="ks-text">Hari Ini</span>
                                                    </div>
                                                    <div class="ks-chart">
                                                        <div id="ks-memory-radial-progress-chart" class="ks-radial-progress-chart ks-white"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3">
                                                <div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
                                                    <div class="ks-statistics">
                                                        <span class="ks-amount" data-count-up="3119">6,555</span>
                                                        <span class="ks-text">Kemarin</span>
                                                    </div>
                                                    <div class="ks-chart">
                                                        <div id="ks-memory-radial-progress-chart" class="ks-radial-progress-chart ks-white"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3">
                                                <div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
                                                    <div class="ks-statistics">
                                                        <span class="ks-amount" data-count-up="3119">6,555</span>
                                                        <span class="ks-text">Minggu Ini</span>
                                                    </div>
                                                    <div class="ks-chart">
                                                        <div class="ks-radial-progress-chart ks-white"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3">
                                                <div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
                                                    <div class="ks-statistics">
                                                        <span class="ks-amount" data-count-up="3119">6,555</span>
                                                        <span class="ks-text">Total</span>
                                                    </div>
                                                    <div class="ks-chart">
                                                        <div id="ks-memory-radial-progress-chart" class="ks-radial-progress-chart ks-white"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/echarts.min.js"></script>
<script src="<?=base_url()?>libs/plyr/plyr.js"></script>

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<div class="ks-mobile-overlay"></div>
</body>
</html>