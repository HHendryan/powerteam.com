<?php 
if($id == 0){
    $wil = 'Provinsi';
}else{
    if($tipe == 'kecamatan'){
        $wil = 'Kelurahan';
        $table1 = 'Target Kec';
        $table2 = 'Target Kel';
        $table3 = 'AVG Target TPS';
    }else if($tipe == 'kabupaten'){
        $wil = 'Kecamatan';
        $table1 = 'Target Kab';
        $table2 = 'Target Kec';
        $table3 = 'AVG Target Kel';
    }else if($tipe == 'provinsi'){
        $wil = 'Kabupaten';
        $table1 = 'Target Prov';
        $table2 = 'Target Kab';
        $table3 = 'AVG Target Kec';
    }
}?>
<?php if($id == 0){?>
<table id="dataTable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>No.</th>
            <th>Provinsi</th>
            <th>Target Provinsi</th>
            <th>Action</th>     
        </tr>
    </thead>
    <tbody>
        <?php foreach ($wilayah as $key => $value) { ?>
        <tr>
            <td style="text-align: left"><?= $value->id?></td>
            <td style="text-align: left"><?= $value->provinsi?></td>
            <td style="text-align: right"><?= $value->target?>%</td>
            <td style="text-align: center"><button type="button" data-id="<?=$value->idProv?>" data-tipe="provinsi" class="btn btn-success btn-sm btn-edit" style="height: 26px;">Edit Target</button>&nbsp;&nbsp;<button type="button" data-id="<?=$value->idProv?>" data-nama="<?=$value->provinsi?>" data-tipe="provinsi" class="btn btn-success btn-sm btn-link" style="height: 26px;">to Provinsi</button></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php }else{?>
<table id="dataTable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>No.</th>
            <th><?=$wil?></th>
            <th><?=$table1?></th>
            <th><?=$table2?></th>
            <?php if($tipe != 'kecamatan'){ ?>
                <th><?=$table3?></th>
            <?php } ?>
            <th>Action</th>     
        </tr>
    </thead>
    <tbody>
        <?php foreach ($wilayah as $key => $value) { ?>
        <tr>
            <td style="text-align: left"><?= $value->id?></td>
            <td style="text-align: left"><?= $value->name?></td>
            <td style="text-align: right"><?= $value->target_wil?>%</td>
            <td style="text-align: right"><?= $value->target_child?>%</td>
            <?php if($tipe != 'kecamatan'){ ?>
                <td style="text-align: right"><?= $value->average?>%</td>
            <?php } ?>
            <td style="text-align: center"><?= $value->button?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php }?>

<script type="text/javascript">
    $("#dataTable1").dataTable({
    dom: 'frt',
    "lengthMenu": [[-1], ["All"]]
});
</script>
<script type="text/javascript">
    $("#dataTable1").on("click", ".btn-edit",function(event){
        var id = $(this).data('id');
        var tipe = $(this).data('tipe');
        load_form_add(id,tipe);
    });

    $("#dataTable1").on("click", ".btn-link",function(event){
        vid = $(this).data('id');
        vtipe = $(this).data('tipe');
        vnama = $(this).data('nama');
        load_table();
    });
</script>