
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.min.css">
    <!-- END THEME STYLES -->
    
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
</head>
<!-- END HEAD -->

<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title" id="head_table">
                <h3>Data Koordinator Lapangan</h3>
                <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah KORLAP</button>
            </section>

        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <div class="table-responsive">
                                <h5>KORCAM : <?php echo $nama ?></h5>
                                <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Jalur</th>
                                            <th>Kel</th>
                                            <th>TPS</th>
                                            <th>Nama</th>
                                            <th>HP</th>
                                            <th>PEMILIH</th>
                                            <th>SMS</th>
                                            <th>LIHAT</th>
                                            <th>ΣHP</th>
                                            <th>Aktif</th>
                                            <th>Mati</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah KORLAP</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group row">
                        <div class="col-8">
                            <input class="form-control" onkeypress="return isNumberKey(event)" type="search" id="nik_search" placeholder="NIK">
                        </div>
                        <div class="col-2">
                            <button type="button"  onclick="cari()" class="btn btn-primary">Cari</button>
                        </div>
                    </div>
                    <div class="row" id="data_dpt" style="display: none">
                        <div class="col-lg-6">
                            <h5 class="modal-title">DATA DPT</h5>
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td>KELURAHAN</td>
                                        <td>:</td>
                                        <td id="kelurahan"></td>
                                    </tr>
                                    <tr>
                                        <td>TPS</td>
                                        <td>:</td>
                                        <td id="tps"></td>
                                    </tr>
                                    <tr>
                                        <td>KK</td>
                                        <td>:</td>
                                        <td id="kk"></td>
                                    </tr>
                                    <tr>
                                        <td>NIK</td>
                                        <td>:</td>
                                        <td id="nik"></td>
                                    </tr>
                                    <tr>
                                        <td>NAMA</td>
                                        <td>:</td>
                                        <td id="nama"></td>
                                    </tr>
                                    <tr>
                                        <td>TEMPAT LAHIR</td>
                                        <td>:</td>
                                        <td id="tempatlahir"></td>
                                    </tr>
                                    <tr>
                                        <td>TANGGAL LAHIR</td>
                                        <td>:</td>
                                        <td id="tanggallahir"></td>
                                    </tr>
                                    <tr>
                                        <td>UMUR</td>
                                        <td>:</td>
                                        <td id="umur"></td>
                                    </tr>
                                    <tr>
                                        <td>STATUS KAWIN</td>
                                        <td>:</td>
                                        <td id="statuskawin"></td>
                                    </tr>
                                    <tr>
                                        <td>JK</td>
                                        <td>:</td>
                                        <td id="jk"></td>
                                    </tr>
                                    <tr>
                                        <td>ALAMAT</td>
                                        <td>:</td>
                                        <td id="alamat"></td>
                                    </tr>
                                    <tr>
                                        <td>RT</td>
                                        <td>:</td>
                                        <td id="rt"></td>
                                    </tr>
                                    <tr>
                                        <td>RW</td>
                                        <td>:</td>
                                        <td id="rw"></td>
                                    </tr>  
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="modal-title"></h5>
                            <form id="input">
                                <input type="hidden" id="id_dpt" name="id_dpt">
                                <input type="hidden" name="korlap" value="<?php echo $id ?>">
                                <div class="form-group">
                                    <label class="form-control-label">KTA</label>
                                    <input type="text" class="form-control" id="kta" name="kta" placeholder="KTA" onkeypress="return isNumberKey(event)">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Jalur</label>
                                    <select id="jalur" class="form-control ks-select" name="jalur" style="width: 100%">
                                        <option disabled selected value>PILIH</option>
                                        <?php foreach ($jalur as $value) { ?>
                                        <option value="<?php echo $value->jalur ?>"><?php echo $value->jalur ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">No. HP</label>
                                    <input type="text" class="form-control" id="hp" name="hp" placeholder="HP" onkeypress="return isNumberKey(event)">
                                </div>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit KORCAM</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="update">
                        <input type="hidden" id="id_edit" name="id">
                        <div class="form-group">
                            <label class="form-control-label">KTA</label>
                            <input type="text" class="form-control" id="kta_edit" name="kta" placeholder="KTA" onkeypress="return isNumberKey(event)">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Jalur</label>
                            <select id="jalur_edit" class="form-control ks-select" name="jalur" style="width: 100%">
                                <option disabled selected value>PILIH</option>
                                <?php foreach ($jalur as $value) { ?>
                                <option value="<?php echo $value->jalur ?>"><?php echo $value->jalur ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">No. HP</label>
                            <input type="text" class="form-control" id="hp_edit" name="hp" placeholder="HP" onkeypress="return isNumberKey(event)">
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<input type="hidden" id="id" value="<?php echo $id ?>">

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        var table = $('#ks-datatable').DataTable({
            ajax: '<?=site_url('tim/get_data_korlap/')?>'+$("#id").val(),
                columns: [
                            { data: 'jalur' },
                            { data: 'kelurahan' },
                            { data: 'tps' },
                            { data: 'nama' },
                            { data: 'hp' },
                            { data: 'pemilih' },
                            { data: 'sms' },
                            { data: 'lihat' },
                            { data: 'jum_hp' },
                            { data: 'aktif' },
                            { data: 'mati' },
                            { data: 'button' }
                        ],
            "autoWidth": false,
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
            ],
            initComplete: function (settings, json) {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
                table.buttons().container().appendTo('#head_table');
            }
        });
        table.buttons().container().appendTo( '#ks-datatable_wrapper .col-md-6:eq(0)' );
        $('#jalur').select2({
            placeholder: "Pilih Jalur"
        });
        $('#jalur_edit').select2({
            placeholder: "Pilih Jalur"
        });
    });
})(jQuery);
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
}
function cari(){
    var nik = $("#nik_search").val();
    if (!nik){
        alert('NIK harus diisi');
    }else{
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'tim/cari_dpt_for_tim/'?>"+nik,
                type : 'get',
                dataType: "json",
                success : function(data){
                    if(data=='N'){
                        alert('data tidak ada atau sudah tergabung dengan tim lain');
                    }else{
                        $('#id_dpt').val(data[0].id);
                        $('#kelurahan').html(data[0].kelurahan);
                        $('#tps').html(data[0].tps);
                        $('#kk').html(data[0].nkk);
                        $('#nik').html(data[0].nik);
                        $('#nama').html(data[0].nama);
                        $('#tempatlahir').html(data[0].tempat_lahir);
                        $('#tanggallahir').html(data[0].tanggal_lahir);
                        $('#umur').html(data[0].umur);
                        $('#statuskawin').html(data[0].kawin);
                        $('#jk').html(data[0].pekerjaan);
                        $('#alamat').html(data[0].alamat);
                        $('#rt').html(data[0].rt);
                        $('#rw').html(data[0].rw);
                        $("#data_dpt").show();
                    }
                },
                error : function(){
                    alert('GAGAL');
                }         
            });
        })
        
        
    }
}
$('#input').submit(function(event){
    event.preventDefault();
                
    Pace.track(function(){
        $.ajax({
            url: "<?= site_url().'tim/input_korlap'?>",
            type : 'post',
            data : $( "#input" ).serialize(),
            dataType: "json",
            success : function(data){
                console.log(data);
                var table = $('#ks-datatable').DataTable();
                table.ajax.reload();
                $("#input")[0].reset();
                $("#data_dpt").hide();
                $("#modal_tambah").modal('hide');                
            },
            error: function(data){
                alert('ERROR');
            }
                        
        });
    });
    return false;
});

$("#ks-datatable").on("click", ".btn-edt",function(event){
    $.ajax({
        url : "<?php echo site_url().'tim/get_tim/'?>"+$(this).data('id'),
        type: "GET",
        dataType: "JSON",
        success: function(data){
            console.log(data);
            $('#id_edit').val(data[0].id);
            $('#kta_edit').val(data[0].kta);
            $("#jalur_edit").val(data[0].jalur).trigger("change");
            $('#hp_edit').val(data[0].hp);
            $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    });
});

$('#update').submit(function(event){
    event.preventDefault();

    Pace.track(function(){
        $.ajax({
            url: "<?= site_url().'tim/update_tim'?>",
            type : 'post',
            data : $( "#update" ).serialize(),
            dataType: "json",
            success : function(data){
                var table = $('#ks-datatable').DataTable();
                table.ajax.reload();
                $("#update")[0].reset();
                $("#modal_edit").modal('hide'); 
            },
            error : function(data){
                alert('GAGAL');
            }
                        
        });
    });
});

$("#ks-datatable").on("click", ".btn-del",function(event){
    var table = $('#ks-datatable').DataTable();
    row = $(this).parents('tr');
    id = $(this).data('id');
    if(confirm('Are you sure delete this data?')){
        event.preventDefault();
        // ajax delete data to database
        Pace.track(function(){
            $.ajax({
                url : "<?php echo site_url('tim/delete_korlap')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    table.row(row).remove().draw();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert('Error get data from ajax');
                }
            });
        });
    }
});
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>