
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/notie/notie.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Data Sudah Upload</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <!-- <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah Data</button> -->
                            <!--button class="btn btn-success" data-toggle="modal" data-target="#modal_excel">Upload Excel</button>
                            <button class="btn btn-danger btn-hapus">DELETE</button-->
                            <!-- <br><br> -->
                            <div class="table-responsive">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button> -->

<!-- Modal -->
<?php echo form_open_multipart('Form/upload_dpt_v2');?>
<div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Upload Data DPT</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      	<table class="table table-striped table-bordered" cellspacing="0" width="100%">
      		<tbody>
      			<tr>
      				<td>ID</td>
      				<td>
      					<span id="iddetail"></span>
      					<input type="hidden" id="iddetailinput" name="id" />
      					<input type="hidden" name="page" value="<?php echo site_url('Form/upload_sudah')?>" />
      				</td>
      			</tr>
      			<tr>
      				<td>Provinsi</td>
      				<td id="provinsi"></td>
      			</tr>
      			<tr>
      				<td>Kabupaten/Kota</td>
      				<td id="kabupaten"></td>
      			</tr>
      			<tr>
      				<td>Kecamatan</td>
      				<td id="kecamatan"></td>
      			</tr>
      			<tr>
      				<td>Kelurahan</td>
      				<td id="kelurahan"></td>
      			</tr>
      			<tr>
      				<td>File CSV</td>
      				<td>
      					<input type="file" onchange="" name="file" />
      					<input type="hidden" id="dpt" />
      				</td>
      			</tr>
      		</tbody>
      	</table>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="delete_id" value="" />
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="" onclick="">Upload</button>
      </div>
    </div>
  </div>
</div>
</form>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>assets/notie/notie.min.js"></script>

<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        $('.flatpickr').flatpickr();
    });
})(jQuery);
</script>
<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
        // $("#kabupaten1").hide();
        $("#kabupaten1_container").hide();
        $("#kabupaten_container").hide();
        $('#div_kec').hide();
        $('#div_kel').hide();
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    }
                );
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "<?php echo site_url('Form/get_data_dpt_sudah_upload')?>", "type": "POST"},
            columns: [
                {
                    "data": "id","title": "No",
                    "orderable": false
                },
                {"data": "provinsi","title": "Provinsi"},
                {"data": "kabupaten","title": "Kabupaten"},
                {"data": "kecamatan","title": "Kecamatan"},
                {"data": "kelurahan","title": "Kelurahan"},
                {"data": "dpt","title": "DPT"},
                {"data": "button","title": "Action"},
            ],
            order: [[1, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>

<!-- CRUD JAVASCRIPT -->
<script type="text/javascript">
    function uploadData() {
    	window.location = '<?php echo site_url('Form/form_data_dpt_upload?id=')?>';
    }

    function detailData(id) {
    	$.ajax({
    		url: '<?php echo site_url('Form/get_data_dpt_upload_detail?id=')?>'+id,
    		dataType: 'json',
    		type: 'get',
    		success: function(json) {
    			$('#provinsi').html(json.provinsi);
    			$('#kabupaten').html(json.kabupaten);
    			$('#kecamatan').html(json.kecamatan);
    			$('#kelurahan').html(json.kelurahan);
    			$('#iddetail').html(json.id);
    			$('#iddetailinput').val(json.id);
    		}
    	});
    }

    function globalReadFile(hidden_id,photo) {
  
	  if (photo.files && photo.files[0]) {

	    var FR= new FileReader();

	    FR.addEventListener("load", function(e) {
	    	//alert(e.target.result);
	      var val = e.target.result.split('base64,')[1];
	      document.getElementById(hidden_id).value = val;
	      //uploadPhotoTemp(hidden_id,val);
	    }); 

	    FR.readAsDataURL( photo.files[0] );

	    
	  }

	  //uploadPhotoTemp(hidden_id);
	  
	}

	function uploadPhotoTemp(idImage) {
	  var image = $('#'+idImage).val();
	  var idx = $('#iddetailinput').val();
	  //alert(image);
	  $.ajax({
	    url: '<?php echo site_url('Form/upload_dpt')?>',
	    dataType: 'json',
	    type: 'post',
	    data: { i: image, id: idx },
	    success: function(json) {
	      if(json.status_code == '000') {
	      	alert(json.status);
	      	$('#modalUpload').hide();
	      	window.location = 'upload_sudah';
	      }
	    }
	  });
	}
</script>
<script type="text/javascript">
    function openDetailDPT(id) {
    	window.location = '<?php echo site_url('Form/data_list_dpt?id=')?>'+id;	
    }
</script>
<script type="text/javascript">
    
</script>


<div class="ks-mobile-overlay"></div>
</body>
</html>