 
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.min.css">
    <!-- END THEME STYLES -->
    
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
</head>
<!-- END HEAD -->

<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title-and-subtitle" id="head_table">
                <div class="ks-title-block">
                    <h3 class="ks-main-title">Data Kab/Kota</h3>
                    <div class="ks-sub-title">Provinsi : <?php echo $sub_header ?></div>
                </div>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <div class="form-group">
                                <label>% PP&ensp;</label>
                                <select id="pp" class="form-control ks-select" name="pp" style="width: 10%">
                                    <option disabled selected value>PILIH</option>
                                    <option value="0.05">5%</option>
                                    <option value="0.1">10%</option>
                                    <option value="0.15">15%</option>
                                    <option value="0.2">20%</option>
                                    <option value="0.25">25%</option>
                                    <option value="0.3">30%</option>
                                    <option value="0.35">35%</option>
                                    <option value="0.4">40%</option>
                                    <option value="0.45">45%</option>
                                    <option value="0.5">50%</option>
                                    <option value="0.55">55%</option>
                                    <option value="0.6">60%</option>
                                    <option value="0.65">65%</option>
                                    <option value="0.7">70%</option>
                                    <option value="0.75">75%</option>
                                    <option value="0.8">80%</option>
                                    <option value="0.85">85%</option>
                                    <option value="0.9">90%</option>
                                    <option value="0.95">95%</option>
                                    <option value="1.0">100%</option>
                                </select>
                            </div>
                            <div class="table-responsive">
                                <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Kab/Kota</th>
                                        <th>Kec</th>
                                        <th>Kel</th>
                                        <th>TPS</th>
                                        <th>DPT</th>
                                        <th id="pp_header">PP(70%)</th>
                                        <th>T(%)</th>
                                        <th>TGT</th>
                                        <th>KORWIL</th>
                                        <th>KORCAM</th>
                                        <th>KORLAP</th>
                                        <th>Pemilih</th>
                                        <th>Realisasi</th>
                                        <th>Persen</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Kab/Kota</th>
                                        <th>Kec</th>
                                        <th>Kel</th>
                                        <th>TPS</th>
                                        <th>DPT</th>
                                        <th>PP(70%)</th>
                                        <th>T(%)</th>
                                        <th>TGT</th>
                                        <th>KORWIL</th>
                                        <th>KORCAM</th>
                                        <th>KORLAP</th>
                                        <th>Pemilih</th>
                                        <th>Realisasi</th>
                                        <th>Persen</th>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>

<input type="hidden" id="id" value="<?php echo $id ?>">

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
    $('#pp').select2({
        placeholder: "Pilih PP"
    });
    $("#pp").change(function(){
        pp = 'PP('+($(this).val()*100)+'%)'
        $('#pp_header').html(pp);
        var table = $('#ks-datatable').DataTable();
        table.ajax.url( '<?=site_url('data/get_data_kab/')?>'+$("#id").val()+'/'+$(this).val() ).load();
    })
</script>

<script type="application/javascript">
	(function ($) {
	    $(document).ready(function() {
	        var table = $('#ks-datatable').DataTable({
	        	ajax: '<?=site_url('data/get_data_kab/')?>'+$("#id").val()+'/'+0.7,
	        	columns: [
							{ data: 'kabupaten' },
							{ data: 'jum_kec' },
							{ data: 'jum_kel' },
							{ data: 'jum_tps' },
							{ data: 'jum_dpt' },
							{ data: 'pp' },
							{ data: 't' },
							{ data: 'target' },
							{ data: 'korwil' },
							{ data: 'korcam' },
							{ data: 'korlap' },
							{ data: 'pemilih' },
							{ data: 'real' },
							{ data: 'persen' }
						],
	            "autoWidth": false,
	            buttons: [
                    { extend: 'copyHtml5', footer: true },
                    { extend: 'excelHtml5', footer: true },
                    { extend: 'csvHtml5', footer: true },
                    { extend: 'pdfHtml5', footer: true }
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
         
                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,.]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var totalkab = api
                        .column( 1 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalkec = api
                        .column( 2 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalkel = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totaltps = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totaldpt = api
                        .column( 5 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalpp = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totaltarget = api
                        .column( 8 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalkorwil = api
                        .column( 9 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalkorcam = api
                        .column( 10 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalkorlap = api
                        .column( 11 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );
                    var totalpemilih = api
                        .column( 12 )
                        .data()
                        .reduce( function (a, b) {
                            return (intVal(a) + intVal(b)).toLocaleString();
                        }, 0 );

                    $( api.column( 0 ).footer() ).html('Total');
                    $( api.column( 1 ).footer() ).html(totalkab);
                    $( api.column( 2 ).footer() ).html(totalkec);
                    $( api.column( 3 ).footer() ).html(totalkel);
                    $( api.column( 4 ).footer() ).html(totaltps);
                    $( api.column( 5 ).footer() ).html(totaldpt);
                    $( api.column( 7 ).footer() ).html(totalpp);
                    $( api.column( 8 ).footer() ).html(totaltarget);
                    $( api.column( 9 ).footer() ).html(totalkorwil);
                    $( api.column( 10 ).footer() ).html(totalkorcam);
                    $( api.column( 11 ).footer() ).html(totalkorlap);
                    $( api.column( 12 ).footer() ).html(totalpemilih);
                },
	            initComplete: function (settings, json) {
	                $('.dataTables_wrapper select').select2({
	                    minimumResultsForSearch: Infinity
	                });
	                table.buttons().container().appendTo('#head_table');
	            }
	        });

	        table.buttons().container().appendTo( '#ks-datatable_wrapper .col-md-6:eq(0)' );
	    });
	})(jQuery);
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>