
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/open-sans/styles.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">

<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pages/auth.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
</head>
<body>

<div class="ks-page">
    <div class="ks-page-content">
        <div class="ks-logo"></div>

        <div class="card panel panel-default ks-light ks-panel ks-login">
            <div class="card-block">
                <form class="form-container" method="post" action="<?=site_url('auth/daftar')?>">
                    <h4 class="ks-header"><a href="<?=base_url()?>" class="ks-logo"><img src="<?=base_url()?>/assets/img/teknopol.png"></a></h4>
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="text" class="form-control" placeholder="Username" name="username">
                            <span class="icon-addon">
                                <span class="la la-at"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            <span class="icon-addon">
                                <span class="la la-key"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <select id="level" class="form-control ks-select" name="level" style="width: 100%">
                            <option disabled selected value>PILIH</option>
                            <option value="3">PILPRES</option>
                            <option value="2">PILGUB</option>
                            <option value="1">PILWALI / PILBUP</option>
                            <!-- <option value="pileg">PILEG</option> -->
                        </select>
                    </div>
                    <div class="form-group" style="display: none" id="form-provinsi">
                        <select id="provinsi" class="form-control ks-select" name="provinsi" style="width: 100%">
                        	<option disabled selected value>PILIH</option>
                        	<?php foreach ($provinsi as $value) { ?>
                        		<option value="<?php echo $value->id ?>"><?php echo $value->provinsi ?></option>
                        	<?php } ?>
                        </select>
                    </div>
                    <div class="form-group" style="display: none" id="form-kabupaten">
                        <select id="kabupaten" class="form-control ks-select" name="kabupaten" style="width: 100%">
                        </select>
                    </div>
                    <div class="form-group" style="display: none" id="form-kecamatan">
                        <select id="kecamatan" class="form-control ks-select" name="kecamatan" style="width: 100%">
                        </select>
                    </div>
                    <div class="form-group" style="display: none" id="form-sub_level">
                        <select id="sub-level" class="form-control ks-select" name="sub_level" style="width: 100%">
                        	<option disabled selected value>PILIH</option>
                        	<option value="dprd">DPRD</option>
                            <option value="dprd_prov">DPRD PROVINSI</option>
                            <option value="dprd_kab">DPRD DAERAH</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-crusta btn-block">SIGN UP</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script type="text/javascript">
    $('#level').select2({
        placeholder: "Pilih Level",
    });
    $('#provinsi').select2({
        placeholder: "Pilih Provinsi"
    });
    $('#kabupaten').select2({
        placeholder: "Pilih kabupaten"
    });
    $('#kecamatan').select2({
        placeholder: "Pilih Kecamatan"
    });
    $('#sub_level').select2({
        placeholder: "Pilih Sub Level"
    });
</script>

<script type="text/javascript">
    $("#level").change(function(){
        if($(this).val() == '3'){
            $('#form-provinsi').hide(); 
                $("#provinsi").select2("val", "PILIH");
            $('#form-kabupaten').hide();
                $("#kabupaten").select2("val", "PILIH");
        }
        if($(this).val() != '3'){
            $('#form-provinsi').show();
                $("#provinsi").select2("val", "PILIH");
            $('#form-kabupaten').hide();
                $("#kabupaten").select2("val", "PILIH"); 
        }
    })

    $("#provinsi").change(function(){
        if ($('#level').val()=='2'){
            
        }else{
            $('#form-kabupaten').show();
                $("#kabupaten").select2("val", "PILIH");
            $('#form-kecamatan').hide();
                $("#kecamatan").select2("val", "PILIH");
            $('#form-sub_level').hide();    
                $("#sub_level").select2("val", "PILIH");
            $.ajax({
                url: "<?php echo site_url('auth/get_select_kab') ?>",
                cache: false,
                type:"POST",
                data:{id_prov:$(this).val()},
                success: function(respond){
                    $("#kabupaten").html(respond);
                }
            })
        }
        
    })
</script>
<!-- <script type="text/javascript">
	$("#level").change(function(){
		if($(this).val()!='pilpres' && $(this).val()!='pileg'){
			$('#form-provinsi').show();	
				$("#provinsi").select2("val", "PILIH");
			$('#form-kabupaten').hide();
				$("#kabupaten").select2("val", "PILIH");
			$('#form-kecamatan').hide();
				$("#kecamatan").select2("val", "PILIH");
			$('#form-sub_level').hide();
				$("#sub_level").select2("val", "PILIH");
		}
		if($(this).val()=='pileg'){
			$('#form-provinsi').hide();	
				$("#provinsi").select2("val", "PILIH");
			$('#form-kabupaten').hide();
				$("#kabupaten").select2("val", "PILIH");
			$('#form-kecamatan').hide();
				$("#kecamatan").select2("val", "PILIH");
			$('#form-sub_level').show();	
				$("#sub_level").select2("val", "PILIH");
		}
		if($(this).val()=='pilpres'){
			$('#form-provinsi').hide();	
				$("#provinsi").select2("val", "PILIH");
			$('#form-kabupaten').hide();
				$("#kabupaten").select2("val", "PILIH");
			$('#form-kecamatan').hide();
				$("#kecamatan").select2("val", "PILIH");
			$('#form-sub_level').hide();	
				$("#sub_level").select2("val", "PILIH");
		}

	})
	$("#provinsi").change(function(){
		if ($('#level').val()=='pilgub'){

		}else{
			$('#form-kabupaten').show();
				$("#kabupaten").select2("val", "PILIH");
			$('#form-kecamatan').hide();
				$("#kecamatan").select2("val", "PILIH");
			$('#form-sub_level').hide();	
				$("#sub_level").select2("val", "PILIH");
			$.ajax({
		    	url: "<?php echo site_url('auth/get_select_kab') ?>",
		    	cache: false,
		    	type:"POST",
		    	data:{id_prov:$(this).val()},
		    	success: function(respond){
		    		$("#kabupaten").html(respond);
		      	}
		    })
		}
		
	})
	$("#sub-level").change(function(){
		alert('cek');
	})
</script> -->
</body>
</html>