
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
</head>
<!-- END HEAD -->

<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title" id="head_table">
                <h3>BAWASLU</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah Data</button>
                            <br><br>
                            <div class="table-responsive">
                                <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tingkatan</th>
                                        <th>Jabatan</th>
                                        <th>Partner</th>
                                        <th>Prospek</th>
                                        <th>Progress Partner</th>
                                        <th>HP</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="input">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tingkatan</label>
                            <select id="tingkatan" class="form-control ks-select" name="tingkatan" style="width: 100%">
                                <option disabled selected value>PILIH</option>
                                <option value="BAWASLU Nasional">BAWASLU Nasional</option>
                                <option value="BAWASLU Provinsi">BAWASLU Provinsi</option>
                                <option value="BAWASLU Kabupaten">BAWASLU Kabupaten</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Jabatan</label>
                            <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Partner</label>
                            <input type="text" class="form-control" id="partner" name="partner" placeholder="Partner">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Prospek</label>
                            <input type="text" class="form-control" id="prospek" name="prospek" placeholder="Prospek">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Pogress Partner</label>
                            <input type="text" class="form-control" id="progress_partner" name="progress_partner" placeholder="Progress Partner">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">No HP</label>
                            <input type="text" class="form-control" id="hp" name="hp" placeholder="No HP">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="update">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <input type="hidden" id="id_edit" name="id">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama</label>
                            <input type="text" class="form-control" id="nama_edit" name="nama" placeholder="Nama">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tingkatan</label>
                            <select id="tingkatan_edit" class="form-control ks-select" name="tingkatan" style="width: 100%">
                                <option disabled selected value>PILIH</option>
                                <option value="BAWASLU Nasional">BAWASLU Nasional</option>
                                <option value="BAWASLU Provinsi">BAWASLU Provinsi</option>
                                <option value="BAWASLU Kabupaten">BAWASLU Kabupaten</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Jabatan</label>
                            <input type="text" class="form-control" id="jabatan_edit" name="jabatan" placeholder="Jabatan">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Partner</label>
                            <input type="text" class="form-control" id="partner_edit" name="partner" placeholder="Partner">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Prospek</label>
                            <input type="text" class="form-control" id="prospek_edit" name="prospek" placeholder="Prospek">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Pogress Partner</label>
                            <input type="text" class="form-control" id="progress_partner_edit" name="progress_partner" placeholder="Progress Partner">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">No HP</label>
                            <input type="text" class="form-control" id="hp_edit" name="hp" placeholder="No HP">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="application/javascript">  
    (function ($) {
        $(document).ready(function() {
            $('#tingkatan').select2({
                placeholder: "Pilih Tingkatan"
            });

            $('#tingkatan_edit').select2({
                placeholder: "Pilih Tingkatan"
            });

            var table = $('#ks-datatable').DataTable({
                ajax: '<?=site_url('penyelenggara/load_data_bawaslu')?>',
                columns: [
                            { data: 'nomor' },
                            { data: 'nama' },
                            { data: 'tingkatan' },
                            { data: 'jabatan' },
                            { data: 'partner' },
                            { data: 'prospek' },
                            { data: 'progress_partner' },
                            { data: 'hp' },
                            { data: 'button' }
                        ],
                "autoWidth": false,
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                ],
                initComplete: function (settings, json) {
                    $('.dataTables_wrapper select').select2({
                        minimumResultsForSearch: Infinity
                    });
                    table.buttons().container().appendTo('#head_table');
                }
            });

            table.buttons().container().appendTo( '#ks-datatable_wrapper .col-md-6:eq(0)' );
        });
    })(jQuery);

    $('#input').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'penyelenggara/add_bawaslu'?>",
                type : 'post',
                data : $( "#input" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#ks-datatable').DataTable();
                    table.ajax.reload();
                    $("#input")[0].reset();
                    $("#modal_tambah").modal('hide');                
                },
                error: function(data){
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });

    $("#ks-datatable").on("click", ".btn-edt",function(event){
        $.ajax({
            url : "<?php echo site_url().'penyelenggara/get_penyelenggara/'?>"+$(this).data('id'),
            type: "GET",
            dataType: "JSON",
            success: function(data){
                console.log(data);
                $('#id_edit').val(data.id);
                $('#nama_edit').val(data.nama);
                $("#tingkatan_edit").val(data.tingkatan).trigger("change");
                $('#jabatan_edit').val(data.jabatan);
                $('#partner_edit').val(data.partner);
                $('#progress_partner_edit').val(data.progress_partner);
                $('#prospek_edit').val(data.prospek);
                $('#hp_edit').val(data.hp);
                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $('#update').submit(function(event){
        event.preventDefault();

        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'penyelenggara/update_penyelenggara'?>",
                type : 'post',
                data : $( "#update" ).serialize(),
                dataType: "json",
                success : function(data){
                    var table = $('#ks-datatable').DataTable();
                    table.ajax.reload();
                    $("#update")[0].reset();
                    $("#modal_edit").modal('hide'); 
                },
                error : function(data){
                    alert('GAGAL');
                }
                            
            });
        });
    });

    $("#ks-datatable").on("click", ".btn-del",function(event){
        id = $(this).data('id');
        if(confirm('Are you sure delete this data?')){
            event.preventDefault();
            // ajax delete data to database
            Pace.track(function(){
                $.ajax({
                    url : "<?php echo site_url('penyelenggara/delete_penyelenggara')?>/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        var table = $('#ks-datatable').DataTable();
                        table.ajax.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        alert('Error get data from ajax');
                    }
                });
            });
        }
    });
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>