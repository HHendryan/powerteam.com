
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/notie/notie.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Data Koordinator</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah Data</button>
                            <button class="btn btn-success" data-toggle="modal" data-target="#modal_excel">Upload Excel</button>
                            <button class="btn btn-danger btn-hapus">DELETE</button>
                            <br><br>
                            <div class="table-responsive">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="input">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama</label>
                            <input type="text" class="form-control" name="cp" placeholder="Nama" >
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Alamat</label>
                            <textarea type="text" class="form-control" name="address" placeholder="Alamat" ></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">No. Telp</label>
                            <input type="text" class="form-control" name="phone" placeholder="No. Telp" onkeypress="return isNumberKey(event)" >
                        </div>
                    
                        <div class="form-group col-md-6">
                            <label class="form-control-label">NIK</label>
                            <input type="text" class="form-control" name="nik" placeholder="NIK" onkeypress="return isNumberKey(event)" >
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">KTA</label>
                            <input type="text" class="form-control" name="kta" placeholder="KTA" onkeypress="return isNumberKey(event)" >
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select" name="type" style="width: 100%" >
                                <?php if($role == 'korprov'){ ?>
                                    <option value="korkab">KORKAB</option>
                                    <option value="korcam">KORCAM</option>
                                    <option value="kordes">KORDES</option>
                                    <option value="kortps">KORTPS</option>
                                <?php }else if($role == 'korkab'){ ?>
                                    <option value="korcam">KORCAM</option>
                                    <option value="kordes">KORDES</option>
                                    <option value="kortps">KORTPS</option>
                                <?php }else{ ?>
                                    <option value="korprov">KORPROV</option>
                                    <option value="korkab">KORKAB</option>
                                    <option value="korcam">KORCAM</option>
                                    <option value="kordes">KORDES</option>
                                    <option value="kortps">KORTPS</option>
                                <?php } ?>
                            </select>
                        </div>

                        <?php if($role == 'korprov'){ ?>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Provinsi</label>
                                <select id="provinsi" class="form-control" name="id_provinces" style="width:100%" readonly>
                                    <option value="<?php echo $provinsi->id ?>"><?php echo $provinsi->name ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kab/Kota</label>
                                <select id="kabupaten" class="form-control ks-select" name="id_regency"  style="width:100%" >
                                    <option disabled selected value>PILIH</option>
                                    <?php foreach ($kabupaten as $value) { ?>
                                    <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kecamatan</label>
                                <select id="kecamatan" class="form-control ks-select" name="id_district"  style="width:100%" >
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kelurahan</label>
                                <select id="kelurahan" class="form-control ks-select" name="id_village"  style="width:100%" >
                                </select>
                            </div>
                        <?php }else if($role == 'korkab'){ ?>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Provinsi</label>
                                <select id="provinsi" class="form-control" name="id_provinces" style="width:100%" >
                                    <option value="<?php echo $provinsi->id ?>"><?php echo $provinsi->name ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kab/Kota</label>
                                <select id="kabupaten" class="form-control ks-select" name="id_regency"  style="width:100%" >
                                    <option value="<?php echo $kabupaten->id ?>"><?php echo $kabupaten->name ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kecamatan</label>
                                <select id="kecamatan" class="form-control ks-select" name="id_district"  style="width:100%" >
                                    <?php foreach ($kecamatan as $value) { ?>
                                        <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kelurahan</label>
                                <select id="kelurahan" class="form-control ks-select" name="id_village"  style="width:100%" >
                                </select>
                            </div>
                        <?php }else{ ?>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Provinsi</label>
                                <select id="provinsi" class="form-control" name="id_provinces" style="width:100%" >
                                    <option disabled selected value>PILIH</option>
                                    <?php foreach ($provinsi as $value) { ?>
                                    <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kab/Kota</label>
                                <select id="kabupaten" class="form-control ks-select" name="id_regency"  style="width:100%" >
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kecamatan</label>
                                <select id="kecamatan" class="form-control ks-select" name="id_district"  style="width:100%" >
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Kelurahan</label>
                                <select id="kelurahan" class="form-control ks-select" name="id_village"  style="width:100%" >
                                </select>
                            </div>
                        <?php } ?>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">TPS</label>
                            <select class="form-control ks-select" name="tps" id="tps" style="width: 100%" >
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="edit">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <input type="hidden" name="id">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama</label>
                            <input type="text" class="form-control" name="cp" placeholder="Nama" >
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Alamat</label>
                            <textarea type="text" class="form-control" name="address" placeholder="Alamat" ></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">No. Telp</label>
                            <input type="text" class="form-control" name="phone" placeholder="No. Telp" onkeypress="return isNumberKey(event)" >
                        </div>
                    
                        <div class="form-group col-md-6">
                            <label class="form-control-label">NIK</label>
                            <input type="text" class="form-control" name="nik" placeholder="NIK" onkeypress="return isNumberKey(event)" >
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <input type="text" class="form-control" name="type" placeholder="type" >
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Provinsi</label>
                            <select id="provinsi_edit" class="form-control" name="id_provinces" style="width:100%" readonly>
                                    <option value="<?php echo $provinsi->id ?>"><?php echo $provinsi->name ?></option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kab/Kota</label>
                            <select class="form-control ks-select select2" id="kab_edit" name="id_regency"  style="width:100%" >
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kecamatan</label>
                            <select class="form-control ks-select select2" id="kec_edit" name="id_district"  style="width:100%" >
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kelurahan</label>
                            <select class="form-control ks-select select2" id="kel_edit" name="id_village"  style="width:100%" >
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_excel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="upload_form">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">File Excel</label>
                            <input type="file" class="form-control" name="file" placeholder="File Excel" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Unique ID</label>
                            <input type="text" class="form-control" name="unique" placeholder="Unique ID" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta" id="save">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Anda yakin akan menghapus data ini?
                <input type="hidden" class="form-control" id="idguraklih" placeholder="" value="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <a href="#" id="submitDel" class="btn btn-danger success">Hapus</a>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>assets/notie/notie.min.js"></script>
<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        $('.flatpickr').flatpickr();
    });
})(jQuery);
</script>
<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('Form/get_data_coordinator')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    {"data": "cp","title": "Nama"},
                    {"data": "address","title": "Alamat"},
                    {"data": "phone","title": "Phone"},
                    {"data": "nik","title": "NIK"},
                    {"data": "type","title": "Tipe"},
                    {"data": "is_block","title": "Block"},
                    {"data": "button","title": "Action"},
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
    });
</script>

<!-- CRUD JAVASCRIPT -->
<script type="text/javascript">
    $('#input').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'form/add_guraklih'?>",
                type : 'post',
                data : $( "#input" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                    document.getElementById("input").reset();
                    $("#modal_tambah").modal('hide');                
                },
                error: function(data){
                    var a =  $( "#input" ).serialize();
                    alert(a);
                }
                            
            });
        });
        return false;
    });

    $('#edit').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'form/edit_coordinator'?>",
                type : 'post',
                data : $( "#edit" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#table').DataTable();
                    document.getElementById("edit").reset();
                    table.ajax.reload();
                    $("#modal_edit").modal('hide');                
                },
                error: function(data){
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });
	
	$("#table").on("click", ".btn-edit",function(event){
        value = $(this).data('id');
		console.log(value);
        $.ajax({
            url : "<?php echo site_url().'form/get_coordinator_by_id/'?>"+value,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                console.log(data);
                $('[name="id"]').val(data.id);
                $('[name="address"]').val(data.address);
                $('[name="type"]').val(data.type);
                $('[name="id_regency"]').val(data.id_village);
                $('[name="id_district"]').val(data.id_districts);
                $('[name="id_provinces"]').val(data.id_provinces);
                $('[name="id_village"]').val(data.id_village);
                $('[name="cp"]').val(data.cp);
                $('[name="nik"]').val(data.nik);
                $('[name="phone"]').val(data.phone);
                $('#kab_edit').append('<option value="'+data.id_kabupaten+'">'+data.kabupaten+'</option>');
                $('#kec_edit').append('<option value="'+data.id_kecamatan+'">'+data.kecamatan+'</option>');
                $('#kel_edit').append('<option value="'+data.id_kelurahan+'">'+data.kelurahan+'</option>');
                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $("#table").on("click", ".btn-delete",function(event){

        id = $(this).data('id');
        $('#confirm-submit').modal('show');
        $('#idguraklih').val(id);
    });

    $('#submitDel').click(function(){
        id = $("#idguraklih").val();
        event.preventDefault();
            
        Pace.track(function(){
            $.ajax({
                url : "<?php echo site_url('form/delete_coordinator')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                    $('#confirm-submit').modal('hide');
                },
                error: function (data){
                    console.log(data);
                    $('#confirm-submit').modal('hide');
                    alert('Error get data from ajax');
                }
            });
        });
    });

    $("#table").on("click", ".btn-blk",function(event){
        id = $(this).data('id');
        event.preventDefault();
            
        Pace.track(function(){
            $.ajax({
                url : "<?php echo site_url('form/block_coordinator')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                },
                error: function (data){
                    console.log(data);
                    alert('Error get data from ajax');
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $('#modal_edit').on('hidden.bs.modal', function(e){
        $("#kab_edit option").remove();
        $("#kel_edit option").remove();
        $("#kec_edit option").remove();
    });

    $('#modal_tambah').on('hidden.bs.modal', function(e){
        $("#kab_edit option").remove();
        $("#kel_edit option").remove();
        $("#kec_edit option").remove();

        $('[name="id"]').val('');
        $('[name="address"]').val('');
        $('[name="type"]').val('');
        $('[name="id_regency"]').val('');
        $('[name="id_district"]').val('');
        $('[name="id_provinces"]').val('');
        $('[name="id_village"]').val('');
        $('[name="cp"]').val('');
        $('[name="nik"]').val('');
        $('[name="phone"]').val('');

    });
</script>

<script type="text/javascript">
    $('.select2').select2();
    $('#provinsi').select2({
        placeholder: "Pilih Provinsi"
    });
    $('#kabupaten').select2({
        placeholder: "Pilih Kabupaten"
    });
    $('#kecamatan').select2({
        placeholder: "Pilih Kecamatan"
    });
    $('#kelurahan').select2({
        placeholder: "Pilih Kelurahan"
    });
    $('#tps').select2({
        placeholder: "Pilih TPS"
    });
</script>

<script type="text/javascript">
    $("#provinsi").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kabupaten").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kecamatan").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kecamatan").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kelurahan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kelurahan").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kelurahan").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_tps') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#tps").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#provinsi_edit").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kab_edit").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kab_edit").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kec_edit").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kec_edit").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kelurahan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kel_edit").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $('#upload_form').submit(function(e){
        e.preventDefault();
        var id = $('[name="unique"]').val();
        var id_uri = encodeURI(id);
        $.ajax({
            url: "<?php echo site_url('data/cek_unique_id/') ?>"+id_uri,
            cache: false,
            type:"POST",
            success: function(respond){
                if(respond == 0){
                    alert('UNIQUE ID sudah ada');
                }else{
                    $("#save").prop('disabled', true);
                    var form = $('#upload_form')[0];
                    var data = new FormData(form);
                    Pace.track(function(){
                        $.ajax({
                            url  : "<?= site_url().'data/upload_guraklih'?>",
                            type : 'POST',
                            enctype: 'multipart/form-data',
                            data : data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            timeout: 600000,
                            dataType: "json",
                            success: function(data){
                                $("#modal_excel").modal('hide'); 
                                $("#save").prop('disabled', false);
                                notie.alert({ type: 1, text: 'Data Berhasil Diupload sejumlah '+data, time: 2,position: 'bottom' })
                                var table = $('#table').DataTable();
                                table.ajax.reload();
                            },
                            error: function(xhr, status, error) {
                                var err = eval("(" + xhr.responseText + ")");
                                alert(err.Message);
                            }
                        });
                    })
                }
            }
        })
      // e.preventDefault();
      // 
    });
</script>
<script type="text/javascript">
    $( ".btn-hapus" ).click(function() {
        notie.input({
            text: 'Masukkan UNIQUE ID',
            submitText: 'Submit',
            cancelText: 'Cancel',
            position: 'bottom',
            cancelCallback: function (value) {

            },
            submitCallback: function (value) {
                id_uri = encodeURI(value);
                if(!id_uri){
                    alert('Unique ID HARUS DIISI');
                }else{
                    Pace.track(function(){
                        $.ajax({
                            url: "<?php echo site_url('data/check_jumlah_guraklih/') ?>"+id_uri,
                            type:"POST",
                            success: function(respond){
                                if(respond == 0){
                                    notie.alert({ type: 3, text: 'Data dengan unique id tersebut tidak ada', time: 2,position: 'bottom'})
                                }else{
                                    notie.confirm({
                                        text: respond,
                                        position: 'bottom',
                                        submitText: 'Hapus',
                                        cancelText: 'Cancel',
                                        cancelCallback: function () {
                                            
                                        },
                                        submitCallback: function () {
                                            Pace.track(function(){
                                                $.ajax({
                                                    url: "<?php echo site_url('data/hapus_data_guraklih/') ?>"+id_uri,
                                                    cache: false,
                                                    type:"POST",
                                                    success: function(respond){
                                                        if(respond == 1){
                                                            notie.alert({ type: 1, text: 'Data berhasil dihapus', time: 2,position: 'bottom'});
                                                            var table = $('#table').DataTable();
                                                            table.ajax.reload();
                                                        }else{
                                                            notie.alert({ type: 3, text: 'Data tidak berhasil dihapus', time: 2,position: 'bottom'});
                                                        }
                                                        
                                                    }
                                                })
                                            })
                                        }
                                    })
                                }
                            }
                        })
                    })    
                }
              },
        })
    });
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>