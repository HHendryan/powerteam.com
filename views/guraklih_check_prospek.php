
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>GURAKLIH</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title" id="head_table">
                <h3>GURAKLIH CEKLIS PROSPEK</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="form-group row">
                                           
                                           <div class="form-group col-sm-2">
                                                <label class="form-control-label">Provinsi</label>
                                                <input type="text" class="form-control" name="provinsi" placeholder="Provinsi" value="<?php echo $area->provinsi ?>" readonly>
                                                <input type="hidden" class="form-control" id="idprovinsi" placeholder="Provinsi" value="<?php echo $area->idProv ?>">
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Kabupaten</label>
                                                <input type="text" class="form-control" name="kabupaten" placeholder="Kabupaten" value="<?php echo $area->kabupaten ?>" readonly>
                                                <input type="hidden" class="form-control" id="idkabupaten" placeholder="Kabupaten" value="<?php echo $area->idKab ?>">
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Kecamatan</label>
                                                <input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan" value="<?php echo $area->kecamatan ?>" readonly>
                                                <input type="hidden" class="form-control" id="idkecamatan" placeholder="Kecamatan" value="<?php echo $area->idKec ?>">
                                            </div>

                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Kelurahan</label>
                                                <input type="text" class="form-control" id="kelurahan" name="kelurahan" placeholder="Kelurahan" value="<?php echo $area->kelurahan ?>" readonly>
                                                <input type="hidden" class="form-control" id="idkelurahan" placeholder="Kelurahan" value="<?php echo $area->idKel ?>">
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">TPS</label>
                                                <select id="tps" class="form-control ks-select" name="tps">
                                                     <option value="" disabled selected>Pilih TPS</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Guraklih</label>
                                                <select id="guraklih" class="form-control ks-select" name="guraklih">
                                                     <option disabled selected value>Pilih Guraklih</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label" style="visibility: hidden">Influencer</label>
                                                <button class="btn btn-crusta" onclick="cari();" style="width: 100%">Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>JK</th>
                                        <th>Alamat</th>
                                        <th>TPS</th>
                                        <th>Ceklis</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="row">
                            <div class="container">
                                <div class="pull-right">
                                    <div class="row">
                                        <div class="col-md-6 projects-list">
                                            <button class="btn btn-warning" onclick="saveChecked()">Submit</button>
                                        </div>
<!--                                          <div class="col-md-6 projects-list">
                                            <button class="btn btn-primary" onclick="saveFinish()" style="width: 100%; margin-left:5px; margin-top:20px;">Submit & Finish</button>
                                         </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Mohon tunggu....
            </div>
        </div>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.select.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    var table;
    var tableinfo;
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
        $('#tps').select2();

        table = $('#ks-datatable').dataTable();

        var idKel = $("#idkelurahan").val();
        $.ajax({
            url: "<?php echo site_url('data/get_tps') ?>",
            cache: false,
            type:"POST",
            data:{id: idKel},
            success: function(respond){
                //alert(respond);
                $("#tps").html(respond);
            }
        });
    });

    function cari(){
        var kel = $("#idkelurahan").val();
        var tps = $("#tps").val();

        if(tps==null|| tps=='all'){
            alert('Form TPS harus diisi');
        }else{
            Pace.track(function(){
                table.fnDestroy();
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#ks-datatable").dataTable({
                    columnDefs: [ {
                        orderable: false,
                        targets:   0
                    } ],
                    initComplete: function() {
                        var api = this.api();
                        $('#ks-datatable_filter input')
                            .off('.DT')
                            .on('keyup.DT', function(e) {
                                if (e.keyCode == 13) {
                                     api.search(this.value).draw();
                                }
                            });
                        },
                        ajax: {"url": '<?=site_url('data/load_data_dpt')?>/'+kel+'/'+tps, "type": "POST"},
                        columns: [
                            {"data": "id"},
                            {"data": "nik"},
                            {"data": "nama"},
                            {"data": "jns_kelamin"},
                            {"data": "alamat"},
                            {"data": "tps"},
                            {"data": "checkbox"},
                        ],

                        rowCallback: function(row, data, iDisplayIndex) {
                            var info = this.fnPagingInfo();
                            var page = info.iPage;
                            var length = info.iLength;
                            var index = page * length + (iDisplayIndex + 1);
                            $('td:eq(0)', row).html(index);
                        },

                        infoCallback: function( settings, start, end, max, total, pre ) {
                            var api = this.api();
                            var pageInfo = api.page.info();
                            tableinfo = api.page.info();
                            //return 'Page '+ (pageInfo.page+1) +' of '+ pageInfo.pages;
                        }
                });
                
                //table.buttons().container().appendTo( '#ks-datatable_wrapper .col-md-6:eq(0)' );
            });
        }
    }(jQuery);

    function saveChecked() {
        var idprov = $("#idprovinsi").val();
        var idkab = $("#idkabupaten").val();
        var idkec = $("#idkecamatan").val();
        var idkel = $("#idkelurahan").val();
        var tps= $("#tps").val();
        var guraklih = $("#guraklih").val();
        if(guraklih && tps){
            
            var array =[];
            table.$("input:checkbox[name=chk]:checked").each(function(){
                array.push($(this).val());
            });

            $.ajax({
                url: "<?php echo site_url('data/set_flag_dpt') ?>",
                cache: false,
                type:"POST",
                data:{array:array, id_prov:idprov, id_kab:idkab, id_kec:idkec, id_kel:idkel, tps:tps, guraklih_id:guraklih},
                success: function(respond){
                    table.api().ajax.reload();
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        alert(xhr.responseJSON);
                    }else{
                        message = xhr.status+' : '+error;
                        alert(message);
                    }
                } 
            });

        }else{
            alert('Silahkan pilih Guraklih dan TPS terlebih dahulu');
        }
    }


    function saveFinish() {
        var idprov = $("#idprovinsi").val();
        var idkab = $("#idkabupaten").val();
        var idkec = $("#idkecamatan").val();
        var idkel = $("#idkelurahan").val();
        var tps= $("#tps").val();
        if(tps){
            $.ajax({
                    url: "<?php echo site_url('data/set_flag_dpt_sum') ?>",
                    cache: false,
                    type:"POST",
                    data:{id_prov:idprov, id_kab:idkab, id_kec:idkec, id_kel:idkel, tps:tps},
                    success: function(respond){
                        alert(respond);
                    },
                    error: function (data){
                        console.log(data);
                        $('#confirm-submit').modal('hide');
                        alert('Error get data from ajax');
                    }
                });
        }else{
            alert('Silahkan pilih tps dan klik submit');
        }
    }
</script>

<script type="text/javascript">
    $('#provinsi').select2({
        placeholder: "Pilih Kabupaten"
    });
    $('#kabupaten').select2();
</script>
<script type="text/javascript">
    $("#provinsi").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kabupaten").html(respond);
                $('#kecamatan').val('').trigger("change");
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kecamatan").html(respond);
                $('#kelurahan').val('').trigger("change");
            }
        })
    })
</script>

<script type="text/javascript">
    $("#kecamatan").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_kelurahan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                //alert(respond);
                $("#kelurahan").html(respond);
                $('#tps').val('').trigger("change");
            }
        })
    })
</script>

<script type="text/javascript">
    $("#kelurahan").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_tps') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                //alert(respond);
                $("#tps").html(respond);
            }
        })
    })
</script>

<script type="text/javascript">
    $("#tps").change(function(){
        tps = $(this).val();
        kel = $("#idkelurahan").val();
        kec = $("#idkecamatan").val();
        kab = $("#idkabupaten").val();
        prov = $("#idprovinsi").val();
        $.ajax({
            url: "<?php echo site_url('data/get_guraklih') ?>",
            cache: false,
            type:"POST",
            data:{tps:tps,kel:kel,kec:kec,kab:kab,prov:prov},
            success: function(respond){
                // console.log(respond);
                $("#guraklih").html(respond);
            }
        })
    })
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>