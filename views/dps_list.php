
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/notie/notie.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>MENU DPS</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="form-group row">
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Provinsi</label>
                                                <select id="provinsi" class="form-control ks-select" name="provinsi">
                                                    <option disabled selected value>PILIH</option>
                                                    <?php foreach ($provinsi as $value) { ?>
                                                    <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Kabupaten</label>
                                                <select id="kabupaten" class="form-control ks-select" name="kabupaten">
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Kecamatan</label>
                                                <select id="kecamatan" class="form-control ks-select" name="kecamatan">
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Kelurahan</label>
                                                <select id="kelurahan" class="form-control ks-select" name="kelurahan">
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">TPS</label>
                                                <select id="tps" class="form-control ks-select" name="tps">
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <button class="btn btn-crusta" onclick="cari()" style="position:absolute;right:0;">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>assets/notie/notie.min.js"></script>

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="text/javascript">
    $('#provinsi').select2({
        placeholder: "Pilih Provinsi"
    });
    $('#kabupaten').select2({
        placeholder: "Pilih Kabupaten"
    });
    $('#kecamatan').select2({
        placeholder: "Pilih Kecamatan"
    });
    $('#kelurahan').select2({
        placeholder: "Pilih Kelurahan"
    });
    $('#tps').select2({
        placeholder: "Pilih TPS"
    });

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var t = $("#ks-datatable").dataTable({
        initComplete: function() {
            var api = this.api();
            $('#table_filter input')
                .off('.DT')
                .on('keyup.DT', function(e) {
                    if (e.keyCode == 13) {
                         api.search(this.value).draw();
                    }
                });
            },
            "autoWidth": false,
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "<?php echo site_url('DPS/load_dps/12/0/0/0/0')?>", "type": "POST"},
            columns: [
                {
                    "data": "id","title": "No",
                    "orderable": false
                },
                {"data": "no_kk","title": "No. KK"},
                {"data": "nik","title": "NIK"},
                {"data": "nama","title": "Nama"},
                {"data": "tempat_lahir","title": "Tempat Lahir"},
                {"data": "tanggal_lahir","title": "Tanggal Lahir"},
                // {"data": "button","title": "Action"},
            ],
            order: [[1, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
    });
</script>
<script type="text/javascript">
    $("#provinsi").change(function(){
        id = $(this).val();
        $.ajax({
            url: "<?php echo site_url('DPS/get_option/kabupaten/') ?>"+id,
            cache: false,
            type:"POST",
            success: function(respond){
                $("#kabupaten").html(respond);
            }
        })
    })
    $("#kabupaten").change(function(){
        id = $(this).val();
        $.ajax({
            url: "<?php echo site_url('DPS/get_option/kecamatan/') ?>"+id,
            cache: false,
            type:"POST",
            success: function(respond){
                $("#kecamatan").html(respond);
            }
        })
    })
    $("#kecamatan").change(function(){
        id = $(this).val();
        $.ajax({
            url: "<?php echo site_url('DPS/get_option/kelurahan/') ?>"+id,
            cache: false,
            type:"POST",
            success: function(respond){
                $("#kelurahan").html(respond);
            }
        })
    })

    $("#kelurahan").change(function(){
        id = $(this).val();
        $.ajax({
            url: "<?php echo site_url('DPS/get_option/tps/') ?>"+id,
            cache: false,
            type:"POST",
            success: function(respond){
                $("#tps").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    function cari(){
        provinsi = $("#provinsi").val();
        kabupaten = $("#kabupaten").val();
        kecamatan = $("#kecamatan").val();
        kelurahan = $("#kelurahan").val();
        tps = $("#tps").val();
        t = $("#ks-datatable").dataTable();
        if(provinsi && kabupaten && kecamatan && kelurahan && tps){
            t.api().ajax.url('<?=site_url('DPS/load_dps')?>'+'/'+provinsi+'/'+kabupaten+'/'+kecamatan+'/'+kelurahan+'/'+tps).load();
            // t.ajax.url( '<?=site_url('DPS/load_tps')?>'+'/'+provinsi+'/'+kabupaten+'/'+kecamatan+'/'+kelurahan ).load();
        }else{
            alert('PILIH WILAYAH DENGAN LENGKAP');
        }
    }
    $( ".btn-add" ).click(function() {
        provinsi = $("#provinsi").val();
        kabupaten = $("#kabupaten").val();
        kecamatan = $("#kecamatan").val();
        kelurahan = $("#kelurahan").val();
        t = $("#ks-datatable").dataTable();
        if(provinsi && kabupaten && kecamatan && kelurahan){
            $("[name=provinsi]").val(provinsi);
            $("[name=kabupaten]").val(kabupaten);
            $("[name=kecamatan]").val(kecamatan);
            $("[name=kelurahan]").val(kelurahan);
            $("#modal_tambah").modal('show');
        }else{
            alert('PILIH WILAYAH DENGAN LENGKAP');
        }
    });

    $("#input_tps").submit(function( event ) {
        $.ajax({
            url: "<?= site_url().'DPS/input_tps'?>",
            type : 'post',
            data : $( "#input_tps" ).serialize(),
            dataType: "json",
            success : function(data){
                var table = $('#ks-datatable').DataTable();
                table.ajax.reload();
                document.getElementById("input_tps").reset();
                $("#modal_tambah").modal('hide');               
            },
            error: function(data){
                console.log(data);
                alert('ERROR');
            }                  
        });
        alert( "Handler for .submit() called." );
        event.preventDefault();
    });

    $("#ks-datatable").on("click", ".btn-hapus",function(event){
        id = $(this).data('id');
        $.ajax({
          url : "<?php echo site_url('DPS/delete')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data){
            var table = $('#ks-datatable').DataTable();
            table.ajax.reload();
          },
          error: function (data){
            console.log(data);
            alert('Error get data from ajax');
          }
        });
    });

    $("#ks-datatable").on("click", ".btn-upload-biasa",function(event){
        provinsi = $(this).data('prov');
        kabupaten = $(this).data('kab');
        kecamatan = $(this).data('kec');
        kelurahan = $(this).data('kel');
        tps = $(this).data('tps');
        $("[name=provinsi]").val(provinsi);
        $("[name=kabupaten]").val(kabupaten);
        $("[name=kecamatan]").val(kecamatan);
        $("[name=kelurahan]").val(kelurahan);
        $("[name=tps]").val(tps);
        $("#modal_upload_biasa").modal('show');
    });

    $("#ks-datatable").on("click", ".btn-upload-gabung",function(event){
        provinsi = $(this).data('prov');
        kabupaten = $(this).data('kab');
        kecamatan = $(this).data('kec');
        kelurahan = $(this).data('kel');
        tps = $(this).data('tps');
        $("[name=provinsi]").val(provinsi);
        $("[name=kabupaten]").val(kabupaten);
        $("[name=kecamatan]").val(kecamatan);
        $("[name=kelurahan]").val(kelurahan);
        $("[name=tps]").val(tps);
        $("#modal_upload_gabung").modal('show');
    });
</script>
<script type="text/javascript">
    $('#upload_dps_biasa').submit(function(e){
        e.preventDefault();
        var id_prov = $('#provinsi_biasa').val();
        alert(id_prov);
        var id = $('[name="unique"]').val();
        var id_uri = encodeURI(id);
        $.ajax({
            url: "<?php echo site_url('DPS/cek_unique/') ?>"+id_uri+'/'+id_prov,
            cache: false,
            type:"POST",
            success: function(respond){
                if(respond == 0){
                    alert('UNIQUE ID sudah ada');
                }else{
                    $(".save").prop('disabled', true);
                    var form = $('#upload_dps_biasa')[0];
                    var data = new FormData(form);
                    Pace.track(function(){
                        $.ajax({
                            url  : "<?= site_url().'DPS/upload_biasa'?>",
                            type : 'POST',
                            enctype: 'multipart/form-data',
                            data : data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            timeout: 600000,
                            dataType: "json",
                            success: function(data){
                                $("#modal_upload_biasa").modal('hide'); 
                                $(".save").prop('disabled', false);
                                notie.alert({ type: 1, text: 'Data Berhasil Diupload sejumlah '+data, time: 2,position: 'bottom' })
                            },
                            error: function(xhr, status, error) {
                                var err = eval("(" + xhr.responseText + ")");
                                alert(err.Message);
                            }
                        });
                    })
                }
            }
        })
      // e.preventDefault();
      // 
    });
</script>
<script type="text/javascript">
    $('#upload_dps_gabung').submit(function(e){
        e.preventDefault();
        var id_prov = $('#provinsi_gabung').val();
        var id = $('[name="unique"]').val();
        var id_uri = encodeURI(id);
        $.ajax({
            url: "<?php echo site_url('DPS/cek_unique/') ?>"+id_uri+'/'+id_prov,
            cache: false,
            type:"POST",
            success: function(respond){
                if(respond == 0){
                    alert('UNIQUE ID sudah ada');
                }else{
                    $(".save").prop('disabled', true);
                    var form = $('#upload_dps_gabung')[0];
                    var data = new FormData(form);
                    Pace.track(function(){
                        $.ajax({
                            url  : "<?= site_url().'DPS/upload_gabung'?>",
                            type : 'POST',
                            enctype: 'multipart/form-data',
                            data : data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            timeout: 600000,
                            dataType: "json",
                            success: function(data){
                                $("#modal_upload_gabung").modal('hide'); 
                                $(".save").prop('disabled', false);
                                notie.alert({ type: 1, text: 'Data Berhasil Diupload sejumlah '+data, time: 2,position: 'bottom' })
                            },
                            error: function(xhr, status, error) {
                                var err = eval("(" + xhr.responseText + ")");
                                alert(err.Message);
                            }
                        });
                    })
                }
            }
        })
      // e.preventDefault();
      // 
    });
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>