
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/notie/notie.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Data Suara Legislator</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah Data</button>
                            <button class="btn btn-success" data-toggle="modal" data-target="#modal_excel">Upload Excel</button>
                            <!-- <button class="btn btn-danger btn-hapus">DELETE</button> -->
                            <br><br>
                            <div class="table-responsive">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="input_legislator">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select select2" id="type" name="type" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DPR RI</option>
                                <option value="2">DPRD PROVINSI</option>
                                <option value="3">DPRD KABUPATEN</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Periode</label>
                            <select class="form-control ks-select select2" name="periode" id="periode" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="2014">2014</option>
                                <option value="2019">2019</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">DAPIL</label>
                            <select class="form-control ks-select select2" name="id_dapil" id="id_dapil" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama</label>
                            <select class="form-control ks-select select2" name="id_legislator" id="id_legislator" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Partai</label>
                            <select class="form-control ks-select select2" name="id_parpol" id="id_parpol" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Provinsi</label>
                            <select id="provinsi" class="form-control" name="idProv" style="width:100%"  required>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kabupaten/ Kota</label>
                            <select class="form-control ks-select select2" id="kabupaten" name="idKab" style="width: 100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kecamatan</label>
                            <select id="kecamatan" class="form-control ks-select select2" name="idKec"  style="width:100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kelurahan</label>
                            <select id="kelurahan" class="form-control ks-select select2" name="idKel"  style="width:100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">TPS</label>
                            <input type="text" class="form-control" id="tps" name="tps" onkeypress="return isNumberKey(event)">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Suara</label>
                            <input type="text" class="form-control" id="suara" name="suara" onkeypress="return isNumberKey(event)" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">State</label>
                            <select class="form-control ks-select select2" name="state" id="state" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DD1</option>
                                <option value="2">DB1</option>
                                <option value="3">DA1</option>
                                <option value="4">C1</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="edit_legislator">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <input type="hidden" name="id">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select" id="edt_type" name="type" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DPR RI</option>
                                <option value="2">DPRD PROVINSI</option>
                                <option value="3">DPRD KABUPATEN</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Periode</label>
                            <select class="form-control ks-select" name="periode" id="edt_periode" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="2014">2014</option>
                                <option value="2019">2019</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">DAPIL</label>
                            <select class="form-control ks-select" name="id_dapil" id="edt_id_dapil" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama</label>
                            <select class="form-control ks-select" name="id_legislator" id="edt_id_legislator" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Partai</label>
                            <select class="form-control ks-select" name="id_parpol" id="edt_id_parpol" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Provinsi</label>
                            <select id="edt_provinsi" class="form-control" name="idProv" style="width:100%"  required>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kabupaten/ Kota</label>
                            <select class="form-control ks-select select2" id="edt_kabupaten" name="idKab" style="width: 100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kecamatan</label>
                            <select id="edt_kecamatan" class="form-control ks-select select2" name="idKec"  style="width:100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kelurahan</label>
                            <select id="edt_kelurahan" class="form-control ks-select select2" name="idKel"  style="width:100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">TPS</label>
                            <input type="text" class="form-control" id="edt_tps" name="tps" onkeypress="return isNumberKey(event)">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Suara</label>
                            <input type="text" class="form-control" id="edt_suara" name="suara" onkeypress="return isNumberKey(event)" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">State</label>
                            <select class="form-control ks-select" name="state" id="edt_state" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DD1</option>
                                <option value="2">DB1</option>
                                <option value="3">DA1</option>
                                <option value="4">C1</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" onclick="closeModalEdit()">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_excel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="upload_form">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select select2" id="type1" name="type1" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DPR RI</option>
                                <option value="2">DPRD PROVINSI</option>
                                <option value="3">DPRD KABUPATEN</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Periode</label>
                            <select class="form-control ks-select select2" name="periode1" id="periode1" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="2014">2014</option>
                                <option value="2019">2019</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">DAPIL</label>
                            <select class="form-control ks-select select2" name="id_dapil1" id="id_dapil1" style="width: 100%" required>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Provinsi</label>
                            <select id="provinsi1" class="form-control ks-select select2" name="idProv1" style="width:100%"  required>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kabupaten/ Kota</label>
                            <select class="form-control ks-select select2" id="kabupaten1" name="idKab1" style="width: 100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kecamatan</label>
                            <select id="kecamatan1" class="form-control ks-select select2" name="idKec1"  style="width:100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kelurahan</label>
                            <select id="kelurahan1" class="form-control ks-select select2" name="idKel1"  style="width:100%">
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">State</label>
                            <select class="form-control ks-select select2" name="state1" id="state1" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DD1</option>
                                <option value="2">DB1</option>
                                <option value="3">DA1</option>
                                <option value="4">C1</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Unique ID</label>
                            <input type="text" class="form-control" name="unique" placeholder="Unique ID" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">File Excel</label>
                            <input type="file" class="form-control" name="file" placeholder="File Excel" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta" id="save">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>assets/notie/notie.min.js"></script>
<?php include 'template/settings.php' ?>
<script type="application/javascript">
    $(document).ready(function() {
        $('.flatpickr').flatpickr();
        $('#menu_swift').trigger('change');
    });
    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
      return true;
    }

    function closeModalEdit() {

        var table = $('#table').DataTable();
        table.ajax.reload();     
        $("#kabupaten option").remove();
        $("#kecamatan option").remove();
        $("#provinsi option").remove();
        $("#suara").val('');
        $("#modal_edit").modal('hide');
    }
</script>
<script type="text/javascript">
    $('.select2').select2();
    $('#provinsi').select2({
        placeholder: "Pilih Provinsi"
    });
    $('#kabupaten').select2({
        placeholder: "Pilih Kabupaten/Kota"
    });
    $('#kecamatan').select2({
        placeholder: "Pilih Kecamatan"
    });
    $('#kecamatan').select2({
        placeholder: "Pilih Kecamatan"
    });
    $('#kelurahan').select2({
        placeholder: "Pilih Kelurahan"
    });

    $("#provinsi").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kabupaten").html(respond);
            }
        })
    });

    $("#edt_provinsi").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#edt_kabupaten").html(respond);
            }
        })
    });

    $("#provinsi1").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kabupaten1").html(respond);
            }
        })
    });

    $("#kabupaten").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kecamatan").html(respond);
            }
        })
    });

    $("#edt_kabupaten").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#edt_kecamatan").html(respond);
            }
        })
    });

    $("#kabupaten1").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kecamatan1").html(respond);
            }
        })
    });

    $("#kecamatan").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kelurahan_by_kec') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kelurahan").html(respond);
            }
        })
    });

    $("#edt_kecamatan").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kelurahan_by_kec') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#edt_kelurahan").html(respond);
            }
        })
    });

    $("#kecamatan1").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kelurahan_by_kec') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kelurahan1").html(respond);
            }
        })
    });

    $("[name=type]").change(function(){
    	periode = $("[name=periode]").val();
    	type = $(this).val();
    	if(periode){
    		get_dapil(type,periode);
    	}
    })

    $("[name=periode]").change(function(){
    	type = $("[name=type]").val();
    	periode = $(this).val();
    	if(type){
    		get_dapil(type,periode);
    	}
    })

    $("[name=id_dapil]").change(function(){
        id_dapil = $(this).val();
        if(id_dapil){
            get_legislator(id_dapil);
            get_prov_dapil(id_dapil);
        }
    })

    $("[name=id_legislator]").change(function(){
        id_legislator = $(this).val();
        if(id_legislator){
            get_legislator_parpol(id_legislator);
        }
    })

    $("#type1").change(function(){
        periode = $("#periode1").val();
        type = $(this).val();
        if(periode){
            get_dapil(type,periode);
        }
    })

    $("#periode1").change(function(){
        type = $("#type1").val();
        periode = $(this).val();
        if(type){
            get_dapil(type,periode);
        }
    })

    $("#id_dapil1").change(function(){
        id_dapil = $(this).val();
        if(id_dapil){
            //get_legislator(id_dapil);
            get_prov_dapil(id_dapil);
        }
    })

    function get_dapil(type,periode){
    	$.ajax({
            url: "<?php echo site_url('pileg/get_dapil') ?>",
            cache: false,
            type:"POST",
            data:{type:type,periode:periode},
            success: function(respond){
            	// console.log(respond);
                $("[name=id_dapil]").html(respond);
                $("#id_dapil1").html(respond);
            }
        })
    }

    function get_dapil_detail(type,periode,id_dapil){
        //alert(id_dapil);
        $.ajax({
            url: "<?php echo site_url('pileg/get_dapil') ?>",
            cache: false,
            type:"POST",
            data:{type:type,periode:periode},
            success: function(respond){
                //console.log(id_dapil);
                $("#edt_id_dapil").html(respond);
                $("#edt_id_dapil").val(id_dapil);
            }
        })
    }

    function get_legislator(dapil){
        $.ajax({
            url: "<?php echo site_url('pileg/get_legislator') ?>",
            cache: false,
            type:"POST",
            data:{id_dapil:dapil},
            success: function(respond){
                // console.log(respond);
                $("[name=id_legislator]").html(respond);
            }
        })
    }

    function get_legislator_detail(dapil, id_legislator){
        $.ajax({
            url: "<?php echo site_url('pileg/get_legislator') ?>",
            cache: false,
            type:"POST",
            data:{id_dapil:dapil},
            success: function(respond){
                // console.log(respond);
                $("#edt_id_legislator").html(respond);
                $("#edt_id_legislator").val(id_legislator);
                //$("[name=id_legislator]").html(respond);
            }
        })
    }

    function get_legislator_parpol(id_legislator){
        $.ajax({
            url: "<?php echo site_url('pileg/get_legislator_parpol') ?>",
            cache: false,
            type:"POST",
            data:{id_legislator:id_legislator},
            success: function(respond){
                // console.log(respond);
                $("[name=id_parpol]").html(respond);
            }
        })
    }

    function get_legislator_parpol_detail(id_legislator, id_parpol){
        $.ajax({
            url: "<?php echo site_url('pileg/get_legislator_parpol') ?>",
            cache: false,
            type:"POST",
            data:{id_legislator:id_legislator},
            success: function(respond){
                // console.log(respond);
                $("#edt_id_parpol").html(respond);
                $("#edt_id_parpol").val(id_parpol);
            }
        })
    }

    function get_prov_dapil(id_dapil){
        $.ajax({
            url: "<?php echo site_url('pileg/get_prov_dapil') ?>",
            cache: false,
            type:"POST",
            data:{id_dapil:id_dapil},
            success: function(respond){
                console.log(respond);
                $("[name=idProv]").html(respond);
                $("#provinsi1").html(respond);
            }
        })
    }

    function get_prov_dapil_detail(id_dapil, idProv){
        $.ajax({
            url: "<?php echo site_url('pileg/get_prov_dapil') ?>",
            cache: false,
            type:"POST",
            data:{id_dapil:id_dapil},
            success: function(respond){
                // console.log(respond);
                $("#edt_provinsi").html(respond);
                $("#edt_provinsi").val(idProv);
            }
        })
    }

    function get_kab_dapil_detail(idProv, idKab){
        $.ajax({
                url: "<?php echo site_url('form/get_kabupaten') ?>",
                cache: false,
                type:"POST",
                data:{id:idProv},
                success: function(respond){
                    $("#edt_kabupaten").html(respond);
                    $("#edt_kabupaten").val(idKab);
                }
            })
    }

    function get_kec_dapil_detail(idKab, idKec){
        if (idKec!=null) {
            $.ajax({
                url: "<?php echo site_url('form/get_kecamatan') ?>",
                cache: false,
                type:"POST",
                data:{id:idKab},
                success: function(respond){
                    $("#edt_kecamatan").html(respond);
                    $("#edt_kecamatan").val(idKec);
                }
            })
        } else {
            $('#edt_kecamatan').find('option').remove().end();
        }
    }

    function get_kel_dapil_detail(idKec, idKel){
        if (idKel!=null) {
            $.ajax({
                url: "<?php echo site_url('form/get_kelurahan_by_kec') ?>",
                cache: false,
                type:"POST",
                data:{id:idKec},
                success: function(respond){
                    $("#edt_kelurahan").html(respond);
                    $("#edt_kelurahan").val(idKel);
                }
            })
        } else{
            $('#edt_kelurahan').find('option').remove().end();
        }
    }

    $('#input_legislator').submit(function(event){
        event.preventDefault();
        //alert($( "#input_legislator" ).serialize());
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'pileg/input_suara_legislator'?>",
                type : 'post',
                data : $( "#input_legislator" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    document.getElementById("provinsi").selectedIndex = 0;
                    document.getElementById('kabupaten').selectedIndex = 0;
                    document.getElementById('kecamatan').selectedIndex = 0;
                    document.getElementById('kelurahan').selectedIndex = 0;
                    document.getElementById('id_parpol').selectedIndex = 0;
                    document.getElementById('id_legislator').selectedIndex = 0;
                    document.getElementById('id_legislator').selectedIndex = 0;
                    document.getElementById('id_dapil').selectedIndex = 0;
                    document.getElementById('type').selectedIndex = 0;
                    document.getElementById('periode').selectedIndex = 0;
                    document.getElementById('state').selectedIndex = 0;
                    document.getElementById("input_legislator").reset();
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                    
                    $("#modal_tambah").modal('hide');                
                },
                error: function(data){
                	console.log(data);
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('pileg/get_table_suara_legislator')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    {"data": "caleg","title": "Nama"},
                    {"data": "periode","title": "Periode"},
                    {"data": "parpol","title": "Partai"},
                    {"data": "state","title": "Tipe"},
                    {"data": "provinsi","title": "Provinsi"},
                    {"data": "kabkot","title": "Kab/Kota"},
                    {"data": "kecamatan","title": "Kecamatan"},
                    {"data": "kelurahan","title": "Kelurahan"},
                    {"data": "tps","title": "tps"},
                    {"data": "suara","title": "Suara"},
                    {"data": "button","title": "Action"},
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
    });
</script>

<!-- CRUD JAVASCRIPT -->
<script type="text/javascript">
    $('#edit_legislator').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'pileg/edit_suara_legislator'?>",
                type : 'post',
                data : $( "#edit_legislator" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#table').DataTable();
                    document.getElementById("edit_legislator").reset();
                    table.ajax.reload();
                    $("#modal_edit").modal('hide');                
                },
                error: function(data){
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });
	
	$("#table").on("click", ".btn-edit",function(event){
        value = $(this).data('id');
		console.log(value);
        var id; 
        $.ajax({
            url : "<?php echo site_url().'pileg/detail_suara_legislator/'?>"+value,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                console.log(data);
                get_dapil_detail(data.type,data.periode, data.id_dapil);
                get_legislator_detail(data.id_dapil, data.id_legislator);
                get_legislator_parpol_detail(data.id_legislator, data.id_parpol);
                get_prov_dapil_detail(data.id_dapil, data.idProv);
                get_kab_dapil_detail(data.idProv, data.idKab);
                get_kec_dapil_detail(data.idKab, data.idKec);
                get_kel_dapil_detail(data.idKec,data.idKel);
                $('[name="id"]').val(data.id);
                $('[name="type"]').val(data.type);
                $('[name="periode"]').val(data.periode);
                $('[name="suara"]').val(data.suara);
                $('[name="state"]').val(data.state);
                $('[name="tps"]').val(data.tps);
                
                // $('[name="id_dapil"]').append('<option value="'+data.id_kabupaten+'">'+data.kabupaten+'</option>');
                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $("#table").on("click", ".btn-delete",function(event){
        id = $(this).data('id');
        event.preventDefault();
            
        Pace.track(function(){
            $.ajax({
                url : "<?php echo site_url('pileg/delete_suara_legislator')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                },
                error: function (data){
                    console.log(data);
                    alert('Error get data from ajax');
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $('#upload_form').submit(function(e){
        e.preventDefault();
        var id = $('[name="unique"]').val();
        var state = $('#state1').val();
        var id_uri = encodeURI(id);
        $.ajax({
            url: "<?php echo site_url('pileg/cek_unique_suara_legislator/') ?>"+id_uri+"/"+state,
            cache: false,
            type:"POST",
            success: function(respond){
                if(respond == 0){
                    alert('UNIQUE ID sudah ada');
                }else{
                    $("#save").prop('disabled', true);
                    var form = $('#upload_form')[0];
                    var data = new FormData(form);
                    Pace.track(function(){
                        $.ajax({
                            url  : "<?= site_url().'pileg/upload_suara_legislator'?>",
                            type : 'POST',
                            enctype: 'multipart/form-data',
                            data : data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            timeout: 600000,
                            dataType: "json",
                            success: function(data){
                                $("#modal_excel").modal('hide'); 
                                $("#save").prop('disabled', false);
                                notie.alert({ type: 1, text: 'Data Berhasil Diupload sejumlah '+data, time: 2,position: 'bottom' })
                                var table = $('#table').DataTable();
                                table.ajax.reload();
                            },
                            error: function(xhr, status, error) {
                                var err = eval("(" + xhr.responseText + ")");
                                alert(err.Message);
                            }
                        });
                    })
                }
            }
        })
      // e.preventDefault();
      // 
    });
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>