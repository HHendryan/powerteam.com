<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/bermuda-gray.css"> -->
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<!-- Bootstrap Date-Picker Plugin -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>MONITORING DPT DOUBLE BY NIK & NAMA & TANGGAL LAHIR</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="form-group row">
                                            <?php if($role == 'korprov'){ ?>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Provinsi</label>
                                                    <select id="provinsi" class="form-control ks-select" name="provinsi">
                                                        <option value="<?php echo $provinsi->id ?>"><?php echo $provinsi->name ?></option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kabupaten</label>
                                                    <select id="kabupaten" class="form-control ks-select" name="kabupaten">
                                                        <option value="all" selected>ALL</option>
                                                        <?php foreach ($kabupaten as $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kecamatan</label>
                                                    <select id="kecamatan" class="form-control ks-select" name="kecamatan">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">kelurahan</label>
                                                    <select id="kelurahan" class="form-control ks-select" name="kelurahan">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">tps</label>
                                                    <select id="tps" class="form-control ks-select" name="tps">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                            <?php }else if($role == 'korkab'){ ?>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Provinsi</label>
                                                    <select id="provinsi" class="form-control ks-select" name="provinsi">
                                                        <option value="<?php echo $provinsi->id ?>"><?php echo $provinsi->name ?></option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kabupaten</label>
                                                    <select id="kabupaten" class="form-control ks-select" name="kabupaten">
                                                        <option value="<?php echo $kabupaten->id ?>"><?php echo $kabupaten->name ?></option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kecamatan</label>
                                                    <select id="kecamatan" class="form-control ks-select" name="kecamatan">
                                                        <option value="all" selected>ALL</option>
                                                        <?php foreach ($kecamatan as $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">kelurahan</label>
                                                    <select id="kelurahan" class="form-control ks-select" name="kelurahan">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">tps</label>
                                                    <select id="tps" class="form-control ks-select" name="tps">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                            <?php }else{ ?>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Provinsi</label>
                                                    <select id="provinsi" class="form-control ks-select" name="provinsi">
                                                        <option value="all" selected>ALL</option>
                                                        <?php foreach ($provinsi as $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kabupaten</label>
                                                    <select id="kabupaten" class="form-control ks-select" name="kabupaten">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kecamatan</label>
                                                    <select id="kecamatan" class="form-control ks-select" name="kecamatan">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kelurahan</label>
                                                    <select id="kelurahan" class="form-control ks-select" name="kelurahan">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">TPS</label>
                                                    <select id="tps" class="form-control ks-select" name="tps">
                                                        <option value="all" selected>ALL</option>
                                                    </select>
                                                </div>
                                            <?php } ?>

                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label col-sm-12" style="visibility: hidden;">Provinsi</label>
                                                <button class="btn btn-crusta" onclick="cari();">Search</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>NAMA</th>
                                        <th>NIK</th>
                                        <th>TANGGAL LAHIR</th>
                                        <th>JUMLAH</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade large-modal"  role="dialog" aria-labelledby="myLargeModalLabel" id="modal_detail_dpt1" aria-hidden="true" width="100%">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail DPT DOUBLE</h5> 

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <br>
            <!--h5 class="modal-title" id="prov_name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sulawesi Tenggara</h5-->
            <form id="edit" width="100%">
            <div class="modal-body" width="100%">
                <div class="container-fluid">
                    <div class="table-responsive">
                                <table id="table_detail_dpt1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nik</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat</th>
                                        <th>Tanggal Lahir</th>
                                        <th>KECAMATAN</th>
                                        <th>KELURAHAN</th>
                                        <th>TPS</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Nik</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat</th>
                                        <th>Tanggal Lahir</th>
                                        <th>KECAMATAN</th>
                                        <th>KELURAHAN</th>
                                        <th>TPS</th>
                                    </tr>

                                </tfoot>
                            </table>
                        </div>
                </div>
            </div>
            <div class="modal-footer" width="100%">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>


<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $('#table_detail_dpt1').DataTable({
        columns: [
                    { data: 'nik' },
                    { data: 'nama' },
                    { data: 'jns_kelamin' },
                    { data: 'alamat' },
                    { data: 'tanggal_lahir' },
                    { data: 'kecamatan'},
                    { data: 'kelurahan'},
                    { data: 'tps' },
                ],
        "autoWidth": false
    });
</script>

<script type="text/javascript">
    $("#datatable").on("click", ".btn-detail",function(event){
        nama = $(this).data('nama');
        tgl = $(this).data('tgl');
        kab = $(this).data('kab');
        nik = $(this).data('id');

        var table = $('#table_detail_dpt1').DataTable();
        //alert("<?= site_url('data/get_detail_dpt_by_nik/')?>"+kab+'/'+id);
        table.ajax.url( "<?= site_url('data/get_detail_dpt_by_nama_tgl_nik/')?>"+kab+'/'+nik+'/'+nama+'/'+tgl).load();
        $("#modal_detail_dpt1").modal('show');
    });
</script>
<script type="application/javascript">
    $(document).ready(function() {
      $('#menu_swift').trigger('change');
      $('.flatpickr').flatpickr();
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    });
</script>
<script type="text/javascript">
    function cari(){
        //var prov = $("#provinsi").val();
        var kab  = $("#kabupaten").val();
        var kec  = $("#kecamatan").val();
        var kel  = $("#kelurahan").val();
        var tps  = $("#tps").val();
        var nama = $("#nama").val();
        var tgl  = 'all';
        
        if (nama=='') {nama='all'};
        if (tgl=='') {tgl='all'};

        if(kab!='all'){
            var table = $("#datatable").dataTable();
            Pace.track(function(){
                table.fnDestroy();
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#datatable").dataTable({
                    columnDefs: [ {
                        orderable: false,
                        targets:   0
                    } ],
                    initComplete: function() {
                        var api = this.api();
                        $('#datatable_filter input')
                            .off('.DT')
                            .on('keyup.DT', function(e) {
                                if (e.keyCode == 13) {
                                     api.search(this.value).draw();
                                }
                            });
                        },
                        ajax: {"url": "<?php echo site_url('data/load_data_dpt_double/')?>"+kab+'/'+kec+'/'+kel+'/'+tps+'/'+'null'+'/'+'null'+'/'+'null', "type": "POST"},
                        columns: [
                            {"data": "id"},
                            {"data": "nama"},
                            {"data": "nik"},
                            {"data": "tanggal_lahir"},
                            {"data": "jml"},
                            {"data": "button"},
                        ],

                        rowCallback: function(row, data, iDisplayIndex) {
                            var info = this.fnPagingInfo();
                            var page = info.iPage;
                            var length = info.iLength;
                            var index = page * length + (iDisplayIndex + 1);
                            $('td:eq(0)', row).html(index);
                        },

                        infoCallback: function( settings, start, end, max, total, pre ) {
                            var api = this.api();
                            var pageInfo = api.page.info();
                            tableinfo = api.page.info();
                        }
                });
                
            });
        } else {
            alert('Masukan kab');
        }
    }
</script>
<script type="text/javascript">
    $('#provinsi').select2({
        placeholder: "Pilih Provinsi"
    });
    $('#kabupaten').select2();
    $('#kecamatan').select2();
    $('#kelurahan').select2();
    $('#tps').select2();
</script>
<script type="text/javascript">
    $("#provinsi").change(function(){
        $.ajax({
            url: "<?php echo site_url('guraklih/get_wilayah/provinsi') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kabupaten").html(respond);
                $('#kecamatan').val('all').trigger("change");
                $('#kelurahan').val('all').trigger("change");
                $('#tps').val('all').trigger("change");
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        $.ajax({
            url: "<?php echo site_url('guraklih/get_wilayah/kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kecamatan").html(respond);
                $('#kelurahan').val('all').trigger("change");
                $('#tps').val('all').trigger("change");
            }
        })
    })
    $("#kecamatan").change(function(){
        $.ajax({
            url: "<?php echo site_url('guraklih/get_wilayah/kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kelurahan").html(respond);
                $('#tps').val('all').trigger("change");
            }
        })
    })
    $("#kelurahan").change(function(){
        $.ajax({
            url: "<?php echo site_url('guraklih/get_wilayah/kelurahan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#tps").html(respond);
            }
        })
    })
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>