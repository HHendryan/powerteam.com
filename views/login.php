
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/open-sans/styles.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pages/auth.min.css">
</head>
<body>

<div class="ks-page">
    <div class="ks-page-content">
        <div class="ks-logo"></div>

        <div class="card panel panel-default ks-light ks-panel ks-login" style="background-color: #DF0000">
            <div class="card-block">
                <form class="form-container" method="post" action="<?=site_url('auth/login')?>">
                    <h4 class="ks-header"><a href="<?=base_url()?>" class="ks-logo"><img src="<?=base_url()?>/assets/img/logo_guraklih.png" width="200" height="175"></a></h4>
                    <!-- <h4 class="ks-header"><a href="<?=base_url()?>" class="ks-logo"><img src="<?=base_url()?>/assets/img/teknopol.png"></a></h4> -->
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="text" class="form-control" placeholder="Username" name="username">
                            <span class="icon-addon">
                                <span class="la la-at"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            <span class="icon-addon">
                                <span class="la la-key"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block" style="background-color: #2b2b2b">Login</button>
                    </div>
                    <div class="ks-text-center">
                 
                    </div>
                    <div class="ks-text-center">
               
                    </div>
                </form>
        </div>
    </div>
</div>

<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="dafters/jquery.backstretch.min.js"></script>
</body>
</html>
<!--common script for all pages 
    <script src="../js/common-scripts.js"></script>
-->
    
  <script>
  $.backstretch("dafters/loginbg.jpg", {
  speed: 500
});
// this image free from https://pixabay.com/en/car-police-cars-caravan-sirens-red-1531277/ no need copy right
  </script>
<script>