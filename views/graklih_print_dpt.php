
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>GURAKLIH</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
</head>
<!-- END HEAD -->
<?php include 'template/header.php'; ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php'; ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title" id="head_table">
                <h3>GURAKLIH Print DPT</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="form-group row">

                                           <?php if($role == 'korprov'){ ?>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Provinsi</label>
                                                    <select id="provinsi" class="form-control ks-select" name="provinsi" readonly>
                                                        <option value="<?php echo $provinsi->idProv ?>"><?php echo $provinsi->provinsi ?></option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kabupaten</label>
                                                    <select id="kabupaten" class="form-control ks-select" name="kabupaten">
                                                        <option value="" disabled selected>Pilih Kabupaten</option>
                                                        <?php foreach ($kabupaten as $value) { ?>
                                                            <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kecamatan</label>
                                                    <select id="kecamatan" class="form-control ks-select" name="kecamatan">
                                                        <option value="" disabled selected>Pilih Kecamatan</option>
                                                    </select>
                                                </div>

                                            <?php }else if($role == 'korkab'){ ?>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Provinsi</label>
                                                    <select id="provinsi" class="form-control ks-select" name="provinsi" readonly>
                                                        <option value="<?php echo $provinsi->idProv ?>"><?php echo $provinsi->provinsi ?></option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kabupaten</label>
                                                    <select id="kabupaten" class="form-control ks-select" name="kabupaten" readonly>
                                                        <option value="" disabled selected>Pilih Kabupaten</option>
                                                        <option value="<?php echo $kabupaten->id ?>"><?php echo $kabupaten->name ?></option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kecamatan</label>
                                                    <select id="kecamatan" class="form-control ks-select" name="kecamatan">
                                                        <option value="" disabled selected>Pilih Kecamatan</option>
                                                        <?php foreach ($kecamatan as $value) { ?>
                                                            <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                            <?php }else{ ?>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Provinsi</label>
                                                    <select id="provinsi" class="form-control ks-select" name="provinsi">
                                                        <option value="all" selected>ALL</option>
                                                        <?php foreach ($provinsi as $value) { ?>
                                                        <option value="<?php echo $value->idProv ?>"><?php echo $value->provinsi ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kabupaten</label>
                                                    <select id="kabupaten" class="form-control ks-select" name="kabupaten">
                                                        <option value="" disabled selected>Pilih Kabupaten</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="form-control-label">Kecamatan</label>
                                                    <select id="kecamatan" class="form-control ks-select" name="kecamatan">
                                                        <option value="" disabled selected>Pilih Kecamatan</option>
                                                    </select>
                                                </div>
                                            <?php } ?>

                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">Kelurahan</label>
                                                <select id="kelurahan" class="form-control ks-select" name="kelurahan">
                                                     <option value="" disabled selected>Pilih Kelurahan</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-sm-2">
                                                <label class="form-control-label">TPS</label>
                                                <select id="tps" class="form-control ks-select" name="tps">
                                                     <option value="" disabled selected>Pilih TPS</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-1">
                                                <label class="form-control-label" style="visibility: hidden">Influencer</label>
                                                <button class="btn btn-crusta" onclick="cari()" style="width: 80px">Submit</button>
                                            </div>
                                            <div class="form-group col-sm-1">
                                                <label class="form-control-label" style="visibility: hidden">Influencer</label>
                                                <button class="btn btn-crusta" onclick="print()" style="width: 80px">Print</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>JK</th>
                                        <th>Alamat</th>
                                        <th>Tanggal Lahir</th>
                                        <th>TPS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>

<?php include 'template/settings.php'; ?>
<script type="text/javascript">
    var kec;
    var kel;
    var tps;
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="text/javascript">
    $('#provinsi').select2({
        placeholder: "Pilih Kabupaten"
    });
    $('#kabupaten').select2();
    $('#kecamatan').select2();
    $('#kelurahan').select2();
    $('#tps').select2();

    var table = $('#ks-datatable').dataTable();
    

</script>
<script type="text/javascript">
    $("#provinsi").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kabupaten").html(respond);
                $('#kecamatan').val('').trigger("change");
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kecamatan").html(respond);
                $('#kelurahan').val('').trigger("change");
            }
        })
    })
</script>

<script type="text/javascript">
    $("#kecamatan").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_kelurahan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                //alert(respond);
                $("#kelurahan").html(respond);
                $('#tps').val('').trigger("change");
            }
        })
    })
</script>

<script type="text/javascript">
    $("#kelurahan").change(function(){
        $.ajax({
            url: "<?php echo site_url('data/get_tps') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                //alert(respond);
                $("#tps").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    function cari(){
        kec = $("#kecamatan").val();
        kel = $("#kelurahan").val();
        tps = $("#tps").val();

        if(tps==null|| tps=='all'){
            alert('Form TPS harus diisi');
        }else{
            Pace.track(function(){
                table.fnDestroy();
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#ks-datatable").dataTable({
                    dom: 'Bfrtip',
                    buttons: [{ 
                        extend: 'pdfHtml5', 
                        footer: true,
                        title: 'Customized PDF Title',
                        filename: 'Daftar DPT' 
                    }],
                    initComplete: function() {
                        table.buttons().container().appendTo('#head_table');
                        var api = this.api();
                        $('#ks-datatable_filter input')
                            .off('.DT')
                            .on('keyup.DT', function(e) {
                                if (e.keyCode == 13) {
                                     api.search(this.value).draw();
                                }
                            });
                        },
                        ajax: {"url": '<?=site_url('data/load_data_dpt')?>/'+kel+'/'+tps, "type": "POST"},
                        columns: [
                            {"data": "no"},
                            {"data": "nik"},
                            {"data": "nama"},
                            {"data": "jns_kelamin"},
                            {"data": "alamat"},
                            {"data": "tanggal_lahir"},
                            {"data": "tps"}
                            
                        ],
                        //order: [[1, 'asc']],
                        rowCallback: function(row, data, iDisplayIndex) {
                            var info = this.fnPagingInfo();
                            var page = info.iPage;
                            var length = info.iLength;
                            var index = page * length + (iDisplayIndex + 1);
                            $('td:eq(0)', row).html(index);
                        }
                });
                
                table.buttons().container().appendTo( '#ks-datatable_wrapper .col-md-6:eq(0)' );
            });
        }
    }(jQuery);
</script>
<script type="text/javascript">
    function print(){
        if(kel && tps){
            window.open("<?=base_url('printer/dpt')?>?kel="+kel+"&tps="+tps);
        }else{
            alert('Silahkan pilih tps dan klik submit');
        }
    }
</script>>
<div class="ks-mobile-overlay"></div>
</body>
</html>