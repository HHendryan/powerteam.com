 
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.min.css">
    <!-- END THEME STYLES -->
    
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
</head>
<!-- END HEAD -->

<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title-and-subtitle" id="head_table">
                <div class="ks-title-block">
                    <h3 class="ks-main-title">Data Pemilih</h3>
                    <div class="ks-sub-title">Kelurahan : <?php echo $sub_header ?> | TPS : <?php echo $tps ?></div>
                </div>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <div class="table-responsive">
                                <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>NKK</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Tempat Lahir</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>NKK</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Tempat Lahir</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit DPT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="update">
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label>NKK</label>
                                <input type="text" class="form-control" id="nkk" name="nkk" placeholder="NKK" onkeypress="return isNumberKey(event)" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <label>NIK</label>
                                <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK" onkeypress="return isNumberKey(event)" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <label>Nama</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-control-label">Tempat Lahir</label>
                                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-control-label">Tanggal Lahir</label>
                                <input type="text" class="form-control calendar" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <label class="form-control-label">Jenis Kelamin</label>
                                <select id="jenis_kelamin" class="form-control ks-select select2" name="jenis_kelamin" style="width: 100%">
                                    <option value="0" disabled selected value>Pilih</option>
                                    <option value="1">Laki-Laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-4">
                                <label class="form-control-label">Agama</label>
                                <select id="agama" class="form-control ks-select select2" name="agama" style="width: 100%">
                                    <option value="0" disabled selected value>Pilih</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen Katolik">Kristen Katolik</option>
                                    <option value="Kristen Protestan">Kristen Protestan</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Konghucu">Konghucu</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-4">
                                <label class="form-control-label">Status</label>
                                <select id="status" class="form-control ks-select select2" name="status" style="width: 100%">
                                    <option value="0" disabled selected value>Pilih</option>
                                    <option value="2">Kawin</option>
                                    <option value="1">Belum Kawin</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-control-label">Pekerjaan</label>
                                <select id="pekerjaan" class="form-control ks-select select2" name="pekerjaan" style="width: 100%">
                                    <option value="0" disabled selected value>Pilih</option>
                                    <option value="1">PNS</option>
                                    <option value="2">Wiraswasta</option>
                                    <option value="3">Pelajar</option>
                                    <option value="4">Pengangguran</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-control-label">Pendidikan</label>
                                <select id="pendidikan" class="form-control ks-select select2" name="pendidikan" style="width: 100%">
                                    <option value="0" disabled selected value>Pilih</option>
                                    <option value="1">SD</option>
                                    <option value="2">SMP</option>
                                    <option value="3">SMA/SMK</option>
                                    <option value="4">S1</option>
                                    <option value="5">S2</option>
                                    <option value="6">S3</option>
                                    <option value="7">Tidak Sekolah</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-control-label">Suku</label>
                                <select id="suku" class="form-control ks-select select2" name="suku" style="width: 100%">
                                    <option value="0" disabled selected value>Pilih</option>
                                    <option value="1">Bugis</option>
                                    <option value="2">Sunda</option>
                                    <option value="3">Melayu</option>
                                    <option value="4">Betawi</option>
                                    <option value="5">Mandar</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-control-label">Golongan Darah</label>
                                <select id="golongan_darah" class="form-control ks-select select2" name="golongan_darah" style="width: 100%">
                                    <option value="0" disabled selected value>Pilih</option>
                                    <option value="1">A</option>
                                    <option value="2">B</option>
                                    <option value="3">AB</option>
                                    <option value="4">O</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-control-label">Handphone</label>
                                <input type="text" class="form-control" id="handphone" name="handphone" placeholder="Handphone" onkeypress="return isNumberKey(event)" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-control-label">Alamat</label>
                                <textarea type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat"></textarea>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>

<input type="hidden" id="id_kel" value="<?php echo $id_kel ?>">
<input type="hidden" id="tps" value="<?php echo $tps ?>">

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="application/javascript">
    (function ($) {
        $(document).ready(function() {
            var table = $('#ks-datatable').DataTable({
                ajax: '<?=site_url('data/get_data_pemilih/')?>'+$("#id_kel").val()+'/'+$("#tps").val(),
                columns: [
                            { data: 'nkk' },
                            { data: 'nik' },
                            { data: 'nama' },
                            { data: 'tempat_lahir' },
                            { data: 'tanggal_lahir' },
                            { data: 'button' }
                        ],
                "autoWidth": false,
                buttons: [
                    { extend: 'copyHtml5', footer: true },
                    { extend: 'excelHtml5', footer: true },
                    { extend: 'csvHtml5', footer: true },
                    { extend: 'pdfHtml5', footer: true }
                ],
                initComplete: function (settings, json) {
                    $('.dataTables_wrapper select').select2({
                        minimumResultsForSearch: Infinity
                    });
                    table.buttons().container().appendTo('#head_table');
                }
            });

            table.buttons().container().appendTo( '#ks-datatable_wrapper .col-md-6:eq(0)' );
        });
    })(jQuery);

    $('.select2').select2({
        placeholder: "Pilih"
    });
    $(".calendar").flatpickr();

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
    }

    $("#ks-datatable").on("click", ".btn-edit",function(event){
        $.ajax({
            url : "<?php echo site_url().'data/get_data_pemilih_details/'?>"+$(this).data('id'),
            type: "GET",
            dataType: "JSON",
            success: function(data){
                console.log(data);
                $('#id').val(data[0].id);
                $('#nkk').val(data[0].nkk);
                $('#nik').val(data[0].nik);
                $('#nama').val(data[0].nama);
                $('#tempat_lahir').val(data[0].tempat_lahir);
                $('#tanggal_lahir').val(data[0].tanggal_lahir);
                $('#jenis_kelamin').val(data[0].jenis_kelamin).trigger("change");
                $('#alamat').val(data[0].alamat);
                $('#agama').val(data[0].agama).trigger("change");
                $('#status').val(data[0].status).trigger("change");
                $('#pekerjaan').val(data[0].pekerjaan).trigger("change");
                $('#pendidikan').val(data[0].pendidikan).trigger("change");
                $('#suku').val(data[0].suku).trigger("change");
                $('#golongan_darah').val(data[0].golongan_darah).trigger("change");
                $('#handphone').val(data[0].handphone);
                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $('#update').submit(function(event){
        event.preventDefault();

        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'data/update_dpt'?>",
                type : 'post',
                data : $( "#update" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#ks-datatable').DataTable();
                    table.ajax.reload();
                    $("#update")[0].reset();
                    $("#modal_edit").modal('hide'); 
                },
                error : function(data){
                    alert('GAGAL');
                }
                            
            });
        });
    });
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>