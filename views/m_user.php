
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
</head>
<!-- END HEAD -->

<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title" id="head_table">
                <h3>List User</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah User</button>
                            <br><br>
                            <div class="table-responsive">
                                <table id="ks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Level</th>
                                        <th>Provinsi</th>
                                        <th>Kabupaten</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="input">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="text" class="form-control" placeholder="Username" name="username" required>
                            <span class="icon-addon">
                                <span class="la la-at"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="password" class="form-control" placeholder="Password" name="password" required>
                            <span class="icon-addon">
                                <span class="la la-key"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <select id="level" class="form-control ks-select" name="level" style="width: 100%" required>
                            <option disabled selected value>PILIH</option>
                            <option value="99">ADMIN</option>
                            <option value="3">PILPRES</option>
                            <option value="2">PILGUB</option>
                            <option value="1">PILWALI / PILBUP</option>
                            <!-- <option value="pileg">PILEG</option> -->
                        </select>
                    </div>
                    <div class="form-group" style="display: none" id="form-provinsi">
                        <select id="provinsi" class="form-control ks-select" name="provinsi" style="width: 100%">
                            <option disabled selected value>PILIH</option>
                            <?php foreach ($provinsi as $value) { ?>
                                <option value="<?php echo $value->id ?>"><?php echo $value->provinsi ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group" style="display: none" id="form-kabupaten">
                        <select id="kabupaten" class="form-control ks-select" name="kabupaten" style="width: 100%">
                        </select>
                    </div>
                    <div class="form-group" style="display: none" id="form-kecamatan">
                        <select id="kecamatan" class="form-control ks-select" name="kecamatan" style="width: 100%">
                        </select>
                    </div>
                    <div class="form-group" style="display: none" id="form-sub_level">
                        <select id="sub-level" class="form-control ks-select" name="sub_level" style="width: 100%">
                            <option disabled selected value>PILIH</option>
                            <option value="dprd">DPRD</option>
                            <option value="dprd_prov">DPRD PROVINSI</option>
                            <option value="dprd_kab">DPRD DAERAH</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <div class="modal-body">
                    <div class="ks-tabs-container ks-tabs-default ks-tabs-no-separator">
                        <ul class="nav ks-nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab1">Edit User</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="tab" data-target="#tab2">Edit Password</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1" role="tabpanel">
                                <form id="update">
                                <div class="form-group">
                                    <label>File Upload &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    <button class="btn btn-primary ks-btn-file">
                                        <span class="la la-cloud-upload ks-icon"></span>
                                        <span class="ks-text">Choose file</span>
                                        <input type="file" name="url_image">
                                    </button>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="id" id="id_edit" required>
                                    <div class="input-icon icon-left icon-lg icon-color-primary">
                                        <input type="text" class="form-control" placeholder="Username" name="username" id="username_edit" required>
                                        <span class="icon-addon">
                                            <span class="la la-at"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select id="level_edit" class="form-control ks-select" name="level" style="width: 100%" required>
                                        <option disabled selected value>PILIH</option>
                                        <option value="99">ADMIN</option>
                                        <option value="3">PILPRES</option>
                                        <option value="2">PILGUB</option>
                                        <option value="1">PILWALI / PILBUP</option>
                                        <!-- <option value="pileg">PILEG</option> -->
                                    </select>
                                </div>
                                <div class="form-group" style="display: none" id="form-provinsi_edit">
                                    <select id="provinsi_edit" class="form-control ks-select" name="provinsi" style="width: 100%">
                                        <option disabled selected value>PILIH</option>
                                        <?php foreach ($provinsi as $value) { ?>
                                            <option value="<?php echo $value->id ?>"><?php echo $value->provinsi ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group" style="display: none" id="form-kabupaten_edit">
                                    <select id="kabupaten_edit" class="form-control ks-select" name="kabupaten" style="width: 100%">
                                    </select>
                                </div>
                                <div class="form-group" style="display: none" id="form-kecamatan_edit">
                                    <select id="kecamatan_edit" class="form-control ks-select" name="kecamatan" style="width: 100%">
                                    </select>
                                </div>
                                <div class="form-group" style="display: none" id="form-sub_level_edit">
                                    <select id="sub-level_edit" class="form-control ks-select" name="sub_level" style="width: 100%">
                                        <option disabled selected value>PILIH</option>
                                        <option value="dprd">DPRD</option>
                                        <option value="dprd_prov">DPRD PROVINSI</option>
                                        <option value="dprd_kab">DPRD DAERAH</option>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-crusta">Save changes</button>
                                </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tab2" role="tabpanel">
                                <form id="password-form">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="id" id="id_pass" required>
                                    <div class="input-icon icon-left icon-lg icon-color-primary">
                                        <input type="text" class="form-control" placeholder="Username" name="username" id="username_pass" disabled>
                                        <span class="icon-addon">
                                            <span class="la la-at"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-icon icon-left icon-lg icon-color-primary">
                                        <input type="password" class="form-control" placeholder="Password" name="password" id="pass" required>
                                        <span class="icon-addon">
                                            <span class="la la-key"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-icon icon-left icon-lg icon-color-primary">
                                        <input type="password" class="form-control" placeholder="Re-enter Password" name="confirm_pass" id="confirm_pass" required>
                                        <span class="icon-addon">
                                            <span class="la la-key"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-crusta">Save changes</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
            
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>

<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
    });
</script>

<script type="text/javascript">
    var password = document.getElementById("pass")
      , confirm_password = document.getElementById("confirm_pass");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>

<!-- INITIALIZATION -->
<script type="application/javascript">  
    (function ($) {
        $(document).ready(function() {
            $('#level').select2({
                placeholder: "Pilih Level",
            });
            $('#provinsi').select2({
                placeholder: "Pilih Provinsi"
            });
            $('#kabupaten').select2({
                placeholder: "Pilih kabupaten"
            });
            $('#kecamatan').select2({
                placeholder: "Pilih Kecamatan"
            });
            $('#sub-level').select2({
                placeholder: "Pilih Sub Level"
            });

            $('#level_edit').select2({
                placeholder: "Pilih Level",
            });
            $('#provinsi_edit').select2({
                placeholder: "Pilih Provinsi"
            });
            $('#kabupaten_edit').select2({
                placeholder: "Pilih kabupaten"
            });
            $('#kecamatan_edit').select2({
                placeholder: "Pilih Kecamatan"
            });
            $('#sub-level_edit').select2({
                placeholder: "Pilih Sub Level"
            });

            var table = $('#ks-datatable').DataTable({
                ajax: '<?=site_url('master/load_user')?>',
                columns: [
                            { data: 'nomor' },
                            { data: 'username' },
                            { data: 'level' },
                            { data: 'provinsi' },
                            { data: 'kabupaten' },
                            { data: 'is_active' },
                            { data: 'button' }
                        ],
                "autoWidth": false,
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                ],
                initComplete: function (settings, json) {
                    $('.dataTables_wrapper select').select2({
                        minimumResultsForSearch: Infinity
                    });
                    table.buttons().container().appendTo('#head_table');
                }
            });

            table.buttons().container().appendTo( '#ks-datatable_wrapper .col-md-6:eq(0)' );
        });
    })(jQuery);
</script>

<!-- JAVASCRIPT COMBOBOX ADD -->
<script type="text/javascript">
    $("#level").change(function(){
        if(($(this).val() == '3') || ($(this).val() == '99')){
            $('#form-provinsi').hide(); 
                $("#provinsi").select2("val", "PILIH");
            $('#form-kabupaten').hide();
                $("#kabupaten").select2("val", "PILIH");
        }
        if(($(this).val() == '2') || ($(this).val() == '1')){
            $('#form-provinsi').show();
                $("#provinsi").select2("val", "PILIH");
            $('#form-kabupaten').hide();
                $("#kabupaten").select2("val", "PILIH"); 
        }
    })

    $("#provinsi").change(function(){
        if ($('#level').val()=='2'){
            
        }else{
            $('#form-kabupaten').show();
                $("#kabupaten").select2("val", "PILIH");
            $('#form-kecamatan').hide();
                $("#kecamatan").select2("val", "PILIH");
            $('#form-sub_level').hide();    
                $("#sub-level").select2("val", "PILIH");
            $.ajax({
                url: "<?php echo site_url('master/get_select_kab') ?>",
                cache: false,
                type:"POST",
                data:{id_prov:$(this).val()},
                success: function(respond){
                    $("#kabupaten").html(respond);
                }
            })
        }
        
    })
</script>

<!-- JAVASCRIPT COMBOBOX EDIT -->
<script type="text/javascript">
    var kab;
    $("#level_edit").change(function(){
        if(($(this).val() == '3') || ($(this).val() == '99')){
            $('#form-provinsi_edit').hide(); 
                $("#provinsi_edit").select2("val", "PILIH");
            $('#form-kabupaten_edit').hide();
                $("#kabupaten").select2("val", "PILIH");
        }
        if(($(this).val() == '2') || ($(this).val() == '1')){
            $('#form-provinsi_edit').show();
                $("#provinsi_edit").select2("val", "PILIH");
            $('#form-kabupaten_edit').hide();
                $("#kabupaten_edit").select2("val", "PILIH"); 
        }
    })

    $("#provinsi_edit").change(function(){
        if ($('#level_edit').val()!='1'){
            
        }else{
            $('#form-kabupaten_edit').show();
                $("#kabupaten_edit").select2("val", "PILIH");
            $('#form-kecamatan_edit').hide();
                $("#kecamatan_edit").select2("val", "PILIH");
            $('#form-sub_level_edit').hide();    
                $("#sub-level_edit").select2("val", "PILIH");
            $.ajax({
                url: "<?php echo site_url('master/get_select_kab_edit') ?>",
                cache: false,
                type:"POST",
                data:{id_prov:$(this).val(),kab:kab},
                success: function(respond){
                    $("#kabupaten_edit").html(respond);
                }
            })
        }
        
    })
</script>

<!-- CRUD JAVASCRIPT -->
<script type="text/javascript">
    $('#input').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'master/add_user'?>",
                type : 'post',
                data : $( "#input" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#ks-datatable').DataTable();
                    table.ajax.reload();
                    $("#input")[0].reset();
                    $("#modal_tambah").modal('hide');                
                },
                error: function(data){
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });

    $("#ks-datatable").on("click", ".btn-del",function(event){
        id = $(this).data('id');
        value = $(this).data('value');
        event.preventDefault();
            
        Pace.track(function(){
            $.ajax({
                url : "<?php echo site_url('master/delete_user')?>/"+id+'/'+value,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    var table = $('#ks-datatable').DataTable();
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert('Error get data from ajax');
                }
            });
        });
    });

    $("#ks-datatable").on("click", ".btn-edt",function(event){
        kab = $(this).data('kab');
        $.ajax({
            url : "<?php echo site_url().'master/get_user/'?>"+$(this).data('id'),
            type: "GET",
            dataType: "JSON",
            success: function(data){
                console.log(data);
                $('#id_edit').val(data.id);
                $('#id_pass').val(data.id);
                $('#username_edit').val(data.username);
                $('#username_pass').val(data.username);
                $("#level_edit").val(data.level).trigger("change");
                $("#provinsi_edit").val(data.provinsi).trigger("change");
                $("#kabupaten_edit").val(data.kabupaten).trigger("change");
                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $('#update').submit(function(event){
        event.preventDefault();
        var form = $('#update')[0];
        var data = new FormData(form);

        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'master/update_user'?>",
                type : 'post',
                enctype: 'multipart/form-data',
                data : data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#ks-datatable').DataTable();
                    table.ajax.reload();
                    $("#update")[0].reset();
                    $("#modal_edit").modal('hide'); 
                },
                error : function(data){
                    alert('GAGAL');
                }
                            
            });
        });
    });

    $('#password-form').submit(function(event){
        event.preventDefault();

        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'master/update_password'?>",
                type : 'post',
                data : $( "#password-form" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#ks-datatable').DataTable();
                    table.ajax.reload();
                    $("#password-form")[0].reset();
                    $("#modal_edit").modal('hide'); 
                },
                error : function(data){
                    alert('GAGAL');
                }
                            
            });
        });
    });
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>