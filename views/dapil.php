
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/notie/notie.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Data Dapil</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah Data</button>
                            <!--button class="btn btn-success" data-toggle="modal" data-target="#modal_excel">Upload Excel</button>
                            <button class="btn btn-danger btn-hapus">DELETE</button-->
                            <br><br>
                            <div class="table-responsive">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="input">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <input type="hidden" name="id_dapil">
                        <div class="form-group col-md-6">
                            <div>
                                <label class="form-control-label">Nama DAPIL</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" >
                            </div>
                            <br />

                            <div>
                                <label class="form-control-label">Periode</label>
                                <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode" >
                            </div>
                            <br />

                            <div>
                                <label class="form-control-label">Tipe</label>
                                <select class="form-control ks-select" id="tipe" name="type" style="width: 100%" >
                                    <option value="1">DPR RI</option>
                                    <option value="2">DPRD PROV</option>
                                    <option value="3">DPRD KAB/KOTA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div>
                                <label class="form-control-label">Provinsi</label>
                                <select id="provinsi" class="form-control" name="provinces" style="width:100%" >
                                    <option disabled=true selected=true value="">PILIH</option>
                                    <?php foreach ($provinsi as $value) { ?>
                                    <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <br />

                            <div>
                                <div id="kabupaten_container">
                                    <label class="form-control-label">Kabupaten/ Kota</label>
                                    <select class="form-control ks-select select2" id="kabupaten" name="kab" style="width: 100%" multiple>
                                    </select>
                                    <br /><br />
                                    <input type="checkbox" id="checkbox_kabupaten" >&nbsp;Select All Kabupaten/Kota
                                </div>
                                <div id="kabupaten1_container" class="kabupaten1">
                                    <label class="form-control-label kabupaten1">Kabupaten/ Kota</label>
                                    <select id="kabupaten1" class="form-control ks-select kabupaten1" name="kab"  style="width:100%" >
                                        <option disabled=true selected=true value="">PILIH</option>
                                    </select>
                                </div>
                            </div>
                            <br />

                            <div id="div_kec">
                                <label class="form-control-label">Kecamatan</label>
                                <select id="kecamatan" class="form-control ks-select select2" name="cam"  style="width:100%" multiple>
                                </select>
                                <br /><br />
                                <input type="checkbox" id="checkbox_kecamatan" >&nbsp;Select all kecamatan<br /><br />
                            </div>

                            <div id="div_kel">
                                <label class="form-control-label">Kelurahan</label>
                                <select id="kelurahan" class="form-control ks-select select2" name="lur"  style="width:100%" multiple>
                                </select>
                                <br /><br />
                                <input type="checkbox" id="checkbox_kelurahan" >&nbsp;Select all kelurahan
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_edit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="edit">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <input type="hidden" id="edt_dapil" name="id_dapil">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama DAPIL</label>
                            <input type="text" class="form-control" id="edt_nama" name="nama" placeholder="Nama" >
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select" id="edt_tipe" name="type" style="width: 100%" >
                                <option value="1">DPR RI</option>
                                <option value="2">DPRD PROV</option>
                                <option value="3">DPRD Kab</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Periode</label>
                            <input type="text" class="form-control" id="edt_periode" name="periode" placeholder="Periode" >
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Provinsi</label>
                            <select id="edt_provinsi" class="form-control" name="provinces" style="width:100%" >
                                <option disabled selected value>PILIH</option>
                                <?php foreach ($provinsi as $value) { ?>
                                <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Kabupaten/ Kota</label>
                            <select class="form-control ks-select select2" id="edt_kabupaten" name="kab" style="width: 100%" multiple>
                            </select>
                            <select id="edt_kabupaten1" class="form-control ks-select" name="kab"  style="width:100%" >
                            </select>
                        </div>

                        <div class="form-group col-md-6" id="edt_div_kec">
                            <label class="form-control-label">Kecamatan</label>
                            <select id="edt_kecamatan" class="form-control ks-select select2" name="cam"  style="width:100%" multiple>
                            </select>
                        </div>

                        <div class="form-group col-md-6" id="edt_div_kel">
                            <label class="form-control-label">Kelurahan</label>
                            <select id="edt_kelurahan" class="form-control ks-select select2" name="cam"  style="width:100%" multiple>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal" onclick="resetFormEdit();">Close</button>
                <button type="submit" class="btn btn-crusta">Udate changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_excel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="upload_form">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">File Excel</label>
                            <input type="file" class="form-control" name="file" placeholder="File Excel" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Unique ID</label>
                            <input type="text" class="form-control" name="unique" placeholder="Unique ID" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta" id="save">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Anda Yakin akan menghapus data ini?
      </div>
      <div class="modal-footer">
        <input type="hidden" id="delete_id" value="" />
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="" onclick="execDeleteChange();">Hapus</button>
      </div>
    </div>
  </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>assets/notie/notie.min.js"></script>

<script type="application/javascript">
(function ($) {
    $(document).ready(function() {
        $('.flatpickr').flatpickr();
    });
})(jQuery);
</script>
<?php include 'template/settings.php' ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#menu_swift').trigger('change');
        // $("#kabupaten1").hide();
        $("#kabupaten1_container").hide();
        $("#kabupaten_container").hide();
        $('#div_kec').hide();
        $('#div_kel').hide();
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('Form/get_data_dapil_v2')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id_dapil","title": "No",
                        "orderable": false
                    },
                    {"data": "nama_dapil","title": "DAPIL"},
                    {"data": "type","title": "Tipe"},
                    {"data": "periode","title": "Periode"},
                    {"data": "button","title": "Action"},
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
    });
</script>

<!-- CRUD JAVASCRIPT -->
<script type="text/javascript">
    $('#input').submit(function(event){
        event.preventDefault();
        Pace.track(function(){
             var kab, kec;
             var nama = $('#nama').val();
             var periode = $('#periode').val();
             var tipe = $('#tipe').val();
             var prov = $('#provinsi').val();
             if(tipe=='3') {
                kab = $('#kabupaten1').val();
                kec = $('#kecamatan').select2("val");
                kel = $('#kelurahan').select2("val");
             } else {
                kab = $('#kabupaten').select2("val");
                kec = $('#kecamatan').select2("val");
                kel = "";
             }

            $.ajax({
                url: "<?= site_url().'form/add_dapil'?>",
                type : 'post',
                data : {tipe: tipe, nama: nama, periode: periode, prov: prov, kab: kab, kec: kec, kel: kel},
                dataType: "json",
                success : function(data){
                    console.log(data);
                    $("#provinsi").val("");
                    $('#kabupaten').val(null).trigger("change");
                    $('#kecamatan').val(null).trigger("change");
                    $('#kelurahan').val(null).trigger("change");
                    $("#modal_tambah").modal('hide');
                    document.getElementById("input").reset();
                    var table = $('#table').DataTable();
                    table.ajax.reload();     
                },
                error: function(data){
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });

    $('#edit').submit(function(event){
        event.preventDefault();
        
        var kab, kec;
        var id = $('#edt_dapil').val();
        var nama = $('#edt_nama').val();
        var periode = $('#edt_periode').val();
        var tipe = $('#edt_tipe').val();
        var prov = $('#edt_provinsi').val();
        if(tipe=='3') {
            kab = $('#edt_kabupaten1').val();
            kec = $('#edt_kecamatan').select2("val");
            kel = $('#edt_kelurahan').select2("val");
        } else {
            kab = $('#edt_kabupaten').select2("val");
            kec = $('#edt_kecamatan').select2("val");
            kel = "";
        }

        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'form/edit_dapil'?>",
                type : 'post',
                data : {id: id, tipe: tipe, nama: nama, periode: periode, prov: prov, kab: kab, kec: kec, kel: kel},
                dataType: "json",
                success : function(data){
                    console.log(data);
                    $("#edt_provinsi").val("");
                    $('#edt_kabupaten').val(null).trigger("change");
                    $('#edt_kecamatan').val(null).trigger("change");
                    $('#edt_kelurahan').val(null).trigger("change");
                    
                    var table = $('#table').DataTable();
                    document.getElementById("edit").reset();
                    table.ajax.reload();
                    $("#modal_edit").modal('hide');                
                },
                error: function(data){
                    alert('ERROR');
                }           
            });
        });
        return false;
    });
	
	$("#table").on("click", ".btn-edit",function(event){
        value = $(this).data('id');
		//console.log(value);
        $.ajax({
            url : "<?php echo site_url().'form/get_dapil_by_id/'?>"+value,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                console.log(data);
                var prov = data.area[0].id_provinces;
                $('#edt_dapil').val(data.dapil.id_dapil);
                $('#edt_tipe').val(data.dapil.type);
                $('#edt_nama').val(data.dapil.nama_dapil);
                $('#edt_periode').val(data.dapil.periode);
                $('#edt_provinsi').val(prov);

                if(data.dapil.type=='3'){
                    $('#edt_kabupaten').next(".select2-container").hide();
                    $('#edt_kabupaten').val(null).trigger("change");
                    $("#edt_kabupaten1").show();
                    $('#edt_kabupaten1').append('<option value="'+data.area[0].id_regency+'">'+data.area[0].kabupaten+'</option>');

                    $('#edt_div_kec').show();

                    $('#edt_kecamatan').removeAttr('onchange');
                    $('#edt_kecamatan').val(null).trigger("change");

                    var arrayKecamatan = [];
                    var option = '';
                    var temp = '';
                    for (var i = 0; i<data.area.length ; i++) {
                        if(temp != data.area[i].id_district) {
                            option += '<option value="'+data.area[i].id_district+'">'+data.area[i].kecamatan+'</option>';
                            arrayKecamatan[i] = data.area[i].id_district;
                            temp = data.area[i].id_district;
                        }
                    };
                    $('#edt_kecamatan').html(option);
                    $('#edt_kecamatan').val(arrayKecamatan).trigger('change');
                    $('#edt_kecamatan').attr('onchange','loadKelurahan(this)');

                    $('#edt_div_kel').show();
                    $('#edt_kelurahan').val(null).trigger("change");

                    var arrayKelurahan = [];
                    var option = '';
                    for (var i = 0; i<data.area.length ; i++) {
                        option += '<option value="'+data.area[i].id_district+'">'+data.area[i].kelurahan+'</option>';
                        arrayKelurahan[i] = data.area[i].id_district;
                    };
                    $('#edt_kelurahan').html(option);
                    $('#edt_kelurahan').val(arrayKelurahan).trigger('change');

                } else {
                    $('#edt_kabupaten').removeAttr('onchange');
                    $('#edt_kabupaten').next(".select2-container").show();
                    $("#edt_kabupaten1").hide();
                    $("#edt_kabupaten1").val("");
                    $('#edt_kecamatan').val(null).trigger("change");

                    $('#edt_div_kel').hide();

                    var arrayKab = [];

                    var option = '';
                    for (var i = 0; i<data.area.length ; i++) {
                        option += '<option value="'+data.area[i].id_regency+'">'+data.area[i].kabupaten+'</option>';
                        arrayKab[i] = data.area[i].id_regency;
                    };
                    $('#edt_kabupaten').html(option);

                    $('#edt_kabupaten').val(arrayKab).trigger('change');
                    $('#edt_kabupaten').attr('onchange','loadKecamatan(this)');

                    $('#edt_div_kec').show();
                    $('#edt_kecamatan').val(null).trigger("change");

                    var arrayKecamatan = [];
                    var option = '';
                    var temp = '';
                    for (var i = 0; i<data.area.length ; i++) {
                        if(temp != data.area[i].id_district) {
                            option += '<option value="'+data.area[i].id_district+'">'+data.area[i].kecamatan+'</option>';
                            arrayKecamatan[i] = data.area[i].id_district;
                            temp = data.area[i].id_district;
                        }
                    };
                    $('#edt_kecamatan').html(option);
                    $('#edt_kecamatan').val(arrayKecamatan).trigger('change');
                    // $('#edt_kecamatan').attr('onchange','loadKelurahan(this)');

                }

                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $("#table").on("click", ".btn-delete",function(event){
        var id = $(this).data('id');
        $('#delete_id').val(id);
    });

    function execDeleteChange() {
        var id = $('#delete_id').val();
        console.log(id);

        $.ajax({
            url : "<?php echo site_url('form/delete_dapil')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data){
                var table = $('#table').DataTable();
                table.ajax.reload();
                $('#modalDelete').modal('hide');
            },
            error: function (data){
                console.log(data);
                alert('Error get data from ajax');
            }
        });
    }

    // $("#table").on("click", ".btn-delete",function(event){
    //     id = $(this).data('id');
    //     event.preventDefault();
            
    //     Pace.track(function(){
    //         $.ajax({
    //             url : "<?php echo site_url('form/delete_dapil')?>/"+id,
    //             type: "POST",
    //             dataType: "JSON",
    //             success: function(data){
    //                 var table = $('#table').DataTable();
    //                 table.ajax.reload();
    //             },
    //             error: function (data){
    //                 console.log(data);
    //                 alert('Error get data from ajax');
    //             }
    //         });
    //     });
    // });

    $("#table").on("click", ".btn-blk",function(event){
        id = $(this).data('id');
        event.preventDefault();
            
        Pace.track(function(){
            $.ajax({
                url : "<?php echo site_url('form/block_coordinator')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                },
                error: function (data){
                    console.log(data);
                    alert('Error get data from ajax');
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $('#modal_edit').on('hidden.bs.modal', function(e){
        $("#kab_edit option").remove();
        $("#kel_edit option").remove();
        $("#kec_edit option").remove();
    });

    $('#modal_tambah').on('hidden.bs.modal', function(e){
        $("#kab_edit option").remove();
        $("#kel_edit option").remove();
        $("#kec_edit option").remove();

        $('[name="id"]').val('');
        $('[name="address"]').val('');
        $('[name="type"]').val('');
        $('[name="id_regency"]').val('');
        $('[name="id_district"]').val('');
        $('[name="id_provinces"]').val('');
        $('[name="id_village"]').val('');
        $('[name="cp"]').val('');
        $('[name="nik"]').val('');
        $('[name="phone"]').val('');

    });
</script>

<script type="text/javascript">
    $('.select2').select2();
    $('#provinsi').select2({
        placeholder: "Pilih Provinsi"
    });
    $('#kabupaten').select2({
        placeholder: "Pilih Kabupaten"
    });
    $('#kecamatan').select2({
        placeholder: "Pilih Kecamatan"
    });
    $('#kelurahan').select2({
        placeholder: "Pilih Kelurahan"
    });
</script>

<script type="text/javascript">
    $("#tipe").change(function(){
        var value = $(this).val();
        console.log(value);
        if (value=='3') {
            // $('#kabupaten').next(".select2-container").hide();
            // $('#checkbox_kabupaten').hide();
            $('#kabupaten_container').hide();
            $('#kabupaten').val(null).trigger("change");
            $("#kabupaten1").show();
            //$('#kabupaten1_container').show();
            $('.kabupaten1').show();
            $('#div_kec').show();
            $('#div_kel').show();
        } else {
            // $('#kabupaten').next(".select2-container").show();
            // $('#checkbox_kabupaten').show();
            $('#kabupaten_container').show();
            // $("#kabupaten1").hide();
            //$('#kabupaten1_container').hide();
            $('.kabupaten1').hide();
            $("#kabupaten1").val("");
            $('#kecamatan').val(null).trigger("change");
            $('#div_kec').show();
            $('#div_kel').hide();
        }
    });
</script>

<script type="text/javascript">
    $("#provinsi").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kabupaten").html(respond);
                $("#kabupaten1").html('<option disabled=true selected=true value="">PILIH</option>'+respond);
            }
        })
    })

    $("#edt_provinsi").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#edt_kabupaten").html(respond);
                $("#edt_kabupaten1").html('<option disabled=true selected=true value="">PILIH</option>'+respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kabupaten1").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kecamatan").html(respond);
            }
        })
    });

    $("#kabupaten").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan_multi') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kecamatan").html(respond);
            }
        })
    });

    // $("#edt_kabupaten1").change(function(){
    //     $.ajax({
    //         url: "<?php echo site_url('form/get_kecamatan') ?>",
    //         cache: false,
    //         type:"POST",
    //         data:{id:$(this).val()},
    //         success: function(respond){
    //             $("#edt_kecamatan").html(respond);
    //         }
    //     })
    // });

    function loadKecamatan(that) {
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan_multi') ?>",
            cache: false,
            type:"POST",
            data:{id:$(that).val()},
            success: function(respond){
                $("#edt_kecamatan").html(respond);
            }
        })
    }
</script>
<script type="text/javascript">
    $("#kecamatan").change(function(){
        console.log('kelurahan 2');
        $.ajax({
            url: "<?php echo site_url('form/get_kelurahan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kelurahan").html(respond);
            }
        })
    });

    function loadKelurahan(that) {
        console.log('kelurahan 1');
        $.ajax({
            url: "<?php echo site_url('form/get_kelurahan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(that).val()},
            success: function(respond){
                $("#edt_kelurahan").html(respond);
            }
        })
    }
    //  $("#edt_kecamatan").change(function(){
    //     console.log('kelurahan 1');
    //     $.ajax({
    //         url: "<?php echo site_url('form/get_kelurahan') ?>",
    //         cache: false,
    //         type:"POST",
    //         data:{id:$(this).val()},
    //         success: function(respond){
    //             $("#edt_kelurahan").html(respond);
    //         }
    //     })
    // });
</script>

<script type="text/javascript">
    $("#provinsi_edit").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kabupaten') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kab_edit").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kab_edit").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kecamatan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kec_edit").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $("#kec_edit").change(function(){
        $.ajax({
            url: "<?php echo site_url('form/get_kelurahan') ?>",
            cache: false,
            type:"POST",
            data:{id:$(this).val()},
            success: function(respond){
                $("#kel_edit").html(respond);
            }
        })
    })
</script>
<script type="text/javascript">
    $('#upload_form').submit(function(e){
        e.preventDefault();
        var id = $('[name="unique"]').val();
        var id_uri = encodeURI(id);
        $.ajax({
            url: "<?php echo site_url('data/cek_unique_id/') ?>"+id_uri,
            cache: false,
            type:"POST",
            success: function(respond){
                if(respond == 0){
                    alert('UNIQUE ID sudah ada');
                }else{
                    $("#save").prop('disabled', true);
                    var form = $('#upload_form')[0];
                    var data = new FormData(form);
                    Pace.track(function(){
                        $.ajax({
                            url  : "<?= site_url().'data/upload_guraklih'?>",
                            type : 'POST',
                            enctype: 'multipart/form-data',
                            data : data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            timeout: 600000,
                            dataType: "json",
                            success: function(data){
                                $("#modal_excel").modal('hide'); 
                                $("#save").prop('disabled', false);
                                notie.alert({ type: 1, text: 'Data Berhasil Diupload sejumlah '+data, time: 2,position: 'bottom' })
                                var table = $('#table').DataTable();
                                table.ajax.reload();
                            },
                            error: function(xhr, status, error) {
                                var err = eval("(" + xhr.responseText + ")");
                                alert(err.Message);
                            }
                        });
                    })
                }
            }
        })
      // e.preventDefault();
      // 
    });
</script>
<script type="text/javascript">
    $( ".btn-hapus" ).click(function() {
        notie.input({
            text: 'Masukkan UNIQUE ID',
            submitText: 'Submit',
            cancelText: 'Cancel',
            position: 'bottom',
            cancelCallback: function (value) {

            },
            submitCallback: function (value) {
                id_uri = encodeURI(value);
                if(!id_uri){
                    alert('Unique ID HARUS DIISI');
                }else{
                    Pace.track(function(){
                        $.ajax({
                            url: "<?php echo site_url('data/check_jumlah_guraklih/') ?>"+id_uri,
                            type:"POST",
                            success: function(respond){
                                if(respond == 0){
                                    notie.alert({ type: 3, text: 'Data dengan unique id tersebut tidak ada', time: 2,position: 'bottom'})
                                }else{
                                    notie.confirm({
                                        text: respond,
                                        position: 'bottom',
                                        submitText: 'Hapus',
                                        cancelText: 'Cancel',
                                        cancelCallback: function () {
                                            
                                        },
                                        submitCallback: function () {
                                            Pace.track(function(){
                                                $.ajax({
                                                    url: "<?php echo site_url('data/hapus_data_guraklih/') ?>"+id_uri,
                                                    cache: false,
                                                    type:"POST",
                                                    success: function(respond){
                                                        if(respond == 1){
                                                            notie.alert({ type: 1, text: 'Data berhasil dihapus', time: 2,position: 'bottom'});
                                                            var table = $('#table').DataTable();
                                                            table.ajax.reload();
                                                        }else{
                                                            notie.alert({ type: 3, text: 'Data tidak berhasil dihapus', time: 2,position: 'bottom'});
                                                        }
                                                        
                                                    }
                                                })
                                            })
                                        }
                                    })
                                }
                            }
                        })
                    })    
                }
              },
        })
    });
</script>
<script type="text/javascript">
    $("#checkbox_kabupaten").click(function(){
        if($("#checkbox_kabupaten").is(':checked') ){
            $("#kabupaten > option").prop("selected","selected");
            $("#kabupaten").trigger("change");
        }else{
            $("#kabupaten > option").removeAttr("selected");
             $("#kabupaten").trigger("change");
         }
    });

    $("#checkbox_kecamatan").click(function(){
        if($("#checkbox_kecamatan").is(':checked') ){
            $("#kecamatan > option").prop("selected","selected");
            $("#kecamatan").trigger("change");
        }else{
            $("#kecamatan > option").removeAttr("selected");
             $("#kecamatan").trigger("change");
         }
    });

    $("#checkbox_kelurahan").click(function(){
        if($("#checkbox_kelurahan").is(':checked') ){
            $("#kelurahan > option").prop("selected","selected");
            $("#kelurahan").trigger("change");
        }else{
            $("#kelurahan > option").removeAttr("selected");
             $("#kelurahan").trigger("change");
         }
    });

    function resetFormEdit(idname) {
        console.log('edit');
        $('input,select,textarea').val('');
        $("#edt_kabupaten1").val("");
        $('#edt_kabupaten').val(null).trigger("change");
    }
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>