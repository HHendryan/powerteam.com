
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>SIPP</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="196x196" href="<?=base_url()?>assets/img/favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/oslo-gray.css">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/themes/sidebar-black.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/sidebar/default.css">
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/datatables-net/extensions/buttons/css/buttons.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/select2/css/select2.min.css"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/select2/select2.min.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/pace/pace.css"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/flatpickr/flatpickr.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/libs/flatpickr/flatpickr.min.css"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/notie/notie.min.css">
</head>
<!-- END HEAD -->
<?php include 'template/header.php' ?> 

<div class="ks-page-container">
    
    <?php include 'template/sidebar.php' ?>

    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Data DAPIL</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body ks-content-nav">
                <div class="ks-nav-body">
                    <div class="ks-nav-body-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-2">
                                    <button class="btn btn-crusta" data-toggle="modal" data-target="#modal_tambah">Tambah Data</button>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control ks-select select2" id="dapil" style="width: 100%" required>
                                        <option disabled selected value>PILIH</option>
                                        <?php foreach ($dapil as $key => $value) { ?>
                                            <option value="<?=$value->id?>|<?=$value->type?>"><?=$value->nama_dapil?> - <?=$value->periode?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-crusta add-wilayah">Tambah Wilayah DAPIL</button>
                                </div>
                            </div>
                            
                            <!-- <button class="btn btn-success" data-toggle="modal" data-target="#modal_excel">Upload Excel</button> -->
                            <!-- <button class="btn btn-danger btn-hapus">DELETE</button> -->
                            <br><br>
                            <div class="table-responsive">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tambah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah DAPIL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="input_dapil">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Tipe</label>
                            <select class="form-control ks-select select2" name="type" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="1">DPR RI</option>
                                <option value="2">DPRD PROVINSI</option>
                                <option value="3">DPRD KABUPATEN</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Periode</label>
                            <select class="form-control ks-select select2" name="periode" style="width: 100%" required>
                                <option disabled selected value>PILIH</option>
                                <option value="2014">2014</option>
                                <option value="2019">2019</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Nama DAPIL</label>
                            <input type="text" class="form-control" name="nama_dapil" placeholder="Nama" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-content" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_wilayah" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah DAPIL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <form id="input_wilayah_dapil">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="form-control-label">ID DAPIL</label>
                            <input type="text" class="form-control" name="id_dapil" readonly required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Parent Area</label>
                            <select class="form-control ks-select select2" id="parent_area" style="width: 100%" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Area</label>
                            <select class="form-control ks-select select2" name="id_area" style="width: 100%" required>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-crusta">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>libs/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>libs/responsejs/response.min.js"></script>
<script src="<?=base_url()?>libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?=base_url()?>libs/tether/js/tether.min.js"></script>
<script src="<?=base_url()?>libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?=base_url()?>libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>libs/flexibility/flexibility.js"></script>
<script src="<?=base_url()?>libs/noty/noty.min.js"></script>
<script src="<?=base_url()?>libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>assets/scripts/common.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?=base_url()?>libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>libs/jszip/jszip.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/pdfmake.min.js"></script>
<script src="<?=base_url()?>libs/pdfmake/vfs_fonts.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>libs/datatables-net/extensions/buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>libs/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/styles/pace/pace.min.js"></script>
<script src="<?=base_url()?>libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>assets/notie/notie.min.js"></script>
<?php include 'template/settings.php' ?>
<script type="application/javascript">
    $(document).ready(function() {
        $('.flatpickr').flatpickr();
        $('#menu_swift').trigger('change');
    });
    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
      return true;
    }
</script>
<script type="text/javascript">
    $('.select2').select2();

    $('#input_dapil').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'pileg/input_dapil'?>",
                type : 'post',
                data : $( "#input_dapil" ).serialize(),
                success : function(data){
                    console.log(data);
                    $("#dapil").html(data);
                    document.getElementById("input_dapil").reset();
                    $("#modal_tambah").modal('hide');                
                },
                error: function(data){
                	console.log(data);
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });

    $( "#dapil" ).change(function() {
        value = $(this).val();
        split = value.split('|');
        id_dapil = split[0];
        type = split[1];
        Pace.track(function(){
            table = $("#table").dataTable();
            table.api().ajax.url('<?=site_url("pileg/get_table_dapil/")?>'+id_dapil+'/'+type).load();
        });
    });

    $(".add-wilayah").click(function(){
        value = $("#dapil").val();
        if(value){
            split = value.split('|');
            id_dapil = split[0];
            type = split[1];
            $("[name=id_dapil]").val(id_dapil);
            $.ajax({
                url: "<?= site_url().'pileg/get_wilayah_dapil/'?>"+tipe,
                type : 'post',
                data : $( "#input_dapil" ).serialize(),
                success : function(data){
                    console.log(data);
                    $("#dapil").html(data);
                    document.getElementById("input_dapil").reset();
                    $("#modal_tambah").modal('hide');                
                },
                error: function(data){
                    console.log(data);
                    alert('ERROR');
                }
                            
            });
        }
        
    })
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#table").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#table_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                             api.search(this.value).draw();
                        }
                    });
                },
                oLanguage: {
                    sProcessing: "loading..."
                },
                processing: true,
                serverSide: true,
                ajax: {"url": "<?php echo site_url('pileg/get_table_dapil/0/1')?>", "type": "POST"},
                columns: [
                    {
                        "data": "id","title": "No",
                        "orderable": false
                    },
                    {"data": "id_dapil","title": "ID DAPIL"},
                    {"data": "area","title": "Area"},
                    {"data": "button","title": "Action"},
                ],
                order: [[1, 'asc']],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
        });
    });
</script>

<!-- CRUD JAVASCRIPT -->
<script type="text/javascript">
    $('#edit').submit(function(event){
        event.preventDefault();
                    
        Pace.track(function(){
            $.ajax({
                url: "<?= site_url().'form/edit'?>",
                type : 'post',
                data : $( "#edit" ).serialize(),
                dataType: "json",
                success : function(data){
                    console.log(data);
                    var table = $('#table').DataTable();
                    document.getElementById("edit").reset();
                    table.ajax.reload();
                    $("#modal_edit").modal('hide');                
                },
                error: function(data){
                    alert('ERROR');
                }
                            
            });
        });
        return false;
    });
	
	$("#table").on("click", ".btn-edit",function(event){
        value = $(this).data('id');
		console.log(value);
        $.ajax({
            url : "<?php echo site_url().'form/get_intangible/'?>"+value,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                console.log(data);
                $('[name="id_intangible"]').val(data.id_intangible);
                $('[name="address"]').val(data.address);
                $('[name="gender"]').val(data.gender);
                $('[name="id_city"]').val(data.id_city);
                $('[name="id_districts"]').val(data.id_districts);
                $('[name="id_pertokohan"]').val(data.id_pertokohan);
                $('[name="id_provinces"]').val(data.id_provinces);
                $('[name="id_village"]').val(data.id_village);
                $('[name="inclination"]').val(data.id_parpol);
                $('[name="influence"]').val(data.influence);
                $('[name="institution"]').val(data.institution);
                $('[name="jabatan"]').val(data.jabatan);
                $('[name="email"]').val(data.email);
                $('[name="facebook"]').val(data.facebook);
                $('[name="twitter"]').val(data.twitter);
                $('[name="last_education"]').val(data.last_education);
                $('[name="name"]').val(data.name);
                $('[name="nik"]').val(data.nik);
                $('[name="occupation"]').val(data.occupation);
                $('[name="organisation"]').val(data.organisation);
                $('[name="phone"]').val(data.phone);
                $('[name="specific_history"]').val(data.specific_history);
                $('[name="status"]').val(data.status);
                $('#kab_edit').append('<option value="'+data.id_kabupaten+'">'+data.kabupaten+'</option>');
                $('#kec_edit').append('<option value="'+data.id_kecamatan+'">'+data.kecamatan+'</option>');
                $('#kel_edit').append('<option value="'+data.id_kelurahan+'">'+data.kelurahan+'</option>');
                $('[name="tanggal_lahir"]').val(data.tanggal_lahir);
                $('[name="tempat_lahir"]').val(data.tempat_lahir);
                $('[name="inclination"]').val(data.inclination);
                $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $("#table").on("click", ".btn-delete",function(event){
        id = $(this).data('id');
        event.preventDefault();
            
        Pace.track(function(){
            $.ajax({
                url : "<?php echo site_url('form/delete')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                },
                error: function (data){
                    console.log(data);
                    alert('Error get data from ajax');
                }
            });
        });
    });
</script>
<script type="text/javascript">
    
</script>
<div class="ks-mobile-overlay"></div>
</body>
</html>