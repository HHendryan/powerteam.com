<div class="ks-navbar-horizontal ks-info">
    <ul class="nav nav-pills">
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>data">Data DPT</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>tim">Data TIM</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>KTA">KTA</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>jalur">Jalur</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>suku">Suku</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>agama">Agama</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>usia">Usia</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>cari/dpt">Cari DPT</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>cari/tim">Cari Tim</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Penyelenggara</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url()?>penyelenggara/kpu" target="_blank">KPU</a>
                <a class="dropdown-item" href="<?=base_url()?>penyelenggara/panwas" target="_blank">PANWAS</a>
                <a class="dropdown-item" href="<?=base_url()?>penyelenggara/bawaslu" target="_blank">BAWASLU</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Political Score</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url()?>score/real_count" target="_blank">Real Count</a>
                <a class="dropdown-item" href="<?=base_url()?>score/quick_count" target="_blank">Quick Count</a>
                <a class="dropdown-item" href="<?=base_url()?>score/exit_pool" target="_blank">Exit Poll</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Master Data</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url()?>master/suku" target="_blank">Suku</a>
                <a class="dropdown-item" href="<?=base_url()?>master/jalur" target="_blank">Jalur</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hasil Olah</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url()?>olah/ganda_by_nik" target="_blank">DPT Ganda By NIK</a>
                <a class="dropdown-item" href="<?=base_url()?>olah/ganda_by_nama" target="_blank">DPT Ganda By Nama</a>
                <a class="dropdown-item" href="<?=base_url()?>olah/ganda_by_nama_sebaran" target="_blank">Sebaran DPT Ganda By Nama</a>s
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tools</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="tools/import_tps" target="_blank">Import TPS</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="ks-icon la la-upload"></span>
                <span>Proses Upload</span>
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?=base_url()?>data/upload_belum">List Belum Upload</a>
                <a class="dropdown-item" href="<?=base_url()?>data/upload_sudah">List Sudah Upload</a>
            </div>
        </li>
    </ul>
</div>