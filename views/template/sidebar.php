    <!-- BEGIN DEFAULT SIDEBAR -->
<div class="ks-column ks-sidebar ks-info">
    <div class="ks-wrapper ks-sidebar-wrapper">
        <section>
            <h5 class="ks-header">Main</h5>
            <ul class="nav nav-pills nav-stacked">
            	<li class="nav-item">
	                <a class="nav-link" href="<?=base_url()?>home">
	                    <span class="ks-icon la la-tachometer"></span>
	                    <span>Dashboard</span>
	                </a>
	            </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-icon la la-user"></span>
                        <span>Koordinator</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?=base_url()?>form/list_guraklih">List Koordinator</a>
                        <a class="dropdown-item" href="<?=base_url()?>data/guraklih_result">Result</a>
                    </div>
                </li>
                <!--li class="nav-item">
                    <a class="nav-link" href="<?=base_url()?>data/guraklih_print_dpt">
                        <span class="ks-icon la la-print"></span>
                        <span>Print DPT</span>
                    </a>
                </li-->
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url()?>data/guraklih_check_area_prospek">
                        <span class="ks-icon la la-check-square"></span>
                        <span>Checklist DPT</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url()?>guraklih/monitoring_guraklih">
                        <span class="ks-icon la la-eye"></span>
                        <span>Monitoring Guraklih</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-icon la la-bullhorn"></span>
                        <span>Influencer</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?=base_url()?>form/tangible">Tangible</a>
                        <a class="dropdown-item" href="<?=base_url()?>form/intangible">Intangible</a>
                        <a class="dropdown-item" href="<?=base_url()?>data/influence_result">Result</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-icon la la-bookmark"></span>
                        <span>Target</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?=base_url()?>target/entry">Entry</a>
                        <!-- <a class="dropdown-item" href="<?=base_url()?>target/monitoring">Monitoring</a> -->
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-icon la la-user"></span>
                        <span>DPT Ganda</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?=base_url()?>data/double_dpt">By NIK</a>
                        <a class="dropdown-item" href="<?=base_url()?>data/double_dpt2">By Nama & Tanggal Lahir</a>
                        <a class="dropdown-item" href="<?=base_url()?>data/double_dpt3">By NIK & Nama & Tanggal Lahir</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-icon la la-upload"></span>
                        <span>Proses Upload</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?=base_url()?>data/dashboard_upload">Dashboard Upload</a>
                        <a class="dropdown-item" href="<?=base_url()?>form/upload_belum">List Belum Upload</a>
                        <a class="dropdown-item" href="<?=base_url()?>form/upload_sudah">List Sudah Upload</a>
                    </div>
                </li>
            </ul>
        </section>
    </div>
</div>
<!-- END DEFAULT SIDEBAR -->	