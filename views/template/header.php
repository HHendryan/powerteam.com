
<body class="ks-navbar-fixed ks-sidebar-sections ks-sidebar-position-fixed ks-theme-primary-light ks-page-loading"> <!-- remove ks-page-header-fixed to unfix header -->
<!-- BEGIN HEADER -->
	<nav class="navbar ks-navbar">
	    <!-- BEGIN HEADER INNER -->
	    <!-- BEGIN LOGO -->
	    <div href="<?=base_url()?>" class="navbar-brand">
	    	<a href="#" class="ks-sidebar-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
        	<a href="#" class="ks-sidebar-mobile-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
        	
	        <div class="ks-navbar-logo">
	            <!-- <a href="<?=base_url()?>" class="ks-logo"><img src="<?=base_url()?>/assets/img/logo1.png"></a> -->
	            <!-- <a href="<?=base_url()?>" class="ks-logo"><img src="<?=base_url()?>/assets/img/teknopol.png"></a> -->
	        </div>
	    </div>
	    <!-- END LOGO -->

	    <!-- BEGIN MENUS -->
	    <div class="ks-wrapper">
	        <nav class="nav navbar-nav">
	            <!-- BEGIN NAVBAR MENU -->
	            <div class="ks-navbar-menu">
	            </div>
	            <!-- END NAVBAR MENU -->

	            <!-- BEGIN NAVBAR ACTIONS -->
	            <div class="ks-navbar-actions">
	                <!-- BEGIN NAVBAR USER -->
	                <div class="nav-item nav-link btn-action-block">
                    <a class="btn btn-crusta-outline">
                        <span class="ks-action"><?php echo $username ?></span>
                        <span class="ks-description"><?php echo $userlevel ?></span>
                    </a>
                </div>
	                <div class="nav-item dropdown ks-user">
	                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
	                        <span class="ks-avatar">
	                            <img src="<?=base_url()?>assets/img/default.jpg" width="36" height="36">
	                           
	                        </span>
	                    </a>
	                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
	                        <!-- <a class="dropdown-item" href="#">
	                            <span class="la la-user ks-icon"></span>
	                            <span>Profile</span>
	                        </a>
	                        <a class="dropdown-item" href="#">
	                            <span class="la la-wrench ks-icon" aria-hidden="true"></span>
	                            <span>Settings</span>
	                        </a>
	                        <a class="dropdown-item" href="#">
	                            <span class="la la-question-circle ks-icon" aria-hidden="true"></span>
	                            <span>Help</span>
	                        </a> -->
	                        <a class="dropdown-item" href="<?=site_url('auth/logout')?>">
	                            <span class="la la-sign-out ks-icon" aria-hidden="true"></span>
	                            <span>Logout</span>
	                        </a>
	                    </div>
	                </div>
	                <!-- END NAVBAR USER -->
	            </div>
	            <!-- END NAVBAR ACTIONS -->
	        </nav>
	    </div>
	    <!-- END MENUS -->
	    <!-- END HEADER INNER -->
	</nav>
	<!-- END HEADER -->