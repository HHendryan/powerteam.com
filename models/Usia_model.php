<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Usia_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}
	
	function get_kabupaten($id_prov){
		$list="<option value='all' selected>ALL</option>";
		$this->db->order_by('kabupaten','ASC');
		$result= $this->db->get_where('kabupaten',array('id_provinsi'=>$id_prov))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->kabupaten</option>";
		}
		return $list;
	}
	function get_kecamatan($id_kab){
		$list="<option value='all' selected>ALL</option>";
		$this->db->order_by('kecamatan','ASC');
		$result= $this->db->get_where('kecamatan',array('id_kabupaten'=>$id_kab))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->kecamatan</option>";
		}
		return $list;
	}

	function get_data_usia($prov,$kab,$kec){
		if($prov == 'all' AND $kab == 'all' AND $kec=='all'){
			$data = $this->db->query("SELECT provinsi.provinsi as wilayah, sum(jml_1) as jml_1, sum(jml_2) as jml_2, sum(jml_3) as jml_3, sum(jml_4) as jml_4, sum(jml_5) as jml_5 from sum_umur inner join provinsi on provinsi.id = sum_umur.provinsi group by provinsi.provinsi")->result();
		}else if($prov != 'all' AND $kab == 'all' AND $kec=='all'){
			$data = $this->db->query("SELECT kabupaten.kabupaten as wilayah, sum(jml_1) as jml_1, sum(jml_2) as jml_2, sum(jml_3) as jml_3, sum(jml_4) as jml_4, sum(jml_5) as jml_5 from sum_umur inner join kabupaten on kabupaten.id = sum_umur.kabupaten WHERE kabupaten.id_provinsi = '$prov' group by kabupaten.kabupaten")->result();
		}else if($prov != 'all' AND $kab != 'all' AND $kec=='all'){
			$data = $this->db->query("SELECT kecamatan.kecamatan as wilayah, sum(jml_1) as jml_1, sum(jml_2) as jml_2, sum(jml_3) as jml_3, sum(jml_4) as jml_4, sum(jml_5) as jml_5 from sum_umur inner join kecamatan on kecamatan.id = sum_umur.kecamatan WHERE kecamatan.id_kabupaten = '$kab' group by kecamatan.kecamatan")->result();
		}else if($prov != 'all' AND $kab != 'all' AND $kec!='all'){
			$data = $this->db->query("SELECT kelurahan.kelurahan as wilayah, sum(jml_1) as jml_1, sum(jml_2) as jml_2, sum(jml_3) as jml_3, sum(jml_4) as jml_4, sum(jml_5) as jml_5 from sum_umur inner join kelurahan on kelurahan.id = sum_umur.kelurahan WHERE kelurahan.id_kecamatan = '$kec' group by kelurahan.kelurahan")->result();
		}
		$i=0;
		foreach ($data as $value) {
			$data[$i]->jml_1 = number_format($value->jml_1);
			$data[$i]->jml_2 = number_format($value->jml_2);
			$data[$i]->jml_3 = number_format($value->jml_3);
			$data[$i]->jml_4 = number_format($value->jml_4);
			$data[$i]->jml_5 = number_format($value->jml_5);
			$i++;
		}
		return $data;
	}
}	