<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Tangible_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}
	
	public function get_pertokohan_tangible() 
    {
      	$result = $this->db->get_where('m_pertokohan', array('type' => '1'))->result();
        if($result)
        {
            foreach ($result as $key => $row)
            {
                $ret [$row->id_pertokohan] = $row->nama;
            }
        }
        
        return $ret;
    }

    public function get_pertokohan_intangible() 
    {
      
        $result = $this->db->get_where('m_pertokohan', array('type' => '0'))->result();

        if($result)
        {
            foreach ($result as $key => $row)
            {
                $ret [$row->id_pertokohan] = $row->nama;
            }
        }
        
        return $ret;
    }
	
	public function get_pertokohan() 
    {
      
        $result = $this->db->get('m_pertokohan')
                           ->result();

        if($result)
        {
            foreach ($result as $key => $row)
            {
                $ret [$row->id_pertokohan] = $row->nama;
            }
        }
        
        return $ret;
    }

	public function get_parpol() 
    {
      
        $result = $this->db->get('m_parpol')
                           ->result();

        if($result)
        {
            foreach ($result as $key => $row)
            {
                $ret [$row->id_parpol] = $row->name;
            }
        }
        
        return $ret;
    }

	function get_kabupaten($id_prov){
		//$list="<option disabled=true selected=true value=''>PILIH</option>";
		$list="";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_regencies',array('province_id'=>$id_prov))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}
	function get_kecamatan($id_kab){
		//$list="<option disabled=true selected=true value=''>PILIH</option>";
		$list="";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_districts',array('regency_id'=>$id_kab))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}
	
	function get_kecamatan_multi($id_kab){
		//$list="<option disabled=true selected=true value=''>PILIH</option>";
		$list="";
		// $this->db->order_by('name','ASC');
		// $result= $this->db->get_where('m_districts',array('regency_id'=>$id_kab))->result();

		$thein = array();
		foreach ($id_kab as $k => $v) {
			$thein[] = $v;
		}
		
		$sql = "
			SELECT *
			FROM 
				`m_districts`
			WHERE 
				`regency_id` IN (".implode(',',$thein).")
			ORDER BY `name` ASC
		";
		$query = $this->db->query($sql);
		$result = $query->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}
	
	function get_kelurahan($id_kec){
		//$list="<option disabled=true selected=true value=''>PILIH</option>";
		$list="";
		//$this->db->order_by('name','ASC');
		//$result= $this->db->get_where('m_villages',array('district_id'=>$id_kec))->result();
		
		$thein = array();
		foreach ($id_kec as $k => $v) {
			$thein[] = $v;
		}
		
		$sql = "
			SELECT *
			FROM 
				`m_villages`
			WHERE 
				`district_id` IN (".implode(',',$thein).")
			ORDER BY `name` ASC
		";
		$query = $this->db->query($sql);
		$result = $query->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}

	function get_kelurahan_by_kec($id_kec){
		$list="";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_villages',array('district_id'=>$id_kec))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}
	
	//CRUD
    function input($table,$data){
    	$user = $this->session->userdata('obc');
    	$data['upd'] = $user['userid'];
    	$this->db->insert($table, $data);
		echo json_encode($data);
    }

    function edit($table,$data){
    	$user = $this->session->userdata('obc');
    	$data['upd'] = $user['userid'];
		$this->db->where('id', $data['id']);
		$this->db->update($table, $data);
		echo json_encode($data);
	}
	
	function add_tangible($data){
		$data['id_intangible'] = $this->create_id();
		$data['type'] = 'tangible';
		if($this->db->insert('intangible', $data)){
			$dats['id_surveyor'] = 771;
			$dats['id_intangible'] = $data['id_intangible'];
			$this->db->insert('surveyor_has_intangible', $dats);
			echo json_encode('Tambah Sukses');
		}
	}
	
	function add_intangible($data){
		$data['id_intangible'] = $this->create_id();
		$data['type'] = 'intangible';
		if($this->db->insert('intangible', $data)){
			$dats['id_surveyor'] = 771;
			$dats['id_intangible'] = $data['id_intangible'];
			$this->db->insert('surveyor_has_intangible', $dats);
			echo json_encode('Tambah Sukses');
		}
	}

	function create_id(){
		$id_lama = $this->db->query('SELECT id_intangible from intangible order by id_intangible desc limit 1')->row('id_intangible');
		return $id_lama+1;
	}
}