<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Penyelenggara_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}

	function update_penyelenggara($id,$data){
		if($this->db->update('m_penyelenggara', $data, array('id' => $id))){
			echo json_encode('SUKSES');	
		}
	}

	function delete_penyelenggara($id){
		$this->db->delete('m_penyelenggara', array('id' => $id));
		echo json_encode('SUKSES');
	}

	function kpu(){
		$data = $this->db->get_where('m_penyelenggara',array('type' => 'kpu'))->result();
		$i=0;
		foreach ($data as $value) {
			$data[$i]->nomor = $i+1;
			$data[$i]->button = '<button data-id='."'".$value->id."'".' class="btn btn-info btn-sm btn-edt"><span class="la la-edit"></span></button><button data-id='."'".$value->id."'".' class="btn btn-danger btn-sm btn-del"><span class="la la-trash"></span></button>';
		}
		return $data;
	}

	function add_kpu($data){
		$data['type'] = 'kpu';

		if($this->db->insert('m_penyelenggara',$data)){
			echo json_encode('SUKSES');
		}	
	}

	function add_panwas($data){
		$data['type'] = 'panwas';

		if($this->db->insert('m_penyelenggara',$data)){
			echo json_encode('SUKSES');
		}	
	}

	function add_bawaslu($data){
		$data['type'] = 'bawaslu';

		if($this->db->insert('m_penyelenggara',$data)){
			echo json_encode('SUKSES');
		}	
	}

	function panwas(){
		$data = $this->db->get_where('m_penyelenggara',array('type' => 'panwas'))->result();
		$i=0;
		foreach ($data as $value) {
			$data[$i]->nomor = $i+1;
			$data[$i]->button = '<button data-id='."'".$value->id."'".' class="btn btn-info btn-sm btn-edt"><span class="la la-edit"></span></button><button data-id='."'".$value->id."'".' class="btn btn-danger btn-sm btn-del"><span class="la la-trash"></span></button>';
		}
		return $data;
	}

	function bawaslu(){
		$data = $this->db->get_where('m_penyelenggara',array('type' => 'bawaslu'))->result();
		$i=0;
		foreach ($data as $value) {
			$data[$i]->nomor = $i+1;
			$data[$i]->button = '<button data-id='."'".$value->id."'".' class="btn btn-info btn-sm btn-edt"><span class="la la-edit"></span></button><button data-id='."'".$value->id."'".' class="btn btn-danger btn-sm btn-del"><span class="la la-trash"></span></button>';
		}
		return $data;
	}
}	