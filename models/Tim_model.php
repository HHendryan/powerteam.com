<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Tim_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}

	//GENERAL FUNCTION
	function get_data_by_nik($nik){
		$data=$this->db->get_where('dpt_sumatera_selatan_view', array('nik' => $nik,'tim_id' => NULL))->result();
		if($data){
			$data[0]->kelurahan = $this->db->get_where('kelurahan', array('id' => $data[0]->kelurahan))->row('kelurahan');
			$data[0]->umur =$this->get_umur($data[0]->tanggal_lahir);
		}else{
			$data='N';
		}
		
		return $data;
	}

	function get_umur($date){
		$diff = abs(strtotime(date("Y-m-d")) - strtotime($date));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		$result = "$years Tahun, $months Bulan, $days Hari";
		return $result;
	}

	function update_tim($id,$data){
		if($this->db->update('tim', $data, array('id' => $id))){
			echo json_encode('SUKSES');	
		}
	}

	//KORWIL FUNCTION
	function get_data_korwil(){
		$user = $this->Auth_model->get_data();
		$data = $this->db->get_where('tim_view', array('id_user' => $user->id,'korwil ' => '1'))->result();
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->kelurahan=$this->db->get_where('kelurahan', array('id' => $value->kelurahan))->row('kelurahan');
			$data[$i]->korcam=$this->db->query("select count(id) as jml from tim_view where korcam = $value->tim_id and id_user=$user->id ")->row('jml');
			$data[$i]->korlap=$this->db->query("select count(id) as jml from tim_view where korcam = $value->tim_id and korlap != '0' and id_user=$user->id ")->row('jml');
			$data[$i]->pemilih=$this->db->query("select count(id) as jml from tim_view where korcam = $value->tim_id and korlap != '0' and pemilih != '0' and id_user=$user->id ")->row('jml');
			$data[$i]->sms='<button data-id='."'".$value->tim_id."'".' class="btn btn-crusta btn-sm"><span class="la la-envelope "></span></button>';
			$data[$i]->lihat='<a class="btn btn-primary btn-sm" href='."'".site_url("tim/korcam/$value->tim_id")."'".' >KORCAM</a>';
			$data[$i]->jum_hp=0;
			$data[$i]->aktif=0;
			$data[$i]->mati=0;
			$data[$i]->button='<button data-id='."'".$value->tim_id."'".' class="btn btn-info btn-sm btn-edt"><span class="la la-edit"></span></button><button data-id='."'".$value->tim_id."'".' class="btn btn-danger btn-sm btn-del"><span class="la la-trash"></span></button>';

			$i++;
		}
		return $data;
	}

	function add_korwil($data){
		$user = $this->Auth_model->get_data();
		$data['id_user'] = $user->id;
		$data['tingkat'] = 'KORWIL';
		$data['korwil'] = '1';

		if($this->db->insert('tim',$data)){
			echo json_encode('SUKSES');
		}		
	}

	function delete_korwil($id){
		$this->db->delete('tim', array('id' => $id));
		$this->db->delete('tim', array('korcam' => $id));
		echo json_encode('SUKSES');
	}

	//KORCAM FUNCTION
	function get_data_korcam($id_korwil){
		$user = $this->Auth_model->get_data();
		$data = $this->db->get_where('tim_view', array('id_user' => $user->id,'korcam' => $id_korwil))->result();
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->kelurahan=$this->db->get_where('kelurahan', array('id' => $value->kelurahan))->row('kelurahan');
			$data[$i]->korlap=$this->db->query("select count(id) as jml from tim_view where korlap = $value->tim_id and id_user=$user->id ")->row('jml');
			$data[$i]->pemilih=$this->db->query("select count(id) as jml from tim_view where korlap = $value->tim_id and pemilih!='0' and id_user=$user->id ")->row('jml');
			$data[$i]->sms='<button data-id='."'".$value->tim_id."'".' class="btn btn-crusta btn-sm"><span class="la la-envelope "></span></button>';
			$data[$i]->lihat='<a class="btn btn-primary btn-sm" href='."'".site_url("tim/korlap/$value->tim_id")."'".' >KORLAP</a>';
			$data[$i]->jum_hp=0;
			$data[$i]->aktif=0;
			$data[$i]->mati=0;
			$data[$i]->button='<button data-id='."'".$value->tim_id."'".' class="btn btn-info btn-sm btn-edt"><span class="la la-edit"></span></button><button data-id='."'".$value->tim_id."'".' class="btn btn-danger btn-sm btn-del"><span class="la la-trash"></span></button>';

			$i++;
		}
		return $data;
	}

	function add_korcam($data){
		$user = $this->Auth_model->get_data();
		$data['id_user'] = $user->id;
		$data['tingkat'] = 'KORCAM';

		$this->db->insert('tim',$data);		
		echo json_encode('SUKSES');
	}

	function delete_korcam($id){
		$this->db->delete('tim', array('id' => $id));
		$this->db->delete('tim', array('korlap' => $id));
		echo json_encode('SUKSES');
	}

	//KORLAP FUNCTION
	function get_data_korlap($id_korcam){
		$user = $this->Auth_model->get_data();
		$data = $this->db->get_where('tim_view', array('id_user' => $user->id,'korlap' => $id_korcam))->result();
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->kelurahan=$this->db->get_where('kelurahan', array('id' => $value->kelurahan))->row('kelurahan');
			$data[$i]->pemilih=$this->db->query("select count(id) as jml from tim_view where pemilih = $value->tim_id and id_user=$user->id ")->row('jml');
			$data[$i]->sms='<button data-id='."'".$value->tim_id."'".' class="btn btn-crusta btn-sm"><span class="la la-envelope "></span></button>';
			$data[$i]->lihat='<a class="btn btn-primary btn-sm" href='."'".site_url("tim/pemilih/$value->tim_id")."'".' >PEMILIH</a>';
			$data[$i]->jum_hp=0;
			$data[$i]->aktif=0;
			$data[$i]->mati=0;
			$data[$i]->button='<button data-id='."'".$value->tim_id."'".' class="btn btn-info btn-sm btn-edt"><span class="la la-edit"></span></button><button data-id='."'".$value->tim_id."'".' class="btn btn-danger btn-sm btn-del"><span class="la la-trash"></span></button>';

			$i++;
		}
		return $data;
	}

	function add_korlap($data){
		$user = $this->Auth_model->get_data();
		$data['id_user'] = $user->id;
		$data['korcam'] = $this->db->get_where('tim',array('id' => $data['korlap']))->row('korcam');
		$data['tingkat'] = 'KORLAP';

		$this->db->insert('tim',$data);		
		echo json_encode('SUKSES');
	}

	function delete_korlap($id){
		$this->db->delete('tim', array('id' => $id));
		$this->db->delete('tim', array('pemilih' => $id));
		echo json_encode('SUKSES');
	}

	//PEMILIH FUNCTION
	function get_data_pemilih($id_korlap){
		$user = $this->Auth_model->get_data();
		$data = $this->db->get_where('tim_view', array('id_user' => $user->id,'pemilih' => $id_korlap))->result();
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->kelurahan=$this->db->get_where('kelurahan', array('id' => $value->kelurahan))->row('kelurahan');
			$data[$i]->button='<button data-id='."'".$value->tim_id."'".' class="btn btn-info btn-sm btn-edt"><span class="la la-edit"></span></button><button data-id='."'".$value->tim_id."'".' class="btn btn-danger btn-sm btn-del"><span class="la la-trash"></span></button>';
			$i++;
		}
		return $data;
	}

	function add_pemilih($data){
		$user = $this->Auth_model->get_data();
		$data['id_user'] = $user->id;
		$data['tingkat'] = 'PEMILIH';
		$data['korlap'] = $this->db->get_where('tim',array('id' => $data['pemilih']))->row('korlap');
		$data['korcam'] = $this->db->get_where('tim',array('id' => $data['korlap']))->row('korcam');

		$this->db->insert('tim',$data);		
		echo json_encode('SUKSES');
	}

	function delete_pemilih($id){
		$this->db->delete('tim', array('id' => $id));
		echo json_encode('SUKSES');
	}
}	