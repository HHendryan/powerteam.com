<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Auth_model extends CI_Model {

	protected $admin;

	//login using username & password
	function login($username, $password){
		$password = md5($password);
		//get admin. return false if not found
		$this->admin = $this->db->get_where('m_login', compact('username'))->row();
		if (!$this->admin){
			redirect('');
		}

		if($this->admin->is_active == '0'){
			redirect('');
		}
		//match password. return false if fail
		if ($this->admin->password != $password){
			redirect('');
		}

		//store username in session
		$sess_data['username'] = $this->admin->username;
		$sess_data['userlevel'] = $this->admin->userlevel;
		$sess_data['role'] = $this->admin->role;
		$sess_data['role_detail'] = $this->admin->role_detail;
		$sess_data['name'] = $this->admin->name;
		$sess_data['profile_pic'] = $this->admin->profile_pic;
		$this->session->set_userdata('guraklih', $sess_data);

		//return admin data
		return $this->admin;
	}

	function logout(){
		$this->session->unset_userdata('guraklih');
		unset($admin);
		redirect('');
	}

	function get_data(){
		//get admin username from session. return null if not found
		$username = $this->session->userdata('guraklih');
		if (!$username){
			return null;
		}

		//load admin data if it isn't loaded
		if (!$this->admin){
			$this->admin = $this->db->get_where('m_login', compact('username'))->row();
		}

		//return admin data
		return $this->admin;
	}

	function can_access_by_login(){
		//get current admin data
		$admin = $this->session->userdata('guraklih');

		//if no admin logged in, return false
		if (!$admin){
			redirect('');
		}
	}


	function is_login(){
		//get current admin data
		$admin =  $this->session->userdata('guraklih');
		if(!$admin){
			return false;
		}else{
			redirect('home');
		}
	}
}