<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Data_model extends CI_Model {
	function get_data_prov($pp){
		$user = $this->Auth_model->get_data();
		$data=$this->db->get('provinsi')->result();
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->provinsi = '<a href='."'".site_url("data/kabupaten/$value->id")."'".'>'."".$value->provinsi."".'</a>';
			$data[$i]->jum_kab = $this->db->query("select count(id) as jml from kabupaten where id_provinsi='$value->id'")->row('jml');
			$data[$i]->jum_kec= $this->db->query("select count(kecamatan.id) as jml from kecamatan inner join kabupaten on kecamatan.id_kabupaten=kabupaten.id where id_provinsi='$value->id'")->row('jml');
			$data[$i]->jum_kel = $this->db->query("select count(kelurahan.id) as jml from kelurahan 
														inner join kecamatan on kelurahan.id_kecamatan=kecamatan.id 
														inner join kabupaten on kecamatan.id_kabupaten=kabupaten.id 
														where id_provinsi='$value->id'")->row('jml');
			$data[$i]->jum_tps=$this->db->query("select count(tps.id) as jml from tps 
														inner join kelurahan on tps.id_kelurahan=kelurahan.id 
														inner join kecamatan on kelurahan.id_kecamatan=kecamatan.id 
														inner join kabupaten on kecamatan.id_kabupaten=kabupaten.id 
														where id_provinsi='$value->id'")->row('jml');
			$jum_dpt = $this->db->query("select sum(jml_dpt) as jml from sum_dpt where provinsi='$value->id'")->row('jml');
			$pp = round($pp*$jum_dpt);
			$tt = round(0.55*$pp);
			$data[$i]->jum_dpt = number_format($jum_dpt,0,",",".");
			$data[$i]->pp=number_format($pp,0,",",".");;
			$data[$i]->target=number_format($tt,0,",",".");
			$data[$i]->t='55%';
			$data[$i]->korwil=$this->db->query("select count(id) as jml from tim_view where korwil = '1' and id_user=$user->id and provinsi=$value->id ")->row('jml');
			$data[$i]->korcam=$this->db->query("select count(id) as jml from tim_view where korcam != '0' and id_user=$user->id and provinsi=$value->id")->row('jml');
			$data[$i]->korlap=$this->db->query("select count(id) as jml from tim_view where korlap != '0' and id_user=$user->id and provinsi=$value->id")->row('jml');
			$data[$i]->pemilih=$this->db->query("select count(id) as jml from tim_view where tingkat = 'PEMILIH' and id_user=$user->id and provinsi=$value->id")->row('jml');
			$data[$i]->real=$data[$i]->korwil+$data[$i]->korcam+$data[$i]->korlap+$data[$i]->pemilih;
			if($tt<=0){
				$data[$i]->persen = 0;
			}else{
				$data[$i]->persen= number_format($data[$i]->real/$tt*100,2).'%';
			}
			$i++;
		}

		return $data;
	}
	function get_data_kab($id,$ppx){
		$user = $this->Auth_model->get_data();
		$data=$this->db->get_where('kabupaten', array('id_provinsi' => $id))->result();
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->kabupaten = '<a href='."'".site_url("data/kecamatan/$value->id")."'".'>'."".$value->kabupaten."".'</a>';
			$data[$i]->jum_kec = $this->db->query("select count(id) as jml from kecamatan where id_kabupaten='$value->id'")->row('jml');
			$data[$i]->jum_kel = $this->db->query("select count(kelurahan.id) as jml from kelurahan inner join kecamatan on kelurahan.id_kecamatan=kecamatan.id where id_kabupaten='$value->id'")->row('jml');
			$data[$i]->jum_tps=$this->db->query("select count(tps.id) as jml from tps 
														inner join kelurahan on tps.id_kelurahan=kelurahan.id 
														inner join kecamatan on kelurahan.id_kecamatan=kecamatan.id 
														where id_kabupaten='$value->id'")->row('jml');
			$jum_dpt = $this->db->query("select sum(jml_dpt) as jml from sum_dpt where kabupaten='$value->id'")->row('jml');
			$pp = round($ppx*$jum_dpt);
			$tt = round(0.55*$pp);
			$data[$i]->jum_dpt = number_format($jum_dpt,0,",",".");
			$data[$i]->pp=number_format($pp,0,",",".");;
			$data[$i]->target=number_format($tt,0,",",".");
			$data[$i]->t='55%';
			$data[$i]->korwil=$this->db->query("select count(id) as jml from tim_view where korwil = '1' and id_user=$user->id and kabupaten=$value->id ")->row('jml');
			$data[$i]->korcam=$this->db->query("select count(id) as jml from tim_view where korcam != '0' and id_user=$user->id and kabupaten=$value->id ")->row('jml');
			$data[$i]->korlap=$this->db->query("select count(id) as jml from tim_view where korlap != '0' and id_user=$user->id and kabupaten=$value->id")->row('jml');
			$data[$i]->pemilih=$this->db->query("select count(id) as jml from tim_view where pemilih != '0' and id_user=$user->id and kabupaten=$value->id")->row('jml');
			$data[$i]->real=$data[$i]->korwil+$data[$i]->korcam+$data[$i]->korlap+$data[$i]->pemilih;
			if($tt<=0){
				$data[$i]->persen = 0;
			}else{
				$data[$i]->persen= number_format($data[$i]->real/$tt*100,2).'%';
			}
			$i++;
		}

		return $data;
	}
	function get_data_kec($id,$ppx){
		$user = $this->Auth_model->get_data();
		if($user->level==0){
			$dapil = explode(";",$user->dapil);
			$last = end($dapil);
			$query="SELECT * FROM kecamatan WHERE ";
			foreach ($dapil as $value) {
				if($value==$last){
					$query.="id=$value";
				}else{
					$query.="id=$value OR ";
				}	
			}
			$data = $this->db->query($query)->result();
		}else{
			$data=$this->db->get_where('kecamatan', array('id_kabupaten' => $id))->result();
		}

		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->kecamatan = '<a href='."'".site_url("data/kelurahan/$value->id")."'".'>'."".$value->kecamatan."".'</a>';
			$data[$i]->jum_kel = $this->db->query("select count(id) as jml from kelurahan where id_kecamatan='$value->id'")->row('jml');
			$data[$i]->jum_tps = $this->db->query("select count(tps.id) as jml from tps 
														inner join kelurahan on tps.id_kelurahan=kelurahan.id 
														where id_kecamatan='$value->id'")->row('jml');
			$jum_dpt = $this->db->query("select sum(jml_dpt) as jml from sum_dpt where kecamatan='$value->id'")->row('jml');
			$pp = round($ppx*$jum_dpt);
			$tt = round(0.55*$pp);
			$data[$i]->jum_dpt= number_format($jum_dpt,0,",",".");
			$data[$i]->pp=number_format($pp,0,",",".");;
			$data[$i]->target=number_format($tt,0,",",".");
			$data[$i]->t='55%';
			$data[$i]->korwil=$this->db->query("select count(id) as jml from tim_view where korwil = '1' and id_user=$user->id and kecamatan=$value->id ")->row('jml');
			$data[$i]->korcam=$this->db->query("select count(id) as jml from tim_view where korcam != '0' and id_user=$user->id and kecamatan=$value->id ")->row('jml');
			$data[$i]->korlap=$this->db->query("select count(id) as jml from tim_view where korlap != '0' and id_user=$user->id and kecamatan=$value->id")->row('jml');
			$data[$i]->pemilih=$this->db->query("select count(id) as jml from tim_view where pemilih != '0' and id_user=$user->id and kecamatan=$value->id")->row('jml');
			$data[$i]->real=$data[$i]->korwil+$data[$i]->korcam+$data[$i]->korlap+$data[$i]->pemilih;
			if($tt<=0){
				$data[$i]->persen = 0;
			}else{
				$data[$i]->persen= number_format($data[$i]->real/$tt*100,2).'%';
			}
			$i++;
		}

		return $data;
	}
	function get_data_kel($id,$ppx){
		$user = $this->Auth_model->get_data();
		$data=$this->db->get_where('kelurahan', array('id_kecamatan' => $id))->result();
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->kelurahan = '<a href='."'".site_url("data/tps/$value->id")."'".'>'."".$value->kelurahan."".'</a>';
			$data[$i]->jum_tps=$this->db->query("select count(tps.id) as jml from tps where id_kelurahan='$value->id'")->row('jml');;
			$jum_dpt = $this->db->query("select sum(jml_dpt) as jml from sum_dpt where kelurahan='$value->id'")->row('jml');
			$pp = round($ppx*$jum_dpt);
			$tt = round(0.55*$pp);
			$data[$i]->jum_dpt = number_format($jum_dpt,0,",",".");
			$data[$i]->pp=number_format($pp,0,",",".");;
			$data[$i]->target=number_format($tt,0,",",".");
			$data[$i]->t='55%';
			$data[$i]->korwil=$this->db->query("select count(id) as jml from tim_view where korwil = '1' and id_user=$user->id and kelurahan=$value->id ")->row('jml');
			$data[$i]->korcam=$this->db->query("select count(id) as jml from tim_view where korcam != '0' and id_user=$user->id and kelurahan=$value->id ")->row('jml');
			$data[$i]->korlap=$this->db->query("select count(id) as jml from tim_view where korlap != '0' and id_user=$user->id and kelurahan=$value->id")->row('jml');
			$data[$i]->pemilih=$this->db->query("select count(id) as jml from tim_view where pemilih != '0' and id_user=$user->id and kelurahan=$value->id")->row('jml');
			$data[$i]->real=$data[$i]->korwil+$data[$i]->korcam+$data[$i]->korlap+$data[$i]->pemilih;
			if($tt<=0){
				$data[$i]->persen = 0;
			}else{
				$data[$i]->persen= number_format($data[$i]->real/$tt*100,2).'%';
			}
			$data[$i]->button='<button type="button" class="btn btn-warning"><i class="la la-search"></i></button>';
			$i++;
		}

		return $data;
	}

	function get_data_tps($id,$ppx){
		$user = $this->Auth_model->get_data();
		$data=$this->db->get_where('tps', array('id_kelurahan' => $id))->result();
		$i=0;
		foreach ($data as $key => $value) {
			$jum_dpt = $this->db->query("select sum(jml_dpt) as jml from sum_dpt where kelurahan=$id and tps=$value->tps")->row('jml');;
			$pp = round($ppx*$jum_dpt);
			$tt = round(0.55*$pp);
			$data[$i]->jum_dpt = number_format($jum_dpt,0,",",".");
			$data[$i]->pp=number_format($pp,0,",",".");;
			$data[$i]->target=number_format($tt,0,",",".");
			$data[$i]->t='55%';
			$data[$i]->korwil=$this->db->query("select count(id) as jml from tim_view where korwil = '1' and id_user=$user->id and kelurahan=$id and tps=$value->tps ")->row('jml');
			$data[$i]->korcam=$this->db->query("select count(id) as jml from tim_view where korcam != '0' and id_user=$user->id and kelurahan=$id and tps=$value->tps ")->row('jml');
			$data[$i]->korlap=$this->db->query("select count(id) as jml from tim_view where korlap != '0' and id_user=$user->id and kelurahan=$id and tps=$value->tps")->row('jml');
			$data[$i]->pemilih=$this->db->query("select count(id) as jml from tim_view where pemilih != '0' and id_user=$user->id and kelurahan=$id and tps=$value->tps")->row('jml');
			$data[$i]->real=$data[$i]->korwil+$data[$i]->korcam+$data[$i]->korlap+$data[$i]->pemilih;
			if($tt<=0){
				$data[$i]->persen = 0;
			}else{
				$data[$i]->persen= number_format($data[$i]->real/$tt*100,2).'%';
			}
			$data[$i]->button='<a type="button" href='."'".site_url("data/pemilih/$id/$value->tps")."'".' class="btn btn-warning">Lihat</a>';
			$i++;
		}

		return $data;
	}

	function get_data_pemilih($id_kel,$tps){
		$data=$this->db->get_where('dpt_sumatera_selatan', array('kelurahan' => $id_kel,'tps' => $tps))->result();
		$i=0;
		foreach ($data as $value) {
			$data[$i]->button='<a type="button" data-id='."'".$value->id."'".' class="btn btn-warning btn-edit">Edit</a>';
			$i++;
		}
		return $data;
	}

	function update_data($data,$table){
		if($this->db->update($table, $data, array('id' => $data['id']))){
			echo json_encode('SUKSES');	
		}
	}

	function get_data_influence($tipe,$condong,$kategori){
		$data=$this->db->get('m_provinces')->result();
		$i=0;
		foreach ($data as $key => $value) {
			// $data[$i]->provinsi = '<a href='."'".site_url("data/influence_kab/$value->id")."'".'>'."".$value->name."".'</a>';
			$data[$i]->provinsi = $value->name;
			$query = "SELECT count(id_intangible) as jml from intangible where is_deleted='0' AND id_provinces = '$value->id'";
			if($tipe != 'all'){
				$query .= " AND type = '$tipe'";
			};
			if($condong != 'all'){
				$query .= " AND inclination = '$condong'";
			};
			if($kategori != 'all'){
				$query .= " AND id_pertokohan = '$kategori'";
			};
			$jum = $this->db->query($query)->row('jml');
			$data[$i]->jum_tangible = $jum;
			$data[$i]->button = '<button class="btn btn-warning btn-sm btn-detail" data-id="'.$value->id.'">Detail</button>';
			// $data[$i]->jum_tangible = '<a href="#">'."".$jum."".'</a>';
			$i++;
		}

		return $data;
	}

	function get_data_influence_kab($id_prov,$tipe,$condong,$kategori){
		$data=$this->db->get_where('m_regencies', array('province_id' => $id_prov))->result();
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->provinsi = $value->name;
			$query = "SELECT count(id_intangible) as jml from intangible where is_deleted='0' AND id_city = '$value->id'";
			if($tipe != 'all'){
				$query .= " AND type = '$tipe'";
			};
			if($condong != 'all'){
				$query .= " AND inclination = '$condong'";
			};
			if($kategori != 'all'){
				$query .= " AND id_pertokohan = '$kategori'";
			};

			$data[$i]->jum_tangible = $this->db->query($query)->row('jml');
			$i++;
		}

		return $data;
	}

	function get_detail_influencer($id_prov,$tipe,$condong,$kategori){
		$query = "SELECT name,phone,gender,id_pertokohan,inclination,informasi_tambahan, (SELECT name from m_regencies where intangible.`id_city` = m_regencies.id) as kab, (SELECT name from m_districts where intangible.`id_districts` = `m_districts`.id ) as kec, (SELECT name from m_villages where intangible.`id_village` = `m_villages`.id ) as kel from intangible where is_deleted='0' AND id_provinces ='$id_prov'";
			if($tipe != 'all'){
				$query .= " AND type = '$tipe'";
			};
			if($condong != 'all'){
				$query .= " AND inclination = '$condong'";
			};
			if($kategori != 'all'){
				$query .= " AND id_pertokohan = '$kategori'";
			};
		$data = $this->db->query($query)->result();
		$i=0;
		foreach ($data as $key => $value) {
				$data[$i]->pertokohan = $this->db->get_where("m_pertokohan",array('id_pertokohan' => $value->id_pertokohan ))->row('nama');
				$data[$i]->kecondongan = $this->db->get_where("m_parpol",array('id_parpol' => $value->inclination ))->row('name');
				$i++;
			}
		return($data);

	}

	/*
    * Function for get kabupaten (Hari Hendryan)
    */
	function get_kabupaten($id_prov){
		$list="<option value='' disabled selected>ALL</option>";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_regencies',array('province_id'=>$id_prov))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}

	/*
    * Function for get Kecamatan (Hari Hendryan)
    */
	function get_kecamatan($id_kab){
		$list="<option value='' disabled selected>ALL</option>";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_districts',array('regency_id'=>$id_kab))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}

	/*
    * Function for get Kelurahan (Hari Hendryan)
    */
	function get_kelurahan($id_kec){
		$list="<option value='' disabled selected>ALL</option>";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_villages',array('district_id'=>$id_kec))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}

	/*
    * Function for insert dapil data (Hari Hendryan)
    */
	function add_m_dapil($data){
		$data1['type'] = $data['tipe'];
		$data1['periode'] = $data['periode'];
		$data1['nama_dapil'] = $data['nama'];
		if($this->db->insert('m_dapil', $data1)){
			$data2['id_dapil'] = $this->get_last_id();
			$data2['id_provinces'] = $data['prov'];

			if ($data['tipe']=='3') {
				$data2['id_regency'] = $data['kab'];
				for ($i=0; $i < sizeof($data['kec']); $i++) { 
					$data2['id_district'] = $data['kec'][$i];
					for ($j=0; $j < sizeof($data['kel']); $j++) { 
						if($data['kec'][$i] == substr($data['kel'][$j],0,7)) {
							$data2['id_village'] = $data['kel'][$j];
							$this->db->insert('dapil_has_area', $data2);
						}
					}
					
				}
				
			} else if($data['tipe']=='1') {
				$data2['id_district'] = "0";
				for ($i=0; $i < sizeof($data['kab']); $i++) { 
					$data2['id_regency'] = $data['kab'][$i];
					$this->db->insert('dapil_has_area', $data2);
				}
			}else {
				$data2['id_district'] = "0";
				for ($i=0; $i < sizeof($data['kab']); $i++) { 
					$data2['id_regency'] = $data['kab'][$i];
					for ($j=0; $j < sizeof($data['kec']); $j++) { 
						$data2['id_district'] = $data['kec'][$j];
						$this->db->insert('dapil_has_area', $data2);
					}
					// $this->db->insert('dapil_has_area', $data2);
				}
			}
			echo json_encode($data2);
		}
	}

	/*
    * Function for update dapil data (Hari Hendryan)
    */
	function edit_m_dapil($data){
		$data1['type'] = $data['tipe'];
		$data1['periode'] = $data['periode'];
		$data1['nama_dapil'] = $data['nama'];

		$this->db->where('id_dapil',$data['id']);
		if($this->db->update('m_dapil',$data1)){
			$data2['id_dapil'] = $data['id'];
			$data2['id_provinces'] = $data['prov'];

			$this->db->query("update db_sipp.dapil_has_area set is_deleted = '1' where id_dapil = '".$data['id']."' ");
			if ($data['tipe']=='3') {
				$data2['id_regency'] = $data['kab'];
				for ($i=0; $i < sizeof($data['kec']); $i++) { 
					$data2['id_district'] = $data['kec'][$i];
					for ($j=0; $j < sizeof($data['kel']); $j++) { 
						if($data['kec'][$i] == substr($data['kel'][$j],0,7)) {
							$data2['id_village'] = $data['kel'][$j];
							$this->db->insert('dapil_has_area', $data2);
						}
					}
				}
			} else {
				$data2['id_district'] = "0";
				for ($i=0; $i < sizeof($data['kab']); $i++) { 
					$data2['id_regency'] = $data['kab'][$i];
					//$this->db->insert('dapil_has_area', $data2);
					for ($j=0; $j < sizeof($data['kec']); $j++) { 
						if($data['kab'][$i] == substr($data['kec'][$j],0,4)) {
							$data2['id_district'] = $data['kec'][$j];
							$this->db->insert('dapil_has_area', $data2);
						}
					}
				}
			}
			echo json_encode($data2);
		}
	}

	/*
    * Function for get dapil id (Hari Hendryan)
    */
	function get_last_id(){
		$id_lama = $this->db->query('SELECT id_dapil from m_dapil order by id_dapil desc limit 1')->row('id_dapil');
		return $id_lama;
	}

	/*
    * Function for get data dpt (Hari Hendryan)
    */
	function get_data_dpt($kel,$tps){
		$id_kab = 	$this->db->query("SELECT c.id from m_villages a
					LEFT JOIN m_districts b on a.district_id=b.id
					LEFT JOIN m_regencies c on b.regency_id=c.id
					where a.id='$kel'")->row('id');
		$tabel_kab = 'kab_'.$id_kab;
		$this->db2 = $this->load->database("db_kab_final",true);
		
		$this->db2->select('id,nik,nama,alamat,tanggal_lahir, tempat_lahir, jns_kelamin, tps, checked');
		$this->db2->from($tabel_kab);
		$this->db2->where('idKel',$kel);		
		$this->db2->where('tps',$tps);		
		$this->db2->where('checked',0);
		$data = $this->db2->get()->result();

		$i=0;
		foreach ($data as $key => $value) {
			if ($value->checked==1){
				$data[$i]->checkbox = '<input type="checkbox" checked class="messageCheckbox" id="chk_'.$value->id.'" name="chk" value="'.$value->id.'">';
			}else{
				$data[$i]->checkbox = '<input type="checkbox" class="messageCheckbox" id="chk_'.$value->id.'" name="chk" value="'.$value->id.'">';
			}
			$i++;
		}		

		return ($data);
	}

	function get_data_dpt_check($kel,$tps,$guraklih){
		$id_kab = 	$this->db->query("SELECT c.id from m_villages a
					LEFT JOIN m_districts b on a.district_id=b.id
					LEFT JOIN m_regencies c on b.regency_id=c.id
					where a.id='$kel'")->row('id');
		$tabel_kab = 'kab_'.$id_kab;
		$this->db2 = $this->load->database("db_kab_final",true);
		
		$this->db2->select('id,nik,nama,alamat,tanggal_lahir, tempat_lahir, jns_kelamin, tps, checked');
		$this->db2->from($tabel_kab);
		$this->db2->where('idKel',$kel);		
		$this->db2->where('tps',$tps);		
		$this->db2->where('checked',1);
		$this->db2->where('guraklihId',$guraklih);
		$data = $this->db2->get()->result();

		$i=0;
		foreach ($data as $key => $value) {
			if ($value->checked==1){
				$data[$i]->checkbox = '<input type="checkbox" checked class="messageCheckbox" id="chk_'.$value->id.'" name="chk" value="'.$value->id.'">';
			}else{
				$data[$i]->checkbox = '<input type="checkbox" class="messageCheckbox" id="chk_'.$value->id.'" name="chk" value="'.$value->id.'">';
			}
			$i++;
		}		

		return ($data);
	}

	/*
    * Function for get data dpt (Hari Hendryan)
    */
	function get_data_dpt_area($kec){

		$queryTabel = "SELECT a.id, a.idProv, a.provinsi, a.idKab, a.kabupaten, a.idKec, a.kecamatan, a.idKel, a.kelurahan, (CASE
								    WHEN status_guraklih = 0 THEN 'NEW'
								    WHEN status_guraklih = 1 THEN 'DRAF'
								    WHEN status_guraklih = 2 THEN 'FINISH' END) AS status FROM m_area a WHERE a.idKec = $kec";
		
		$this->db1 = $this->load->database("db_kab_final",true);
		$query = $this->db1->query($queryTabel);

		$i=1;
		$data["data"] = array();
		foreach ($query->result() as $row)
		{
			$tmp = array();

			$tmp["no"] = $i;
			$tmp["idProv"] = $row->idProv;
            $tmp["provinsi"] = $row->provinsi;
            $tmp["idKab"] = $row->idKab;
            $tmp["kabupaten"] = $row->kabupaten;
            $tmp["idKec"] = $row->idKec;
            $tmp["kecamatan"] = $row->kecamatan;
            $tmp["idKel"] = $row->idKel;
            $tmp["kelurahan"] = $row->kelurahan;
            $tmp["status"] = $row->status;
            $tmp["action"] = '<tr>
						    	<td>
						    		<div class="row pad" style="padding-bottom: 3%">
                                        <button type="button" data-tipe="check" data-idkel='.$row->idKel.' class="btn btn-sm btn-primary btn-detail" style="height: 26px; width:100px;">Check</button>
                                    </div>
                                    <div class="row pad" style="padding-bottom: 3%">
                                        <button type="button" data-tipe="uncheck" data-idkel='.$row->idKel.' class="btn btn-sm btn-primary btn-detail" style="height: 26px; width:100px;">Uncheck</button>
                                    </div>
						    	</td>
						    </tr>';
            
            array_push($data["data"], $tmp);
            $i++;
		}

		return json_encode($data);
	}

	/*
    * Function for set dpt flag (Hari Hendryan)
    */
	function set_flag_dpt($id, $status, $idProv, $idKab, $idKec, $idKel, $tps){

		$user = $this->session->userdata('guraklih');
		$array = explode( '_', $id );

		$q = "UPDATE kab_".$array[1]." SET checked = $status WHERE id=".$array[0];
		$this->db2 = $this->load->database("db_kab_final",true);
		$query = $this->db2->query($q);
		
		$id_team = $this->db->query("SELECT id FROM m_team WHERE id_regency='$array[1]' LIMIT 1")->row('id');
		$data = $this->db2->query("SELECT * FROM kab_".$array[1]." WHERE id=".$array[0])->row();
		$id_dpt = $data->id;
		$nokk = $data->no_kk;

		$input['id_dpt'] = $data->id;
		$input['id_team'] = $id_team;
		$input['nama'] = $data->nama;
		$input['alamat'] = $data->alamat;
		$input['nik'] = $data->nik;
		$input['phone'] = '0';
		$input['jk'] = $data->jns_kelamin;
		$input['status_kawin'] = 'b';
		$input['tempat_lahir'] = $data->tempat_lahir;
		$input['tanggal_lahir'] = $data->tanggal_lahir;
		$input['pekerjaan'] = '';
		$input['jabatan'] = '';
		$input['tps'] = '0';
		$input['is_dpt'] = '0';
		$input['is_deleted'] = '0';
		$input['id_area'] = $array[1];
		$input['id_provinces'] = $idProv;
		$input['id_regency'] = $idKab;
		$input['id_district'] = $idKec;
		$input['id_village'] = $idKel;
		$input['tps'] = $tps;

		$result = $this->db->insert('pemilih_prospek',$input);

		$tabelProspek = $this->db->get_where('m_dpt', array('idProv' => $idProv))->row()->tabel_prospek;

		$input1['idProv'] = $idProv;
		$input1['idKab'] = $idKab;
		$input1['idKec'] = $idKec;
		$input1['idKel'] = $idKel;
		$input1['tps'] = $tps;
		$input1['id_team'] = $id_team;
		$input1['nokk'] = $nokk;
		$input1['nik'] = $data->nik;
		$input1['nama'] = $data->nama;
		$input1['alamat'] = $data->alamat;
		$input1['tanggal_lahir'] = $data->tanggal_lahir;
		$input1['usia'] = $data->usia;
		$input1['upd'] = $user['username'];
		$input1['id_dpt'] = $id_dpt;

		$result = $this->db->insert($tabelProspek,$input1);

		return json_encode($input);
	}


	/*
    * Function for set dpt flag (Hari Hendryan)
    */
	function set_flag_dpt_sum($idProv, $idKab, $idKec, $idKel, $tps){
		$total = 0;

		$this->db2 = $this->load->database("db_kab_final",true);

		$total = $this->db2->query("SELECT count(1) as jml from kab_".$idKab." WHERE idKec='$idKec' AND idKel='$idKel' AND tps='$tps' AND checked = 1")->row('jml');

		$tabelSum = $this->db->get_where('m_dpt', array('idProv' => $idProv))->row()->tabel_sum;
		if ($tabelSum!=NULL) {
			$q = "UPDATE $tabelSum SET prospek = $total WHERE idKab='$idKab' AND idKec='$idKec' AND idKel='$idKel' AND tps='$tps'";
			$query = $this->db->query($q);
		}
		return json_encode("Data tersimpan..");
	}

	/*
    * Function for clear dpt flag (Hari Hendryan)
    */
	function clear_flag_dpt($idKab, $idKec, $idKel, $tps, $id){

		$array = explode( '_', $id );
		$id = $array[0];

		$q = "UPDATE kab_".$idKab." SET checked = 0 WHERE idKab='$idKab' AND idKec='$idKec' AND idKel='$idKel' AND tps='$tps' AND id=$id";
		$this->db2 = $this->load->database("db_kab_final",true);
		$query = $this->db2->query($q);

		$q = "UPDATE pemilih_prospek SET is_deleted='1' WHERE id_regency=$idKab AND id_district=$idKec AND id_village=$idKel AND tps='$tps' AND id_dpt=$id";
		$query = $this->db->query($q);

		$tabelProspekTabel = "SELECT a.tabel_prospek as tabel FROM m_dpt a, m_regencies b WHERE a.idProv = b.province_id AND b.id=$idKab";
		$tabel = $this->db->query($tabelProspekTabel)->row('tabel');


		$q = "DELETE FROM $tabel WHERE idKab=$idKab AND idKec=$idKec AND idKel = $idKel AND tps=$tps AND id_dpt=$id";
		$query = $this->db->query($q);
		
		return json_encode("sukses");
	}

	/*
    * Function for set status guraklih (Hari Hendryan)
    */
	function set_status_guraklih($id, $status){

		$q = "UPDATE m_area SET status_guraklih = $status WHERE id = $id";
		$this->db1 = $this->load->database("db_kab_final",true);
		$query = $this->db1->query($q);
		
		return json_encode("sukses");
	}

	function get_data_dpt_print($kel,$tps){

		$this->db2 = $this->load->database("db_kab_final",true);

		$id_kab = $this->db2->query("SELECT  idKab as id_kab from m_area
			where idKel = $kel")->row('id_kab');
		
		$queryTabel = "SELECT id,nik,nama,alamat,tanggal_lahir, tempat_lahir, jns_kelamin, tps FROM kab_".$id_kab." WHERE idKel = $kel AND tps= $tps";

		$data["data"] = array();
		$i=1;
		$query = $this->db2->query($queryTabel);
		foreach ($query->result() as $row)
		{
			$tmp = array();

			$tmp["no"] = $i;
			$tmp["id"] = $row->id;
            $tmp["nama"] = $row->nama;
            $tmp["nik"] = $row->nik;
            $tmp["alamat"] = $row->alamat;
            $tmp["tanggal_lahir"] = $row->tanggal_lahir;
            $tmp["tps"] = $row->tps;
            $tmp["jns_kelamin"] = $row->jns_kelamin;

            array_push($data["data"], $tmp);
            $i++;
		}

		return $data;
	}

	/*
    * Function for get data dpt double(Hari Hendryan)
    */
	function get_data_dpt_double($kab, $kec, $kel, $tps, $nik, $nama, $tgl){
		$tabel_kab = 'kab_'.$kab;
		$this->db2 = $this->load->database("db_kab_final",true);
		$type = 0;
		
		$this->db2->from($tabel_kab);

		if($kec != 'all'){
			$this->db2->where('idKec',$kec);
		}
		if($kel != 'all'){
			$this->db2->where('idKel',$kel);
		}
		if($tps != 'all'){
			$this->db2->where('tps',$tps);
		}

		if ($nik!='null') {

			$type = 1;
			if ($nik != 'all') {
				$this->db2->where('nik',$nik);
			}
			$this->db2->select('id, nik, COUNT(1) as jml');
			$this->db2->where('substring(nik,15,1) !=', '*');
			$this->db2->group_by('nik');
		}

		if ($nama!='null') {
			$type = 2;
			$this->db2->select('id, nama, tanggal_lahir, COUNT(1) as jml');
			$this->db2->group_by('nama');
			$this->db2->group_by('tanggal_lahir');
			if ($nama != 'all') {
				$this->db2->where('nama',utf8_decode(urldecode($nama)));
			}
		}

		if ($nik=='null' && $nama=='null' && $tgl=='null') {
			$type = 3;
			$this->db2->select('id, nama, nik, tanggal_lahir, COUNT(1) as jml');
			$this->db2->where('substring(nik,15,1) !=', '*');
			$this->db2->group_by('nama');
			$this->db2->group_by('tanggal_lahir');
			$this->db2->group_by('nik');
		}

		$this->db2->having('jml>1');
		$data = $this->db2->get()->result();	

		$i=0;
		foreach ($data as $key => $value) {
			if ($type==1) {
				$data[$i]->button = '<button class="btn btn-warning btn-sm btn-detail" data-id="'.$value->nik.'" data-kab="'.$kab.'" >Detail</button>';
			} else if ($type==2) {
				$data[$i]->button = '<button class="btn btn-warning btn-sm btn-detail" data-nama="'.$value->nama.'" data-tgl="'.$value->tanggal_lahir.'" data-kab="'.$kab.'" >Detail</button>';
			} else {
				$data[$i]->button = '<button class="btn btn-warning btn-sm btn-detail" data-id="'.$value->nik.'" data-nama="'.$value->nama.'" data-tgl="'.$value->tanggal_lahir.'" data-kab="'.$kab.'" >Detail</button>';
			}
			
			$i++;
		}	

		return ($data);
	}

	/*
    * Function for get data dpt double(Hari Hendryan)
    */
	function create_custom_table($id){
		$sql = "
			CREATE TABLE IF NOT EXISTS `db_kab_final`.`kab_".$id."` (
		        `id` int(9) NOT NULL AUTO_INCREMENT,
		        `idKab` varchar(4) DEFAULT NULL,
		        `idKec` varchar(7) DEFAULT NULL,
		        `idKel` varchar(10) DEFAULT NULL,
		        `tps` varchar(4) DEFAULT NULL,
		        `no_kk` varchar(30) DEFAULT NULL,
		        `nik` varchar(16) DEFAULT NULL,
		        `nama` varchar(80) DEFAULT NULL,
		        `tempat_lahir` varchar(30) DEFAULT NULL,
		        `tanggal_lahir` varchar(12) DEFAULT NULL,
		        `usia` varchar(2) DEFAULT NULL,
		        `jns_kelamin` varchar(2) DEFAULT NULL,
		        `alamat` varchar(120) DEFAULT NULL,
		        `rt` varchar(5) DEFAULT NULL,
		        `rw` varchar(5) DEFAULT NULL,
		        `disabilitas` varchar(2) DEFAULT NULL,
		        `status_perkawinan` varchar(2) DEFAULT NULL,
		        `lup` datetime DEFAULT NULL,
		        `checked` int(1) NOT NULL DEFAULT '0',
		        `guraklihId` int(3) DEFAULT NULL,
		        PRIMARY KEY (`id`),
		        KEY `idx1` (`idKab`,`idKec`,`idKel`,`tps`),
		        KEY `idx5` (`guraklihId`)
		    ) ENGINE=InnoDB DEFAULT CHARSET=latin1
		";
		$this->db->query($sql);
	}

	function insert_dpt($id,$idkec,$idkel,$tps,$no_kk,$nik,$nama,$tempat,$tanggal,$usia,$gender,$alamat,$rt,$rw,$disabilitas,$status_kawin) {
		$sql = "
			INSERT INTO `db_kab_final`.`kab_".$id."` (`idKab`,`idKec`,`idKel`,`tps`,`no_kk`,`nik`,`nama`,`tempat_lahir`,`tanggal_lahir`,`usia`,`jns_kelamin`,`alamat`,`rt`,`rw`,`disabilitas`,`status_perkawinan`,`lup`)
			VALUES ('".$id."','".$idkec."','".$idkel."','".$tps."','".$no_kk."','".$nik."','".$nama."','".$tempat."','".$tanggal."','".$usia."','".$gender."','".$alamat."','".$rt."','".$rw."','".$disabilitas."','".$status_kawin."','".date('Y-m-d H:i:s')."');
		";
		$this->db->query($sql);
	}

	function get_detail_area($idx) {

	    $sql = "
            SELECT * 
            FROM db_kab_final.m_area
            WHERE id = ".$idx."
        ";

        $area = $this->db->query($sql);
        return $area->result();
	}

	function update_dpt($idx,$row) {

		$sql = "
            UPDATE db_kab_final.m_area
            SET dpt = '".$row."'
            WHERE id = ".$idx."
        ";

        return $this->db->query($sql);
	}

	function update_dpt_v2($idx,$idkab) {

		$sql = "
            SELECT *
            FROM db_kab_final.kab_".$idkab."
        ";

        $query = $this->db->query($sql);
        $total = $query->num_rows();

        // UPDATE m_area
        $sql = "
            UPDATE db_kab_final.m_area
            SET dpt = '".$total."'
            WHERE id = ".$idx."
        ";

        $this->db->query($sql);

        // FIND TOTAL DPT PER PROV
        $sql = "
            SELECT *
            FROM db_kab_final.m_area
            WHERE id = ".$idx."
        ";

        $query = $this->db->query($sql);
        $idProv = 0;
        foreach ($query->result() as $k => $v) {
        	$idProv += $v->idProv;
        }

		$sql = "
            SELECT SUM(dpt) as dpt
            FROM db_kab_final.m_area
            WHERE idProv = ".$idProv."
        ";

        $query = $this->db->query($sql);
        $total_dpt = 0;
        foreach ($query->result() as $k => $v) {
        	$total_dpt += $v->dpt;
        }

        $sql = "
            SELECT COUNT(idKel) as total
            FROM db_kab_final.kab_".$idkab."
            GROUP by idKel
        ";

        $query = $this->db->query($sql);
        $total_desa = 0;
        foreach ($query->result() as $k => $v) {
        	$total_desa += $v->total;
        }

        // UPDATE m_dpt

        $sql = "
            UPDATE db_sipp.m_dpt
            SET 
            	dpt = '".$total_dpt."',
            	desa = '".$total_desa."'
            WHERE idProv = ".$idProv."
        ";

        return $this->db->query($sql);

        
	}

}