<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class KTA_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}

	function get_data_kta($ket){
		$user = $this->Auth_model->get_data();
		if($ket=='all'){
			$data=$this->db->get_where('tim_view', array('id_user' => $user->id))->result();
		}else{
			$data=$this->db->get_where('tim_view', array('cetak' => $ket,'id_user' => $user->id))->result();
		}
		$i=0;
		foreach ($data as $key => $value) {
			$data[$i]->kabupaten = $this->db->get_where('kabupaten', array('id' => $value->kabupaten))->row('kabupaten');
			$data[$i]->kecamatan = $this->db->get_where('kecamatan', array('id' => $value->kecamatan))->row('kecamatan');
			$data[$i]->kelurahan = $this->db->get_where('kelurahan', array('id' => $value->kelurahan))->row('kelurahan');
			$i++;
		}
		return $data;
	}
}	