<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Target_model extends CI_Model {
	function get_data($id,$tipe){
		if($id == 0){
			$wilayah = $this->db->get('am_prov_target')->result();
		}else{
			if($tipe == 'provinsi'){
				$table_wilayah = 'm_regencies';
				$id_select = 'province_id';
				
				$table_target = 'am_prov_target';
				$id_target = 'idProv';

				$table_child_target = 'am_kab_target';
				$id_child_target = 'idKab';
				$child_tipe = 'kabupaten';
				$child_judul = 'to kecamatan';
				$table_grandchild_target = 'am_kec_target';
				$target = $this->db->get_where($table_target, array($id_target => $id))->row('target');
			};

			if($tipe == 'kabupaten'){
				$table_wilayah = 'm_districts';
				$id_select = 'regency_id';
				
				$table_target = 'am_kab_target';
				$id_target = 'idKab';

				$table_child_target = 'am_kec_target';
				$id_child_target = 'idKec';
				$child_tipe = 'kecamatan';
				$child_judul = 'to kelurahan';
				$table_grandchild_target = 'am_kel_target';
				$target = $this->db->get_where($table_target, array($id_target => $id))->row('target');
			};

			if($tipe == 'kecamatan'){
				$table_wilayah = 'm_villages';
				$id_select = 'district_id';
				
				$table_target = 'am_kec_target';
				$id_target = 'idKec';

				$table_child_target = 'am_kel_target';
				$id_child_target = 'idKel';
				$child_tipe = 'kelurahan';
				$child_judul = '';
				$target = $this->db->get_where($table_target, array($id_target => $id))->row('target');
			};

			$this->db->where($id_select,$id);
			$wilayah = $this->db->get($table_wilayah)->result();
			$i=0;
			foreach ($wilayah as $key => $value) {
				$id_ = $value->id;
				$wilayah[$i]->target_wil = $target;
				$wilayah[$i]->target_child = $this->db->get_where($table_child_target, array($id_child_target => $value->id))->row('target');
				if($tipe != 'kecamatan'){
					$wilayah[$i]->average = $this->db->query("SELECT ROUND(AVG(target),2) as rata from $table_grandchild_target where $id_child_target=$id_")->row('rata');
					$wilayah[$i]->button = '<button type="button" data-id="'.$value->id.'" data-tipe="'.$child_tipe.'" class="btn btn-success btn-sm btn-edit" style="height: 26px;">Edit Target</button>&nbsp;&nbsp;<button type="button" data-id="'.$value->id.'" data-nama="'.$value->name.'" data-tipe="'.$child_tipe.'" class="btn btn-success btn-sm btn-link" style="height: 26px;">'.$child_judul.'</button>';
				}else{
					$wilayah[$i]->button = '<button type="button" data-id="'.$value->id.'" data-tipe="'.$child_tipe.'" class="btn btn-success btn-sm btn-edit" style="height: 26px;">Edit Target</button>';	
				}
				
				$i++;
			}
		}
		

		$data['wilayah'] = $wilayah;

		return $data;
	}
}