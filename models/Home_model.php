<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Home_model extends CI_Model {
	function load_graph_data(){
		$data['jml_pemiih'] = $this->db->query("select kabupaten.kabupaten as nama_kab, sum(jml_dpt) as jml from sum_dpt inner join kabupaten on kabupaten.id=sum_dpt.kabupaten group by sum_dpt.kabupaten order by jml desc limit 5")->result();
		$data['jenis_kelamin'] = $this->db->query("select sum(jml_laki) as laki, sum(jml_wanita) as wanita from sum_jeniskelamin")->result();
		$data['usia'] = $this->db->query("select sum(jml_1) as jml_1, sum(jml_2) as jml_2, sum(jml_3) as jml_3, sum(jml_4) as jml_4, sum(jml_5) as jml_5 from sum_umur")->result();
		$data['status'] = $this->db->query("select sum(jml_kawin) as jml_kawin, sum(jml_belumkawin) as jml_belumkawin 	from sum_status")->result();
		return $data;
	}

	function get_tree_kabupaten($id){
		$data = $this->get_tree('kabupaten',$id);
		echo json_encode($data);
	}

	function get_tree_kecamatan($id){
		$data = $this->get_tree('kecamatan',$id);
		echo json_encode($data);
	}

	function get_tree_kelurahan($id){
		$data = $this->get_tree('kelurahan',$id);
		echo json_encode($data);
	}

	function get_table($tipe){
		if($tipe == 'provinsi'){
			$table = 'm_provinces';
		}else if($tipe == 'kabupaten'){
			$table = 'm_regencies';
		}else if($tipe == 'kecamatan'){
			$table = 'm_districts';
		}else if($tipe == 'kelurahan'){
			$table = 'm_villages';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function get_parent_table($tipe){
		if($tipe == 'provinsi'){
			$table = '';
		}else if($tipe == 'kabupaten'){
			$table = 'province_id';
		}else if($tipe == 'kecamatan'){
			$table = 'regency_id';
		}else if($tipe == 'kelurahan'){
			$table = 'district_id';
		}else{
			$table = 'Sasdasd';
		}
		return $table;
	}

	function get_tree($tipe,$id){
		$table= $this->get_table($tipe);
		$parent_table = $this->get_parent_table($tipe);
		$result = $this->db->get_where($table, array($parent_table => $id))->result();
		$i=0;
		foreach ($result as $key => $value) {
			$data[$i]['title'] = "$value->name";
			$data[$i]['nama'] = "$value->name";
			$data[$i]['lazy'] = TRUE;
			$data[$i]['id'] = "$value->id";
			$data[$i]['tipe'] = $tipe;
			$data[$i]['logo'] = '$value->logo';
			$i++;
		}
		return $data;
	}

	function get_data($id,$tipe){

		$this->db1 = $this->load->database("db_kab",true);

		$where = '';
		$where_guraklih = '';
		$query_lanjutan = '';
		$query_realisasi = '';
		$where_wilayah="";
		$idarea = "";

		if($id == 0){
			$tabel = $this->db->get('m_dpt')->result();	
			$jum_dpt = 0;
			$i = 0;
			foreach ($tabel as $key => $value) {
				if($value->tabel_sum != NULL){
					$jml = 0;
					$sum_tabel = $value->tabel_sum;
					$hasil = $this->db->query("SELECT SUM(dpt) as jumlah from $sum_tabel")->row('jumlah');
					$jum_dpt=$jum_dpt+$hasil;
					$detail_prov = $this->db->get_where('m_provinces', array('id' => $value->idProv))->row();

					$jml= $this->db->query("SELECT jml_prospek as jml_pemilih from v_prospek WHERE prov='$value->idProv'")->row('jml');
					
					$new_table = $value->tabel_sum;
					$data['tabel'][$i]['id'] = $i+1;
					$data['tabel'][$i]['id_wilayah'] = $value->idProv;
					$data['tabel'][$i]['logo_wilayah'] = $detail_prov->logo;
					$data['tabel'][$i]['wilayah'] = $value->provinsi;
					$data['tabel'][$i]['guraklih'] = $this->db->query("SELECT count(1) as jml from m_team WHERE id_provinces ='$value->idProv' AND type='kortps' AND is_deleted='0' ")->row('jml');
					$data['tabel'][$i]['realisasi'] = $jml;

					if($new_table != NULL){
						$data['tabel'][$i]['jum_dpt'] = $this->db->query("SELECT sum(dpt) as jml from $new_table")->row('jml');
						$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT count(1) as jml from $new_table")->row('jml');
					}else{
						$data['tabel'][$i]['jum_dpt'] = 0;
						$data['tabel'][$i]['jum_tps'] = 0;
					}
					$i++;	
				}	
			}
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon")->row('jml');
		}else{

			if($tipe == 'provinsi'){
				$id_prov = $id;
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');	
				$where_guraklih .= "and id_regency = ";
				$query_lanjutan .= "inner join m_regencies b on a.idKab=b.id WHERE tps!='999' group by idKab order by name ASC";
				$query_realisasi .= "and id_provinces = ";
				$jenis = 'idKab';
				$where_wilayah = "";
				$idarea = $id;
			}else if($tipe == 'kabupaten'){
				$id_prov = $this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKab = $id";
				$where_guraklih .= "and id_district = ";
				$query_lanjutan .= "inner join m_districts b on a.idKec=b.id where 1=1 AND tps!='999' $where group by idKec order by name ASC";
				$jenis = 'idKec';
				$where_wilayah = " WHERE idKab= ";
				$idarea = substr($id, 0,2) ;
				$query_realisasi .= "and id_regency = ";
			}else if($tipe == 'kecamatan'){
				$id_prov = $this->db->query("SELECT b.province_id from m_districts a left join m_regencies b on a.regency_id=b.id WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKec = $id";
				$where_guraklih .= "and id_village = ";
				$query_lanjutan .= "inner join m_villages b on a.idKel=b.id where 1=1 AND tps!='999' $where group by idKel order by name ASC";
				$jenis = 'idKel';
				$query_realisasi .= "and id_district = ";
				$where_wilayah = " WHERE idKec= ";
				$idarea = substr($id, 0,2) ;
			}else if($tipe == 'kelurahan'){
				$id_prov = $this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKel = $id";
				$where_guraklih .= "and tps = ";
				$query_lanjutan .= "where 1=1 AND tps!='999' $where group by tps";
				$query_realisasi .= "and id_village = ";
				$where_wilayah = " WHERE idKel= ";
				$idarea = substr($id, 0,2) ;
			}
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon where id_area='$id_prov'")->row('jml');
			if($tabel){
				$jum_dpt = $this->db->query("SELECT IFNULL(SUM(dpt),0) as jumlah from $tabel where 1=1 $where")->row('jumlah');	
			}else{
				$jum_dpt = 0;
			}

			if($tabel != NULL){
				if($tipe == 'kelurahan'){
					$tabel_ = $this->db->query("SELECT id, tps as name,sum(dpt) as jml, idKel as kel from $tabel a $query_lanjutan")->result();
				}else{
					$tabel_ = $this->db->query("SELECT b.id, b.logo, name,sum(dpt) as jml from $tabel a $query_lanjutan")->result();
				}
				
				if($tabel_ != NULL){
					$i = 0;

					foreach ($tabel_ as $key => $value) {
						$id_hasil = $value->id;
						$data['tabel'][$i]['id'] = $i+1;
						$data['tabel'][$i]['id_wilayah'] = $value->id;
						if($tipe != 'kelurahan'){
							$data['tabel'][$i]['logo_wilayah'] = $value->logo;
							$data['tabel'][$i]['guraklih'] = $this->db->query("SELECT count(1) as jml from m_team a, m_team_has_tps b WHERE a.type='kortps' AND a.is_deleted='0' AND a.id=b.id_team $where_guraklih  $value->id")->row('jml');
							//$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT count(1) as jml from pemilih_prospek WHERE is_deleted='0' $where_guraklih $value->id")->row('jml');
							//$data['tabel'][$i]['realisasi1'] = "SELECT count(1) as jml from pemilih_prospek WHERE is_deleted='0' $where_guraklih $value->id";

							$tabelSum = $this->db->get_where('m_dpt', array('idProv' => $idarea))->row()->tabel_sum;

							if ($tabelSum!=NULL) {
								if ($tipe == 'provinsi') {
									$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT sum(prospek) as jml_pemilih from $tabelSum ")->row('jml_pemilih');
								} else {
									$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT sum(prospek) as jml_pemilih from $tabelSum ".$where_wilayah." $value->id")->row('jml_pemilih');
								}
								
							}
						}else{
							$data['tabel'][$i]['logo_wilayah'] = '';
							$data['tabel'][$i]['guraklih'] = $this->db->query("SELECT count(1) as jml from m_team a, m_team_has_tps b WHERE a.type='kortps' AND a.is_deleted='0' AND a.id=b.id_team AND b.is_deleted='0' AND a.id_village=$value->kel $where_guraklih  $value->name")->row('jml');
							//$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT count(1) as jml from pemilih_prospek WHERE is_deleted='0' AND id_village = $value->kel $where_guraklih $value->name")->row('jml');

							$tabelSum = $this->db->get_where('m_dpt', array('idProv' => $idarea))->row()->tabel_sum;
							if ($tabelSum!=NULL) {
								$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT sum(prospek) as jml_pemilih from $tabelSum ".$where_wilayah." $value->kel")->row('jml_pemilih');
							}
						}

						$data['tabel'][$i]['wilayah'] = $value->name;
						if($tipe == 'kelurahan'){
							$data['tabel'][$i]['jum_tps'] = 1;
						}else{
							$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT COUNT(1) as jml from $tabel where $jenis=$id_hasil")->row('jml');
						}
						
						if($value->jml != NULL){
							$data['tabel'][$i]['jum_dpt'] = $value->jml;
						}else{
							$data['tabel'][$i]['jum_dpt'] = 0;
						}
						$i++;
					}
				}else{
					$data['tabel'][0]['id'] = '';
					$data['tabel'][0]['id_wilayah'] = '';
					$data['tabel'][0]['logo_wilayah'] = '';
					$data['tabel'][0]['wilayah'] = '';
					$data['tabel'][0]['jum_tps'] = 0;
					$data['tabel'][0]['jum_dpt'] = 0;
				}
			}else{
				$data['tabel'][0]['id'] = '';
				$data['tabel'][0]['id_wilayah'] = '';
				$data['tabel'][0]['logo_wilayah'] = '';
				$data['tabel'][0]['wilayah'] = '';
				$data['tabel'][0]['jum_tps'] = 0;
				$data['tabel'][0]['jum_dpt'] = 0;
			}
			
		}
		
		$data['jumlah_dpt'] = $jum_dpt;
		$data['jumlah_paslon'] = $jum_paslon;
		
		if($tabel){
			$data['target_suara'] = round($jum_dpt*0.75/$jum_paslon+($jum_dpt*0.75*0.05));
		}else{
			$data['target_suara'] = 0;
		}
		
		// $data['jumlah_influencer'] = ;
		return $data;	
	}

	function get_data2($id,$tipe){

		$this->db1 = $this->load->database("db_kab",true);

		$where = '';
		$where_guraklih = '';
		$query_lanjutan = '';
		$query_realisasi = '';
		$tabel_target = '';
		$where_wilayah="";
		$idarea = "";

		if($id == 0){
			$tabel = $this->db->get('m_dpt')->result();	
			$jum_dpt = 0;
			$i = 0;
			foreach ($tabel as $key => $value) {
				if($value->tabel_sum != NULL){
					$jml = 0;
					$sum_tabel = $value->tabel_sum;
					$hasil = $this->db->query("SELECT SUM(dpt) as jumlah from $sum_tabel")->row('jumlah');
					$jum_dpt=$jum_dpt+$hasil;
					$detail_prov = $this->db->get_where('m_provinces', array('id' => $value->idProv))->row();
					$jml= $this->db->query("SELECT jml_prospek as jml_pemilih from v_prospek WHERE prov='$value->idProv'")->row('jml');
					
					$trgt_prov = $this->db->get_where('am_prov_target', array('idProv' => $value->idProv))->row();

					$new_table = $value->tabel_sum;
					$data['tabel'][$i]['id'] = $i+1;
					$data['tabel'][$i]['id_wilayah'] = $value->idProv;
					$data['tabel'][$i]['logo_wilayah'] = $detail_prov->logo;
					$data['tabel'][$i]['wilayah'] = $value->provinsi;
					$data['tabel'][$i]['guraklih'] = $this->db->query("SELECT count(1) as jml from m_team WHERE id_provinces ='$value->idProv' AND type='kortps' AND is_deleted='0' ")->row('jml');
					$data['tabel'][$i]['realisasi'] = $jml;
					$data['tabel'][$i]['target'] = $trgt_prov->target;

					if($new_table != NULL){
						$data['tabel'][$i]['jum_dpt'] = $this->db->query("SELECT sum(dpt) as jml from $new_table")->row('jml');
						$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT count(1) as jml from $new_table")->row('jml');
					}else{
						$data['tabel'][$i]['jum_dpt'] = 0;
						$data['tabel'][$i]['jum_tps'] = 0;
					}
					$i++;	
				}	
			}
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon")->row('jml');
		}else{
			if($tipe == 'provinsi'){
				$id_prov = $id;
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');	
				$where_guraklih .= "and id_regency = ";
				$query_lanjutan .= "inner join m_regencies b on a.idKab=b.id WHERE tps!='999' group by idKab order by name ASC";
				$query_realisasi .= "and id_provinces = ";
				$jenis = 'idKab';
				$tabel_target = 'am_kab_target';
				$where_wilayah = " WHERE idKab= ";
				$idarea = $id;
			}else if($tipe == 'kabupaten'){
				$id_prov = $this->db->get_where('m_regencies', array('id' => $id))->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKab = $id";
				$where_guraklih .= "and id_district = ";
				$query_lanjutan .= "inner join m_districts b on a.idKec=b.id where 1=1 AND tps!='999' $where group by idKec order by name ASC";
				$jenis = 'idKec';
				$query_realisasi .= "and id_regency = ";
				$tabel_target = 'am_kec_target';
				$where_wilayah = " WHERE idKec= ";
				$idarea = substr($id, 0,2) ;
			}else if($tipe == 'kecamatan'){
				$id_prov = $this->db->query("SELECT b.province_id from m_districts a left join m_regencies b on a.regency_id=b.id WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKec = $id";
				$where_guraklih .= "and id_village = ";
				$query_lanjutan .= "inner join m_villages b on a.idKel=b.id where 1=1 AND tps!='999' $where group by idKel order by name ASC";
				$jenis = 'idKel';
				$query_realisasi .= "and id_district = ";
				$tabel_target = 'am_kel_target';
				$where_wilayah = " WHERE idKel= ";
				$idarea = substr($id, 0,2) ;
			}else if($tipe == 'kelurahan'){
				$id_prov = $this->db->query("SELECT c.province_id from m_villages a left join m_districts b on a.district_id=b.id 
											LEFT JOIN m_regencies c on c.id=b.regency_id
											WHERE a.id=$id")->row('province_id');
				$tabel = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row('tabel_sum');
				$where .= "and idKel = $id";
				$where_guraklih .= "and tps = ";
				$query_lanjutan .= "where 1=1 AND tps!='999' $where group by tps";
				$query_realisasi .= "and id_village = ";
				$tabel_target = 'am_kel_target';
				$where_wilayah = " WHERE idKel=  ";
				$idarea = substr($id, 0,2) ;
				$jenis = 'idKel';
			}
			$jum_paslon = $this->db->query("SELECT count(1) as jml from m_paslon where id_area='$id_prov'")->row('jml');
			if($tabel){
				$jum_dpt = $this->db->query("SELECT IFNULL(SUM(dpt),0) as jumlah from $tabel where 1=1 $where")->row('jumlah');	
			}else{
				$jum_dpt = 0;
			}

			if($tabel != NULL){
				if($tipe == 'kelurahan'){
					$tabel_ = $this->db->query("SELECT id, tps as name,sum(dpt) as jml, idKel as kel from $tabel a $query_lanjutan")->result();
				}else{
					$tabel_ = $this->db->query("SELECT b.id, b.logo, name,sum(dpt) as jml from $tabel a $query_lanjutan")->result();
				}
				
				if($tabel_ != NULL){
					$i = 0;
					foreach ($tabel_ as $key => $value) {
						$id_hasil = $value->id;
						$data['tabel'][$i]['id'] = $i+1;
						$data['tabel'][$i]['id_wilayah'] = $value->id;
						if($tipe != 'kelurahan'){
							$data['tabel'][$i]['logo_wilayah'] = $value->logo;
							$data['tabel'][$i]['guraklih'] = $this->db->query("SELECT count(1) as jml from m_team a, m_team_has_tps b WHERE a.type='kortps' AND a.is_deleted='0' AND a.id=b.id_team $where_guraklih  $value->id")->row('jml');
							//$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT count(1) as jml from pemilih_prospek WHERE is_deleted='0' $where_guraklih $value->id")->row('jml');
							$data['tabel'][$i]['target'] = $this->db->query("SELECT target as jml from $tabel_target WHERE $jenis = $value->id")->row('jml');
							$tabelSum = $this->db->get_where('m_dpt', array('idProv' => $id_prov))->row()->tabel_sum;
							if ($tabelSum!=NULL) {
								$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT sum(prospek) as jml_pemilih from $tabelSum ".$where_wilayah." $value->id")->row('jml_pemilih');
								//$data['tabel'][$i]['realisasi1'] = "SELECT sum(prospek) as jml_pemilih from $tabelSum ".$where_wilayah." $value->id";
							}

							//$data['tabel'][$i]['target1'] ="SELECT target as jml from $tabel_target WHERE $jenis = $value->id";
						}else{
							$data['tabel'][$i]['logo_wilayah'] = '';
							$data['tabel'][$i]['guraklih'] = $this->db->query("SELECT count(1) as jml from m_team a, m_team_has_tps b WHERE a.type='kortps' AND a.is_deleted='0' AND a.id=b.id_team AND b.is_deleted='0' AND a.id_village=$value->kel $where_guraklih  $value->name")->row('jml');
							//$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT count(1) as jml from pemilih_prospek WHERE is_deleted='0' AND id_village = $value->kel $where_guraklih $value->name")->row('jml');
							$data['tabel'][$i]['target'] = $this->db->query("SELECT target as jml from $tabel_target WHERE $jenis = $value->kel")->row('jml');
							//$data['tabel'][$i]['target1'] ="SELECT target as jml from $tabel_target WHERE $jenis = $value->kel";
							$tabelSum = $this->db->get_where('m_dpt', array('idProv' => $idarea))->row()->tabel_sum;
							if ($tabelSum!=NULL) {
								$data['tabel'][$i]['realisasi'] = $this->db->query("SELECT sum(prospek) as jml_pemilih from $tabelSum ".$where_wilayah." $value->kel AND tps =  $value->name")->row('jml_pemilih');
								//$data['tabel'][$i]['realisasi1'] = "SELECT sum(prospek) as jml_pemilih from $tabelSum ".$where_wilayah." $value->name";
							}
						}
						
						$data['tabel'][$i]['wilayah'] = $value->name;
						if($tipe == 'kelurahan'){
							$data['tabel'][$i]['jum_tps'] = 1;
						}else{
							$data['tabel'][$i]['jum_tps'] = $this->db->query("SELECT COUNT(1) as jml from $tabel where $jenis=$id_hasil")->row('jml');
						}
						
						if($value->jml != NULL){
							$data['tabel'][$i]['jum_dpt'] = $value->jml;
						}else{
							$data['tabel'][$i]['jum_dpt'] = 0;
						}
						$i++;
					}
				}else{
					$data['tabel'][0]['id'] = '';
					$data['tabel'][0]['id_wilayah'] = '';
					$data['tabel'][0]['logo_wilayah'] = '';
					$data['tabel'][0]['wilayah'] = '';
					$data['tabel'][0]['jum_tps'] = 0;
					$data['tabel'][0]['jum_dpt'] = 0;
				}
			}else{
				$data['tabel'][0]['id'] = '';
				$data['tabel'][0]['id_wilayah'] = '';
				$data['tabel'][0]['logo_wilayah'] = '';
				$data['tabel'][0]['wilayah'] = '';
				$data['tabel'][0]['jum_tps'] = 0;
				$data['tabel'][0]['jum_dpt'] = 0;
			}
			
		}
		
		$data['jumlah_dpt'] = $jum_dpt;
		$data['jumlah_paslon'] = $jum_paslon;
		
		if($tabel){
			$data['target_suara'] = round($jum_dpt*0.75/$jum_paslon+($jum_dpt*0.75*0.05));
		}else{
			$data['target_suara'] = 0;
		}
		
		// $data['jumlah_influencer'] = ;
		return $data;	
	}
}