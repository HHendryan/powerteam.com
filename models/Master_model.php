<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Master_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}

	function data_user(){
		$data = $this->db->get('user')->result();
		$i=0;
		foreach ($data as $value) {
			$data[$i]->nomor = $i+1;
			$data[$i]->button = '<button data-id='."'".$value->id."'".' data-kab='."'".$value->kabupaten."'".' class="btn btn-info btn-sm btn-edt"><span class="la la-edit"></span></button>';
			$data[$i]->provinsi = $this->db->get_where('provinsi',array('id' => $value->provinsi))->row('provinsi');
			$data[$i]->kabupaten = $this->db->get_where('kabupaten',array('id' => $value->kabupaten))->row('kabupaten');
			$data[$i]->kecamatan = $this->db->get_where('kecamatan',array('id' => $value->kecamatan))->row('kecamatan');

			if($value->level==99){
				$data[$i]->level = 'ADMIN';
			}else if ($value->level==3){
				$data[$i]->level = 'PILPRES';
			}else if ($value->level==2){
				$data[$i]->level = 'PILGUB';
			}else if ($value->level==1){
				$data[$i]->level = 'PILWALI';
			}else if ($value->level==0){
				$data[$i]->level = 'PILEG';
			}

			if($value->is_active=='1'){
				$data[$i]->is_active = '<a class="badge badge-crusta btn-del" data-id='."'".$value->id."'".' data-value='."'".$value->is_active."'".'>AKTIF</a>';
			}else{
				$data[$i]->is_active = '<a class="badge badge-danger btn-del" data-id='."'".$value->id."'".' data-value='."'".$value->is_active."'".'>TIDAK AKTIF</a>';
			}
			$i++;
		}
		return $data;
	}

	function select_kabupaten($id){
		$this->db->order_by('kabupaten','ASC');
		$data = $this->db->get_where('kabupaten',array('id_provinsi' => $id))->result();
		$list = "<option disabled selected value>--- PILIH ---</option>";
		foreach ($data as $value ){
			$list.= "<option value='$value->id'>$value->kabupaten</option>";
		}
		return $list;
	}

	function select_kabupaten_edit($id,$kab){
		$this->db->order_by('kabupaten','ASC');
		$data = $this->db->get_where('kabupaten',array('id_provinsi' => $id))->result();

		if($kab == NULL){
			$list="<option disabled selected value>--- PILIH ---</option>";
		}else{
			$kabupaten_nama = $this->db->get_where('kabupaten',array('id'=>$kab))->row('kabupaten');  
			$list="<option value='$kab'>$kabupaten_nama</option>";
		}

		foreach ($data as $value ){
			$list.= "<option value='$value->id'>$value->kabupaten</option>";
		}
		return $list;
	}

	function add_user($data){
		$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
		if($this->db->insert('user', $data)){
			echo json_encode('Tambah Sukses');
		}
	}

	function update_user($data){
		$filepath = $this->db->get_where('user',array('id' => $data['id'], ))->row('url_image');

		$this->load->config('upload', TRUE);
		$this->load->library('upload', $this->config->item('upload'));

		if($_FILES['url_image']['error']==4){

		}else{
			if(!$this->upload->do_upload('url_image')){
				set_status_header(422);
				echo json_encode($this->upload->display_errors());
				return;
			}else{
				if($filepath!='assets/img/default.jpg'){
					unlink($filepath);
				}
				$filepath= "assets/img/".$this->upload->data('file_name');
				$data['url_image'] = $filepath;
			}
		}

		if(($data['level'] == 99) OR ($data['level'] == 3)){
			$data['provinsi'] = NULL;
			$data['kabupaten'] = NULL;
		}
		if($data['level'] == 2){
			$data['kabupaten'] = NULL;
		}
		if($this->db->update('user', $data, array('id' => $data['id']))){
			echo json_encode('SUKSES');	
		}
	}

	function update_password($data){
		$dats['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
		if($this->db->update('user', $dats, array('id' => $data['id']))){
			echo json_encode('SUKSES');	
		}
	}

	function delete_user($id,$value){
		if($value == '1'){
			$data['is_active'] = '0';
		}else{
			$data['is_active'] = '1';
		}
		
		if($this->db->update('user', $data, "id = $id")){
			echo json_encode('Hapus Sukses');
		}
	}
}	