<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Olah_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}

	function dpt_by_nik($nik){
		$this->db->select("a.nik,a.nama,a.tanggal_lahir,b.kecamatan,c.kelurahan,a.tps,a.alamat");
		$this->db->from('dpt_sumatera_selatan a');
		$this->db->join('kecamatan b', 'a.kecamatan = b.id', 'left');
		$this->db->join('kelurahan c', 'a.kelurahan = c.id', 'left');
		$this->db->where('a.nik',$nik);
		$result = $this->db->get();
		if($result->num_row()>1){
			$result = $result->result();
		}else{
			return NULL;
		}
		return $result;
	}
	function dpt_by_nama($nama,$tanggal_lahir){
		$this->db->select("a.nik,a.nama,a.tanggal_lahir,b.kecamatan,c.kelurahan,a.tps,a.alamat");
		$this->db->from('dpt_sumatera_selatan a');
		$this->db->join('kecamatan b', 'a.kecamatan = b.id', 'left');
		$this->db->join('kelurahan c', 'a.kelurahan = c.id', 'left');
		$this->db->where('a.nama',$nama);
		$this->db->where('a.tanggal_lahir',$tanggal_lahir);
		$result = $this->db->get();
		if($result->num_row()>1){
			$result = $result->result();
		}else{
			return NULL;
		}
		return $result;
	}
}	