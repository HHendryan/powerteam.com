<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Jalur_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}

	function summary_jalur_prov(){
		$user = $this->Auth_model->get_data();
		$result=$this->db->get('provinsi')->result();
		$jalur=$this->db->get('jalur')->result();
		$i=0;
		foreach ($result as $value) {
				$data[$i]['wilayah'] = '<a href='."'".site_url("jalur/kabupaten/$value->id")."'".'>'."".$value->provinsi."".'</a>';
			foreach ($jalur as $key) {
				$data[$i][$key->jalur] = $this->db->query("select count(id) as jml from tim_view where id_user=$user->id and jalur='$key->jalur' ")->row('jml');
			}
			$i++;
		}
		return $data;
	}

	function summary_jalur_kab($id_prov){
		$user = $this->Auth_model->get_data();
		$result=$this->db->get_where('kabupaten', array('id_provinsi' => $id_prov))->result();
		$jalur=$this->db->get('jalur')->result();
		$i=0;
		foreach ($result as $value) {
				$data[$i]['wilayah'] = '<a href='."'".site_url("jalur/kecamatan/$value->id")."'".'>'."".$value->kabupaten."".'</a>';
			foreach ($jalur as $key) {
				$data[$i][$key->jalur] = $this->db->query("select count(id) as jml from tim_view where kabupaten = $value->id and id_user=$user->id and jalur='$key->jalur' ")->row('jml');
			}
			$i++;
		}
		return $data;
	}
	function summary_jalur_kec($id_kab){
		$user = $this->Auth_model->get_data();
		$result=$this->db->get_where('kecamatan', array('id_kabupaten' => $id_kab))->result();
		$jalur=$this->db->get('jalur')->result();
		$i=0;
		foreach ($result as $value) {
				$data[$i]['wilayah'] = '<a href='."'".site_url("jalur/kelurahan/$value->id")."'".'>'."".$value->kecamatan."".'</a>';
			foreach ($jalur as $key) {
				$data[$i][$key->jalur] = $this->db->query("select count(id) as jml from tim_view where kecamatan = $value->id and id_user=$user->id and jalur='$key->jalur' ")->row('jml');
			}
			$i++;
		}
		return $data;
	}
	function summary_jalur_kel($id_kec){
		$user = $this->Auth_model->get_data();
		$result=$this->db->get_where('kelurahan', array('id_kecamatan' => $id_kec))->result();
		$jalur=$this->db->get('jalur')->result();
		$i=0;
		foreach ($result as $value) {
				$data[$i]['wilayah'] = '<a href='."'".site_url("jalur/tps/$value->id")."'".'>'."".$value->kelurahan."".'</a>';
			foreach ($jalur as $key) {
				$data[$i][$key->jalur] = $this->db->query("select count(id) as jml from tim_view where kelurahan = $value->id and id_user=$user->id and jalur='$key->jalur' ")->row('jml');
			}
			$i++;
		}
		return $data;
	}
}	