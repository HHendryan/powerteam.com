<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for handle all of data about GURAKLIH (Hari Hendryan)
class Guraklih_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
		$this->db1 = $this->load->database("db_kab_final",true);
	}
	
	/*
	* function for add guraklih data (Hari Hendryan)
	*/
	function add_guraklih($data){
		$data['nama_team'] = 'guraklih';
        $data['id_tenant'] = 1;
		$tps = $data['tps'];
		unset($data['tps']);
		if($this->db->insert('m_team', $data)){
			$dats['id_team'] = $this->get_id();
			$dats['tps'] = $tps;
			$this->db->insert('m_team_has_tps', $dats);
			echo json_encode('Tambah Sukses');
		}
	}

	/*
	* function for get last id (Hari Hendryan)
	*/
	function get_id(){
		$id_lama = $this->db->query('SELECT id from m_team order by id desc limit 1')->row('id');
		return $id_lama;
	}

	/*
	* function for get guraklih data by type (Hari Hendryan)
	*/
	function get_data_guraklih($tipe){
		$data=$this->db1->get('m_dpt')->result();
		$i=0;
		foreach ($data as $key => $value) {
			// $data[$i]->provinsi = '<a href='."'".site_url("data/influence_kab/$value->id")."'".'>'."".$value->name."".'</a>';
			$data[$i]->provinsi = $value->provinsi;
			$query = "SELECT count(1) as jml from m_team where is_deleted='0' AND id_provinces ='$value->idProv'";
			if($tipe != 'all'){
				$query .= " AND type = '$tipe'";
			};
			$jum = $this->db->query($query)->row('jml');
			$data[$i]->jum_tangible = $jum;
			$data[$i]->button = '<button class="btn btn-warning btn-sm btn-detail" data-id="'.$value->id.'">Detail</button>';
			$i++;
		}

		return $data;
	}

	/*
	* function for get guraklih target (Hari Hendryan)
	*/
	function get_target_guraklih($area) {
		if ($area=='ALL') {
			$queryArea = "SELECT id FROM m_provinces";
			$data = $this->db->query($queryArea)->result();
			$data1 = array();
			$i=0; $total=0; $target=0;

			foreach ($data as $key => $value) {
				$queryTabel = "SELECT tabel_sum as tabel FROM m_dpt WHERE idProv = $value->id";
				$table = $this->db->query($queryTabel)->row('tabel');
				
				if ($table!=null) {
					$qTotalDpt = "SELECT sum(dpt) as jml FROM $table";
					$totalDpt = $this->db->query($qTotalDpt)->row('jml');
					$total = $total+$totalDpt;
				}
			}

			$queryTarget = "SELECT ROUND((($total*0.75)/(SELECT COUNT(1) FROM m_paslon WHERE is_deleted='0'))+(($total*0.75)*0.05)) AS target";
			$target = $target+$this->db->query($queryTarget)->row('target');

			$qResult = "SELECT COUNT(1) as jml FROM pemilih_prospek WHERE is_deleted='0'";

			$data1['total'] = $total;
			$data1['target'] = $target;
			$data1['hasil'] = $this->db->query($qResult)->row('jml')+0;
			$data1['sisa'] = $target - $data1['hasil'];

			return $data1;
		}
	}

	/*
    * Function for get detail guraklih data (Hari Hendryan)
    */
	function get_detail_guraklih($id_prov, $tipe){
		$query = "SELECT cp, phone, nik, address, (SELECT name from m_regencies where m_team.`id_regency` = m_regencies.id) as kab, (SELECT name from m_districts where m_team.`id_district` = `m_districts`.id ) as kec, (SELECT name from m_villages where m_team.`id_village` = `m_villages`.id ) as kel, type FROM m_team WHERE is_deleted='0' AND id_provinces='$id_prov'";
		
		if($tipe != 'all'){
			$query .= " AND type = '$tipe'";
		};
			
		$data = $this->db->query($query)->result();
		return($data);
	}

	/*
    * Function for get TPS (Hari Hendryan)
    */
	function get_tps($idkel) {
		$this->db2 = $this->load->database("db_kab_final",true);
		$list="<option value='' disabled selected>ALL</option>";

		$id_kab = $this->db2->query("SELECT  idKab as id_kab from m_area
			where idKel = $idkel")->row('id_kab');

		$queryTabel = "SELECT DISTINCT tps FROM kab_".$id_kab." ";
		$result = $this->db2->query($queryTabel)->result();

		foreach ($result as $value ){
			$list.= "<option value='$value->tps'>$value->tps</option>";
		}

		return $list;
	}
}