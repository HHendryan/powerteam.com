<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for admin user
class Wilayah_model extends CI_Model {
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Auth_model');
	}
	

	function get_kabupaten($id_prov){
		$list="<option disabled selected value>PILIH</option>";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_regencies',array('province_id'=>$id_prov))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}
	function get_kecamatan($id_kab){
		$list="<option disabled selected value>PILIH</option>";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_districts',array('regency_id'=>$id_kab))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}
	
	function get_kelurahan($id_kec){
		$list="<option disabled selected value>PILIH</option>";
		$this->db->order_by('name','ASC');
		$result= $this->db->get_where('m_villages',array('district_id'=>$id_kec))->result();
		foreach ($result as $value ){
			$list.= "<option value='$value->id'>$value->name</option>";
		}
		return $list;
	}
	
	//CRUD
    function input($table,$data){
    	$user = $this->session->userdata('obc');
    	$data['upd'] = $user['userid'];
    	$this->db->insert($table, $data);
		echo json_encode($data);
    }

    function edit($table,$data){
		$this->db->where('id', $data['id']);
		$this->db->update($table, $data);
		echo json_encode($data);
	}
	function add_kelurahan(){
		$data =$this->input->post();
		// $dats['id'] = $this->create_id_kelurahan($data['id']);
		echo json_encode($data);
	}
	
	function create_id_kelurahan($id_kecamatan){
		$id_kelurahan = $this->db->query('SELECT id FROM m_villages WHERE district_id=$id_kecamatan order by id DESC LIMIT 1')->row('id');
		return $id_kelurahan+1;
	}
	
	
}